#pragma once

#include <stdbool.h>

#define max(a,b)                \
    ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
       _a > _b ? _a : _b;       \
     })

#define maxFloatIEEE(a,b)                   \
    ({ __typeof__ (a) _a = (a);             \
       __typeof__ (b) _b = (b);             \
       (_a > _b || NAN == _a) ? _a : _b;    \
     })

#define min(a,b)                \
    ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
       _a < _b ? _a : _b;       \
     })

void show_backtrace();
void write_backtrace(FILE *file);

bool func_is_known(const void *func);

void fprintfIntArray(const int *array, const int nmemb);
void fprintfFloatArray(const float *array, const int nmemb);

void para_memset(void *s, const int c, const size_t n);
void para_memcpy(void *dest, const void *src, const size_t n);

void prescale_output(const float factor, float *array, const size_t num_elem);

//#ifndef V0
float *im2col_padded_stride2(const float *Im, const int *dim, const int *stride,
                             const int *pad, const int *filterDim, const int ii,
                             float *C_in);
float *im2col_padded_stride1(const float *Im, const int *dim, const int *stride,
                             const int *pad, const int *filterDim, const int ii,
                             float *C_in);
//#else
float *im2col_padded_stride1_v0(const float *Im, const int *dim, const int *stride,
                             const int *pad, const int *filterDim);
//#endif

void col2im(float *Y, const int *dim, const int *stride, float *tmpBuff);
float *col2im_back(float *dim_col, const int h_prime, const int w_prime,
                   const int stride, const int hh, const int ww, const int c);
