#include <stdio.h>
#include <stdlib.h>
#include <dispatch/dispatch.h>

/*
 * An example of executing a set of blocks on a serial dispatch queue.
 * Usage: hello [name]...
 */

void hello(void *ctx)
{
    printf("Hello my old friend\n");
}

void goodby(void *ctx)
{
    printf("Sayonara\n");
}

int main(int argc, char *argv[])
{
    int i;

    /* Create a serial queue. */
    dispatch_queue_t greeter = dispatch_queue_create("Greeter", NULL);

    for (i = 1; i < argc; i++)
    {
        /* Add some work to the queue. */
        dispatch_async_f(greeter, NULL, hello);
    }

    /* Add a last item to the queue. */
    dispatch_sync_f(greeter, NULL, goodby);

    return 0;
}
