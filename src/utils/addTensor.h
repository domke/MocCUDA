#pragma once

void addTensorNd(const float alpha, const float *A,
                 const int *dimA, const int *strideA, const int nbDimsA,
                 const float beta, float *C,
                 const int *dimC, const int *strideC, const int nbDimsC);
