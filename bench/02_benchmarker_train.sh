#!/bin/bash
SDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )" && pwd )"
BMSN="$( basename "${BASH_SOURCE[0]:-$0}" )"

if [ -z "${BACKEND}" ]; then
	if [ -z "$1" ]; then	BACKEND="moccuda"
	else			BACKEND="`echo $1 | tr '[:upper:]' '[:lower:]'`"
	fi
fi
if lscpu | grep 'sve' >/dev/null 2>&1 && [[ "${BACKEND}" != *"moccuda"* ]]; then
	if [[ "${BACKEND}" = *"170"* ]]; then
		source ${SDIR}/../init.env "FUJITORCH170"
	else
		source ${SDIR}/../init.env "FUJITORCH150"
	fi
else
	source ${SDIR}/../init.env
fi

if [[ "${BACKEND}" = *"moccuda"* ]]; then
	PRELOADLIBS="${PRELOADLIBS:+${PRELOADLIBS} }${SDIR}/../lib/libMocCUDA.so"
	PRELOG="${BACKEND}"
	RUNVERS="--gpu=0"
elif [[ "${BACKEND}" = *"cuda"* ]]; then
	PRELOG="${BACKEND}"
	RUNVERS="--gpu=0"
elif [[ "${BACKEND}" = *"dnnl"* ]]; then
	PRELOG="${BACKEND}"
	RUNVERS="--backend=DNNL"
elif [[ "${BACKEND}" = *"native"* ]]; then
	PRELOG="${BACKEND}"
	RUNVERS="--backend=native"
else
	echo "ERR: requested backend not supported or does not exist" ; exit 1
fi
if [ -n "${PRELOADLIBS}" ]; then	PRELOAD="LD_PRELOAD=\"${PRELOADLIBS}\""
else					PRELOAD=""
fi

if [[ "$(hostname -s)" = "kiev"* ]]; then	NUMA=(0 0-1)
elif [[ "$(hostname -s)" = "epyc"* ]]; then	NUMA=(0 0-1 0-3 0-5 0-7)
elif lscpu | grep 'sve' >/dev/null 2>&1; then	NUMA=(4 4-5 4-6 4-7)
fi
POW6="$(for x in $(seq 0 6); do echo $((2**x)) ; done)"
POW8="$(for x in $(seq 0 8); do echo $((2**x)) ; done)"
FAC6_48="$(seq 6 6 48)"
FAC12_288="6 $(seq 12 12 288)"

TMPLOG="/dev/shm/`hostname -s`-${BASHPID}"
RUNINFO="Time:\|samples_per_second"
LOGDIR="${SDIR}/../log" ; mkdir -p "${LOGDIR}"

cd "${BENCHMARKER_ROOT}"/
for N in ${!NUMA[@]}; do
	for OMP in $(echo ${POW6} ${FAC6_48} | sed -e 's/ /\n/g' | sort -n); do
		for BS in $(echo ${POW8} ${FAC12_288} | sed -e 's/ /\n/g' | sort -n); do
			LOG="${LOGDIR}/${BMSN}_${PRELOG}_cmg$((1+$N))_omp${OMP}.log"
			export OMP_NUM_THREADS=${OMP}
			PRE="numactl -N ${NUMA[$N]} -m ${NUMA[$N]}"
			echo "batch size: ${BS}  problem size: $((4*${BS}))  numa: ${NUMA[$N]}  omp: ${OMP}  backend: ${BACKEND}" | tee -a ${LOG}
			timeout --kill-after=30s 10m bash -c "${PRELOAD} ${PRE} python3 -m benchmarker --framework=pytorch --problem=resnet50 --mode=training --problem_size=$((4*${BS})) --batch_size=${BS} ${RUNVERS}" > ${TMPLOG} 2>&1
			if [ "x$?" = "x137" ] || [ "x$?" = "x124" ]; then echo "OOM/time killer, stop here" >> ${LOG}; break; fi
			/usr/bin/grep "${RUNINFO}" ${TMPLOG} | tee -a ${LOG}
			sleep 1
		done
	done
done
