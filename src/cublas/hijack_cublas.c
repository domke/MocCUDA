#include <stdlib.h>
#include <stdbool.h>
#ifdef REFLAPACK
    #define blasint int
    #define BLASFUNC(FUNC) FUNC##_
    #include <cblas_mangling.h>
    #include <cblas_f77.h>
#elif defined FUJITSU
    #define blasint FJ_MATHLIB_TYPE_INT
    #ifndef PARAFUJI
        #define BLASFUNC(FUNC) sl_cblas_##FUNC
    #else
        #define BLASFUNC(FUNC) FUNC##_
        #include <fj_lapack.h>
    #endif
#elif defined ARMPL
    #define blasint armpl_int_t
    #define openblas_get_num_threads armpl_get_num_threads
    #define openblas_set_num_threads armpl_set_num_threads
    #define BLASFUNC(FUNC) FUNC##_
#else
    #include <f77blas.h>
#endif
#include <cblas.h>
#include <omp.h>

#include "hijack_cublas.h"
#include "async.h"
#include "utils.h"

cublasStatus_t cublasCreate_v2(cublasHandle_t *handle)
{
    ENTER(cublas_lib_entry, cublasCreate_v2);

    cublasStatus_t rcs = CUBLAS_STATUS_SUCCESS;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    *handle = (struct cublasContext *)calloc(1, sizeof(**handle));
    if (!*handle)
        rcs = CUBLAS_STATUS_ALLOC_FAILED;
    else
        (*handle)->__inited__ = true;
#else //USE_MocCUDA
    rcs = REAL_CUBLAS_CALL(cublas_lib_entry, cublasCreate_v2, handle);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  phandle = %p, handle = %p\n",
            rcs, handle, (void*)(*handle));
    #endif

    LEAVE(cublas_lib_entry, cublasCreate_v2);
    return rcs;
}

cublasStatus_t cublasDestroy_v2(cublasHandle_t handle)
{
    ENTER(cublas_lib_entry, cublasDestroy_v2);

    cublasStatus_t rcs = CUBLAS_STATUS_SUCCESS;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (handle && !handle->__inited__)
        return CUBLAS_STATUS_NOT_INITIALIZED;
    if (handle) {
        fake_cuda_synchronize((*handle).stream);
        free(handle);
    }
#else //USE_MocCUDA
    rcs = REAL_CUBLAS_CALL(cublas_lib_entry, cublasDestroy_v2, handle);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  handle = %p\n", rcs, handle);
    #endif

    LEAVE(cublas_lib_entry, cublasDestroy_v2);
    return rcs;
}

cublasStatus_t cublasSetStream_v2(cublasHandle_t handle,
                                  cudaStream_t streamId)
{
    ENTER(cublas_lib_entry, cublasSetStream_v2);

    cublasStatus_t rcs = CUBLAS_STATUS_SUCCESS;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (!handle->__inited__)
        return CUBLAS_STATUS_NOT_INITIALIZED;
    (*handle).stream = streamId;
#else //USE_MocCUDA
    rcs = REAL_CUBLAS_CALL(cublas_lib_entry, cublasSetStream_v2, handle,
                           streamId);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  handle = %p, streamId = %p\n",
            rcs, handle, streamId);
    #endif

    LEAVE(cublas_lib_entry, cublasSetStream_v2);
    return rcs;
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    char tA[1], tB[1];
    blasint M[1], N[1], K[1];
    blasint LDA[1], LDB[1], LDC[1];
    float alp[1], bet[1];
    float *A, *B, *C;
} cublasSgemm_async_t;

__attribute__((used,noinline))
static void cublasSgemm_async_fn(void *ctx)
{
    ENTER(cublas_lib_entry, cublasSgemm_async_fn);

    cublasSgemm_async_t c = *((cublasSgemm_async_t *)ctx);

    #ifdef REFLAPACK
    int i;
    const int omp_max_thr = omp_get_max_threads();
    #pragma omp parallel for firstprivate(omp_max_thr), schedule(static,1)
    for (i = 0; i < min(c.N[0], omp_max_thr); i++) {
        div_t res = div(c.N[0], omp_max_thr);
        blasint new_n, prev_n;
        if (0 == res.rem) {
            new_n = res.quot;
            prev_n = i * new_n;
        } else {
            if (i < res.rem) {
                new_n = res.quot + 1;
                prev_n = i * new_n;
            } else {
                new_n = res.quot;
                prev_n = res.rem * (new_n + 1) + (i - res.rem) * new_n;
            }
        }
        blasint cN[1] = {new_n};
        blasint cLDB[1] = {c.K[0]};
        float *B = &(c.B[c.K[0] * prev_n]);
        if ('N' != c.tB[0]) {
            cLDB[0] = c.N[0];
            B = &(c.B[prev_n]);
        }
        TIME_GEMM_START(ColMaj, ('T' == c.tA[0]), ('T' == c.tB[0]),
                        c.M[0], cN[0], c.K[0], c.alp[0], c.bet[0],
                        c.LDA[0], cLDB[0], c.LDC[0]);
        BLASFUNC(sgemm)(c.tA, c.tB, c.M, cN, c.K, c.alp, c.A, c.LDA,
                        B, cLDB, c.bet, &(c.C[c.M[0] * prev_n]), c.LDC);
        TIME_GEMM_STOP(core, c.M[0], cN[0], c.K[0]);
    }
    #elif defined FUJITSU
    #ifndef PARAFUJI
    int i;
    const int omp_max_thr = omp_get_max_threads();
    #pragma omp parallel for firstprivate(omp_max_thr), schedule(static,1)
    for (i = 0; i < min(c.N[0], omp_max_thr); i++) {
        div_t res = div(c.N[0], omp_max_thr);
        blasint new_n, prev_n;
        if (0 == res.rem) {
            new_n = res.quot;
            prev_n = i * new_n;
        } else {
            if (i < res.rem) {
                new_n = res.quot + 1;
                prev_n = i * new_n;
            } else {
                new_n = res.quot;
                prev_n = res.rem * (new_n + 1) + (i - res.rem) * new_n;
            }
        }
        blasint new_LDB = c.K[0];
        float *B = &(c.B[c.K[0] * prev_n]);
        if ('N' != c.tB[0]) {
            new_LDB = c.N[0];
            B = &(c.B[prev_n]);
        }
        TIME_GEMM_START(ColMaj, ('T' == c.tA[0]), ('T' == c.tB[0]),
                        c.M[0], new_n, c.K[0], c.alp[0], c.bet[0],
                        c.LDA[0], new_LDB, c.LDC[0]);
        BLASFUNC(sgemm)(CblasColMajor,
                        ('N' == c.tA[0]) ? CblasNoTrans : CblasTrans,
                        ('N' == c.tB[0]) ? CblasNoTrans : CblasTrans,
                        c.M[0], new_n, c.K[0], c.alp[0], c.A, c.LDA[0],
                        B, new_LDB, c.bet[0], &(c.C[c.M[0] * prev_n]),
                        c.LDC[0]);
        TIME_GEMM_STOP(core, c.M[0], new_n, c.K[0]);
    }
    #else //PARAFUJI
        TIME_GEMM_START(ColMaj, ('T' == c.tA[0]), ('T' == c.tB[0]),
                        c.M[0], c.N[0], c.K[0], c.alp[0], c.bet[0],
                        c.LDA[0], c.LDB[0], c.LDC[0]);
        BLASFUNC(sgemm)(c.tA, c.tB, c.M, c.N, c.K, c.alp, c.A, c.LDA,
                        c.B, c.LDB, c.bet, c.C, c.LDC, 1, 1);
        TIME_GEMM_STOP(fullChip, c.M[0], c.N[0], c.K[0]);
    #endif //PARAFUJI
    #else
    int numThr = openblas_get_num_threads();
    openblas_set_num_threads(omp_get_max_threads());
    TIME_GEMM_START(ColMaj, ('T' == c.tA[0]), ('T' == c.tB[0]),
                    c.M[0], c.N[0], c.K[0], c.alp[0], c.bet[0],
                    c.LDA[0], c.LDB[0], c.LDC[0]);
    BLASFUNC(sgemm)(c.tA, c.tB, c.M, c.N, c.K, c.alp, c.A, c.LDA, c.B, c.LDB,
                    c.bet, c.C, c.LDC);
    TIME_GEMM_STOP(fullChip, c.M[0], c.N[0], c.K[0]);
    openblas_set_num_threads(numThr);
    #endif

    free(ctx);

    LEAVE(cublas_lib_entry, cublasSgemm_async_fn);
}
#endif //USE_GCD

cublasStatus_t cublasSgemm_v2(cublasHandle_t handle,
                              cublasOperation_t transa,
                              cublasOperation_t transb,
                              int m, int n, int k,
                              const float *alpha, const float *A, int lda,
                              const float *B, int ldb,
                              const float *beta, float *C, int ldc)
{
    ENTER(cublas_lib_entry, cublasSgemm_v2);

    cublasStatus_t rcs = CUBLAS_STATUS_SUCCESS;

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (!handle->__inited__)
        return CUBLAS_STATUS_NOT_INITIALIZED;
    if (m < 0 || n < 0 || k < 0)
        return CUBLAS_STATUS_INVALID_VALUE;

    #if defined USE_GCD && USE_GCD >= 1
    cublasSgemm_async_t *ctx;
    assert((ctx = (cublasSgemm_async_t *)malloc(sizeof(*ctx))));
    *ctx = (cublasSgemm_async_t){.tA[0] = 'N', .tB[0] = 'N',
                                 .M[0] = (blasint)m, .N[0] = (blasint)n,
                                 .K[0] = (blasint)k,
                                 .LDA[0] = (blasint)lda, .LDB[0] = (blasint)ldb,
                                 .LDC[0] = (blasint)ldc,
                                 .alp[0] = alpha[0], .bet[0] = beta[0],
                                 .A = (float *)A, .B = (float *)B, .C = C};
    if (CUBLAS_OP_T == transa)
        ctx->tA[0] = 'T';
    if (CUBLAS_OP_T == transb)
        ctx->tB[0] = 'T';
    fake_cuda_dispatch(ctx, cublasSgemm_async_fn, (*handle).stream);
    #else //USE_GCD
    char tA[1] = {'N'}, tB[1] = {'N'};
    if (CUBLAS_OP_T == transa)
        tA[0] = 'T';
    if (CUBLAS_OP_T == transb)
        tB[0] = 'T';
    assert(CUBLAS_OP_C != transa && CUBLAS_OP_C != transb);
    blasint M[1] = {m}, N[1] = {n}, K[1] = {k};
    blasint LDA[1] = {lda}, LDB[1] = {ldb}, LDC[1] = {ldc};
    float alp[1] = {alpha[0]}, bet[1] = {beta[0]};
    float *dA = (float *)A, *dB = (float *)B;

    #ifdef REFLAPACK
    int i;
    const int omp_max_thr = omp_get_max_threads();
    #pragma omp parallel for firstprivate(omp_max_thr), schedule(static,1)
    for (i = 0; i < min(N[0], omp_max_thr); i++) {
        div_t res = div(N[0], omp_max_thr);
        blasint new_n, prev_n;
        if (0 == res.rem) {
            new_n = res.quot;
            prev_n = i * new_n;
        } else {
            if (i < res.rem) {
                new_n = res.quot + 1;
                prev_n = i * new_n;
            } else {
                new_n = res.quot;
                prev_n = res.rem * (new_n + 1) + (i - res.rem) * new_n;
            }
        }
        blasint cN[1] = {new_n};
        blasint cLDB[1] = {K[0]};
        float *new_B = &(dB[K[0] * prev_n]);
        if ('N' != tB[0]) {
            cLDB[0] = N[0];
            new_B = &(dB[prev_n]);
        }
        TIME_GEMM_START(ColMaj,
                        (CUBLAS_OP_T == transa), (CUBLAS_OP_T == transb),
                        *M, *cN, *K, *alp, *bet, *LDA, *cLDB, *LDC);
        BLASFUNC(sgemm)(tA, tB, M, cN, K, alp, dA, LDA, new_B, cLDB,
                        bet, &(C[M[0] * prev_n]), LDC);
        TIME_GEMM_STOP(core, M[0], cN[0], K[0]);
    }
    #elif defined FUJITSU
    #ifndef PARAFUJI
    int i;
    const int omp_max_thr = omp_get_max_threads();
    #pragma omp parallel for firstprivate(omp_max_thr), schedule(static,1)
    for (i = 0; i < min(n, omp_max_thr); i++) {
        div_t res = div(n, omp_max_thr);
        blasint new_n, prev_n;
        if (0 == res.rem) {
            new_n = res.quot;
            prev_n = i * new_n;
        } else {
            if (i < res.rem) {
                new_n = res.quot + 1;
                prev_n = i * new_n;
            } else {
                new_n = res.quot;
                prev_n = res.rem * (new_n + 1) + (i - res.rem) * new_n;
            }
        }
        blasint new_ldb = k;
        float *new_B = &(B[k * prev_n]);
        if (CUBLAS_OP_N != transb) {
            new_ldb = n;
            new_B = &(B[prev_n]);
        }
        TIME_GEMM_START(ColMaj,
                        (CUBLAS_OP_T == transa), (CUBLAS_OP_T == transb),
                        m, new_n, k, *alpha, *beta, lda, new_ldb, ldc);
        BLASFUNC(sgemm)(CblasColMajor,
                        (CUBLAS_OP_N == transa) ? CblasNoTrans : CblasTrans,
                        (CUBLAS_OP_N == transb) ? CblasNoTrans : CblasTrans,
                        m, new_n, k, *alpha, A, lda, new_B, new_ldb, *beta,
                        &(C[m * prev_n]), ldc);
        TIME_GEMM_STOP(core, m, new_n, k);
    }
    #else //PARAFUJI
        TIME_GEMM_START(ColMaj,
                        (CUBLAS_OP_T == transa), (CUBLAS_OP_T == transb),
                        *M, *N, *K, *alp, *bet, *LDA, *LDB, *LDC);
        BLASFUNC(sgemm)(tA, tB, M, N, K, alp, dA, LDA, dB, LDB, bet, C, LDC,
                        1, 1);
        TIME_GEMM_STOP(fullChip, *M, *N, *K);
    #endif //PARAFUJI
    #else
    int numThr = openblas_get_num_threads();
    openblas_set_num_threads(omp_get_max_threads());
    TIME_GEMM_START(ColMaj, (CUBLAS_OP_T == transa), (CUBLAS_OP_T == transb),
                    *M, *N, *K, *alp, *bet, *LDA, *LDB, *LDC);
    BLASFUNC(sgemm)(tA, tB, M, N, K, alp, dA, LDA, dB, LDB, bet, C, LDC);
    TIME_GEMM_STOP(fullChip, *M, *N, *K);
    openblas_set_num_threads(numThr);
    #endif
    #endif //USE_GCD
#else //USE_MocCUDA
    rcs = REAL_CUBLAS_CALL(cublas_lib_entry, cublasSgemm_v2,
                           handle, transa, transb, m, n, k,
                           alpha, A, lda, B, ldb, beta, C, ldc);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr,
            "\tret=%d  handle = %p, transa = %d, transb = %d, "
            "m = %d, n = %d, k = %d, alpha = %f, A = %p, lda = %d, "
            "B = %p, ldb = %d, beta = %f, C = %p, ldc = %d\n",
            rcs, handle, transa, transb, m, n, k, *((float*)alpha), A, lda,
            B, ldb, *((float*)beta), C, ldc);
    #endif

    LEAVE(cublas_lib_entry, cublasSgemm_v2);
    return rcs;
}
