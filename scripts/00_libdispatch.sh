#!/bin/bash
SDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )" && pwd )"
source "${SDIR}/host.env"

cd ${SDIR}/../dep ; mkdir -p install
WDIR="$(pwd)"

# libblocksruntime dependency for libdispatch
cd ${WDIR}/blocks-runtime/
libtoolize ; aclocal ; autoheader ; autoconf ; automake --add-missing
rm -rf build ; mkdir -p build ; cd build
CC="${MocCC} ${MocCFLAGS} -fcommon" CXX="${MocCXX} ${MocCXXFLAGS} -fcommon" \
	../configure --prefix="${WDIR}/install/libblocksruntime" \
	--disable-shared --enable-static --with-pic
make -j$(nproc) install V=1

# libpwq dependency for libdispatch
cd ${WDIR}/libpwq/
rm -rf build ; mkdir -p build ; cd build
cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_VERBOSE_BUILD:BOOL=ON \
	-DCMAKE_INSTALL_PREFIX="${WDIR}/install/libpwq" -DCMAKE_INSTALL_LIBDIR=lib \
	-DCMAKE_C_COMPILER="${MocCC}" -DCMAKE_C_FLAGS="${MocCFLAGS} -fcommon" \
	-DCMAKE_CXX_COMPILER="${MocCXX}" -DCMAKE_CXX_FLAGS="${MocCXXFLAGS} -fcommon" \
	-DSTATIC_WORKQUEUE=ON
make -j$(nproc) install VERBOSE=1

# libkqueue dependency for libdispatch
cd ${WDIR}/libkqueue/
rm -rf build ; mkdir -p build ; cd build
cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_VERBOSE_BUILD:BOOL=ON \
	-DCMAKE_INSTALL_PREFIX="${WDIR}/install/libkqueue" -DCMAKE_INSTALL_LIBDIR=lib \
	-DCMAKE_C_COMPILER="${MocCC}" -DCMAKE_C_FLAGS="${MocCFLAGS} -fcommon" \
	-DCMAKE_CXX_COMPILER="${MocCXX}" -DCMAKE_CXX_FLAGS="${MocCXXFLAGS} -fcommon" \
	-DSTATIC_KQUEUE=ON
make -j$(nproc) install VERBOSE=1

# finally build libdispatch itself
cd ${WDIR}/libdispatch
git checkout -- \
	./configure \
	./cmake/FindBlocksRuntime.cmake \
	./cmake/Findpthread_workqueue.cmake \
	./cmake/Findkqueue.cmake
sed -i  -e "s#/usr/bin/env python.*#/usr/bin/env python2#" \
	./configure
sed -i  -e "s#INCLUDE_DIR Block.h#INCLUDE_DIR Block.h PATHS ${WDIR}/install/libblocksruntime/include#g" \
	-e "s#LIBRARIES \"BlocksRuntime\")#LIBRARIES \"BlocksRuntime\" PATHS ${WDIR}/install/libblocksruntime/lib)#g" \
	./cmake/FindBlocksRuntime.cmake
sed -i  -e "s#_DIRS pthread_workqueue.h#_DIRS pthread_workqueue.h PATHS ${WDIR}/install/libpwq/include#g" \
	-e "s#_LIBRARIES \"pthread_workqueue\")#_LIBRARIES \"pthread_workqueue\" PATHS ${WDIR}/install/libpwq/lib)#g" \
	./cmake/Findpthread_workqueue.cmake
sed -i  -e "s#event.h PATH_#event.h PATHS ${WDIR}/install/libkqueue/include PATH_#g" \
	-e "s#_LIB \"kqueue\")#_LIB \"kqueue\" PATHS ${WDIR}/install/libkqueue/lib64)#g" \
	./cmake/Findkqueue.cmake
rm -rf build ; mkdir -p build ; cd build
CFLAGS="${MocCFLAGS} -fPIC -fcommon" CXXFLAGS="${MocCXXFLAGS} -fPIC -fcommon" \
	  ../configure --cc="${MocCC}" --c++="${MocCXX}" --build-type=Release \
	  --prefix=${WDIR}/install/libdispatch
#sed -i -e "s#^C_FLAGS = #C_FLAGS = -fPIC #" ./src/CMakeFiles/libdispatch_static.dir/flags.make
make -j$(nproc) install VERBOSE=1

cat <<EOF > ${WDIR}/libdispatch.env
export LIBBRT_ROOT=${WDIR}/install/libblocksruntime
export LIBPWQ_ROOT=${WDIR}/install/libpwq
export LIBKQU_ROOT=${WDIR}/install/libkqueue
export LIBDIS_ROOT=${WDIR}/install/libdispatch
EOF
