subroutine memcpy_float(dst, src, num)
    integer*4, intent(in) :: num
    real*4, intent(out) :: dst(num)
    real*4, intent(in) :: src(num)
    integer*4 :: i

    do i = 1, num
        dst(i) = src(i)
    end do
end subroutine memcpy_float

subroutine memcpy_double(dst, src, num)
    integer*4, intent(in) :: num
    real*8, intent(out) :: dst(num)
    real*8, intent(in) :: src(num)
    integer*4 :: i

    do i = 1, num
        dst(i) = src(i)
    end do
end subroutine memcpy_double

subroutine memcpy_byte(dst, src, num)
    integer*4, intent(in) :: num
    byte, intent(out) :: dst(num)
    byte, intent(in) :: src(num)
    integer*4 :: i

    do i = 1, num
        dst(i) = src(i)
    end do
end subroutine memcpy_byte

subroutine memcpy_float_image_stride2(dst, d_len, im, im_h, im_w, &
                                      start_h, start_w, pad_h, pad_w)
    integer*4, intent(in) :: d_len, im_h, im_w, start_h, start_w, &
                             pad_h, pad_w
    real*4, intent(out) :: dst(d_len)
    real*4, intent(in) :: im(im_w,im_h)  !flip because of ColMajor
    integer*4 :: r_s_h, r_s_w, d_i, pre_sft, post_sft, x, y

! reference version:
!    real*4, dimension (:), allocatable :: t_dst
!    real*4, dimension (:,:), allocatable :: t_im
!
!    allocate(t_im(im_w+2*pad_w,im_h+2*pad_h), t_dst(d_len))
!    t_im=0
!    t_dst=0
!    t_im(pad_w+1:,pad_h+1:) = im(:,:)
!    r_s_h = start_h+1
!    d_i = 1
!    do y = 1, (im_h+1) / 2
!        r_s_w = start_w+1
!        do x = 1, (im_w+1) / 2
!            t_dst(d_i) = t_im(r_s_w,r_s_h)
!            d_i = d_i+1
!            r_s_w = r_s_w+2
!        end do
!        r_s_h = r_s_h+2
!    end do
!    deallocate(t_dst, t_im)

    d_i = 1
    pre_sft = 0
    post_sft = 0
    r_s_h = -pad_h + start_h + 1
    r_s_w = -pad_w + start_w + 1

    do while (r_s_h < 1)
        d_i = d_i + (im_w+1) / 2
        r_s_h = r_s_h + 2
    end do

    if (r_s_w > 1) then
        x = ((im_w+1) / 2) * 2 - 1
        do while (x + r_s_w - 1 > im_w)
            post_sft = post_sft + 1
            x = x - 2
        end do
    end if
    do while (r_s_w < 1)
        pre_sft = pre_sft + 1
        r_s_w = r_s_w + 2
    end do

    do y = r_s_h, min(im_h, im_h + (-pad_h + start_h)), 2
        d_i = d_i + pre_sft
        do x = r_s_w, min(im_w, im_w + (-pad_w + start_w)), 2
            dst(d_i) = im(x,y)
            d_i = d_i + 1
        end do
        d_i = d_i + post_sft
    end do
end subroutine memcpy_float_image_stride2
