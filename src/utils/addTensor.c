#include <string.h>
#ifdef REFLAPACK
    #define blasint int
#elif defined FUJITSU
    #define blasint FJ_MATHLIB_TYPE_INT
#elif defined ARMPL
    #define blasint armpl_int_t
#endif
#include <cblas.h>

//C = alpha * A + beta * C
static inline void _addTensor_1d_(const float alpha, const float *A,
                                  const int dimA[1], const float beta,
                                  float *C, const int dimC[1])
{
    int i;
    if (1 == dimA[0]) {
        const float alphaA = alpha * A[0];

        /*if (0.0 == alphaA && 1.0 == beta) {
            // do nothing;
        } else*/ if (0.0 != alphaA && 1.0 == beta) {
            #pragma omp parallel for firstprivate(alphaA), schedule(static)
            for (i = 0; i < dimC[0]; i++)
                C[i] += alphaA;
        } else if (0.0 == alphaA && 1.0 != beta) {
            cblas_sscal(((blasint)dimC[0]), beta, C, ((blasint)1));
        } else if (0.0 != alphaA && 1.0 != beta) {
            #pragma omp parallel for firstprivate(alphaA,beta), schedule(static)
            for (i = 0; i < dimC[0]; i++)
                C[i] = alphaA + beta * C[i];
        }
    } else if (1.0 == beta) {
        cblas_saxpy(((blasint)dimC[0]), alpha, A, ((blasint)1), C,
                    ((blasint)1));
    } else {
        #pragma omp parallel for firstprivate(alpha,beta), schedule(static)
        for (i = 0; i < dimC[0]; i++)
            C[i] = alpha * A[i] + beta * C[i];
    }
}

void addTensorNd(const float alpha, const float *A,
                 const int *dimA, const int *strideA, const int nbDimsA,
                 const float beta, float *C,
                 const int *dimC, const int *strideC, const int nbDimsC)
{
    int i;
    const int nA = dimA[0] * strideA[0], nC = dimC[0] * strideC[0];
    //if A is as big as C
    if (0 == memcmp(dimA, dimC, nbDimsC * sizeof(int)))
        _addTensor_1d_(alpha, A, &nA, beta, C, &nC);
    //or if A is just scalar
    else if (1 == nA)
        _addTensor_1d_(alpha, A, &nA, beta, C, &nC);
    //else we have to do blending
    else if (1 == dimA[0]) {
        #pragma omp parallel for schedule(static)
        for (i = 0; i < dimC[0]; i++)
            addTensorNd(alpha, A, dimA+1, strideA+1, nbDimsA-1,
                        beta, C+i*strideC[0], dimC+1, strideC+1, nbDimsC-1);
    } else {
        #pragma omp parallel for schedule(static)
        for (i = 0; i < dimC[0]; i++)
            addTensorNd(alpha, A+i*strideA[0], dimA+1, strideA+1, nbDimsA-1,
                        beta, C+i*strideC[0], dimC+1, strideC+1, nbDimsC-1);
    }
}

#ifdef TESTME
int main(){
    int dimA[2]={4, 3}, strideA[2]={3, 1};
    int dimC[2]={4, 3}, strideC[2]={3, 1};
    float A[12] = {0.8965, 0.6224, 0.7101,
                   0.3060, 0.1128, 0.8101,
                   0.8638, 0.2311, 0.2618,
                   0.8927, 0.6958, 0.5139};
    float C[12] = {0.8891, 0.3824, 0.9848,
                   0.8880, 0.4159, 0.6920,
                   0.6698, 0.4432, 0.3289,
                   0.8616, 0.9379, 0.5460};
    addTensorNd(1.0, A, dimA, strideA, 2, 1.0, C, dimC, strideC, 2);
    int i, j;
    for (i=0; i<4; i++) {
        for (j=0; j<3; j++)
            printf("%f,", C[i*3+j]);
        printf("\n");
    }
    //tensor([[1.7856, 1.0048, 1.6950],
    //        [1.1939, 0.5287, 1.5020],
    //        [1.5336, 0.6742, 0.5907],
    //        [1.7543, 1.6337, 1.0599]], device='cuda:0')
    return 0;
}
#endif
