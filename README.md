# MocCUDA

## get repo
```
git clone https://gitlab.com/domke/MocCUDA.git
cd MocCUDA/
git submodule update --init --recursive
```

## build dependencies
```
# take a look at ./scripts/host.env for environment adjustments
for NR in $(seq -w 00 06); do
	bash ./scripts/${NR}_*.sh
done
# for comparing to Fujitsu's pytorch, one needs the following:
bash ./scripts/08_*.sh
```

## load exec environment
```
source ./init.env
```

## extract cuda device properties from an existing device we want to emulate
```
cd ./misc ; make -B ; cd -
make -f Makefile.<node> -B RedirectCUDA='-DUSE_MocCUDA=0'
LD_PRELOAD=$(pwd)/lib/libMocCUDA.so ./misc/dump_cudaGetDeviceProperties.exe
# => will dump a cudaDeviceProp.b file which we need when a GPU is missing in the node
```

## build the MocCUDA libraries to emulate the device (node \in [kiev|fugaku]; thunderx2 outdated)
```
# individually, e.g.:
DEBUG="-DGEMM_TIMINGS" make -f Makefile.<node> -B ; mv lib/libMocCUDA.so lib/libMocCUDA_timing.so
make -f Makefile.<node> -B
# or all via:
bash ./scripts/07_*.sh
```

## debug build and passthru cuda calls to real gpu (only for devs)
```
python3 -m pip install --upgrade --force-reinstall --no-deps ./dep/horovod/debug_dist/horovod-*.whl
python3 -m pip install --upgrade --force-reinstall --no-deps ./dep/pytorch/debug_dist/torch-*.whl
RedirectCUDA="-DUSE_MocCUDA=0" DEBUG="-DDEBUG -O0 -g" make -f Makefile.<node> -B
LD_PRELOAD=~/fake_a64fx_cuda/lib/libMocCUDA.so <some_test> | tee log
```

## running benchmarks
```
# on fugaku
bash ./bench/submit_fugaku.sh
# or individually, e.g.:
for NR in $(seq -w 00 04); do
	bash ./bench/${NR}_*.sh
fi
```

## Additional notes:
* this repo does not include 3rd party and proprietary libraries
* if a dependency is not met, the scripts usually point it out and ask the user to install the missing lib/tool/etc.
