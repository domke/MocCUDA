#ifdef DEBUG
#define UNW_LOCAL_ONLY
#include <libunwind.h>
#include <demangle.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#ifdef REFLAPACK
    #define blasint int
#elif defined FUJITSU
    #define blasint FJ_MATHLIB_TYPE_INT
#elif defined ARMPL
    #define blasint armpl_int_t
#endif
#include <cblas.h>
#include <math.h>

#include "utils.h"
#include "passthrough.h"

#ifdef MEMCPYINFRT
void memcpy_float_(float*, const float*, const int*);
void memcpy_double_(double*, const double*, const int*);
void memcpy_byte_(char*, const char*, const int*);
void memcpy_float_image_stride2_(float*, const int*, float*, const int*,
                                 const int*, const int*, const int*,
                                 const int*, const int*);
#endif

static void _backtrace(FILE *file)
{
#ifdef DEBUG
    unw_cursor_t cursor;
    unw_context_t context;

    // Initialize cursor to current frame for local unwinding.
    unw_getcontext(&context);
    unw_init_local(&cursor, &context);

    // Unwind frames one by one, going up the frame stack.
    while (unw_step(&cursor) > 0) {
        unw_word_t offset, pc;
        unw_get_reg(&cursor, UNW_REG_IP, &pc);
        if (pc == 0)
            break;
        fprintf(file, "\t\t0x%lx:", pc);

        char sym[256];

        if (0 == unw_get_proc_name(&cursor, sym, sizeof(sym), &offset)) {
            char* nameptr = sym;
            char* demangled = cplus_demangle(sym, DMGL_PARAMS);
            if (demangled)
                nameptr = demangled;
            fprintf(file, " (%s+0x%lx)\n", nameptr, offset);
        } else {
            fprintf(file,
                    " -- error: unable to obtain symbol name for this frame\n");
        }
    }
#endif
}

void show_backtrace()
{
    _backtrace(stderr);
}

void write_backtrace(FILE *file)
{
    _backtrace(file);
}

static void* known_func_array[256];
static int known_func = 0;

bool func_is_known(const void *func)
{
    int i;
    void *tFunc = (void*)func;
    for (i = 0; i < known_func; i++)
        if (tFunc == known_func_array[i])
            return true;
    known_func_array[known_func++] = tFunc;
    return false;
}

void fprintfIntArray(const int *array, const int nmemb)
{
    int i = 0;
    if (nmemb < 1)
        return;
    fprintf(stderr, "[%d", array[0]);
    for (i = 1; i < nmemb; i++)
        fprintf(stderr, ",%d", array[i]);
    fprintf(stderr, "]");
}

void fprintfFloatArray(const float *array, const int nmemb)
{
    int i = 0;
    if (nmemb < 1)
        return;
    fprintf(stderr, "[%.4e", array[0]);
    for (i = 1; i < nmemb; i++)
        fprintf(stderr, ",%.4e", array[i]);
    fprintf(stderr, "]");
}

static inline char* split_array(char *arr, const size_t elem,
                                const size_t parts,
                                size_t *chk, const size_t chk_num)
{
    size_t chunk;
    ldiv_t res = ldiv(elem, parts);
    if (0 == res.rem) {
        chunk = res.quot;
        arr += chk_num * chunk;
    } else {
        if (chk_num < res.rem) {
            chunk = res.quot + 1;
            arr += chk_num * chunk;
        } else {
            chunk = res.quot;
            arr += res.rem * (chunk + 1) + (chk_num - res.rem) * chunk;
        }
    }
    *chk = chunk;
    return arr;
}

void para_memset(void *s, const int c, const size_t n)
{
    size_t i;
    char *arr = (char *)s;
    #pragma omp parallel
    {
        const int ompnt = omp_get_num_threads();
        size_t chunk;
        #pragma omp for firstprivate(arr), schedule(static,1), nowait
        for (i = 0; i < ompnt; i++) {
            arr = split_array(arr, n, ompnt, &chunk, i);
            memset(arr, c, chunk);
        }
    }
}

void para_memcpy(void *dest, const void *src, size_t n)
{
    size_t i;
    char *s = (char *)src, *d = (char *)dest;
    //fprintf(stderr, "%p <- %p-%p : %lu\n", d, s, s+n, n);
    #pragma omp parallel
    {
        const int ompnt = omp_get_num_threads();
        size_t chunk;
        #pragma omp for firstprivate(s,d), schedule(static,1), nowait
        for (i = 0; i < ompnt; i++) {
            s = split_array(s, n, ompnt, &chunk, i);
            d = d + (s - (char *)src);
#ifndef MEMCPYINFRT
            memcpy(d, s, chunk);
#else
            const int c = chunk;
            memcpy_byte_(d, s, &c);
#endif
            //fprintf(stderr, "%d: %p <- %p-%p : %lu\n",
            //        omp_get_thread_num(), d, s, s+chunk, chunk);
        }
    }
}

void prescale_output(const float factor, float *array, const size_t num_elem)
{
    if (0.0 == factor)
        para_memset(array, 0, num_elem * sizeof(float));
    else if (1.0 != factor)
        cblas_sscal(((blasint)num_elem), factor, array, ((blasint)1));
    //else if (1.0 == factor)
    //      nothing to do
}

//#ifndef V0
#ifndef MEMCPYINFRT
void memcpy_with_indices(float *d, const float *s, const int len_s,
                         const int *idx, const int fixed_offset_to_idx,
                         int len_idx)
{
    int i = 0;
    if (fixed_offset_to_idx < 0)
        for (; idx[i] + fixed_offset_to_idx < 0; i++, d++) { /* just inc i */ }
    else
        for (; idx[len_idx-1] + fixed_offset_to_idx > len_s - 1; len_idx--) {}

    for (; i < len_idx; i++, d++)
        *d = s[idx[i] + fixed_offset_to_idx];
}
#endif

float *im2col_padded_stride2(const float *Im, const int *dim, const int *stride,
                             const int *pad, const int *filterDim, const int ii,
                             float *C_in)
{
    ENTER(cudnn_lib_entry, im2col_padded_stride2);

    const int c = dim[1], h = dim[2], w = dim[3];
    const int padH = pad[0], padW = pad[1];
    const int k_h = filterDim[2], k_w = filterDim[3];
    const int c_h = c * k_h * k_w, c_w = (int)ceil(.5 * h) * (int)ceil(.5 * w);
    float *C = NULL;
    if (C_in)
        C = C_in;
    else
        C = (float *)malloc(c_h * c_w * sizeof(float));
    if (!C)
        return NULL;
#ifndef MEMCPYINFRT
    int *idx = (int*)malloc(c_w * sizeof(int)), i, j, k = 0;
    if (!idx)
        return NULL;
    for (i = 0; i < h; i += 2)
        for (j = 0; j < w; j += 2)
            idx[k++] = i * w + j;
    //for (i = 0; i < k; i++)
    //    fprintf(stderr, "%d,", idx[i]); fprintf(stderr, "\n");
#else
    const int k = c_w;
#endif

    int channel, row, col;
    #pragma omp parallel for \
    firstprivate(ii,c,h,w,padH,padW,k_h,k_w,c_h,c_w,k), private(row,col), \
    schedule(static)
    for (channel = 0; channel < c; channel++) {
        //first simply copy a compressed version of the images
#ifndef MEMCPYINFRT
        int offset = padH * w + padW
            + (w - (int)ceil(.5 * k_w) + 1 - padW /* to cover 1st substr. */);
        for (row = 0; row < k_h*k_w; row++) {
            offset -= ((0 == row % k_w) ?
                       (w - (int)ceil(.5 * k_w) + 1 - padW) : 1);
            float *image = (float*)Im + (ii * stride[0] + channel * stride[1]);
            float *dest = &C[((channel * k_h*k_w) + row) * c_w];
            memcpy_with_indices(dest, image, h * w, idx, -offset, c_w);
            //for (i = 0; i < k; i++)
            //    fprintf(stderr, "%f,", dest[i]); fprintf(stderr, "\n");
        }
#else
        float *image = (float*)Im + (ii * stride[0] + channel * stride[1]);
        for (row = 0; row < k_h; row++) {
            for (col = 0; col < k_w; col++) {
                float *dest = &C[((channel * k_h*k_w) + row*k_w+col) * c_w];
                memcpy_float_image_stride2_(dest, &c_w, image, &h, &w, &row,
                                            &col, &padH, &padW);
            }
        }
#endif
        //then fill zeros to account for padding
        int im_px_h, im_px_w, p, round;
        //  top padding
        col = 0; // start from first image pixel and go forward
        for (im_px_h = 0; im_px_h < padH; im_px_h += 2) {
            for (im_px_w = 0; im_px_w < w; im_px_w += 2) {
                for (p = 0, row = 0; p < (padH-im_px_h)*k_w; p++, row++) {
                    //fprintf(stderr, "setting C[%d,%d]=0\n",row,col);
                    C[((channel * k_h*k_w) + row) * c_w + col] = 0.0;
                }
                col++;
            }
        }
        // bottom padding
        col = k - 1; // start from last image pixel and go backwards
        for (im_px_h = 0; im_px_h < padH-(h%2 ? 0 : 1); im_px_h += 2) {
            for (im_px_w = 0; im_px_w < w; im_px_w += 2) {
                for (p = 0, row = k_h*k_w-1;
                     p < (padH-(h%2 ? 0 : 1)-im_px_h)*k_w;
                     p++, row--) {
                    //fprintf(stderr, "setting C[%d,%d]=0\n",row,col);
                    C[((channel * k_h*k_w) + row) * c_w + col] = 0.0;
                }
                col--;
            }
        }
        // left padding
        col = 0; // start from first image pixel again and jump downwards
        for (im_px_w = 0; im_px_w < padW; im_px_w += 2) {
            for (im_px_h = 0; im_px_h < h; im_px_h += 2) {
                for (round = 0; round < padW-im_px_w; round++) {
                    for (p = 0, row = round; p < k_h; p++, row+=k_w) {
                        //fprintf(stderr, "setting C[%d,%d]=0\n",row,col);
                        C[((channel * k_h*k_w) + row) * c_w + col] = 0.0;
                    }
                }
                col += (int)ceil(.5 * w);
            }
            // reset after one full iteration
            col = /*start*/0 + (im_px_w + 1);
        }
        // right padding
        col = k - 1; // start from last image pixel and jump upwards
        for (im_px_w = 0; im_px_w < padW-(w%2 ? 0 : 1); im_px_w += 2) {
            for (im_px_h = 0; im_px_h < h; im_px_h += 2) {
                for (round = 0; round < padW-(w%2 ? 0 : 1)-im_px_w; round++) {
                    for (p = 0, row = k_h*k_w-1-round; p < k_h; p++, row-=k_w) {
                        //fprintf(stderr, "setting C[%d,%d]=0\n",row,col);
                        C[((channel * k_h*k_w) + row) * c_w + col] = 0.0;
                    }
                }
                col -= (int)ceil(.5 * w);
            }
            // reset after one full iteration
            col = /*start*/(k - 1) - (im_px_w + 1);
        }
        /*for (row=0;row<k_h*k_w;row++) {
            fprintfFloatArray(&C[((channel * k_h*k_w) + row) * c_w], k);
            fprintf(stderr,"\n");
        }*/
    }
/*
    free(idx);
*/

    LEAVE(cudnn_lib_entry, im2col_padded_stride2);
    return C;
}

float *im2col_padded_stride1(const float *Im, const int *dim, const int *stride,
                             const int *pad, const int *filterDim, const int ii,
                             float *C_in)
{
    ENTER(cudnn_lib_entry, im2col_padded_stride1);

    const int i = 0;
    const int n = 1, c = dim[1], h = dim[2], w = dim[3];
    const int padH = pad[0], padW = pad[1];
    const int k_h = filterDim[2], k_w = filterDim[3];
    const int c_h = c * k_h * k_w, c_w = n * h * w;
    float *C = NULL;
    if (C_in)
        C = C_in;
    else
        C = (float *)malloc(c_h * c_w * sizeof(float));
    if (!C)
        return NULL;

    int channel, row, col;
    #pragma omp parallel for \
    firstprivate(ii,i,n,c,h,w,padH,padW,k_h,k_w,c_h,c_w), private(row,col), \
    schedule(static)
    for (channel = 0; channel < c; channel++) {
        //first simply copy the images
        //int offset = padH * w + padW + (w + padW - k_w /*to cover 1st substr.*/);
        int offset = padH * w + padW
            + (w - (int)ceil(.5 * k_w) + 1 - padW /*to cover 1st substr.*/);
        for (row = 0; row < k_h*k_w; row++) {
            //offset += ((0 == row % k_w) ? -(w + padW - k_w) : -1);
            offset -= ((0 == row % k_w) ?
                       (w - (int)ceil(.5 * k_w) + 1 - padW) : 1);
            float *image = (float*)Im + (ii * stride[0] + channel * stride[1]);
            float *dest = &C[((channel * k_h*k_w) + row) * c_w + (i * (h * w))];
            int nb = h * w;
            if (offset > 0) {
                dest += offset;
                nb -= offset;
            } else if (offset < 0) {
                image += -offset;
                nb -= -offset;
            }
#ifndef MEMCPYINFRT
            memcpy(dest, image, nb * sizeof(float));
#else
            memcpy_float_(dest, image, &nb);
#endif
        }
        //then fill zeros to account for padding
        int im_px_h, im_px_w, p, round;
        //  top padding
        col = 0; // start from first image pixel and go forward
        for (im_px_h = 0; im_px_h < padH; im_px_h++) {
            for (im_px_w = 0; im_px_w < w; im_px_w++) {
                for (p = 0, row = 0; p < (padH-im_px_h)*k_w; p++, row++) {
                    //fprintf(stderr, "setting C[%d,%d]=0\n",row,col);
                    C[((channel * k_h*k_w) + row) * c_w + (i * h*w) + col] = 0.0;
                }
                col++;
            }
        }
        // bottom padding
        col = h * w - 1; // start from last image pixel and go backwards
        for (im_px_h = 0; im_px_h < padH; im_px_h++) {
            for (im_px_w = 0; im_px_w < w; im_px_w++) {
                for (p = 0, row = k_h*k_w-1; p < (padH-im_px_h)*k_w; p++, row--) {
                    //fprintf(stderr, "setting C[%d,%d]=0\n",row,col);
                    C[((channel * k_h*k_w) + row) * c_w + (i * h*w) + col] = 0.0;
                }
                col--;
            }
        }
        // left padding
        col = 0; // start from first image pixel again and jump downwards
        for (im_px_w = 0; im_px_w < padW; im_px_w++) {
            for (im_px_h = 0; im_px_h < h; im_px_h++) {
                for (round = 0; round < padW-im_px_w; round++) {
                    for (p = 0, row = round; p < k_h; p++, row+=k_w) {
                        //fprintf(stderr, "setting C[%d,%d]=0\n",row,col);
                        C[((channel * k_h*k_w) + row) * c_w + (i * h*w) + col] = 0.0;
                    }
                }
                col += w;
            }
            // reset after one full iteration
            col = /*start*/0 + (im_px_w + 1);
        }
        // right padding
        col = h * w - 1; // start from last image pixel and jump upwards
        for (im_px_w = 0; im_px_w < padW; im_px_w++) {
            for (im_px_h = 0; im_px_h < h; im_px_h++) {
                for (round = 0; round < padW-im_px_w; round++) {
                    for (p = 0, row = k_h*k_w-1-round; p < k_h; p++, row-=k_w) {
                        //fprintf(stderr, "setting C[%d,%d]=0\n",row,col);
                        C[((channel * k_h*k_w) + row) * c_w + (i * h*w) + col] = 0.0;
                    }
                }
                col -= w;
            }
            // reset after one full iteration
            col = /*start*/(h * w - 1) - (im_px_w + 1);
        }
        /*for (row=0;row<k_h*k_w;row++) {
            fprintfFloatArray(&C[((channel * k_h*k_w) + row) * c_w + (i * (h * w))], h * w);
            fprintf(stderr,"\n");
        }*/
    }

    LEAVE(cudnn_lib_entry, im2col_padded_stride1);
    return C;
}
//#else //V0
//#error "not the right path? -> missing stride2 func"
float *im2col_padded_stride1_v0(const float *Im, const int *dim, const int *stride,
                             const int *pad, const int *filterDim)
{
    ENTER(cudnn_lib_entry, im2col_padded_stride1);

    int i;
    const int n = dim[0], c = dim[1], h = dim[2], w = dim[3];
    const int padH = pad[0], padW = pad[1];
    const int k_h = filterDim[2], k_w = filterDim[3];
    const int c_h = c * k_h * k_w, c_w = n * h * w;
    //allocate a bit bigger, then we can use this mat in col2im as temp storage
    float *C = (float *)malloc(max(c_h * c_w, filterDim[0] * c_w) * sizeof(float));
    if (!C)
        return NULL;
    #pragma omp parallel for \
    firstprivate(n,c,h,w,padH,padW,k_h,k_w,c_h,c_w), schedule(static)
    for (i = 0; i < n; i++)
    {
        int channel, row, col;
        for (channel = 0; channel < c; channel++) {
            //first simply copy the images
            //int offset = padH * w + padW + (w + padW - k_w /*to cover 1st substr.*/);
            int offset = padH * w + padW
                + (w - (int)ceil(.5 * k_w) + 1 - padW /*to cover 1st substr.*/);
            for (row = 0; row < k_h*k_w; row++) {
                //offset += ((0 == row % k_w) ? -(w + padW - k_w) : -1);
                offset -= ((0 == row % k_w) ?
                           (w - (int)ceil(.5 * k_w) + 1 - padW) : 1);
                float *image =
                    (float*)Im + (i * stride[0] + channel * stride[1]);
                float *dest = &C[((channel * k_h*k_w) + row) * c_w + (i * (h * w))];
                int nb = h * w;
                if (offset > 0) {
                    dest += offset;
                    nb -= offset;
                } else if (offset < 0) {
                    image += -offset;
                    nb -= -offset;
                }
#ifndef MEMCPYINFRT
                memcpy(dest, image, nb * sizeof(float));
#else
                memcpy_float_(dest, image, &nb);
#endif
            }
            //then fill zeros to account for padding
            int im_px_h, im_px_w, p, round;
            //  top padding
            col = 0; // start from first image pixel and go forward
            for (im_px_h = 0; im_px_h < padH; im_px_h++) {
                for (im_px_w = 0; im_px_w < w; im_px_w++) {
                    for (p = 0, row = 0; p < (padH-im_px_h)*k_w; p++, row++) {
                        //fprintf(stderr, "setting C[%d,%d]=0\n",row,col);
                        C[((channel * k_h*k_w) + row) * c_w + (i * (h * w)) + col] = 0.0;
                    }
                    col++;
                }
            }
            // bottom padding
            col = h * w - 1; // start from last image pixel and go backwards
            for (im_px_h = 0; im_px_h < padH; im_px_h++) {
                for (im_px_w = 0; im_px_w < w; im_px_w++) {
                    for (p = 0, row = k_h*k_w-1; p < (padH-im_px_h)*k_w; p++, row--) {
                        //fprintf(stderr, "setting C[%d,%d]=0\n",row,col);
                        C[((channel * k_h*k_w) + row) * c_w + (i * (h * w)) + col] = 0.0;
                    }
                    col--;
                }
            }
            // left padding
            col = 0; // start from first image pixel again and jump downwards
            for (im_px_w = 0; im_px_w < padW; im_px_w++) {
                for (im_px_h = 0; im_px_h < h; im_px_h++) {
                    for (round = 0; round < padW-im_px_w; round++) {
                        for (p = 0, row = round; p < k_h; p++, row+=k_w) {
                            //fprintf(stderr, "setting C[%d,%d]=0\n",row,col);
                            C[((channel * k_h*k_w) + row) * c_w + (i * (h * w)) + col] = 0.0;
                        }
                    }
                    col += w;
                }
                // reset after one full iteration
                col = /*start*/0 + (im_px_w + 1);
            }
            // right padding
            col = h * w - 1; // start from last image pixel and jump upwards
            for (im_px_w = 0; im_px_w < padW; im_px_w++) {
                for (im_px_h = 0; im_px_h < h; im_px_h++) {
                    for (round = 0; round < padW-im_px_w; round++) {
                        for (p = 0, row = k_h*k_w-1-round; p < k_h; p++, row-=k_w) {
                            //fprintf(stderr, "setting C[%d,%d]=0\n",row,col);
                            C[((channel * k_h*k_w) + row) * c_w + (i * (h * w)) + col] = 0.0;
                        }
                    }
                    col -= w;
                }
                // reset after one full iteration
                col = /*start*/(h * w - 1) - (im_px_w + 1);
            }
            /*for (row=0;row<k_h*k_w;row++) {
                fprintfFloatArray(&C[((channel * k_h*k_w) + row) * c_w + (i * (h * w))], h * w);
                fprintf(stderr,"\n");
            }*/
        }
    }

    LEAVE(cudnn_lib_entry, im2col_padded_stride1);
    return C;
}
//#endif //V0

void col2im(float *Y, const int *dim, const int *stride, float *tmpBuff)
{
    ENTER(cudnn_lib_entry, col2im);

    int i;
    const int n = dim[0], f = dim[1], h = dim[2], w = dim[3];
    const int s0 = stride[0], imSize = h * w;
    const int y_w = n * imSize;
    #pragma omp parallel for firstprivate(n,f,s0,imSize,y_w), schedule(static)
    for (i = 0; i < n; i++) {
        int filter;
        for (filter = 0; filter < f; filter++) {
#ifndef MEMCPYINFRT
            memcpy(&tmpBuff[i * s0 + filter * imSize],
                   &Y[filter * y_w + i * imSize],
                   imSize * sizeof(float));
#else
            memcpy_float_(&tmpBuff[i * s0 + filter * imSize],
                          &Y[filter * y_w + i * imSize],
                          &imSize);
#endif
        }
    }
    para_memcpy(Y, tmpBuff, n * s0 * sizeof(float));

    LEAVE(cudnn_lib_entry, col2im);
}

float *col2im_back(float *dim_col, const int h_prime, const int w_prime,
                   const int stride, const int hh, const int ww, const int c)
{
    ENTER(cudnn_lib_entry, col2im_back);

    const int H = (h_prime - 1) * stride + hh, W = (w_prime - 1) * stride + ww;
    float *dx = (float*)calloc(c * H * W, sizeof(float));
    if (!dx)
        return NULL;
    int j, channel, dx_h, dx_w /*, r_h, r_w*/;
    for (j = 0; j < h_prime * w_prime; j++) {
        float *row = &dim_col[j * (c * hh * ww)];
        const int h_start = (j / w_prime) * stride,
              w_start = (j % w_prime) * stride;
        for (channel = 0; channel < c; channel++) {
            float *t_row = row + channel * (hh * ww);
            for (dx_h = h_start /*, r_h = 0*/;
                 dx_h < h_start + hh; dx_h++ /*, r_h++*/) {
                float *t_dx = dx + channel * (H * W) + dx_h * W + w_start;
                for (dx_w = w_start /*, r_w = 0*/;
                     dx_w < w_start + ww; dx_w++ /*, r_w++*/)
                    *(t_dx++) += *(t_row++);
                    //dx[channel * (H * W) + dx_h * W + dx_w] +=
                    //    row[channel * (hh * ww) + r_h * ww + r_w];
            }
        }
    }

    LEAVE(cudnn_lib_entry, col2im_back);
    return dx;
}
