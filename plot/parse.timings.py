#!/usr/bin/env python3

import re
from sys import argv

reg = re.compile(r'.*LEAVE\s+([_:\w]+)\s+\(rt:([+-]?(?:0|[1-9]\d*)(?:\.\d*)?(?:[eE][+\-]?\d+))\)')

res = {}

with open(argv[1], 'r') as f:
    for line in f:
        r_m = reg.match(line)
        if r_m:
            fn, ti = r_m.group(1), float(r_m.group(2))
            if fn not in res:
                res[fn] = 0.0
            res[fn] += ti
        elif 'LEAVE' in line:
            print('issue:', line)

for ti, fn in sorted([[ti, fn] for fn, ti in res.items()]):
    if '_async' in fn or 'cpy' in fn:
        print(fn, ti)
    elif 'im2col' in fn or 'col2im' in fn:
        print(fn, ti / float(argv[2]))
