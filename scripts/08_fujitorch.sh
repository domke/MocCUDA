#!/bin/bash
SDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )" && pwd )"
source "${SDIR}/host.env"

cd ${SDIR}/../dep ; mkdir -p install
WDIR="$(pwd)"

if [[ "$(hostname -s)" = "kiev"* ]]; then

	exit 0

elif [[ "$(hostname -s)" = "epyc"* ]]; then

	exit 0

elif lscpu | grep 'sve' >/dev/null 2>&1; then

	export FJTORCH150_DIR="/home/apps/oss/PyTorch-1.5.0"

	cd ${WDIR}/install
	rm -rf ./fj150_venv

	export PATH="${FJTORCH150_DIR}/bin:${PATH}"
	export LD_LIBRARY_PATH="${FJTORCH150_DIR}/lib:${LD_LIBRARY_PATH}"
	python3 -m venv --system-site-packages fj150_venv
	source ./fj150_venv/bin/activate

	#install additional stuff for benchmarker
	CC="fcc" CFLAGS="-Nclang -Ofast" CXX="FCC" CXXFLAGS="-Nclang -Ofast" \
	LD="fcc -Nclang" LDSHARED="fcc -Nclang -shared" \
	python3 -m pip install --upgrade begins py-cpuinfo pydataset 'system_query[cpu, hdd, ram, swap]'

	deactivate


	export FJTORCH170_DIR="/home/apps/oss/PyTorch-1.7.0"

	cd ${WDIR}/install
	rm -rf ./fj170_venv

	export PATH="${FJTORCH170_DIR}/bin:${PATH}"
	export LD_LIBRARY_PATH="${FJTORCH170_DIR}/lib:${LD_LIBRARY_PATH}"
	python3 -m venv --system-site-packages fj170_venv
	source ./fj170_venv/bin/activate

	#install additional stuff for benchmarker
	CC="fcc" CFLAGS="-Nclang -Ofast" CXX="FCC" CXXFLAGS="-Nclang -Ofast" \
	LD="fcc -Nclang" LDSHARED="fcc -Nclang -shared" \
	python3 -m pip install --upgrade begins py-cpuinfo pydataset 'system_query[cpu, hdd, ram, swap]'

	deactivate

else
	echo 'Err: unknown system; config please' ; exit 1
fi

cat <<EOF > ${WDIR}/fujitorch150.env
#export PRELOADLIBS=${FJTORCH150_DIR}/lib/libtcmalloc.so    #XXX: sys upgrade broken tcmal
export LD_LIBRARY_PATH="${FJTORCH150_DIR}/lib:\${LD_LIBRARY_PATH}"
export VENV_ROOT=${WDIR}/install/fj150_venv
source \${VENV_ROOT}/bin/activate
export VENV_SITEPACKAGES=\$(python -c 'import sys ; print([p for p in sys.path if "site-packages" in p][0])')
EOF
cat <<EOF > ${WDIR}/fujitorch170.env
export PRELOADLIBS=${FJTORCH170_DIR}/lib/libtcmalloc.so
export LD_LIBRARY_PATH="${FJTORCH170_DIR}/lib:\${LD_LIBRARY_PATH}"
export VENV_ROOT=${WDIR}/install/fj170_venv
source \${VENV_ROOT}/bin/activate
export VENV_SITEPACKAGES=\$(python -c 'import sys ; print([p for p in sys.path if "site-packages" in p][0])')
EOF
