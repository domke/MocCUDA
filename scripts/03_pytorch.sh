#!/bin/bash
SDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )" && pwd )"
source "${SDIR}/host.env"
source "${SDIR}/../dep/python.env"

cd ${SDIR}/../dep/pytorch
TORCHDIR=$(pwd)

buildDEBUG=0
buildRELEASE=1

git submodule update --init --recursive
git checkout -- \
	cmake/public/cuda.cmake \
	cmake/Modules/FindOpenBLAS.cmake \
	cmake/Modules_CUDA_fix/FindCUDNN.cmake \
	aten/src/ATen/native/cuda/Activation.cu \
	aten/src/ATen/native/cuda/ReduceOpsKernel.cu \
	aten/src/ATen/native/sparse/cuda/SparseCUDABlas.cu \
	aten/src/THCUNN/LogSigmoid.cu \
	tools/setup_helpers/cmake.py
patch -p1 < ${SDIR}/../patches/torch.v1.4.0.patch
sed -i -e '/CMAKE_INSTALL_PREFIX/a\            "CMAKE_VERBOSE_MAKEFILE": "ON",' tools/setup_helpers/cmake.py

CC="${MocCC}" CFLAGS="${MocCFLAGS}" CXX="${MocCXX}" CXXFLAGS="${MocCXXFLAGS}" \
python3 -m pip install --upgrade -r requirements.txt

if [[ "$(hostname -s)" = "kiev"* ]]; then

	if [ ${buildDEBUG} -ge 1 ]; then
		rm -rf build
		CC="${MocCC}" CFLAGS="${MocCFLAGS}" CXX="${MocCXX}" CXXFLAGS="${MocCXXFLAGS}" \
			BUILD_TYPE=Debug DEBUG=ON REL_WITH_DEB_INFO=ON USE_GFLAGS=ON \
			MAX_JOBS=48 VERBOSE=1 USE_NATIVE_ARCH=ON \
			USE_CUDA=ON CUDA_USE_STATIC_CUDA_RUNTIME=OFF \
			USE_CUDNN=ON USE_STATIC_CUDNN=OFF CUDNN_STATIC=OFF \
			USE_NCCL=ON USE_STATIC_NCCL=OFF USE_SYSTEM_NCCL=ON \
			USE_FBGEMM=OFF USE_NUMPY=ON \
			USE_NNPACK=ON USE_QNNPACK=ON \
			USE_DISTRIBUTED=ON USE_MPI=ON USE_GLOO=OFF \
			BUILD_CAFFE2_OPS=ON CAFFE2_STATIC_LINK_CUDA=OFF \
			USE_OPENMP=ON USE_FFMPEG=OFF DISABLE_NUMA=ON \
			TORCH_CUDA_ARCH_LIST="7.5" \
			TORCH_NVCC_FLAGS="-ccbin $(basename ${MocCXX})" \
			BLAS=MKL USE_MKL=ON USE_MKLDNN=ON MKL_THREADING=OMP USE_MKLDNN_CBLAS=ON \
			USE_PROF=ON BUILD_TEST=OFF BUILD_NAMEDTENSOR=OFF USE_STATIC_DISPATCH=OFF \
			CUDA_HOME=${CUDA_TOOLKIT_ROOT_DIR} \
			CUDNN_INCLUDE_DIR=${CUDNN_ROOT}/include CUDNN_LIB_DIR=${CUDNN_ROOT}/lib64 \
		python3 setup.py bdist_wheel 2>&1 | tee comp_debug
			#TORCH_CUDA_ARCH_LIST="6.1;7.5" \  -> 7.5 runs on RTX 20?0 versions
		mkdir -p debug_dist ; mv dist/torch-*.whl debug_dist/
	fi

	if [ ${buildRELEASE} -ge 1 ]; then
		rm -rf build
		CC="${MocCC}" CFLAGS="${MocCFLAGS}" CXX="${MocCXX}" CXXFLAGS="${MocCXXFLAGS}" \
			BUILD_TYPE=Release DEBUG=OFF REL_WITH_DEB_INFO=OFF USE_GFLAGS=OFF \
			MAX_JOBS=48 VERBOSE=1 USE_NATIVE_ARCH=ON \
			USE_CUDA=ON CUDA_USE_STATIC_CUDA_RUNTIME=OFF \
			USE_CUDNN=ON USE_STATIC_CUDNN=OFF CUDNN_STATIC=OFF \
			USE_NCCL=ON USE_STATIC_NCCL=OFF USE_SYSTEM_NCCL=ON \
			USE_FBGEMM=OFF USE_NUMPY=ON \
			USE_NNPACK=ON USE_QNNPACK=ON \
			USE_DISTRIBUTED=ON USE_MPI=ON USE_GLOO=OFF \
			BUILD_CAFFE2_OPS=ON CAFFE2_STATIC_LINK_CUDA=OFF \
			USE_OPENMP=ON USE_FFMPEG=OFF DISABLE_NUMA=ON \
			TORCH_CUDA_ARCH_LIST="7.5" \
			TORCH_NVCC_FLAGS="-ccbin $(basename ${MocCXX})" \
			BLAS=MKL USE_MKL=ON USE_MKLDNN=ON MKL_THREADING=OMP USE_MKLDNN_CBLAS=ON \
			USE_PROF=ON BUILD_TEST=OFF BUILD_NAMEDTENSOR=OFF USE_STATIC_DISPATCH=OFF \
			CUDA_HOME=${CUDA_TOOLKIT_ROOT_DIR} \
			CUDNN_INCLUDE_DIR=${CUDNN_ROOT}/include CUDNN_LIB_DIR=${CUDNN_ROOT}/lib64 \
		python3 setup.py bdist_wheel 2>&1 | tee comp_release
			#TORCH_CUDA_ARCH_LIST="6.1;7.5" \  -> 7.5 runs on RTX 20?0 versions
	fi

elif [[ "$(hostname -s)" = "epyc"* ]]; then

	rm -rf build

	sed -i -e 's/cudnn\.h/cudnn_version\.h/g' cmake/public/cuda.cmake
	sed -i -e 's/S openblas P/S openblaso P/g' cmake/Modules/FindOpenBLAS.cmake

	# does NOT work with cuda >= 11
	CUDA_TOOLKIT_ROOT_DIR="/opt/cuda-10.2.0"
	CUDNN_ROOT="/opt/cudnn-10.2-v8.2.1.32"

	DEBUG=OFF REL_WITH_DEB_INFO=OFF MAX_JOBS=48 VERBOSE=1 \
		CFLAGS="" CXXFLAGS="" USE_NATIVE_ARCH=ON CC="${MocCC}" CXX="${MocCXX}" \
		USE_CUDA=ON USE_CUDNN=ON USE_STATIC_CUDNN=OFF USE_NCCL=OFF USE_SYSTEM_NCCL=OFF \
		USE_FBGEMM=OFF USE_NUMPY=ON \
		USE_NNPACK=ON USE_QNNPACK=ON \
		USE_DISTRIBUTED=ON USE_MPI=ON USE_GLOO=OFF BUILD_CAFFE2_OPS=ON \
		USE_OPENMP=ON USE_FFMPEG=OFF DISABLE_NUMA=ON \
		TORCH_CUDA_ARCH_LIST="6.1;7.5" TORCH_NVCC_FLAGS="-allow-unsupported-compiler -ccbin clang++" \
		BLAS=OpenBLAS USE_MKLDNN=ON MKL_THREADING=OMP USE_MKLDNN_CBLAS=ON \
		USE_PROF=ON BUILD_TEST=OFF BUILD_NAMEDTENSOR=OFF USE_STATIC_DISPATCH=OFF \
		CUDA_HOME=${CUDA_TOOLKIT_ROOT_DIR} \
		CUDNN_INCLUDE_DIR=${CUDNN_ROOT}/include CUDNN_LIB_DIR=${CUDNN_ROOT}/lib64 \
	python3 setup.py bdist_wheel 2>&1 | tee comp

elif lscpu | grep 'sve' >/dev/null 2>&1; then

	rm -rf build

	#sed -i -e 's/cudnn\.h/cudnn_version\.h/g' cmake/public/cuda.cmake
	sed -i -e 's/ZERO_MACRO 0.f/ZERO_MACRO T{0}/g' aten/src/THCUNN/LogSigmoid.cu

FUCK=me
if [ -n $FUCK ]; then
	mkdir -p ssl2/lib ssl2/include ; rm -f ssl2/include/cblas.h ssl2/lib/libopenblas.so
	ln -s "$(dirname $(which fcc))/../include/cblas.h" ssl2/include/cblas.h
	#ln -s "$(dirname $(which fcc))/../lib64/libfjlapacksve.so" ssl2/lib/libopenblas.so
	#ln -s "$(dirname $(which fcc))/../lib64/libfjlapackexsve.so" ssl2/lib/libopenblas.so

	clang -DFUJITSU -O2 -fPIC -I$(pwd)/ssl2/include \
		-c ${SDIR}/../src/misc/blas_dummy.c -o ssl2/lib/blas_dummy.o
	clang -DFUJITSU -O2 -fPIC -I$(pwd)/ssl2/include \
		ssl2/lib/blas_dummy.o -shared -o ssl2/lib/libopenblas.so \
		-fuse-ld=lld -L$(readlink -f $(dirname $(which fcc))/../lib64) -lfjlapacksve \
		-Wl,-rpath=$(readlink -f $(dirname $(which clang))/../lib) \
		$(readlink -f $(dirname $(which mpifcc))/../lib64)/fjhpctag.o \
		$(readlink -f $(dirname $(which mpifcc))/../lib64)/fjlang08.o \
		-lfj90rt2 -lssl2mtsve -lfj90i -lfj90fmt_sve -lfj90f -lfjsrcinfo -lfj90rt \
		-lfjprofcore -lfjprofomp \
		-lm -lrt -lpthread -lelf -lz -ldl
else
	ARMPLVER="21.1_RHEL-8_gcc-8.2"
	if [ ! -f ${TORCHDIR}/../arm-performance-libraries_${ARMPLVER}.tar ]; then
		echo "ERR: please download Free Arm Performance Libraries (e.g. ${ARMPLVER}) from https://developer.arm.com/tools-and-software/server-and-hpc/downloads/arm-performance-libraries and place tar in folder ${TORCHDIR}/../" ; exit 1
	elif [ ! -d ${TORCHDIR}/../install/armpl ]; then
		mkdir -p ${TORCHDIR}/../install/armpl
		tar -xf ${TORCHDIR}/../arm-performance-libraries_${ARMPLVER}.tar -C ${TORCHDIR}/../install/armpl --strip-components 1
		cd ${TORCHDIR}/../install/armpl
		bash ./arm-performance-libraries_21.1_RHEL-8.sh --accept --install-to $(pwd) --force
		cd -
	fi
	#You can add these to your environment by running:
	#	$ module use $(pwd)/modulefiles
	#Alternatively:	$ export MODULEPATH=$MODULEPATH:$(pwd)/modulefiles
	mkdir -p ssl2/ ; rm -f ssl2/include/ ssl2/lib/
	ln -s ${TORCHDIR}/../install/armpl/armpl_*/include_lp64_mp/ ssl2/include
	ln -s ${TORCHDIR}/../install/armpl/armpl_*/lib ssl2/lib
	ln -s ${TORCHDIR}/../install/armpl/armpl_*/lib/libarmpl_lp64_mp.so ssl2/lib/libopenblas.so
#else
#	spack install openblas@develop %clang@13.0.0 threads=openmp ^perl/cfeod65	#XXX broken
fi

	#fcc -Nclang -DFUJITSU -O2 -fPIC -I$(pwd)/ssl2/include \
	#	-c ${SDIR}/../src/misc/blas_dummy.c -o ssl2/lib/blas_dummy.o
	#fcc -Nclang -DFUJITSU -O2 -fPIC -I$(pwd)/ssl2/include \
	#	ssl2/lib/blas_dummy.o -shared -o ssl2/lib/libopenblas.so -lfjlapacksve

	#echo -e '#include <cblas.h>\n#include <omp.h>\nint dummy (void);\nint dummy (void) { return 0; }' > blas_dummy.c

	#clang -DFUJITSU -O2 -fPIC -fopenmp -I$(pwd)/ssl2/include \
	#	-c ${SDIR}/../src/misc/blas_dummy.c -o ssl2/lib/blas_dummy.o
	#clang ssl2/lib/blas_dummy.o -DFUJITSU -O2 -fPIC -fopenmp \
	#	-Wl,-shared -o ssl2/lib/libopenblas.so \
	#	-fuse-ld=lld -L$(readlink -f $(dirname $(which mpifcc))/../lib64) -Wl,-rpath=$(readlink -f $(dirname $(which clang))/../lib) $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjhpctag.o $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjlang08.o $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjomp.o -lfjomphk -lfjomp -lfj90rt2 -lssl2mtexsve -lssl2mtsve -lfjlapack -lfj90i -lfj90fmt_sve -lfj90f -lfjsrcinfo -lfj90rt -lfjompcrt -lfjprofcore -lfjprofomp -lm -lrt -latomic -lpthread -lelf -lz -ldl

	#clang blas_dummy.o -DFUJITSU -O2 -fPIC -fopenmp  -shared -o  libopenblas.so -L"$(dirname $(which fccpx))/../lib64" -fuse-ld=lld -lfjlapackexsve -lfjomphk -lfjomp -lfj90i -lfj90fmt_sve -lfj90f -lfjsrcinfo -lfj90rt -lfjompcrt -lfjprofcore -lfjprofomp -lm -lrt -latomic -lpthread -ldl
	#clang blas_dummy.o -DFUJITSU -O2 -fPIC -fopenmp  -shared -o  libopenblas.so -L/opt/FJSVstclanga/v1.1.0/lib64 -fuse-ld=lld -lfjlapackexsve

#	fcc -Nclang -DFUJITSU -O2 -fPIC -fopenmp -I$(pwd)/ssl2/include \
#		-c ${SDIR}/../src/misc/blas_dummy.c -o ssl2/lib/blas_dummy.o
#	fcc -Nclang -DFUJITSU -O2 -fPIC -fopenmp \
#		ssl2/lib/blas_dummy.o -shared -o ssl2/lib/libopenblas.so -lfjlapackexsve
#	echo DONE
#	sleep 60

	if [ ${buildDEBUG} -ge 1 ]; then
		LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:$(pwd)/ssl2/lib" \
		CC="${MocCC}" CFLAGS="${MocCFLAGSnoLTO} -msve-vector-bits=scalable" \
		CXX="${MocCXX}" CXXFLAGS="${MocCXXFLAGSnoLTO} -msve-vector-bits=scalable" \
			BUILD_TYPE=Debug DEBUG=ON REL_WITH_DEB_INFO=ON USE_GFLAGS=ON \
			MAX_JOBS=24 VERBOSE=1 USE_NATIVE_ARCH=OFF \
			USE_CUDA=ON CUDA_USE_STATIC_CUDA_RUNTIME=OFF \
			USE_CUDNN=ON USE_STATIC_CUDNN=OFF CUDNN_STATIC=OFF \
			USE_NCCL=ON USE_STATIC_NCCL=OFF USE_SYSTEM_NCCL=ON \
			USE_FBGEMM=OFF USE_NUMPY=ON \
			USE_NNPACK=ON USE_QNNPACK=ON \
			USE_DISTRIBUTED=ON USE_MPI=ON USE_GLOO=OFF \
			BUILD_CAFFE2_OPS=ON CAFFE2_STATIC_LINK_CUDA=OFF \
			USE_OPENMP=ON USE_FFMPEG=OFF DISABLE_NUMA=ON \
			TORCH_CUDA_ARCH_LIST="7.5" \
			TORCH_NVCC_FLAGS="-ccbin $(basename ${MocCXX})" \
			BLAS=OpenBLAS OpenBLAS_HOME=$(pwd)/ssl2 USE_LAPACK=ON \
			USE_MKLDNN=OFF MKL_THREADING=OMP USE_MKLDNN_CBLAS=ON \
			USE_PROF=ON BUILD_TEST=OFF BUILD_NAMEDTENSOR=OFF USE_STATIC_DISPATCH=OFF \
			CUDA_HOME="${CUDA_TOOLKIT_ROOT_DIR}" \
			CUDNN_INCLUDE_DIR="${CUDNN_ROOT}/include" CUDNN_LIB_DIR="${CUDNN_ROOT}/lib64" \
		python3 setup.py bdist_wheel 2>&1 | tee comp_debug
		mkdir -p debug_dist ; mv dist/torch-*.whl debug_dist/
	fi

	if [ ${buildRELEASE} -ge 1 ]; then
		LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:$(pwd)/ssl2/lib" \
		CC="${MocCC}" CFLAGS="${MocCFLAGSnoLTO} -msve-vector-bits=scalable" \
		CXX="${MocCXX}" CXXFLAGS="${MocCXXFLAGSnoLTO} -msve-vector-bits=scalable" \
			BUILD_TYPE=Release DEBUG=OFF REL_WITH_DEB_INFO=OFF USE_GFLAGS=OFF \
			MAX_JOBS=24 VERBOSE=1 USE_NATIVE_ARCH=OFF \
			USE_CUDA=ON CUDA_USE_STATIC_CUDA_RUNTIME=OFF \
			USE_CUDNN=ON USE_STATIC_CUDNN=OFF CUDNN_STATIC=OFF \
			USE_NCCL=ON USE_STATIC_NCCL=OFF USE_SYSTEM_NCCL=ON \
			USE_FBGEMM=OFF USE_NUMPY=ON \
			USE_NNPACK=ON USE_QNNPACK=ON \
			USE_DISTRIBUTED=ON USE_MPI=ON USE_GLOO=OFF \
			BUILD_CAFFE2_OPS=ON CAFFE2_STATIC_LINK_CUDA=OFF \
			USE_OPENMP=ON USE_FFMPEG=OFF DISABLE_NUMA=ON \
			TORCH_CUDA_ARCH_LIST="7.5" \
			TORCH_NVCC_FLAGS="-ccbin $(basename ${MocCXX})" \
			BLAS=OpenBLAS OpenBLAS_HOME=$(pwd)/ssl2 USE_LAPACK=ON \
			USE_MKLDNN=OFF MKL_THREADING=OMP USE_MKLDNN_CBLAS=ON \
			USE_PROF=ON BUILD_TEST=OFF BUILD_NAMEDTENSOR=OFF USE_STATIC_DISPATCH=OFF \
			CUDA_HOME="${CUDA_TOOLKIT_ROOT_DIR}" \
			CUDNN_INCLUDE_DIR="${CUDNN_ROOT}/include" CUDNN_LIB_DIR="${CUDNN_ROOT}/lib64" \
		python3 setup.py bdist_wheel 2>&1 | tee comp_release
	fi

if [ -n $FUCK ]; then
	rm -f ssl2/lib/libopenblas.so
	#ln -s "$(dirname $(which fcc))/../lib64/libfjlapacksve.so" ssl2/lib/libopenblas.so
	echo 'int dummy(void); int dummy(void){return 0;}' >dummy.c
	fcc dummy.c -c -fPIC
	fcc dummy.o -lfjlapacksve -lfj90i -lfj90f -lfjsrcinfo -lelf -shared -o ssl2/lib/libopenblas.so
	#fcc ./dummy.o -lfjlapackexsve -fopenmp -v -lfj90i -lfj90f -lfjsrcinfo -lelf -shared -o libopenblas.so -Nlibomp -Nclang -Knolargepage
	#-Wl,--unresolved-symbols=ignore-all
fi

#		DEBUG=OFF REL_WITH_DEB_INFO=OFF MAX_JOBS=48 VERBOSE=1 \
#		CC=$(which clang) CXX=$(which clang++) \
#		CFLAGS="-mcpu=a64fx -msve-vector-bits=scalable" \
#		CXXFLAGS="-mcpu=a64fx -msve-vector-bits=scalable" \
#		BLAS=OpenBLAS OpenBLAS_HOME=$(pwd)/ssl2 USE_LAPACK=ON \
#		USE_CUDA=OFF USE_NCCL=OFF USE_SYSTEM_NCCL=OFF USE_ROCM=OFF \
#		USE_FBGEMM=OFF USE_FFMPEG=OFF USE_GFLAGS=OFF \
#		USE_MKLDNN=ON MKL_THREADING=OMP USE_MKLDNN_CBLAS=ON \
#		USE_PROF=ON BUILD_TEST=OFF BUILD_NAMEDTENSOR=OFF USE_STATIC_DISPATCH=OFF \
#		USE_DISTRIBUTED=ON USE_MPI=ON USE_GLOO=OFF USE_TENSORPIPE=OFF \
#	exit

#	curl -O http://www.ijg.org/files/jpegsrc.v9d.tar.gz
#	tar zxf jpegsrc.v9d.tar.gz ; cd jpeg-9d
#	./configure --prefix="$(pwd)/.local" --enable-shared
#	make -j32 install
#	cd -
#	JPEG_ROOT="$(pwd)/jpeg-9d" python3 -m pip install --upgrade torchvision==0.2.2
#	python3 -m pip install --upgrade transformers==4.5.0 || python3 -m pip install --upgrade transformers==4.5.0 --no-deps

else
	echo 'Err: unknown system; config please' ; exit 1
fi

cd "${SDIR}"/	# step outside build dir to prevent strange pip issues
if ! python3 -m pip list | grep torch >/dev/null 2>&1 ; then
	python3 -m pip install --upgrade "${TORCHDIR}"/dist/torch-*.whl
else
	python3 -m pip install --upgrade --force-reinstall --no-deps "${TORCHDIR}"/dist/torch-*.whl
fi

cat <<EOF > ${TORCHDIR}/../torch.env
export TORCH_BUILD_ROOT=${TORCHDIR}
EOF






exit





export CUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda-10.0
export PATH=${CUDA_TOOLKIT_ROOT_DIR}/bin:${PATH}
export LD_LIBRARY_PATH=${CUDA_TOOLKIT_ROOT_DIR}/lib64:${LD_LIBRARY_PATH}
export CC=$(which gcc) ; export CXX=$(which g++) ; export NVCC=${CUDA_TOOLKIT_ROOT_DIR}/bin/nvcc

export CUDNN_ROOT=/work/opt/cuda/cudnn-10.0-linux-x64-v7.6.5.32
export CUDNN_INCLUDE_DIR=$CUDNN_ROOT/include
export CUDNN_LIB_DIR=$CUDNN_ROOT/lib64
export LD_LIBRARY_PATH=$CUDNN_LIB_DIR:$LD_LIBRARY_PATH

#export SCOREP_TOTAL_MEMORY=4089446400
#export SCOREP_ENABLE_PROFILING=true
#export SCOREP_ENABLE_TRACING=false

export ARCH_OPT_FLAGS="-march=native -O3 -Wno-deprecated -fvisibility-inlines-hidden -fdiagnostics-color=always -fno-math-errno -fno-trapping-math"
export CMAKE_C_FLAGS="$ARCH_OPT_FLAGS"
export CMAKE_CXX_FLAGS="$CMAKE_C_FLAGS -faligned-new"
export CFLAGS="$CMAKE_C_FLAGS"

# set CAFFE2_STATIC_LINK_CUDA to ON in CMakeLists.txt
# for x in ./aten/src/ATen/CMakeLists.txt ./cmake/public/cuda.cmake ./torch/share/cmake/Caffe2/public/cuda.cmake ; do sed -i -e 's/libcufft_static\.a/libcufft_static_nocallback.a/g' $x; done
BLAS=MKL BUILD_NAMEDTENSOR=OFF BUILD_TYPE=${buildTYPE} DISABLE_NUMA=1 \
	PERF_WITH_AVX=1 PERF_WITH_AVX2=1 PERF_WITH_AVX512=1 \
	USE_CUDA=ON USE_EXCEPTION_PTR=1 USE_GFLAGS=OFF USE_GLOG=OFF USE_MKL=ON USE_MKLDNN=ON \
	USE_MPI=OFF USE_NCCL=OFF USE_NNPACK=ON USE_OPENMP=ON USE_STATIC_DISPATCH=OFF \
	MAGMA_HOME=/home/jens/magma-2.5.2 USE_STATIC_NCCL=0 CUDA_USE_STATIC_CUDA_RUNTIME=1 CUDNN_STATIC=1 \
	CAFFE2_STATIC_LINK_CUDA=1 USE_STATIC_CUDNN=1 VERBOSE=1 BUILD_TEST=0 NO_DISTRIBUTED=1 \
	python3 setup.py bdist_wheel 2>&1 | tee comp

rm -rf build
export NCCL_ROOT=/work/opt/cuda/nccl_2.5.6-1+cuda10.0_x86_64 ; export NCCL_ROOT_DIR=${NCCL_ROOT}
export NCCL_INCLUDE_DIR=${NCCL_ROOT}/include ; export NCCL_LIB_DIR=${NCCL_ROOT}/lib
export LD_LIBRARY_PATH=${NCCL_LIB_DIR}:${LD_LIBRARY_PATH}
BLAS=MKL BUILD_NAMEDTENSOR=OFF BUILD_TYPE=${buildTYPE} DISABLE_NUMA=1 \
	PERF_WITH_AVX=1 PERF_WITH_AVX2=1 PERF_WITH_AVX512=1 \
	USE_CUDA=ON USE_EXCEPTION_PTR=1 USE_GFLAGS=OFF USE_GLOG=OFF USE_MKL=ON USE_MKLDNN=ON \
	USE_MPI=ON USE_NCCL=ON USE_NNPACK=ON USE_OPENMP=ON USE_STATIC_DISPATCH=OFF \
	USE_STATIC_NCCL=OFF CUDA_USE_STATIC_CUDA_RUNTIME=OFF CUDNN_STATIC=OFF CAFFE2_STATIC_LINK_CUDA=OFF \
	USE_STATIC_CUDNN=OFF VERBOSE=1 BUILD_TEST=0 USE_DISTRIBUTED=ON \
	TORCH_CUDA_ARCH_LIST="6.0;6.1;7.0;7.5" MAX_JOBS=48 \
	python3 setup.py bdist_wheel 2>&1 | tee comp
python3 -m pip install --user --upgrade ~/pytorch/dist/torch-1.4.0a0+7f73f1d-cp36-cp36m-linux_x86_64.whl

HOROVOD_WITH_PYTORCH=1 HOROVOD_WITHOUT_TENSORFLOW=1 HOROVOD_WITHOUT_MXNET=1 HOROVOD_WITHOUT_GLOO=1 \
	HOROVOD_WITH_MPI=1 HOROVOD_GPU=CUDA \
	HOROVOD_GPU_ALLREDUCE=MPI HOROVOD_GPU_ALLGATHER=MPI \
	HOROVOD_GPU_BROADCAST=MPI HOROVOD_CPU_OPERATIONS=MPI \
	python3 setup.py bdist_wheel 2>&1 | tee comp
python3 -m pip install --user --upgrade ~/horovod/dist/horovod-0.19.0-cp36-cp36m-linux_x86_64.whl

