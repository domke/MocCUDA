#include <stdlib.h>
#include "hijack_cudnn.h"

cudnnStatus_t cudnnActivationBackward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnActivationForward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnAddTensor(cudnnHandle_t handle,
                             const void *alpha,
                             const cudnnTensorDescriptor_t aDesc,
                             const void *A,
                             const void *beta,
                             const cudnnTensorDescriptor_t cDesc,
                             void *C)
{
    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;
    return rcs;
}

cudnnStatus_t cudnnBatchNormalizationBackward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnBatchNormalizationBackwardEx(cudnnHandle_t handle,
                                                cudnnBatchNormMode_t mode,
                                                cudnnBatchNormOps_t bnOps,
                                                const void *alphaDataDiff,
                                                const void *betaDataDiff,
                                                const void *alphaParamDiff,
                                                const void *betaParamDiff,
                                                const cudnnTensorDescriptor_t xDesc,
                                                const void *xData,
                                                const cudnnTensorDescriptor_t yDesc,
                                                const void *yData,
                                                const cudnnTensorDescriptor_t dyDesc,
                                                const void *dyData,
                                                const cudnnTensorDescriptor_t dzDesc,
                                                void *dzData,
                                                const cudnnTensorDescriptor_t dxDesc,
                                                void *dxData,
                                                const cudnnTensorDescriptor_t dBnScaleBiasDesc,
                                                const void *bnScaleData,
                                                const void *bnBiasData,
                                                void *dBnScaleData, //resultBnScaleDiff
                                                void *dBnBiasData,  //resultBnBiasDiff
                                                double epsilon,
                                                const void *savedMean,
                                                const void *savedInvVariance,
                                                cudnnActivationDescriptor_t activationDesc,
                                                void *workSpace,
                                                size_t workSpaceSizeInBytes,
                                                void *reserveSpace,
                                                size_t reserveSpaceSizeInBytes)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnBatchNormalizationForwardInference()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnBatchNormalizationForwardTraining()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnBatchNormalizationForwardTrainingEx(cudnnHandle_t handle,
                                                       cudnnBatchNormMode_t mode,
                                                       cudnnBatchNormOps_t bnOps,
                                                       const void *alpha,
                                                       const void *beta,
                                                       const cudnnTensorDescriptor_t xDesc,
                                                       const void *xData,
                                                       const cudnnTensorDescriptor_t zDesc,
                                                       const void *zData,
                                                       const cudnnTensorDescriptor_t yDesc,
                                                       void *yData,
                                                       const cudnnTensorDescriptor_t bnScaleBiasMeanVarDesc,
                                                       const void *bnScale,
                                                       const void *bnBias,
                                                       double exponentialAverageFactor,
                                                       void *resultRunningMean,
                                                       void *resultRunningVariance,
                                                       double epsilon,
                                                       void *resultSaveMean,
                                                       void *resultSaveInvVariance,
                                                       cudnnActivationDescriptor_t activationDesc,
                                                       void *workspace,
                                                       size_t workSpaceSizeInBytes,
                                                       void *reserveSpace,
                                                       size_t reserveSpaceSizeInBytes)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnConvolutionBackwardBias()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnConvolutionBackwardData(cudnnHandle_t handle,
                                           const void *alpha,
                                           const cudnnFilterDescriptor_t wDesc,
                                           const void *w,
                                           const cudnnTensorDescriptor_t dyDesc,
                                           const void *dy,
                                           const cudnnConvolutionDescriptor_t convDesc,
                                           cudnnConvolutionBwdDataAlgo_t algo,
                                           void *workSpace,
                                           size_t workSpaceSizeInBytes,
                                           const void *beta,
                                           const cudnnTensorDescriptor_t dxDesc,
                                           void *dx)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnConvolutionBackwardFilter(cudnnHandle_t handle,
                                             const void *alpha,
                                             const cudnnTensorDescriptor_t xDesc,
                                             const void *x,
                                             const cudnnTensorDescriptor_t dyDesc,
                                             const void *dy,
                                             const cudnnConvolutionDescriptor_t convDesc,
                                             cudnnConvolutionBwdFilterAlgo_t algo,
                                             void *workSpace,
                                             size_t workSpaceSizeInBytes,
                                             const void *beta,
                                             const cudnnFilterDescriptor_t dwDesc,
                                             void *dw)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnConvolutionBiasActivationForward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnConvolutionForward(cudnnHandle_t handle,
                                      const void *alpha,
                                      const cudnnTensorDescriptor_t xDesc,
                                      const void *x,
                                      const cudnnFilterDescriptor_t wDesc,
                                      const void *w,
                                      const cudnnConvolutionDescriptor_t convDesc,
                                      cudnnConvolutionFwdAlgo_t algo,
                                      void *workSpace,
                                      size_t workSpaceSizeInBytes,
                                      const void *beta,
                                      const cudnnTensorDescriptor_t yDesc,
                                      void *y)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCopyAlgorithmDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreate(cudnnHandle_t *handle)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateActivationDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateAlgorithmDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateAlgorithmPerformance()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateAttnDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t
cudnnCreateConvolutionDescriptor(cudnnConvolutionDescriptor_t *convDesc)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateCTCLossDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateDropoutDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateFilterDescriptor(cudnnFilterDescriptor_t *filterDesc)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateFusedOpsConstParamPack()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateFusedOpsPlan()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateFusedOpsVariantParamPack()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateLRNDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateOpTensorDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreatePersistentRNNPlan()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreatePoolingDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateReduceTensorDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateRNNDataDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateRNNDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateSeqDataDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateSpatialTransformerDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateTensorDescriptor(cudnnTensorDescriptor_t *tensorDesc)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCreateTensorTransformDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnCTCLoss()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDeriveBNTensorDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroy(cudnnHandle_t handle)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyActivationDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyAlgorithmDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyAlgorithmPerformance()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyAttnDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t
cudnnDestroyConvolutionDescriptor(cudnnConvolutionDescriptor_t convDesc)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyCTCLossDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyDropoutDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyFilterDescriptor(cudnnFilterDescriptor_t filterDesc)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyFusedOpsConstParamPack()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyFusedOpsPlan()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyFusedOpsVariantParamPack()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyLRNDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyOpTensorDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyPersistentRNNPlan()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyPoolingDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyReduceTensorDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyRNNDataDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyRNNDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroySeqDataDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroySpatialTransformerDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyTensorDescriptor(cudnnTensorDescriptor_t tensorDesc)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDestroyTensorTransformDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDivisiveNormalizationBackward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDivisiveNormalizationForward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDropoutBackward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDropoutForward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDropoutGetReserveSpaceSize()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnDropoutGetStatesSize()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnFindConvolutionBackwardDataAlgorithm()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnFindConvolutionBackwardDataAlgorithmEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnFindConvolutionBackwardFilterAlgorithm()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnFindConvolutionBackwardFilterAlgorithmEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnFindConvolutionForwardAlgorithm()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnFindConvolutionForwardAlgorithmEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnFindRNNBackwardDataAlgorithmEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnFindRNNBackwardWeightsAlgorithmEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnFindRNNForwardInferenceAlgorithmEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnFindRNNForwardTrainingAlgorithmEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnFusedOpsExecute()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetActivationDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetAlgorithmDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetAlgorithmPerformance()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetAlgorithmSpaceSize()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetAttnDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetBatchNormalizationBackwardExWorkspaceSize(cudnnHandle_t handle,
                                                                cudnnBatchNormMode_t mode,
                                                                cudnnBatchNormOps_t bnOps,
                                                                const cudnnTensorDescriptor_t xDesc,
                                                                const cudnnTensorDescriptor_t yDesc,
                                                                const cudnnTensorDescriptor_t dyDesc,
                                                                const cudnnTensorDescriptor_t dzDesc,
                                                                const cudnnTensorDescriptor_t dxDesc,
                                                                const cudnnTensorDescriptor_t dBnScaleBiasDesc,
                                                                const cudnnActivationDescriptor_t activationDesc,
                                                                size_t *sizeInBytes)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetBatchNormalizationForwardTrainingExWorkspaceSize(cudnnHandle_t handle,
                                                                       cudnnBatchNormMode_t mode,
                                                                       cudnnBatchNormOps_t bnOps,
                                                                       const cudnnTensorDescriptor_t xDesc,
                                                                       const cudnnTensorDescriptor_t zDesc,
                                                                       const cudnnTensorDescriptor_t yDesc,
                                                                       const cudnnTensorDescriptor_t bnScaleBiasMeanVarDesc,
                                                                       const cudnnActivationDescriptor_t activationDesc,
                                                                       size_t *sizeInBytes)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetBatchNormalizationTrainingExReserveSpaceSize(cudnnHandle_t handle,
                                                                   cudnnBatchNormMode_t mode,
                                                                   cudnnBatchNormOps_t bnOps,
                                                                   const cudnnActivationDescriptor_t activationDeactivationDesc,
                                                                   const cudnnTensorDescriptor_t xDesc,
                                                                   size_t *sizeInBytes)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetCallback()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolution2dDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolution2dForwardOutputDim()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardDataAlgorithm()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardDataAlgorithmMaxCount()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardDataAlgorithm_v7(cudnnHandle_t handle,
                                                          const cudnnFilterDescriptor_t filterDesc,
                                                          const cudnnTensorDescriptor_t diffDesc,
                                                          const cudnnConvolutionDescriptor_t convDesc,
                                                          const cudnnTensorDescriptor_t gradDesc,
                                                          const int requestedAlgoCount,
                                                          int *returnedAlgoCount,
                                                          cudnnConvolutionBwdDataAlgoPerf_t *perfResults)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardDataWorkspaceSize(cudnnHandle_t handle,
                                                           const cudnnFilterDescriptor_t wDesc,
                                                           const cudnnTensorDescriptor_t dyDesc,
                                                           const cudnnConvolutionDescriptor_t convDesc,
                                                           const cudnnTensorDescriptor_t dxDesc,
                                                           cudnnConvolutionBwdDataAlgo_t algo,
                                                           size_t *sizeInBytes)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardFilterAlgorithm()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardFilterAlgorithmMaxCount()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardFilterAlgorithm_v7(cudnnHandle_t handle,
                                                            const cudnnTensorDescriptor_t srcDesc,
                                                            const cudnnTensorDescriptor_t diffDesc,
                                                            const cudnnConvolutionDescriptor_t convDesc,
                                                            const cudnnFilterDescriptor_t gradDesc,
                                                            const int requestedAlgoCount,
                                                            int *returnedAlgoCount,
                                                            cudnnConvolutionBwdFilterAlgoPerf_t *perfResults)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardFilterWorkspaceSize()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionForwardAlgorithm()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionForwardAlgorithmMaxCount()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionForwardAlgorithm_v7(cudnnHandle_t handle,
                                                     const cudnnTensorDescriptor_t srcDesc,
                                                     const cudnnFilterDescriptor_t filterDesc,
                                                     const cudnnConvolutionDescriptor_t convDesc,
                                                     const cudnnTensorDescriptor_t destDesc,
                                                     const int requestedAlgoCount,
                                                     int *returnedAlgoCount,
                                                     cudnnConvolutionFwdAlgoPerf_t *perfResults)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionForwardWorkspaceSize()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionGroupCount()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionMathType()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionNdDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionNdForwardOutputDim()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionReorderType()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetCTCLossDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetCTCLossDescriptorEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetCTCLossWorkspaceSize()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetCudartVersion()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetDropoutDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

const char *cudnnGetErrorString(cudnnStatus_t status)
{

    char *rs = malloc(sizeof(char));
    return rs;
}

cudnnStatus_t cudnnGetFilter4dDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetFilterNdDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetFilterSizeInBytes()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetFoldedConvBackwardDataDescriptors()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetFusedOpsConstParamPackAttribute()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetFusedOpsVariantParamPackAttribute()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetLRNDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetMultiHeadAttnBuffers()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetMultiHeadAttnWeights()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetOpTensorDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetPooling2dDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetPooling2dForwardOutputDim()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetPoolingNdDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetPoolingNdForwardOutputDim()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetProperty()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetReduceTensorDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetReductionIndicesSize()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetReductionWorkspaceSize()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNBackwardDataAlgorithmMaxCount()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNBackwardWeightsAlgorithmMaxCount()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNBiasMode()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNDataDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNForwardInferenceAlgorithmMaxCount()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNForwardTrainingAlgorithmMaxCount()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNLinLayerBiasParams()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNLinLayerMatrixParams()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNMatrixMathType()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNPaddingMode()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNParamsSize()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNProjectionLayers()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNTrainingReserveSize()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetRNNWorkspaceSize()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetSeqDataDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetStream()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetTensor4dDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetTensorNdDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetTensorSizeInBytes()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetTensorTransformDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnGetVersion()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnIm2Col()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnInitTransformDest()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnLRNCrossChannelBackward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnLRNCrossChannelForward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnMakeFusedOpsPlan()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnMultiHeadAttnBackwardData()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnMultiHeadAttnBackwardWeights()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnMultiHeadAttnForward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnOpTensor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnPoolingBackward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnPoolingForward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnQueryRuntimeError()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnReduceTensor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnReorderFilterAndBias()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnRestoreAlgorithm()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnRestoreDropoutDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnRNNBackwardData()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnRNNBackwardDataEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnRNNBackwardWeights()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnRNNBackwardWeightsEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnRNNForwardInference()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnRNNForwardInferenceEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnRNNForwardTraining()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnRNNForwardTrainingEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnRNNGetClip()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnRNNSetClip()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSaveAlgorithm()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnScaleTensor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetActivationDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetAlgorithmDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetAlgorithmPerformance()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetAttnDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetCallback()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetConvolution2dDescriptor(cudnnConvolutionDescriptor_t convDesc,
                                              int pad_h, int pad_w,
                                              int u, int v,
                                              int dilation_h, int dilation_w,
                                              cudnnConvolutionMode_t mode,
                                              cudnnDataType_t computeType)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetConvolutionGroupCount(cudnnConvolutionDescriptor_t convDesc,
                                            int groupCount)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetConvolutionMathType(cudnnConvolutionDescriptor_t convDesc,
                                          cudnnMathType_t mathType)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetConvolutionNdDescriptor(cudnnConvolutionDescriptor_t convDesc,
                                              int arrayLength,
                                              const int padA[],
                                              const int filterStrideA[],
                                              const int dilationA[],
                                              cudnnConvolutionMode_t mode,
                                              cudnnDataType_t computeType)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetConvolutionReorderType()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetCTCLossDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetCTCLossDescriptorEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetDropoutDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetFilter4dDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetFilterNdDescriptor(cudnnFilterDescriptor_t filterDesc,
                                         cudnnDataType_t dataType,
                                         cudnnTensorFormat_t format,
                                         int nbDims,
                                         const int filterDimA[])
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetFusedOpsConstParamPackAttribute()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetFusedOpsVariantParamPackAttribute()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetLRNDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetOpTensorDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetPersistentRNNPlan()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetPooling2dDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetPoolingNdDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetReduceTensorDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetRNNAlgorithmDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetRNNBiasMode()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetRNNDataDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetRNNDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetRNNDescriptor_v5()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetRNNDescriptor_v6()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetRNNMatrixMathType()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetRNNPaddingMode()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetRNNProjectionLayers()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetSeqDataDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetSpatialTransformerNdDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetStream(cudnnHandle_t handle, cudaStream_t streamId)
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetTensor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetTensor4dDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetTensor4dDescriptorEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetTensorNdDescriptor(cudnnTensorDescriptor_t tensorDesc,
                                         cudnnDataType_t dataType,
                                         int nbDims,
                                         const int dimA[],
                                         const int strideA[])
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetTensorNdDescriptorEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSetTensorTransformDescriptor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSoftmaxBackward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSoftmaxForward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSpatialTfGridGeneratorBackward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSpatialTfGridGeneratorForward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSpatialTfSamplerBackward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnSpatialTfSamplerForward()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnTransformFilter()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnTransformTensor()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}

cudnnStatus_t cudnnTransformTensorEx()
{
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    return rcs;
}
