#include <stdlib.h>
#include <cmath>

#include <THC/THCTensor.h>

#include <ATen/native/TensorIterator.h>
#include <c10/core/Scalar.h>

#include <THCUNN/THCUNN.h>

#include <ATen/native/Pool.h>

#include <ATen/cuda/detail/OffsetCalculator.cuh>

extern "C" {
#include "utils.h"
#include "passthrough.h"
#include "async.h"
}

#if defined USE_MocCUDA && USE_MocCUDA >= 1
namespace at { namespace native {

template<int N>
static OffsetCalculator<N> make_offset_calculator(const TensorIterator& iter)
{
    AT_ASSERT(N == iter.ntensors());
    std::array<const int64_t*, N> strides;
    for (int i = 0; i < N; i++) {
        strides[i] = iter.strides(i).data();
    }
    return OffsetCalculator<N>(iter.ndim(), iter.shape().data(),
                               strides.data());
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *result, *a, *b;
    float alpha;
    int nbElem;
} add_kernel_cuda_async_f_t;

__attribute__((used,noinline))
static void add_kernel_cuda_async_f_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, add_kernel_cuda_async_f_fn);

    add_kernel_cuda_async_f_t c = *((add_kernel_cuda_async_f_t *)ctx);

    float *result = c.result;
    const float *a = c.a, *b = c.b;
    const float alpha = c.alpha;
    const int nbElem = c.nbElem;
    int i;
    if (b) {
        #pragma omp parallel for firstprivate(alpha), schedule(static)
        for (i = 0; i < nbElem; i++)
            result[i] = a[i] + alpha * b[i];
    } else {
        #pragma omp parallel for firstprivate(alpha), schedule(static)
        for (i = 0; i < nbElem; i++)
            result[i] = a[i] + alpha;
    }

    free(ctx);

    LEAVE(hijack_torch_cuda, add_kernel_cuda_async_f_fn);
}

typedef struct {
    long int *result, *a, *b;
    long int alpha;
    int nbElem;
} add_kernel_cuda_async_li_t;

__attribute__((used,noinline))
static void add_kernel_cuda_async_li_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, add_kernel_cuda_async_li_fn);

    add_kernel_cuda_async_li_t c = *((add_kernel_cuda_async_li_t *)ctx);

    long int *result = c.result;
    const long int *a = c.a, *b = c.b;
    const int64_t alpha = c.alpha;
    const int nbElem = c.nbElem;
    int i;
    if (b) {
        #pragma omp parallel for firstprivate(alpha), schedule(static)
        for (i = 0; i < nbElem; i++)
            result[i] = a[i] + alpha * b[i];
    } else {
        #pragma omp parallel for firstprivate(alpha), schedule(static)
        for (i = 0; i < nbElem; i++)
            result[i] = a[i] + alpha;
    }

    free(ctx);

    LEAVE(hijack_torch_cuda, add_kernel_cuda_async_li_fn);
}
#endif //USE_GCD

#if !defined USE_POLTERGEIST || USE_POLTERGEIST == 0
void add_kernel_cuda(TensorIterator& iter, Scalar alpha_scalar)
{
    ENTER(hijack_torch_cuda, add_kernel_cuda);

    if (!iter.is_contiguous()) {
        fprintf(stderr, "non-contiguous in add_kernel_cuda\n");
        LEAVE(hijack_torch_cuda, add_kernel_cuda);
        return;
    }

    assert(iter.dtype(0) == iter.dtype(1) && iter.dtype(0) == iter.dtype(2) &&
           iter.is_contiguous() && !iter.is_cpu_scalar(1));

    float alpha = 0;
    if (alpha_scalar.isIntegral(0/*isBool*/))
        alpha = (float)alpha_scalar.to<int64_t>();
    else if (alpha_scalar.isFloatingPoint())
        alpha = (float)alpha_scalar.to<double>();
    else
        assert(0);
    if (iter.is_cpu_scalar(2)) {
        if (kFloat == iter.dtype(2))
            alpha *= (float)iter.scalar_value<float>(2);
        else if (kLong == iter.dtype(2))
            alpha *= (float)iter.scalar_value<long int>(2);
        else
            assert(0);
    }

    const int nbElem = (int)iter.numel();
    if (kFloat == iter.dtype(0)) {
        float *result = (float*)iter.data_ptr(0);
        const float *a = (float*)iter.data_ptr(1);
        const float *b = iter.is_cpu_scalar(2) ? NULL :
                         (float*)iter.data_ptr(2);
        #if defined USE_GCD && USE_GCD >= 1
        add_kernel_cuda_async_f_t *ctx;
        assert((ctx = (add_kernel_cuda_async_f_t *)malloc(sizeof(*ctx))));
        *ctx = (add_kernel_cuda_async_f_t){.result = result,
                                           .a = (float*)a,
                                           .b = (float*)b,
                                           .alpha = alpha,
                                           .nbElem = nbElem};
        fake_cuda_dispatch(ctx, add_kernel_cuda_async_f_fn,
                           at::cuda::getCurrentCUDAStream());
        #else //USE_GCD
        int i;
        if (b) {
            #pragma omp parallel for firstprivate(alpha), schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i] = a[i] + alpha * b[i];
        } else {
            #pragma omp parallel for firstprivate(alpha), schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i] = a[i] + alpha;
        }
        #endif //USE_GCD
    } else if (kLong == iter.dtype(0)) {
        long int *result = (long int*)iter.data_ptr(0);
        const long int *a = (long int*)iter.data_ptr(1);
        const long int *b = iter.is_cpu_scalar(2) ? NULL :
                            (long int*)iter.data_ptr(2);
        #if defined USE_GCD && USE_GCD >= 1
        add_kernel_cuda_async_li_t *ctx;
        assert((ctx = (add_kernel_cuda_async_li_t *)malloc(sizeof(*ctx))));
        *ctx = (add_kernel_cuda_async_li_t){.result = result,
                                            .a = (long int*)a,
                                            .b = (long int*)b,
                                            .alpha = (long int)alpha,
                                            .nbElem = nbElem};
        fake_cuda_dispatch(ctx, add_kernel_cuda_async_li_fn,
                           at::cuda::getCurrentCUDAStream());
        #else //USE_GCD
        int i;
        if (b) {
            #pragma omp parallel for firstprivate(alpha), schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i] = a[i] + (long int)alpha * b[i];
        } else {
            #pragma omp parallel for firstprivate(alpha), schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i] = a[i] + (long int)alpha;
        }
        #endif //USE_GCD
    } else
        assert(0);

    LEAVE(hijack_torch_cuda, add_kernel_cuda);
}
#endif //USE_POLTERGEIST

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *result, *a, *b;
    float inv_b;
    int nbElem;
    bool contiguous, trivial_1d;
    int s0, s1, s2;
    struct OffsetCalculator<3> offset_calc;
} div_kernel_cuda_async_f_t;

__attribute__((used,noinline))
static void div_kernel_cuda_async_f_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, div_kernel_cuda_async_f_fn);

    div_kernel_cuda_async_f_t c = *((div_kernel_cuda_async_f_t *)ctx);

    float *result = c.result;
    const float *a = c.a, *b = c.b;
    const float inv_b = c.inv_b;
    const int nbElem = c.nbElem;
    int i;
    if (c.contiguous) {
        if (b) {
            #pragma omp parallel for schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i] = a[i] / b[i];
        } else {
            #pragma omp parallel for firstprivate(inv_b), schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i] = a[i] * inv_b;
        }
    } else if (c.trivial_1d) {
        const int s0 = c.s0, s1 = c.s1, s2 = c.s2;
        if (b){
            const size_t sof = sizeof(float);
            #pragma omp parallel for firstprivate(s0,s1,s2,sof), \
            schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i * s0 / sof] = a[i * s1 / sof] / b[i * s2 / sof];
        } else {
            const size_t sof = sizeof(float);
            #pragma omp parallel for firstprivate(s0,s1,sof), schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i * s0 / sof] = a[i * s1 / sof] * inv_b;
        }
    } else {
        const struct OffsetCalculator<3> offset_calc = c.offset_calc;
        const size_t sof = sizeof(float);
        if (b) {
            #pragma omp parallel for firstprivate(sof), schedule(static)
            for (i = 0; i < nbElem; i++) {
                auto offsets = offset_calc.get(i);
                result[offsets.data[0] / sof] = a[offsets.data[1] / sof]
                                                / b[offsets.data[2] / sof];
            }
        } else {
            #pragma omp parallel for firstprivate(inv_b,sof), schedule(static)
            for (i = 0; i < nbElem; i++) {
                auto offsets = offset_calc.get(i);
                result[offsets.data[0] / sof] = inv_b
                                                * a[offsets.data[1] / sof];
            }
        }
    }

    free(ctx);

    LEAVE(hijack_torch_cuda, div_kernel_cuda_async_f_fn);
}
#endif //USE_GCD

void div_kernel_cuda(TensorIterator& iter)
{
    ENTER(hijack_torch_cuda, div_kernel_cuda);

    const int nbElem = (int)iter.numel();
    if (!nbElem) {
        LEAVE(hijack_torch_cuda, div_kernel_cuda);
        return;
    }
    if (0 /*!iter.is_contiguous() && iter.is_trivial_1d()*/) {
        fprintf(stderr, "non-contiguous / not-supported in div_kernel_cuda\n");
        LEAVE(hijack_torch_cuda, div_kernel_cuda);
        return;
    }

    assert(kFloat == iter.dtype(0) && kFloat == iter.dtype(1));

    float *result = (float*)iter.data_ptr(0);
    float *a = (float*)iter.data_ptr(1);
    #if defined USE_GCD && USE_GCD >= 1
    div_kernel_cuda_async_f_t *ctx;
    assert((ctx = (div_kernel_cuda_async_f_t *)malloc(sizeof(*ctx))));
    #else //USE_GCD
    int i;
    #endif //USE_GCD
    if (iter.is_contiguous()) {
        if (!isIntegralType(iter.common_dtype(), false/*includeBool*/)
            && iter.is_cpu_scalar(2)) {
            float div;
            if (kFloat == iter.dtype(2))
                div = (float)iter.scalar_value<float>(2);
            else if (kDouble == iter.dtype(2))
                div = (float)iter.scalar_value<double>(2);
            else if (kLong == iter.dtype(2))
                div = (float)iter.scalar_value<long int>(2);
            else
                assert(0);
            const float inv_b = (float)(1.0 / div);
            #if defined USE_GCD && USE_GCD >= 1
            ctx->result = result;
            ctx->a = a;
            ctx->b = NULL;
            ctx->inv_b = inv_b;
            ctx->nbElem = nbElem;
            ctx->contiguous = true;
            fake_cuda_dispatch(ctx, div_kernel_cuda_async_f_fn,
                               at::cuda::getCurrentCUDAStream());
            #else //USE_GCD
            #pragma omp parallel for firstprivate(inv_b), schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i] = a[i] * inv_b;
            #endif //USE_GCD
        } else {
            assert(kFloat == iter.dtype(iter.ntensors() - 1));
            float *b = (float*)iter.data_ptr(2);
            #if defined USE_GCD && USE_GCD >= 1
            ctx->result = result;
            ctx->a = a;
            ctx->b = b;
            ctx->inv_b = 0;
            ctx->nbElem = nbElem;
            ctx->contiguous = true;
            fake_cuda_dispatch(ctx, div_kernel_cuda_async_f_fn,
                               at::cuda::getCurrentCUDAStream());
            #else //USE_GCD
            #pragma omp parallel for schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i] = a[i] / b[i];
            #endif //USE_GCD
        }
    } else if (iter.is_trivial_1d()) {
        assert(3 == iter.ntensors() && iter.common_dtype() != kBool /*later*/);
        assert(iter.needs_dynamic_casting());
        auto inner_strides = iter.get_inner_strides();
        at::detail::Array<int, 3> strides;
        for (int i = 0; i < 3; i++)
            strides[i] = inner_strides[i];
        if (!isIntegralType(iter.common_dtype(), false/*includeBool*/)
            && iter.is_cpu_scalar(2)) {
            float div;
            if (kFloat == iter.dtype(2))
                div = (float)iter.scalar_value<float>(2);
            else if (kDouble == iter.dtype(2))
                div = (float)iter.scalar_value<double>(2);
            else if (kLong == iter.dtype(2))
                div = (float)iter.scalar_value<long int>(2);
            else
                assert(0);
            const float inv_b = (float)(1.0 / div);
            #if defined USE_GCD && USE_GCD >= 1
            ctx->result = result;
            ctx->a = a;
            ctx->b = NULL;
            ctx->inv_b = inv_b;
            ctx->nbElem = nbElem;
            ctx->contiguous = false;
            ctx->trivial_1d = true;
            ctx->s0 = strides[0];
            ctx->s1 = strides[1];
            ctx->s2 = 0;
            fake_cuda_dispatch(ctx, div_kernel_cuda_async_f_fn,
                               at::cuda::getCurrentCUDAStream());
            #else //USE_GCD
            const size_t sof = sizeof(float);
            #pragma omp parallel for schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i * strides[0] / sof] = a[i * strides[1] / sof] * inv_b;
            #endif //USE_GCD
        } else {
            assert(0); //later
        }
    } else {
        if (!isIntegralType(iter.common_dtype(), false/*includeBool*/)
            && iter.is_cpu_scalar(2)) {
            float div;
            if (kFloat == iter.dtype(2))
                div = (float)iter.scalar_value<float>(2);
            else if (kDouble == iter.dtype(2))
                div = (float)iter.scalar_value<double>(2);
            else if (kLong == iter.dtype(2))
                div = (float)iter.scalar_value<long int>(2);
            else
                assert(0);
            const float inv_b = (float)(1.0 / div);
            //const auto offset_calc = make_offset_calculator<3>(iter);
            const struct OffsetCalculator<3> offset_calc =
                make_offset_calculator<3>(iter);
            #if defined USE_GCD && USE_GCD >= 1
            ctx->result = result;
            ctx->a = a;
            ctx->b = NULL;
            ctx->inv_b = inv_b;
            ctx->nbElem = nbElem;
            ctx->contiguous = false;
            ctx->trivial_1d = false;
            ctx->offset_calc = offset_calc;
            fake_cuda_dispatch(ctx, div_kernel_cuda_async_f_fn,
                               at::cuda::getCurrentCUDAStream());
            #else //USE_GCD
            const size_t sof = sizeof(float);
            //offsets shouldnt overlap -> omp fine
            #pragma omp parallel for firstprivate(inv_b,sof), schedule(static)
            for (i = 0; i < nbElem; i++) {
                auto offsets = offset_calc.get(i);
                result[offsets.data[0] / sof] = inv_b
                                                * a[offsets.data[1] / sof];
            }
            #endif //USE_GCD
        } else {
            assert(kFloat == iter.dtype(iter.ntensors() - 1));
            float *b = (float*)iter.data_ptr(2);
            //auto offset_calc = make_offset_calculator<3>(iter);
            const struct OffsetCalculator<3> offset_calc =
                make_offset_calculator<3>(iter);
            #if defined USE_GCD && USE_GCD >= 1
            ctx->result = result;
            ctx->a = a;
            ctx->b = b;
            ctx->inv_b = 0;
            ctx->nbElem = nbElem;
            ctx->contiguous = false;
            ctx->trivial_1d = false;
            ctx->offset_calc = offset_calc;
            fake_cuda_dispatch(ctx, div_kernel_cuda_async_f_fn,
                               at::cuda::getCurrentCUDAStream());
            #else //USE_GCD
            const size_t sof = sizeof(float);
            int i;
            #pragma omp parallel for firstprivate(sof), schedule(static)
            for (i = 0; i < nbElem; i++) {
                auto offsets = offset_calc.get(i);
                result[offsets.data[0] / sof] = a[offsets.data[1] / sof]
                                                / b[offsets.data[2] / sof];
            }
            #endif //USE_GCD
        }
    }

    LEAVE(hijack_torch_cuda, div_kernel_cuda);
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *result, *a, *b;
    float alpha;
    int nbElem;
    bool contiguous;
    int s0, s1, s2;
} mul_kernel_cuda_async_f_t;

__attribute__((used,noinline))
static void mul_kernel_cuda_async_f_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, mul_kernel_cuda_async_f_fn);

    mul_kernel_cuda_async_f_t c = *((mul_kernel_cuda_async_f_t *)ctx);

    float *result = c.result;
    const float *a = c.a, *b = c.b;
    const float alpha = c.alpha;
    const int nbElem = c.nbElem;
    int i;
    if (c.contiguous) {
        if (a) {
            #pragma omp parallel for schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i] = a[i] * b[i];
        } else {
            #pragma omp parallel for firstprivate(alpha), schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i] = alpha * b[i];
        }
    } else {
        const size_t sof = sizeof(float);
        const int s0 = c.s0, s1 = c.s1/*, s2 = c.s2*/;
        if (a) {
            assert(0);
        } else {
            #pragma omp parallel for firstprivate(alpha,sof,s0,s1), \
            schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i * s0 / sof] = alpha * b[i * s1 / sof];
        }
    }

    free(ctx);

    LEAVE(hijack_torch_cuda, mul_kernel_cuda_async_f_fn);
}

typedef struct {
    bool *result, *a, *b;
    bool alpha;
    int nbElem;
} mul_kernel_cuda_async_b_t;

__attribute__((used,noinline))
static void mul_kernel_cuda_async_b_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, mul_kernel_cuda_async_b_fn);

    mul_kernel_cuda_async_b_t c = *((mul_kernel_cuda_async_b_t *)ctx);

    bool *result = c.result;
    const bool *a = c.a, *b = c.b;
    const bool alpha = c.alpha;
    const int nbElem = c.nbElem;
    int i;
    if (a) {
        #pragma omp parallel for schedule(static)
        for (i = 0; i < nbElem; i++)
            result[i] = a[i] && b[i];
    } else {
        #pragma omp parallel for firstprivate(alpha), schedule(static)
        for (i = 0; i < nbElem; i++)
            result[i] = alpha && b[i];
    }

    free(ctx);

    LEAVE(hijack_torch_cuda, mul_kernel_cuda_async_b_fn);
}
#endif //USE_GCD

void mul_kernel_cuda(TensorIterator& iter)
{
    ENTER(hijack_torch_cuda, mul_kernel_cuda);

    const int nbElem = (int)iter.numel();
    if (!nbElem) {
        LEAVE(hijack_torch_cuda, mul_kernel_cuda);
        return;
    }
    if (!iter.is_contiguous() && !iter.is_trivial_1d()) {
        fprintf(stderr, "non-contiguous / not-supported in mul_kernel_cuda\n");
        LEAVE(hijack_torch_cuda, mul_kernel_cuda);
        return;
    }

    assert(iter.common_dtype() != kBool); // skip bool suppoer for now
    assert(kFloat == iter.dtype(0) && kFloat == iter.dtype(1));
    assert(iter.can_use_32bit_indexing());

    if (iter.is_contiguous()) {
        if (!iter.is_cpu_scalar(1) && !iter.is_cpu_scalar(2)) {
            if (iter.common_dtype() == kBool) {
                bool *result = (bool*)iter.data_ptr(0);
                bool *a = (bool*)iter.data_ptr(1);
                bool *b = (bool*)iter.data_ptr(2);
                #if defined USE_GCD && USE_GCD >= 1
                mul_kernel_cuda_async_b_t *ctx;
                assert((ctx =
                            (mul_kernel_cuda_async_b_t *)malloc(sizeof(*ctx))));
                *ctx = (mul_kernel_cuda_async_b_t){.result = result,
                                                   .a = (bool*)a,
                                                   .b = (bool*)b,
                                                   .alpha = 0,
                                                   .nbElem = nbElem};
                fake_cuda_dispatch(ctx, mul_kernel_cuda_async_b_fn,
                                   at::cuda::getCurrentCUDAStream());
                #else //USE_GCD
                int i;
                #pragma omp parallel for schedule(static)
                for (i = 0; i < nbElem; i++)
                    result[i] = a[i] && b[i];
                #endif //USE_GCD
            } else {
                float *result = (float*)iter.data_ptr(0);
                float *a = (float*)iter.data_ptr(1);
                float *b = (float*)iter.data_ptr(2);
                #if defined USE_GCD && USE_GCD >= 1
                mul_kernel_cuda_async_f_t *ctx;
                assert((ctx =
                            (mul_kernel_cuda_async_f_t *)malloc(sizeof(*ctx))));
                *ctx = (mul_kernel_cuda_async_f_t){.result = result,
                                                   .a = (float*)a,
                                                   .b = (float*)b,
                                                   .alpha = 0,
                                                   .nbElem = nbElem,
                                                   .contiguous = true,
                                                   .s0 = 0, .s1 = 0, .s2 = 0};
                fake_cuda_dispatch(ctx, mul_kernel_cuda_async_f_fn,
                                   at::cuda::getCurrentCUDAStream());
                #else //USE_GCD
                int i;
                #pragma omp parallel for schedule(static)
                for (i = 0; i < nbElem; i++)
                    result[i] = a[i] * b[i];
                #endif //USE_GCD
            }
        } else {
            if (iter.common_dtype() == kBool) {
                bool *result = (bool*)iter.data_ptr(0), a, *b;
                if (iter.is_cpu_scalar(1)) {
                    a = iter.scalar_value<bool>(1);
                    b = (bool*)iter.data_ptr(2);
                } else {
                    a = iter.scalar_value<bool>(2);
                    b = (bool*)iter.data_ptr(1);
                }
                #if defined USE_GCD && USE_GCD >= 1
                mul_kernel_cuda_async_b_t *ctx;
                assert((ctx =
                            (mul_kernel_cuda_async_b_t *)malloc(sizeof(*ctx))));
                *ctx = (mul_kernel_cuda_async_b_t){.result = result,
                                                   .a = NULL,
                                                   .b = b,
                                                   .alpha = a,
                                                   .nbElem = nbElem};
                fake_cuda_dispatch(ctx, mul_kernel_cuda_async_b_fn,
                                   at::cuda::getCurrentCUDAStream());
                #else //USE_GCD
                int i;
                #pragma omp parallel for firstprivate(a), schedule(static)
                for (i = 0; i < nbElem; i++)
                    result[i] = a && b[i];
                #endif //USE_GCD
            } else {
                float *result = (float*)iter.data_ptr(0), a, *b;
                if (iter.is_cpu_scalar(1)) {
                    if (kFloat == iter.dtype(1))
                        a = (float)iter.scalar_value<float>(1);
                    else if (kDouble == iter.dtype(1))
                        a = (float)iter.scalar_value<double>(1);
                    else
                        assert(0);
                    b = (float*)iter.data_ptr(2);
                } else {
                    if (kFloat == iter.dtype(2))
                        a = (float)iter.scalar_value<float>(2);
                    else if (kDouble == iter.dtype(2))
                        a = (float)iter.scalar_value<double>(2);
                    else
                        assert(0);
                    b = (float*)iter.data_ptr(1);
                }
                #if defined USE_GCD && USE_GCD >= 1
                mul_kernel_cuda_async_f_t *ctx;
                assert((ctx =
                            (mul_kernel_cuda_async_f_t *)malloc(sizeof(*ctx))));
                *ctx = (mul_kernel_cuda_async_f_t){.result = result,
                                                   .a = NULL,
                                                   .b = b,
                                                   .alpha = a,
                                                   .nbElem = nbElem,
                                                   .contiguous = true,
                                                   .s0 = 0, .s1 = 0, .s2 = 0};
                fake_cuda_dispatch(ctx, mul_kernel_cuda_async_f_fn,
                                   at::cuda::getCurrentCUDAStream());
                #else //USE_GCD
                int i;
                #pragma omp parallel for firstprivate(a), schedule(static)
                for (i = 0; i < nbElem; i++)
                    result[i] = a * b[i];
                #endif //USE_GCD
            }
        }
    } else if (iter.is_trivial_1d()) {
        assert(3 == iter.ntensors() && iter.common_dtype() != kBool /*later*/);
        assert(iter.needs_dynamic_casting() && kDouble == iter.dtype(2));
        auto inner_strides = iter.get_inner_strides();
        at::detail::Array<int, 3> strides;
        for (int i = 0; i < 3; i++)
            strides[i] = inner_strides[i];
        if (!iter.is_cpu_scalar(1) && !iter.is_cpu_scalar(2)) {
            assert(0); // below not supporting dynamic_casting
            /*
            float *result = (float*)iter.data_ptr(0);
            float *a = (float*)iter.data_ptr(1);
            float *b = (float*)iter.data_ptr(2);
            pragma omp parallel for schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i * strides[0]] = a[i * strides[1]] * b[i * strides[2]];
            */
        } else {
            float *result = (float*)iter.data_ptr(0), a, *b;
            if (iter.is_cpu_scalar(1)) {
                if (kFloat == iter.dtype(1))
                    a = (float)iter.scalar_value<float>(1);
                else if (kDouble == iter.dtype(1))
                    a = (float)iter.scalar_value<double>(1);
                else
                    assert(0);
                b = (float*)iter.data_ptr(2);
                strides[1] = strides[2];
            } else {
                if (kFloat == iter.dtype(2))
                    a = (float)iter.scalar_value<float>(2);
                else if (kDouble == iter.dtype(2))
                    a = (float)iter.scalar_value<double>(2);
                else
                    assert(0);
                b = (float*)iter.data_ptr(1);
            }
            #if defined USE_GCD && USE_GCD >= 1
            mul_kernel_cuda_async_f_t *ctx;
            assert((ctx = (mul_kernel_cuda_async_f_t *)malloc(sizeof(*ctx))));
            *ctx = (mul_kernel_cuda_async_f_t){.result = result,
                                               .a = NULL,
                                               .b = b,
                                               .alpha = a,
                                               .nbElem = nbElem,
                                               .contiguous = false,
                                               .s0 = strides[0],
                                               .s1 = strides[1],
                                               .s2 = strides[2]};
            fake_cuda_dispatch(ctx, mul_kernel_cuda_async_f_fn,
                               at::cuda::getCurrentCUDAStream());
            #else //USE_GCD
            const size_t sof = sizeof(float);
            int i;
            #pragma omp parallel for firstprivate(a,sof), schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i * strides[0] / sof] = a * b[i * strides[1] / sof];
            #endif //USE_GCD
        }
    } else {
        assert(0); //later
    }

    LEAVE(hijack_torch_cuda, mul_kernel_cuda);
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *result, *x, *other;
    float thrshld, val;
    int nbElem;
} threshold_kernel_cuda_async_f_t;

__attribute__((used,noinline))
static void threshold_kernel_cuda_async_f_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, threshold_kernel_cuda_async_f_fn);

    threshold_kernel_cuda_async_f_t c =
        *((threshold_kernel_cuda_async_f_t *)ctx);

    float *result = c.result;
    const float *x = c.x, *other = c.other;
    const float thrshld = c.thrshld, val = c.val;
    const int nbElem = c.nbElem;
    int i;
    #pragma omp parallel for firstprivate(thrshld,val), schedule(static)
    for (i = 0; i < nbElem; i++)
        result[i] = (x[i] <= thrshld) ? val : other[i];

    free(ctx);

    LEAVE(hijack_torch_cuda, threshold_kernel_cuda_async_f_fn);
}
#endif //USE_GCD

template <typename scalar_t>
__attribute__((__used__))
void threshold_kernel_impl(TensorIterator& iter, scalar_t thrshld, scalar_t val)
{
    ENTER(hijack_torch_cuda, threshold_kernel_impl);

    if (!iter.is_contiguous()) {
        fprintf(stderr, "non-contiguous in threshold_kernel\n");
        LEAVE(hijack_torch_cuda, threshold_kernel);
        return;
    }

    assert(kFloat == iter.dtype(0) && kFloat == iter.dtype(1) &&
           kFloat == iter.dtype(2) && iter.is_contiguous() &&
           !iter.is_cpu_scalar(1) && !iter.is_cpu_scalar(2));

    /*float thrshld, val;*/
    float *result = (float*)iter.data_ptr(0);
    const float *x = (float*)iter.data_ptr(1);
    const float *other = (float*)iter.data_ptr(2);
    const int nbElem = (int)iter.numel();

    /*
    if (threshold.isFloatingPoint())
        thrshld = (float)threshold.to<double>();
    else if (threshold.isIntegral())
        thrshld = (float)threshold.to<int64_t>();
    else
        assert(0);
    if (value.isFloatingPoint())
        val = (float)value.to<double>();
    else if (value.isIntegral())
        val = (float)value.to<int64_t>();
    else
        assert(0);
    */

    #if defined USE_GCD && USE_GCD >= 1
    threshold_kernel_cuda_async_f_t *ctx;
    assert((ctx = (threshold_kernel_cuda_async_f_t *)malloc(sizeof(*ctx))));
    *ctx = (threshold_kernel_cuda_async_f_t){.result = result,
                                             .x = (float*)x,
                                             .other = (float*)other,
                                             .thrshld = thrshld, .val = val,
                                             .nbElem = nbElem};
    fake_cuda_dispatch(ctx, threshold_kernel_cuda_async_f_fn,
                       at::cuda::getCurrentCUDAStream());
    #else //USE_GCD
    int i;
    #pragma omp parallel for firstprivate(thrshld,val), schedule(static)
    for (i = 0; i < nbElem; i++)
        result[i] = (x[i] <= thrshld) ? val : other[i];
    #endif //USE_GCD

    LEAVE(hijack_torch_cuda, threshold_kernel_impl);
}

/*
static Tensor threshold_out(optional<Tensor> opt_result, const Tensor& self,
                            Scalar threshold, Scalar value, const Tensor& other)
{
    Tensor result = opt_result.value_or(Tensor());
    auto iter = TensorIterator::binary_op(result, self, other);
    threshold_kernel_impl(iter, threshold, value);
    return iter.output();
}

Tensor& threshold_(Tensor& self, Scalar threshold, Scalar value)
{
    threshold_out(make_optional(self), self, threshold, value, self);
    return self;
}
*/

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *result1, *result2, *a;
    float factor;
    int nbElem;
    bool contiguous;
    bool trivial_1d;
    int s;
    struct OffsetCalculator<2> offset_calc;
} mean_kernel_cuda_async_f_t;

#include <omp.h>
__attribute__((used,noinline))
static void mean_kernel_cuda_async_f_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, mean_kernel_cuda_async_f_fn);

    mean_kernel_cuda_async_f_t c = *((mean_kernel_cuda_async_f_t *)ctx);

    float *result1 = c.result1, *result2 = c.result2;
    const float *a = c.a;
    const float factor = c.factor;
    const int nbElem = c.nbElem;
    int i;
    float sum = 0.0;
    if (c.contiguous) {
        #pragma omp parallel for reduction(+:sum), schedule(static)
        for (i = 0; i < nbElem; i++)
            sum += a[i];
        //
        result1[0] = sum * factor;
        if (result2)
            result2[0] = result1[0];
    } else {
        const size_t sof = sizeof(float);
        if (c.trivial_1d) {
            const int s = c.s;
            #pragma omp parallel for reduction(+:sum), \
            firstprivate(s,sof), schedule(static)
            for (i = 0; i < nbElem; i++)
                sum += a[i * s / sof];
            //
            result1[0] = sum * factor;
            if (result2)
                result2[0] = result1[0];
        } else {
            volatile int guard = -1;
            #pragma omp parallel firstprivate(sof,factor)
            {
                const struct OffsetCalculator<2> offset_calc = c.offset_calc;
                int last_i = -1;
                int64_t old_off = -1, off = -1;
                float tmp_res = 0.0;
                #pragma omp for schedule(static), nowait
                for (i = 0; i < nbElem; i++) {
                    last_i = i;
                    auto offsets = offset_calc.get(i);
                    off = offsets.data[0] / sof;
                    if (off > old_off) {
                        if (old_off > -1) {
                            result1[old_off] = factor * tmp_res;
                            if (result2)
                                result2[old_off] = result1[old_off];
                        } /* delay the first write */
                        old_off = off;
                        tmp_res = 0.0;
                    }// else if (off < old_off)
                    //    assert(0);    // -> not seen in tests
                    tmp_res += a[offsets.data[1] / sof];
                    /* and skip the last write, which will be done hereafter */
                }
                /*------------implicit barrier--------------------------------*/
                #pragma omp single
                {
                    guard = omp_get_num_threads() - 1;
                }
                /* check if there was an overlap in an ordered manner */
                //while (guard != omp_get_thread_num()); //wait for "my" turn
                while (1) {
                    if (guard == omp_get_thread_num())
                        break;
                    __sync_synchronize();
                }
                if (last_i > -1) {
                    if (nbElem -1 == last_i /* last thread just writes*/) {
                        result1[off] = factor * tmp_res;
                        if (result2)
                            result2[off] = result1[off];
                    } else {
                        /* check overlap with successor */
                        auto offsets = offset_calc.get(last_i + 1);
                        if (offsets.data[0] / sof == off) {
                            result1[off] += factor * tmp_res;
                            if (result2)
                                result2[off] = result1[off];
                        } else /* if no overlap then set directly */ {
                            result1[off] = factor * tmp_res;
                            if (result2)
                                result2[off] = result1[off];
                        }
                    }
                }
                #pragma omp atomic
                guard--;
            }
        }
    }

    free(ctx);

    LEAVE(hijack_torch_cuda, mean_kernel_cuda_async_f_fn);
}
#endif //USE_GCD

//NOTE: need __noinline__ in ATen/native/cuda/ReduceOpsKernel.cu
template <typename scalar_t, typename acc_t=scalar_t, typename out_t=scalar_t>
__attribute__((__used__))
void mean_kernel_impl(TensorIterator& iter)
{
    ENTER(hijack_torch_cuda, mean_kernel_impl);

    const int nbElem = (int)iter.numel();
    if (!nbElem) {
        LEAVE(hijack_torch_cuda, mean_kernel_impl);
        return;
    }
    if (!iter.is_contiguous() && iter.needs_dynamic_casting()) {
        fprintf(stderr, "non-contiguous / not-supported in mean_kernel_impl\n");
        LEAVE(hijack_torch_cuda, mean_kernel_impl);
        return;
    }

    assert(kFloat == iter.dtype(0) && kFloat == iter.dtype(1) &&
           kFloat == iter.dtype(iter.ntensors() - 1) &&
           (iter.is_contiguous() || !iter.needs_dynamic_casting()) &&
           iter.can_use_32bit_indexing() && 2 == iter.ntensors());

    float *result1 = (float*)iter.data_ptr(0);
    float *result2 = NULL;
    if (iter.noutputs() > 1)
        result2 = (float*)iter.data_ptr(1);
    const float *a = (float*)iter.data_ptr(iter.ntensors() - 1);
    const float factor = float(iter.num_output_elements()) / iter.numel();
    #if defined USE_GCD && USE_GCD >= 1
    mean_kernel_cuda_async_f_t *ctx;
    assert((ctx = (mean_kernel_cuda_async_f_t *)malloc(sizeof(*ctx))));
    #else
    int i;
    const size_t sof = sizeof(float);
    float sum = 0.0;
    #endif //USE_GCD
    if (iter.is_contiguous() && 1 == iter.num_output_elements()) {
        #if defined USE_GCD && USE_GCD >= 1
        ctx->result1 = result1;
        ctx->result2 = result2;
        ctx->a = (float*)a;
        ctx->factor = factor;
        ctx->nbElem = nbElem;
        ctx->contiguous = true;
        fake_cuda_dispatch(ctx, mean_kernel_cuda_async_f_fn,
                           at::cuda::getCurrentCUDAStream());
        #else //USE_GCD
        #pragma omp parallel for reduction(+:sum), schedule(static)
        for (i = 0; i < nbElem; i++)
            sum += a[i];
        //
        result1[0] = sum * factor;
        if (result2)
            result2[0] = result1[0];
        #endif //USE_GCD
    } else if (iter.is_trivial_1d() && !iter.needs_dynamic_casting()
               && 1 == iter.num_output_elements()) {
        auto inner_strides = iter.get_inner_strides();
        at::detail::Array<int, 2/*ntensors*/> strides;
        for (int i = 0; i < 2/*ntensors*/; i++)
            strides[i] = inner_strides[i];
        assert(0 == strides.data[0]); //make sure we only acc into 1 target
        #if defined USE_GCD && USE_GCD >= 1
        ctx->result1 = result1;
        ctx->result2 = result2;
        ctx->a = (float*)a;
        ctx->factor = factor;
        ctx->nbElem = nbElem;
        ctx->contiguous = false;
        ctx->trivial_1d = true;
        ctx->s = strides.data[1];
        fake_cuda_dispatch(ctx, mean_kernel_cuda_async_f_fn,
                           at::cuda::getCurrentCUDAStream());
        #else //USE_GCD
        const int s = strides.data[1];
        #pragma omp parallel for reduction(+:sum), \
        firstprivate(s), schedule(static)
        for (i = 0; i < nbElem; i++)
            sum += a[i * s / sof];
        //
        result1[0] = sum * factor;
        if (result2)
            result2[0] = result1[0];
        #endif //USE_GCD
    } else if (!iter.is_trivial_1d() && !iter.needs_dynamic_casting()) {
        auto offset_calc = make_offset_calculator<2>(iter);
        #if defined USE_GCD && USE_GCD >= 1
        ctx->result1 = result1;
        ctx->result2 = result2;
        ctx->a = (float*)a;
        ctx->factor = factor;
        ctx->nbElem = nbElem;
        ctx->contiguous = false;
        ctx->trivial_1d = false;
        ctx->offset_calc = offset_calc;
        fake_cuda_dispatch(ctx, mean_kernel_cuda_async_f_fn,
                           at::cuda::getCurrentCUDAStream());
        #else //USE_GCD
        int64_t old_off = -1;
        //no!!: #pragma omp parallel for firstprivate(old_off,sof), schedule(static)
        for (i = 0; i < nbElem; i++) {
            auto offsets = offset_calc.get(i);
            const int64_t off = offsets.data[0] / sof;
            if (off > old_off) {
                result1[off] = 0.0;
                old_off = off;
            } else if (off < old_off)
                assert(0);
            result1[off] += factor * a[offsets.data[1] / sof];
            if (result2)
                result2[off] = result1[off];
        }
        #endif //USE_GCD
    } else
        assert(0);

    LEAVE(hijack_torch_cuda, mean_kernel_impl);
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *result1, *result2, *a;
    int nbElem, nbOutp;
    bool contiguous;
    bool trivial_1d;
    int s;
    struct OffsetCalculator<2> offset_calc;
} sum_kernel_cuda_async_f_t;

__attribute__((used,noinline))
static void sum_kernel_cuda_async_f_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, sum_kernel_cuda_async_f_fn);

    sum_kernel_cuda_async_f_t c = *((sum_kernel_cuda_async_f_t *)ctx);

    float *result1 = c.result1, *result2 = c.result2;
    const float *a = c.a;
    const int nbElem = c.nbElem, nbOutp = c.nbOutp;
    int i;
    float sum = 0.0;
    if (c.contiguous) {
        if (1 == nbOutp) {
            #pragma omp parallel for reduction(+:sum), schedule(static)
            for (i = 0; i < nbElem; i++)
                sum += a[i];
            //
            result1[0] = sum;
            if (result2)
                result2[0] = result1[0];
        } else {
            int j;
            int64_t inputs_per_output = nbElem / nbOutp;
            #pragma omp parallel for private(sum), schedule(static)
            for (j = 0; j < nbOutp; j++) {
                sum = 0.0;
                for (i = j * inputs_per_output;
                     i < min((j + 1) * inputs_per_output, nbElem); i++)
                    sum += a[i];
                result1[j] = sum;
                if (result2)
                    result2[j] = result1[j];
            }
        }
    } else {
        const size_t sof = sizeof(float);
        if (c.trivial_1d) {
            const int s = c.s;
            #pragma omp parallel for reduction(+:sum), \
            firstprivate(s,sof), schedule(static)
            for (i = 0; i < nbElem; i++)
                sum += a[i * s / sof];
            //
            result1[0] = sum;
            if (result2)
                result2[0] = result1[0];
        } else {
            volatile int guard = -1;
            #pragma omp parallel firstprivate(sof)
            {
                const struct OffsetCalculator<2> offset_calc = c.offset_calc;
                int last_i = -1;
                int64_t old_off = -1, off = -1;
                float tmp_res = 0.0;
                #pragma omp for schedule(static), nowait
                for (i = 0; i < nbElem; i++) {
                    last_i = i;
                    auto offsets = offset_calc.get(i);
                    off = offsets.data[0] / sof;
                    if (off > old_off) {
                        if (old_off > -1) {
                            result1[old_off] = tmp_res;
                            if (result2)
                                result2[old_off] = result1[old_off];
                        } /* delay the first write */
                        old_off = off;
                        tmp_res = 0.0;
                    }// else if (off < old_off)
                    //    assert(0);    // -> not seen in tests
                    tmp_res += a[offsets.data[1] / sof];
                    /* and skip the last write, which will be done hereafter */
                }
                /*------------implicit barrier--------------------------------*/
                #pragma omp single
                {
                    guard = omp_get_num_threads() - 1;
                }
                /* check if there was an overlap in an ordered manner */
                //while (guard != omp_get_thread_num()); //wait for "my" turn
                while (1) {
                    if (guard == omp_get_thread_num())
                        break;
                    __sync_synchronize();
                }
                if (last_i > -1) {
                    if (nbElem -1 == last_i /* last thread just writes*/) {
                        result1[off] = tmp_res;
                        if (result2)
                            result2[off] = result1[off];
                    } else {
                        /* check overlap with successor */
                        auto offsets = offset_calc.get(last_i + 1);
                        if (offsets.data[0] / sof == off) {
                            result1[off] += tmp_res;
                            if (result2)
                                result2[off] = result1[off];
                        } else /* if no overlap then set directly */ {
                            result1[off] = tmp_res;
                            if (result2)
                                result2[off] = result1[off];
                        }
                    }
                }
                #pragma omp atomic
                guard--;
            }
        }
    }

    free(ctx);

    LEAVE(hijack_torch_cuda, sum_kernel_cuda_async_f_fn);
}
#endif //USE_GCD

//NOTE: need __noinline__ in ATen/native/cuda/ReduceOpsKernel.cu
template <typename scalar_t, typename acc_t=scalar_t, typename out_t=scalar_t>
__attribute__((__used__))
void sum_kernel_impl(TensorIterator& iter)
{
    ENTER(hijack_torch_cuda, sum_kernel_impl);

    const int nbElem = (int)iter.numel();
    if (!nbElem) {
        LEAVE(hijack_torch_cuda, sum_kernel_impl);
        return;
    }
    if (!iter.is_contiguous() && iter.needs_dynamic_casting()) {
        fprintf(stderr, "non-contiguous / not-supported in sum_kernel_impl\n");
        LEAVE(hijack_torch_cuda, sum_kernel_impl);
        return;
    }

    assert(kFloat == iter.dtype(0) && kFloat == iter.dtype(1) &&
           kFloat == iter.dtype(iter.ntensors() - 1) &&
           (iter.is_contiguous() || !iter.needs_dynamic_casting()) &&
           iter.can_use_32bit_indexing() && 2 == iter.ntensors());

    float *result1 = (float*)iter.data_ptr(0);
    float *result2 = NULL;
    if (iter.noutputs() > 1)
        result2 = (float*)iter.data_ptr(1);
    float *a = (float*)iter.data_ptr(iter.ntensors() - 1);
    #if defined USE_GCD && USE_GCD >= 1
    sum_kernel_cuda_async_f_t *ctx;
    assert((ctx = (sum_kernel_cuda_async_f_t *)malloc(sizeof(*ctx))));
    #else //USE_GCD
    int i;
    const size_t sof = sizeof(float);
    float sum = 0.0;
    #endif //USE_GCD
    if (iter.is_contiguous()) {
        #if defined USE_GCD && USE_GCD >= 1
        ctx->result1 = result1;
        ctx->result2 = result2;
        ctx->a = (float*)a;
        ctx->nbElem = nbElem;
        ctx->nbOutp = iter.num_output_elements();
        ctx->contiguous = true;
        fake_cuda_dispatch(ctx, sum_kernel_cuda_async_f_fn,
                           at::cuda::getCurrentCUDAStream());
        #else //USE_GCD
        if (1 == iter.num_output_elements()) {
            #pragma omp parallel for reduction(+:sum), schedule(static)
            for (i = 0; i < nbElem; i++)
                sum += a[i];
            //
            result1[0] = sum;
            if (result2)
                result2[0] = result1[0];
        } else {
            int j;
            int64_t num_outputs = iter.num_output_elements();
            int64_t inputs_per_output = iter.numel() / num_outputs;
            #pragma omp parallel for private(sum), schedule(static)
            for (j = 0; j < num_outputs; j++) {
                sum = 0.0;
                for (i = j * inputs_per_output;
                     i < min((j + 1) * inputs_per_output, nbElem); i++)
                    sum += a[i];
                result1[j] = sum;
                if (result2)
                    result2[j] = result1[j];
            }
        }
        #endif //USE_GCD
    } else if (iter.is_trivial_1d() && !iter.needs_dynamic_casting()
               && 1 == iter.num_output_elements()) {
        auto inner_strides = iter.get_inner_strides();
        at::detail::Array<int, 2/*ntensors*/> strides;
        for (int i = 0; i < 2/*ntensors*/; i++)
            strides[i] = inner_strides[i];
        assert(0 == strides.data[0]); //make sure we only acc into 1 target
        #if defined USE_GCD && USE_GCD >= 1
        ctx->result1 = result1;
        ctx->result2 = result2;
        ctx->a = (float*)a;
        ctx->nbElem = nbElem;
        ctx->nbOutp = iter.num_output_elements();
        ctx->contiguous = false;
        ctx->trivial_1d = true;
        ctx->s = strides.data[1];
        fake_cuda_dispatch(ctx, sum_kernel_cuda_async_f_fn,
                           at::cuda::getCurrentCUDAStream());
        #else //USE_GCD
        const int s = strides.data[1];
        #pragma omp parallel for reduction(+:sum), \
        firstprivate(s), schedule(static)
        for (i = 0; i < nbElem; i++)
            sum += a[i * s / sof];
        //
        result1[0] = sum;
        if (result2)
            result2[0] = result1[0];
        #endif //USE_GCD
    } else if (!iter.is_trivial_1d() && !iter.needs_dynamic_casting()) {
        auto offset_calc = make_offset_calculator<2>(iter);
        #if defined USE_GCD && USE_GCD >= 1
        ctx->result1 = result1;
        ctx->result2 = result2;
        ctx->a = (float*)a;
        ctx->nbElem = nbElem;
        ctx->nbOutp = iter.num_output_elements();
        ctx->contiguous = false;
        ctx->trivial_1d = false;
        ctx->offset_calc = offset_calc;
        fake_cuda_dispatch(ctx, sum_kernel_cuda_async_f_fn,
                           at::cuda::getCurrentCUDAStream());
        #else //USE_GCD
        int64_t old_off = -1;
        //no!!: #pragma omp parallel for firstprivate(old_off,sof), schedule(static)
        for (i = 0; i < nbElem; i++) {
            auto offsets = offset_calc.get(i);
            const int64_t off = offsets.data[0] / sof;
            if (off > old_off) {
                result1[off] = 0.0;
                old_off = off;
            } else if (off < old_off)
                assert(0);
            result1[off] += a[offsets.data[1] / sof];
            if (result2)
                result2[off] = result1[off];
        }
        #endif //USE_GCD
    } else
        assert(0);

    LEAVE(hijack_torch_cuda, sum_kernel_impl);
}

void __ignore_its_just_for_initing_the_template__()
{
    TensorIterator iter;
    threshold_kernel_impl<float>(iter, (float)0.0, (float)0.0);
    mean_kernel_impl<float, float, float>(iter);
    sum_kernel_impl<float, float, float>(iter);
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *result;
    float val;
    int nbElem;
} fill_kernel_cuda_async_f_t;

__attribute__((used,noinline))
static void fill_kernel_cuda_async_f_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, fill_kernel_cuda_async_f_fn);

    fill_kernel_cuda_async_f_t c = *((fill_kernel_cuda_async_f_t *)ctx);

    float *result = c.result;
    const float val = c.val;
    const int nbElem = c.nbElem;
    int i;
    #pragma omp parallel for firstprivate(val), schedule(static)
    for (i = 0; i < nbElem; i++)
        result[i] = val;

    free(ctx);

    LEAVE(hijack_torch_cuda, fill_kernel_cuda_async_f_fn);
}
#endif //USE_GCD

void fill_kernel_cuda(TensorIterator& iter, Scalar value)
{
    ENTER(hijack_torch_cuda, fill_kernel_cuda);

    if (!iter.is_contiguous()) {
        fprintf(stderr, "non-contiguous in fill_kernel_cuda\n");
        LEAVE(hijack_torch_cuda, fill_kernel_cuda);
        return;
    }

    assert(kFloat == iter.dtype(0) && iter.is_contiguous());

    float val;
    if (value.isIntegral(0/*isBool*/))
        val = (float)value.to<int64_t>();
    else if (value.isFloatingPoint())
        val = (float)value.to<double>();
    else
        assert(0);

    float *result = (float*)iter.data_ptr(0);
    const int nbElem = (int)iter.numel();
    #if defined USE_GCD && USE_GCD >= 1
    fill_kernel_cuda_async_f_t *ctx;
    assert((ctx = (fill_kernel_cuda_async_f_t *)malloc(sizeof(*ctx))));
    *ctx = (fill_kernel_cuda_async_f_t){.result = result,
                                        .val = val,
                                        .nbElem = nbElem};
    fake_cuda_dispatch(ctx, fill_kernel_cuda_async_f_fn,
                       at::cuda::getCurrentCUDAStream());
    #else //USE_GCD
    int i;
    #pragma omp parallel for firstprivate(val), schedule(static)
    for (i = 0; i < nbElem; i++)
        result[i] = val;
    #endif //USE_GCD

    LEAVE(hijack_torch_cuda, fill_kernel_cuda);
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *result, *x;
    int nbElem;
    bool contiguous;
    struct OffsetCalculator<2> offset_calc;
} copy_kernel_cuda_async_f_t;

__attribute__((used,noinline))
static void copy_kernel_cuda_async_f_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, copy_kernel_cuda_async_f_fn);

    copy_kernel_cuda_async_f_t c = *((copy_kernel_cuda_async_f_t *)ctx);

    float *result = c.result;
    const float *x = c.x;
    const int nbElem = c.nbElem;
    int i;
    if (c.contiguous) {
        #pragma omp parallel for schedule(static)
        for (i = 0; i < nbElem; i++)
            result[i] = x[i];
    } else {
        const struct OffsetCalculator<2> offset_calc = c.offset_calc;
        const size_t sof = sizeof(float);
        #pragma omp parallel for firstprivate(sof), schedule(static)
        for (i = 0; i < nbElem; i++) {
            auto offsets = offset_calc.get(i);
            result[offsets.data[0] / sof] = x[offsets.data[1] / sof];
        }
    }

    free(ctx);

    LEAVE(hijack_torch_cuda, copy_kernel_cuda_async_f_fn);
}

typedef struct {
    bool *result, *x;
    int nbElem;
    bool contiguous;
    struct OffsetCalculator<2> offset_calc;
} copy_kernel_cuda_async_b_t;

__attribute__((used,noinline))
static void copy_kernel_cuda_async_b_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, copy_kernel_cuda_async_b_fn);

    copy_kernel_cuda_async_b_t c = *((copy_kernel_cuda_async_b_t *)ctx);

    bool *result = c.result;
    const bool *x = c.x;
    const int nbElem = c.nbElem;
    int i;
    if (c.contiguous) {
        #pragma omp parallel for schedule(static)
        for (i = 0; i < nbElem; i++)
            result[i] = x[i];
    } else {
        const struct OffsetCalculator<2> offset_calc = c.offset_calc;
        const size_t sob = sizeof(bool);
        #pragma omp parallel for firstprivate(sob), schedule(static)
        for (i = 0; i < nbElem; i++) {
            auto offsets = offset_calc.get(i);
            result[offsets.data[0] / sob] = x[offsets.data[1] / sob];
        }
    }

    free(ctx);

    LEAVE(hijack_torch_cuda, copy_kernel_cuda_async_b_fn);
}
#endif //USE_GCD

void copy_device_to_device(TensorIterator& iter, bool non_blocking)
{
    ENTER(hijack_torch_cuda, copy_device_to_device);

    if (iter.dtype(0) != iter.dtype(1)) {
        fprintf(stderr, "unsupported  in copy_device_to_device\n");
        LEAVE(hijack_torch_cuda, copy_device_to_device);
        return;
    }

    const int nbElem = (int)iter.numel();
    #if defined USE_GCD && USE_GCD >= 1
    #else //USE_GCD
    int i;
    const size_t sob = sizeof(bool);
    const size_t sof = sizeof(float);
    #endif //USE_GCD
    if (iter.is_contiguous()) {
        if (kBool == iter.dtype(1)) {
            bool *result = (bool*)iter.data_ptr(0);
            const bool *x = (bool*)iter.data_ptr(1);
            #if defined USE_GCD && USE_GCD >= 1
            copy_kernel_cuda_async_b_t *ctx;
            assert((ctx = (copy_kernel_cuda_async_b_t *)malloc(sizeof(*ctx))));
            ctx->result = result;
            ctx->x = (bool*)x;
            ctx->nbElem = nbElem;
            ctx->contiguous = true;
            fake_cuda_dispatch(ctx, copy_kernel_cuda_async_b_fn,
                               at::cuda::getCurrentCUDAStream());
            #else //USE_GCD
            #pragma omp parallel for schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i] = x[i];
            #endif //USE_GCD
        } else if (kFloat == iter.dtype(1)) {
            float *result = (float*)iter.data_ptr(0);
            const float *x = (float*)iter.data_ptr(1);
            #if defined USE_GCD && USE_GCD >= 1
            copy_kernel_cuda_async_f_t *ctx;
            assert((ctx = (copy_kernel_cuda_async_f_t *)malloc(sizeof(*ctx))));
            ctx->result = result;
            ctx->x = (float*)x;
            ctx->nbElem = nbElem;
            ctx->contiguous = true;
            fake_cuda_dispatch(ctx, copy_kernel_cuda_async_f_fn,
                               at::cuda::getCurrentCUDAStream());
            #else //USE_GCD
            #pragma omp parallel for schedule(static)
            for (i = 0; i < nbElem; i++)
                result[i] = x[i];
            #endif //USE_GCD
        } else
            assert(0);
    } else if (!iter.is_trivial_1d() && !iter.needs_dynamic_casting()) {
        auto offset_calc = make_offset_calculator<2>(iter);
        if (kBool == iter.dtype(1)) {
            bool *result = (bool*)iter.data_ptr(0);
            const bool *x = (bool*)iter.data_ptr(1);
            #if defined USE_GCD && USE_GCD >= 1
            copy_kernel_cuda_async_b_t *ctx;
            assert((ctx = (copy_kernel_cuda_async_b_t *)malloc(sizeof(*ctx))));
            ctx->result = result;
            ctx->x = (bool*)x;
            ctx->nbElem = nbElem;
            ctx->contiguous = false;
            ctx->offset_calc = offset_calc;
            fake_cuda_dispatch(ctx, copy_kernel_cuda_async_b_fn,
                               at::cuda::getCurrentCUDAStream());
            #else //USE_GCD
            // shouldnt overlap -> omp fine
            #pragma omp parallel for firstprivate(sob), schedule(static)
            for (i = 0; i < nbElem; i++) {
                auto offsets = offset_calc.get(i);
                result[offsets.data[0] / sob] = x[offsets.data[1] / sob];
            }
            #endif //USE_GCD
        } else if (kFloat == iter.dtype(1)) {
            float *result = (float*)iter.data_ptr(0);
            const float *x = (float*)iter.data_ptr(1);
            #if defined USE_GCD && USE_GCD >= 1
            copy_kernel_cuda_async_f_t *ctx;
            assert((ctx = (copy_kernel_cuda_async_f_t *)malloc(sizeof(*ctx))));
            ctx->result = result;
            ctx->x = (float*)x;
            ctx->nbElem = nbElem;
            ctx->contiguous = false;
            ctx->offset_calc = offset_calc;
            fake_cuda_dispatch(ctx, copy_kernel_cuda_async_f_fn,
                               at::cuda::getCurrentCUDAStream());
            #else //USE_GCD
            #pragma omp parallel for firstprivate(sof), schedule(static)
            for (i = 0; i < nbElem; i++) {
                auto offsets = offset_calc.get(i);
                result[offsets.data[0] / sof] = x[offsets.data[1] / sof];
            }
            #endif //USE_GCD
        } else
            assert(0);
    } else
        assert(0);

    LEAVE(hijack_torch_cuda, copy_device_to_device);
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *bottom_data, *top_data;
    int64_t /*num = nbatch,*/ channels,
            height, width, pooled_height, pooled_width;
    int kernel_h, kernel_w, stride_h, stride_w, pad_h, pad_w,
        dilation_h, dilation_w, count;
    int64_t *top_mask;
} max_pool2d_cuda_async_forw_t;

__attribute__((used,noinline))
static void max_pool2d_cuda_async_forw_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, max_pool2d_cuda_async_forw_fn);

    max_pool2d_cuda_async_forw_t c = *((max_pool2d_cuda_async_forw_t *)ctx);

    float *bottom_data = c.bottom_data, *top_data = c.top_data;
    const int64_t /*num = nbatch,*/ channels = c.channels,
          height = c.height, width = c.width,
          pooled_height = c.pooled_height, pooled_width = c.pooled_width;
    const int kernel_h = c.kernel_h, kernel_w = c.kernel_w,
          stride_h = c.stride_h, stride_w = c.stride_w,
          pad_h = c.pad_h, pad_w = c.pad_w,
          dilation_h = c.dilation_h, dilation_w = c.dilation_w, count = c.count;
    int64_t *top_mask = c.top_mask;
    int index;
    #pragma omp parallel for \
    firstprivate(channels,height,width,pooled_height,pooled_width, \
                 kernel_h,kernel_w,stride_h,stride_w,pad_h,pad_w, \
                 dilation_h,dilation_w), \
    schedule(static)
    for (index = 0; index < count; index++) {
        int pw = index % pooled_width;
        int ph = (index / pooled_width) % pooled_height;
        int c = (index / pooled_width / pooled_height) % channels;
        int n = index / pooled_width / pooled_height / channels;
        int hstart = ph * stride_h - pad_h;
        int wstart = pw * stride_w - pad_w;
        int hend = min(hstart + (kernel_h-1) * dilation_h + 1, height);
        int wend = min(wstart + (kernel_w-1) * dilation_w + 1, width);
        while(hstart < 0)
            hstart += dilation_h;
        while(wstart < 0)
            wstart += dilation_w;
        float maxval = -INFINITY;
        int maxidx = hstart * width + wstart;
        //bottom_data += (n * channels + c) * height * width;
        float *tmp_bottom_data = &bottom_data[(n * channels + c)
                                              * height * width];
        int h, w;
        for (h = hstart; h < hend; h += dilation_h) {
            for (w = wstart; w < wend; w += dilation_w) {
                float val = tmp_bottom_data[h * width + w];
                if (val > maxval || NAN == val) {
                    maxidx = h * width + w;
                    maxval = val;
                }
            }
        }
        top_data[index] = maxval;
        top_mask[index] = maxidx;
    }

    free(ctx);

    LEAVE(hijack_torch_cuda, max_pool2d_cuda_async_forw_fn);
}
#endif //USE_GCD

std::tuple<Tensor, Tensor> max_pool2d_with_indices_cuda(const Tensor& input_,
                                                        IntArrayRef kernel_size,
                                                        IntArrayRef stride,
                                                        IntArrayRef padding,
                                                        IntArrayRef dilation,
                                                        bool ceil_mode)
{
    ENTER(hijack_torch_cuda, max_pool2d_with_indices_cuda);

    Tensor output = at::empty({0}, input_.options());
    Tensor indices = at::empty({0}, input_.options().dtype(kLong));

    const int kH = safe_downcast<int, int64_t>(kernel_size[0]);
    const int kW = kernel_size.size() == 1 ?
        kH : safe_downcast<int, int64_t>(kernel_size[1]);
    const int dH = stride.empty() ? kH : safe_downcast<int, int64_t>(stride[0]);
    const int dW = stride.empty() ?
        kW : stride.size() == 1 ? dH : safe_downcast<int, int64_t>(stride[1]);
    const int padH = safe_downcast<int, int64_t>(padding[0]);
    const int padW = padding.size() == 1 ?
        padH : safe_downcast<int, int64_t>(padding[1]);
    const int dilationH = safe_downcast<int, int64_t>(dilation[0]);
    const int dilationW = dilation.size() == 1 ?
        dilationH : safe_downcast<int, int64_t>(dilation[1]);
    const auto memory_format = input_.suggest_memory_format();
    const int64_t nbatch = input_.ndimension() == 4 ? input_.size(-4) : 1;
    const int64_t nInputPlane = input_.size(-3);
    const int64_t inputHeight = input_.size(-2);
    const int64_t inputWidth = input_.size(-1);
    const int64_t outputWidth =
        pooling_output_shape<int64_t>(inputWidth, kW, padW, dW, dilationW,
                                      ceil_mode);
    const int64_t outputHeight =
        pooling_output_shape<int64_t>(inputHeight, kH, padH, dH, dilationH,
                                      ceil_mode);

    Tensor input = input_.contiguous(memory_format);
    //const int64_t in_stride_c = input.stride(-3);
    //const int64_t in_stride_h = input.stride(-2);
    //const int64_t in_stride_w = input.stride(-1);

    output.resize_({nbatch, nInputPlane, outputHeight, outputWidth});
    indices.resize_({nbatch, nInputPlane, outputHeight, outputWidth});

    output.unsafeGetTensorImpl()->empty_tensor_restride(memory_format);
    indices.unsafeGetTensorImpl()->empty_tensor_restride(memory_format);

    const int count = safe_downcast<int, int64_t>(output.numel());

    assert(kFloat == output.scalar_type() && kFloat == input.scalar_type());
    float *output_data = output.data_ptr<float>();
    float *input_data = input.data_ptr<float>();
    int64_t *indices_data = indices.data_ptr<int64_t>();

    switch (memory_format) {
        case MemoryFormat::ChannelsLast:
        {
            assert(0);
            break;
        }
        case MemoryFormat::Contiguous:
        {
            #if defined USE_GCD && USE_GCD >= 1
            max_pool2d_cuda_async_forw_t *ctx;
            assert((ctx =
                        (max_pool2d_cuda_async_forw_t *)malloc(sizeof(*ctx))));
            *ctx = (max_pool2d_cuda_async_forw_t){.bottom_data = input_data,
                                                  .top_data = output_data,
                                                  .channels = nInputPlane,
                                                  .height = inputHeight,
                                                  .width = inputWidth,
                                                  .pooled_height = outputHeight,
                                                  .pooled_width = outputWidth,
                                                  .kernel_h = kH,
                                                  .kernel_w = kW,
                                                  .stride_h = dH,
                                                  .stride_w = dW,
                                                  .pad_h = padH,
                                                  .pad_w = padW,
                                                  .dilation_h = dilationH,
                                                  .dilation_w = dilationW,
                                                  .count = count,
                                                  .top_mask = indices_data};
            fake_cuda_dispatch(ctx, max_pool2d_cuda_async_forw_fn,
                               at::cuda::getCurrentCUDAStream());
            #else //USE_GCD
            float *bottom_data = input_data, *top_data = output_data;
            const int64_t /*num = nbatch,*/ channels = nInputPlane,
                  height = inputHeight, width = inputWidth,
                  pooled_height = outputHeight, pooled_width = outputWidth;
            const int kernel_h = kH, kernel_w = kW,
                  stride_h = dH, stride_w = dW,
                  pad_h = padH, pad_w = padW,
                  dilation_h = dilationH, dilation_w = dilationW;
            int64_t *top_mask = indices_data;
            int index;
            #pragma omp parallel for \
            firstprivate(channels,height,width,pooled_height,pooled_width, \
                         kernel_h,kernel_w,stride_h,stride_w,pad_h,pad_w, \
                         dilation_h,dilation_w), \
            schedule(static)
            for (index = 0; index < count; index++) {
                int pw = index % pooled_width;
                int ph = (index / pooled_width) % pooled_height;
                int c = (index / pooled_width / pooled_height) % channels;
                int n = index / pooled_width / pooled_height / channels;
                int hstart = ph * stride_h - pad_h;
                int wstart = pw * stride_w - pad_w;
                int hend = min(hstart + (kernel_h-1) * dilation_h + 1, height);
                int wend = min(wstart + (kernel_w-1) * dilation_w + 1, width);
                while(hstart < 0)
                    hstart += dilation_h;
                while(wstart < 0)
                    wstart += dilation_w;
                float maxval = -INFINITY;
                int maxidx = hstart * width + wstart;
                //bottom_data += (n * channels + c) * height * width;
                float *tmp_bottom_data = &bottom_data[(n * channels + c)
                                                      * height * width];
                int h, w;
                for (h = hstart; h < hend; h += dilation_h) {
                    for (w = wstart; w < wend; w += dilation_w) {
                        float val = tmp_bottom_data[h * width + w];
                        if (val > maxval || NAN == val) {
                            maxidx = h * width + w;
                            maxval = val;
                        }
                    }
                }
                top_data[index] = maxval;
                top_mask[index] = maxidx;
            }
            #endif //USE_GCD
            break;
        }
        default: assert(0);
    }

    if(input.ndimension() == 3)
        output.resize_({nInputPlane, outputHeight, outputWidth});

    LEAVE(hijack_torch_cuda, max_pool2d_with_indices_cuda);
    return std::tuple<Tensor, Tensor>(output, indices);
}

inline int p_start(int size, int pad, int kernel, int dilation, int stride)
{
    return (size + pad < ((kernel - 1) * dilation + 1)) ? 0 :
        (size + pad - ((kernel - 1) * dilation + 1)) / stride + 1;
}

inline int p_end(int size, int pad, int pooled_size, int stride)
{
    return min((size + pad) / stride + 1, pooled_size);
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *top_diff, *bottom_diff;
    int64_t num, channels, height, width, pooled_height, pooled_width;
    int kernel_h, kernel_w, stride_h, stride_w, pad_h, pad_w,
        dilation_h, dilation_w;
    int64_t *top_mask;
} max_pool2d_cuda_async_back_t;

__attribute__((used,noinline))
static void max_pool2d_cuda_async_back_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, max_pool2d_cuda_async_back_fn);

    max_pool2d_cuda_async_back_t c = *((max_pool2d_cuda_async_back_t *)ctx);

    float *top_diff = c.top_diff, *bottom_diff = c.bottom_diff;
    const int64_t num = c.num, channels = c.channels,
          height = c.height, width = c.width,
          pooled_height = c.pooled_height, pooled_width = c.pooled_width;
    const int kernel_h = c.kernel_h, kernel_w = c.kernel_w,
          stride_h = c.stride_h, stride_w = c.stride_w,
          pad_h = c.pad_h, pad_w = c.pad_w,
          dilation_h = c.dilation_h, dilation_w = c.dilation_w;
    int64_t *top_mask = c.top_mask;
    int64_t index = 0;
    /*
    #pragma omp parallel for \
        firstprivate(num,channels,height,width,pooled_height,pooled_width, \
                 kernel_h,kernel_w,stride_h,stride_w,pad_h,pad_w, \
                 dilation_h,dilation_w), \
        schedule(static)
    for (index = 0; index < height * width; index++) {
    */
    #pragma omp parallel firstprivate(num,channels,height,width, \
                                      pooled_height,pooled_width,index)
    {
        const int omp_thr = omp_get_thread_num();
        const ldiv_t res = ldiv(height * width, omp_get_max_threads());
        int64_t idx_start, idx_chunk;
        if (0 == res.rem) {
            idx_chunk = res.quot;
            idx_start = omp_thr * idx_chunk;
        } else {
            if (omp_thr < res.rem) {
                idx_chunk = res.quot + 1;
                idx_start = omp_thr * idx_chunk;
            } else {
                idx_chunk = res.quot;
                idx_start = res.rem * (idx_chunk + 1)
                            + (omp_thr - res.rem) * idx_chunk;
            }
        }
        for (index = idx_start; index < idx_start + idx_chunk; index++) {
            const int h = index / width;
            const int w = index - h * width;
            const int hXwidthPw = h * width + w;
            const int phstart = p_start(h, pad_h, kernel_h, dilation_h,
                                        stride_h);
            const int phend = p_end(h, pad_h, pooled_height, stride_h);
            const int pwstart = p_start(w, pad_w, kernel_w, dilation_w,
                                        stride_w);
            const int pwend = p_end(w, pad_w, pooled_width, stride_w);
            int n, c, ph, pw;
            #pragma omp for schedule(static), nowait
            for (n = 0; n < num; n++) {
                for (c = 0; c < channels; c++) {
                    float gradient = 0.0;
                    const int offset =
                        (n * channels + c) * pooled_height * pooled_width;
                    //top_diff += offset;
                    const float *tmp_top_diff = &top_diff[offset];
                    //top_mask += offset;
                    const int64_t *tmp_top_mask = &top_mask[offset];
                    if ((phstart + 1 != phend) || (pwstart + 1 != pwend)) {
                        for (ph = phstart; ph < phend; ph++)
                            for (pw = pwstart; pw < pwend; pw++)
                                if (hXwidthPw == tmp_top_mask[ph * pooled_width
                                                              + pw])
                                    gradient += tmp_top_diff[ph * pooled_width
                                                             + pw];
                    } else {
                        if (hXwidthPw == tmp_top_mask[phstart * pooled_width
                                                      + pwstart])
                            gradient += tmp_top_diff[phstart * pooled_width
                                                     + pwstart];
                    }
                    bottom_diff[(n * channels + c) * height * width + index]
                        = gradient;
                }
            }
        }
    }

    free(ctx);

    LEAVE(hijack_torch_cuda, max_pool2d_cuda_async_back_fn);
}
#endif //USE_GCD

Tensor max_pool2d_with_indices_backward_cuda(const Tensor& gradOutput_,
                                             const Tensor& input_,
                                             IntArrayRef kernel_size,
                                             IntArrayRef stride,
                                             IntArrayRef padding,
                                             IntArrayRef dilation,
                                             bool ceil_mode,
                                             const Tensor& indices)
{
    ENTER(hijack_torch_cuda, max_pool2d_with_indices_backward_cuda);

    auto gradInput = at::zeros_like(input_, LEGACY_CONTIGUOUS_MEMORY_FORMAT);

    const int kH = safe_downcast<int, int64_t>(kernel_size[0]);
    const int kW = (kernel_size.size() == 1) ? kH :
                   safe_downcast<int, int64_t>(kernel_size[1]);
    const int dH = stride.empty() ? kH : safe_downcast<int, int64_t>(stride[0]);
    const int dW = stride.empty() ? kW :
                        (stride.size() == 1) ? dH :
                        safe_downcast<int, int64_t>(stride[1]);
    const int padH = safe_downcast<int, int64_t>(padding[0]);
    const int padW = padding.size() == 1 ? padH :
                     safe_downcast<int, int64_t>(padding[1]);
    const int dilationH = safe_downcast<int, int64_t>(dilation[0]);
    const int dilationW = (dilation.size() == 1) ? dilationH :
                          safe_downcast<int, int64_t>(dilation[1]);

    const auto memory_format = input_.suggest_memory_format();
    assert(memory_format != at::MemoryFormat::ChannelsLast &&
           (input_.ndimension() == 3 || input_.ndimension() == 4));
    const Tensor input = input_.contiguous(memory_format);

    const int64_t nbatch = input.ndimension() == 4 ? input.size(-4) : 1;
    const int64_t nInputPlane = input.size(-3);
    const int64_t inputHeight = input.size(-2);
    const int64_t inputWidth = input.size(-1);

    //const int64_t in_stride_c = input.stride(-3);
    //const int64_t in_stride_h = input.stride(-2);
    //const int64_t in_stride_w = input.stride(-1);

    const int64_t outputHeight =
        pooling_output_shape<int64_t>(inputHeight, kH, padH, dH, dilationH,
                                      ceil_mode);
    const int64_t outputWidth =
        pooling_output_shape<int64_t>(inputWidth, kW, padW, dW, dilationW,
                                      ceil_mode);

    //max_pool2d_backward_shape_check(...)

    const Tensor gradOutput = gradOutput_.contiguous(memory_format);

    //const int64_t out_stride_c = gradOutput.stride(-3);
    //const int64_t out_stride_h = gradOutput.stride(-2);
    //const int64_t out_stride_w = gradOutput.stride(-1);

    gradInput.resize_as_(input);
    gradInput.unsafeGetTensorImpl()->empty_tensor_restride(memory_format);

    //const int64_t count = input.numel();

    assert(kFloat == gradOutput.scalar_type()
           && kFloat == gradInput.scalar_type());
    float *gradOutput_data = gradOutput.data_ptr<float>();
    float *gradInput_data = gradInput.data_ptr<float>();
    int64_t *indices_data = indices.data_ptr<int64_t>();

    switch (memory_format) {
        case MemoryFormat::ChannelsLast:
        {
            assert(0);
            break;
        }
        case MemoryFormat::Contiguous:
        {
            #if defined USE_GCD && USE_GCD >= 1
            max_pool2d_cuda_async_back_t *ctx;
            assert((ctx =
                        (max_pool2d_cuda_async_back_t *)malloc(sizeof(*ctx))));
            *ctx = (max_pool2d_cuda_async_back_t){.top_diff = gradOutput_data,
                                                  .bottom_diff = gradInput_data,
                                                  .num = nbatch,
                                                  .channels = nInputPlane,
                                                  .height = inputHeight,
                                                  .width = inputWidth,
                                                  .pooled_height = outputHeight,
                                                  .pooled_width = outputWidth,
                                                  .kernel_h = kH,
                                                  .kernel_w = kW,
                                                  .stride_h = dH,
                                                  .stride_w = dW,
                                                  .pad_h = padH,
                                                  .pad_w = padW,
                                                  .dilation_h = dilationH,
                                                  .dilation_w = dilationW,
                                                  .top_mask = indices_data};
            fake_cuda_dispatch(ctx, max_pool2d_cuda_async_back_fn,
                               at::cuda::getCurrentCUDAStream());
            #else //USE_GCD
            float *top_diff = gradOutput_data, *bottom_diff = gradInput_data;
            const int64_t num = nbatch, channels = nInputPlane,
                  height = inputHeight, width = inputWidth,
                  pooled_height = outputHeight, pooled_width = outputWidth;
            const int kernel_h = kH, kernel_w = kW,
                  stride_h = dH, stride_w = dW,
                  pad_h = padH, pad_w = padW,
                  dilation_h = dilationH, dilation_w = dilationW;
            int64_t *top_mask = indices_data;
            int index;
            #pragma omp parallel for \
            firstprivate(channels,height,width,pooled_height,pooled_width, \
                         kernel_h,kernel_w,stride_h,stride_w,pad_h,pad_w, \
                         dilation_h,dilation_w), \
            schedule(static)
            for (index = 0; index < height * width; index++) {
                int h = index / width;
                int w = index - h * width;
                int phstart = p_start(h, pad_h, kernel_h, dilation_h, stride_h);
                int phend = p_end(h, pad_h, pooled_height, stride_h);
                int pwstart = p_start(w, pad_w, kernel_w, dilation_w, stride_w);
                int pwend = p_end(w, pad_w, pooled_width, stride_w);
                int n, c, ph, pw;
                for (n = 0; n < num; n++) {
                    for (c = 0; c < channels; c++) {
                        float gradient = 0.0;
                        int offset =
                            (n * channels + c) * pooled_height * pooled_width;
                        //top_diff += offset;
                        float *tmp_top_diff = &top_diff[offset];
                        //top_mask += offset;
                        int64_t *tmp_top_mask = &top_mask[offset];
                        if ((phstart + 1 != phend) || (pwstart + 1 != pwend)) {
                            for (ph = phstart; ph < phend; ph++)
                                for (pw = pwstart; pw < pwend; pw++)
                                    if (tmp_top_mask[ph * pooled_width + pw]
                                        == h * width + w)
                                        gradient +=
                                            tmp_top_diff[ph * pooled_width
                                                         + pw];
                        } else {
                            if (tmp_top_mask[phstart * pooled_width + pwstart]
                                == h * width + w)
                                gradient +=
                                    tmp_top_diff[phstart * pooled_width
                                                 + pwstart];
                        }
                        bottom_diff[(n * channels + c) * height * width
                                    + index] = gradient;
                    }
                }
            }
            #endif //USE_GCD
            break;
        }
        default: assert(0);
    }

    LEAVE(hijack_torch_cuda, max_pool2d_with_indices_backward_cuda);
    return gradInput;
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *output_data_base, *input_data_base;
    int64_t dim_size, outer_size, CHUNK_SIZE;
} log_softmax_cuda_async_forw_t;

__attribute__((used,noinline))
static void log_softmax_cuda_async_forw_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, log_softmax_cuda_async_forw_fn);

    log_softmax_cuda_async_forw_t c = *((log_softmax_cuda_async_forw_t *)ctx);

    float *output_data_base = c.output_data_base,
          *input_data_base = c.input_data_base;
    const int64_t dim_size = c.dim_size, outer_size = c.outer_size,
          CHUNK_SIZE = c.CHUNK_SIZE;
    int64_t ii;
    #pragma omp parallel for \
    firstprivate(CHUNK_SIZE,dim_size), schedule(static)
    for (ii = 0; ii < outer_size; ii += CHUNK_SIZE) {
        float tmp_sum_scalar[CHUNK_SIZE];
        float max_input_arr[CHUNK_SIZE];
        int64_t j, loop_end = CHUNK_SIZE;
        if (ii + CHUNK_SIZE > outer_size)
            loop_end = outer_size - ii;
        for (j = 0; j < loop_end; j++) {
            int64_t /*i = ii + j,*/ k;
            const float *input_data = input_data_base + (ii + j) * dim_size;
            max_input_arr[j] = -INFINITY;
            //vec256::reduce_all
            for (k = 0; k < dim_size; k++)
                max_input_arr[j] = maxFloatIEEE(max_input_arr[j],
                                                input_data[k]);
        }
        for (j = 0; j < loop_end; j++) {
            int64_t /*i = ii + j,*/ k;
            const float *input_data = input_data_base + (ii + j) * dim_size;
            tmp_sum_scalar[j] = 0.0;
            const float max_input = max_input_arr[j];
            //vec256::map_reduce_all
            for (k = 0; k < dim_size; k++)
                tmp_sum_scalar[j] +=
                    (float)std::exp(input_data[k] - max_input);
        }
        //vec256::map
        for (j = 0; j < loop_end; j++)
            tmp_sum_scalar[j] = (float)std::log(tmp_sum_scalar[j]);
        for (j = 0; j < loop_end; j++) {
            int64_t /*i = ii + j,*/ k;
            float *output_data = output_data_base + (ii + j) * dim_size;
            const float *input_data = input_data_base + (ii + j) * dim_size;
            const float tmp_sum = tmp_sum_scalar[j];
            const float max_input = max_input_arr[j];
            //vec256::map
            for (k = 0; k < dim_size; k++)
                output_data[k] = input_data[k] - max_input - tmp_sum;
        }
    }

    free(ctx);

    LEAVE(hijack_torch_cuda, log_softmax_cuda_async_forw_fn);
}
#endif //USE_GCD

// imitate log_softmax_cpu from SoftMax.cpp
Tensor log_softmax_cuda(const Tensor &input_, const int64_t dim_,
                        const bool half_to_float)
{
    ENTER(hijack_torch_cuda, log_softmax_cuda);

    assert(!half_to_float); //not supported yet

    auto input = input_.contiguous();
    Tensor output = half_to_float ?
        at::empty_like(input, input.options().dtype(ScalarType::Float),
                       LEGACY_CONTIGUOUS_MEMORY_FORMAT) :
        at::empty_like(input, LEGACY_CONTIGUOUS_MEMORY_FORMAT);

    if (input.dim() == 0) input = input.view(1);
    const int64_t dim = maybe_wrap_dim(dim_, input.dim());
    const int64_t dim_size = input.size(dim);

    assert(input.ndimension() > 0 && dim == input.ndimension() - 1);

    if (0 == input.numel()) {
        LEAVE(hijack_torch_cuda, log_softmax_cuda);
        return output;
    }

    int64_t ii, inner_size = 1, outer_size = 1;
    //cudaStream_t stream = at::cuda::getCurrentCUDAStream();
    for (ii = 0; ii < dim; ii++)
        outer_size *= input.size(ii);
    for (ii = dim + 1; ii < input.dim(); ii++)
        inner_size *= input.size(ii);

    if (inner_size == 1) {
        //const int ILP = 2;
        //dim3 grid(outer_size);
        //dim3 block = SoftMax_getBlockSize(ILP, dim_size);
        assert(kFloat == output.scalar_type() && kFloat == input.scalar_type());
        float *output_data_base = output.data_ptr<float>();
        float *input_data_base = input.data_ptr<float>();
        const int64_t CHUNK_SIZE = 512 / (8 * sizeof(float)); //adj to a64fx
        #if defined USE_GCD && USE_GCD >= 1
        log_softmax_cuda_async_forw_t *ctx;
        assert((ctx = (log_softmax_cuda_async_forw_t *)malloc(sizeof(*ctx))));
        *ctx = (log_softmax_cuda_async_forw_t)
                    {.output_data_base = output_data_base,
                     .input_data_base = input_data_base,
                     .dim_size = dim_size, .outer_size = outer_size,
                     .CHUNK_SIZE = CHUNK_SIZE};
        fake_cuda_dispatch(ctx, log_softmax_cuda_async_forw_fn,
                           at::cuda::getCurrentCUDAStream());
        #else //USE_GCD
        #pragma omp parallel for \
        firstprivate(CHUNK_SIZE,dim_size), schedule(static)
        for (ii = 0; ii < outer_size; ii += CHUNK_SIZE) {
            float tmp_sum_scalar[CHUNK_SIZE];
            float max_input_arr[CHUNK_SIZE];
            int64_t j, loop_end = CHUNK_SIZE;
            if (ii + CHUNK_SIZE > outer_size)
                loop_end = outer_size - ii;
            for (j = 0; j < loop_end; j++) {
                int64_t /*i = ii + j,*/ k;
                const float *input_data = input_data_base + (ii + j) * dim_size;
                max_input_arr[j] = -INFINITY;
                //vec256::reduce_all
                for (k = 0; k < dim_size; k++)
                    max_input_arr[j] = maxFloatIEEE(max_input_arr[j],
                                                    input_data[k]);
            }
            for (j = 0; j < loop_end; j++) {
                int64_t /*i = ii + j,*/ k;
                const float *input_data = input_data_base + (ii + j) * dim_size;
                tmp_sum_scalar[j] = 0.0;
                const float max_input = max_input_arr[j];
                //vec256::map_reduce_all
                for (k = 0; k < dim_size; k++)
                    tmp_sum_scalar[j] +=
                        (float)std::exp(input_data[k] - max_input);
            }
            //vec256::map
            for (j = 0; j < loop_end; j++)
                tmp_sum_scalar[j] = (float)std::log(tmp_sum_scalar[j]);
            for (j = 0; j < loop_end; j++) {
                int64_t /*i = ii + j,*/ k;
                float *output_data = output_data_base + (ii + j) * dim_size;
                const float *input_data = input_data_base + (ii + j) * dim_size;
                const float tmp_sum = tmp_sum_scalar[j];
                const float max_input = max_input_arr[j];
                //vec256::map
                for (k = 0; k < dim_size; k++)
                    output_data[k] = input_data[k] - max_input - tmp_sum;
            }
        }
        #endif //USE_GCD
    } else {
        assert(0);
        //uint32_t smem_size;
        //dim3 grid, block;
        //... do something
    }
    //THCudaCheck(cudaGetLastError());

    LEAVE(hijack_torch_cuda, log_softmax_cuda);
    return output;
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *grad_input_data_base, *grad_data_base, *output_data_base;
    int64_t dim_size, outer_size;
} log_softmax_cuda_async_back_t;

__attribute__((used,noinline))
static void log_softmax_cuda_async_back_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, log_softmax_cuda_async_back_fn);

    log_softmax_cuda_async_back_t c = *((log_softmax_cuda_async_back_t *)ctx);

    float *grad_input_data_base = c.grad_input_data_base,
          *grad_data_base = c.grad_data_base,
          *output_data_base = c.output_data_base;
    const int64_t dim_size = c.dim_size, outer_size = c.outer_size;
    int64_t i;
    #pragma omp parallel for firstprivate(dim_size), schedule(static)
    for (i = 0; i < outer_size; i++) {
        float *grad_input_data = grad_input_data_base + i * dim_size;
        const float *grad_data = grad_data_base + i * dim_size;
        const float *output_data = output_data_base + i * dim_size;
        float sum = 0.0;
        int64_t j;
        //vec256::reduce_all
        for (j = 0; j < dim_size; j++)
            sum += grad_data[j];
        //vec256::map2
        for (j = 0; j < dim_size; j++)
            grad_input_data[j] =
                grad_data[j] - ((float)std::exp(output_data[j]) * sum);
    }

    free(ctx);

    LEAVE(hijack_torch_cuda, log_softmax_cuda_async_back_fn);
}
#endif //USE_GCD

// imitate log_softmax_backward_cpu from SoftMax.cpp
Tensor log_softmax_backward_cuda(const Tensor &grad_, const Tensor &output_,
                                 int64_t dim_, const Tensor &input_)
{
    ENTER(hijack_torch_cuda, log_softmax_backward_cuda);

    const bool half_to_float = (grad_.scalar_type() != input_.scalar_type());

    assert(!half_to_float); //not supported yet

    const int64_t dim = maybe_wrap_dim(dim_, grad_.dim());
    Tensor grad_input = half_to_float ?
        at::empty_like(grad_, grad_.options().dtype(ScalarType::Half),
                       LEGACY_CONTIGUOUS_MEMORY_FORMAT) :
        at::empty_like(grad_, LEGACY_CONTIGUOUS_MEMORY_FORMAT);
    if (0 == grad_.numel() || 0 == output_.numel()) {
        LEAVE(hijack_torch_cuda, log_softmax_backward_cuda);
        return grad_input;
    }

    auto grad = grad_.contiguous();
    if (grad.dim() == 0) grad = grad.view(1);

    auto output = output_.contiguous();
    if (output.dim() == 0) output = output.view(1);

    assert(grad.ndimension() > 0 && dim == grad.ndimension() - 1);
    //log_softmax_backward_lastdim_kernel(kCPU, grad_input, grad, output);

    int64_t i, inner_size = 1, outer_size = 1;
    const int64_t dim_size = output.size(dim);
    for (i = 0; i < dim; ++i)
        outer_size *= output.size(i);
    for (i = dim + 1; i < output.dim(); ++i)
        inner_size *= output.size(i);
    //cpu & cuda kernel same? -> seems so -> one can only wish for consistency
    /*{
        int64_t tos = 1;
        for (i = 0; i < grad.ndimension() - 1; ++i) tos *= grad.size(i);
        assert(tos == outer_size);
    }*/

    //cudaStream_t stream = at::cuda::getCurrentCUDAStream();
    if (inner_size == 1) {
        //const int ILP = 2;
        //dim3 grid(outer_size);
        //dim3 block = SoftMax_getBlockSize(ILP, dim_size);
        assert(kFloat == output.scalar_type()
               && kFloat == grad_input.scalar_type()
               && kFloat == grad.scalar_type());
        float *grad_input_data_base = grad_input.data_ptr<float>();
        float *grad_data_base = grad.data_ptr<float>();
        float *output_data_base = output.data_ptr<float>();
        #if defined USE_GCD && USE_GCD >= 1
        log_softmax_cuda_async_back_t *ctx;
        assert((ctx = (log_softmax_cuda_async_back_t *)malloc(sizeof(*ctx))));
        *ctx = (log_softmax_cuda_async_back_t)
                    {.grad_input_data_base = grad_input_data_base,
                     .grad_data_base = grad_data_base,
                     .output_data_base = output_data_base,
                     .dim_size = dim_size, .outer_size = outer_size};
        fake_cuda_dispatch(ctx, log_softmax_cuda_async_back_fn,
                           at::cuda::getCurrentCUDAStream());
        #else //USE_GCD
        #pragma omp parallel for firstprivate(dim_size), schedule(static)
        for (i = 0; i < outer_size; i++) {
            float *grad_input_data = grad_input_data_base + i * dim_size;
            const float *grad_data = grad_data_base + i * dim_size;
            const float *output_data = output_data_base + i * dim_size;
            float sum = 0.0;
            int64_t j;
            //vec256::reduce_all
            for (j = 0; j < dim_size; j++)
                sum += grad_data[j];
            //vec256::map2
            for (j = 0; j < dim_size; j++)
                grad_input_data[j] =
                    grad_data[j] - ((float)std::exp(output_data[j]) * sum);
        }
        #endif //USE_GCD
    } else {
        assert(0);
        //uint32_t smem_size;
        //dim3 grid, block;
        //... do something
    }
    //THCudaCheck(cudaGetLastError());

    LEAVE(hijack_torch_cuda, log_softmax_backward_cuda);
    return grad_input;
}

}} // namespace at::native

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *input_data, *weights_data;
    THCIndex_t *target_data;
    float *output_data, *total_weight_data;
    int64_t ignore_index, reduction;
    int nframe, ndim;
} nllcrit_cuda_async_forw_t;

__attribute__((used,noinline))
static void nllcrit_cuda_async_forw_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, nllcrit_cuda_async_forw_fn);

    nllcrit_cuda_async_forw_t c = *((nllcrit_cuda_async_forw_t *)ctx);

    const float *input_data = c.input_data, *weights_data = c.weights_data;
    const THCIndex_t *target_data = c.target_data;
    float *output_data = c.output_data,
          *total_weight_data = c.total_weight_data;
    const int64_t ignore_index = c.ignore_index, reduction = c.reduction;
    const int nframe = c.nframe, ndim = c.ndim;

    float outputAcc = 0.0, total_weightAcc = 0.0, cur_weight = 0.0;
    int i = 0, t = 0;
    #pragma omp parallel for \
    firstprivate(cur_weight,t,nframe,ndim), \
    reduction(+:outputAcc,total_weightAcc), schedule(static)
    for (i = 0; i < nframe; i++) {
        t = target_data[i];
        if (t != (int)ignore_index) {
            //assert(t >= 0 && t < n_classes);
            cur_weight = weights_data ? weights_data[t] : 1.0;
            outputAcc += -1 * (input_data[i * ndim + t]) * cur_weight;
            total_weightAcc += cur_weight;
        }
    }
    *total_weight_data = total_weightAcc;
    if ((reduction == at::Reduction::Mean) && total_weightAcc > 0)
        *output_data = outputAcc / total_weightAcc;
    else
        *output_data = outputAcc;

    free(ctx);

    LEAVE(hijack_torch_cuda, nllcrit_cuda_async_forw_fn);
}
#endif //USE_GCD

//THNN_() -> resolves to THNN_Cuda
void THNN_CudaClassNLLCriterion_updateOutput(THCState *state,
                                             THCTensor *input,
                                             THCIndexTensor *target,
                                             THCTensor *output,
                                             int64_t reduction,
                                             THCTensor *weights,
                                             THCTensor *total_weight,
                                             int64_t ignore_index)
{
    ENTER(hijack_torch_cuda, THNN_CudaClassNLLCriterion_updateOutput);

    int n_dims = THCudaTensor_nDimensionLegacyNoScalars(state, input);
    //int n_classes = THCudaTensor_sizeLegacyNoScalars(state, input, n_dims - 1);

    /*
    if (weights)
        THCUNN_assertSameGPU(state, 5, input, target, weights, output,
                             total_weight);
    else
        THCUNN_assertSameGPU(state, 4, input, target, output, total_weight);
    */

    //int64_t batch_size =
    //    (n_dims == 1) ? 1 : THCudaTensor_sizeLegacyNoScalars(state, input, 0);
    //int64_t num_targets =
    //    THCudaLongTensor_sizeLegacyNoScalars(state, target, 0);

    if (reduction == at::Reduction::None && n_dims == 2)
        assert(0);
        // skip this part for now -> not needed for resnet

    //THCTensor_() -> resolves to THCudaTensor_ because of THNN_Cuda
    THCudaTensor_resize1d(state, output, 1);
    THCudaTensor_resize1d(state, total_weight, 1);

    input = THCudaTensor_newContiguous(state, input);
    weights = weights ? THCudaTensor_newContiguous(state, weights) : NULL;
    target = THCIndexTensor_(newContiguous)(state, target);

    float *input_data = THCudaTensor_data(state, input);
    float *weights_data = weights ? THCudaTensor_data(state, weights) : NULL;
    THCIndex_t *target_data = THCIndexTensor_(data)(state, target);
    float *output_data = THCudaTensor_data(state, output);
    float *total_weight_data = THCudaTensor_data(state, total_weight);

    if (THCudaTensor_nDimensionLegacyNoScalars(state, input) == 1) {
        assert(0);
        // skip this part for now -> not needed for resnet
    } else if (THCudaTensor_nDimensionLegacyNoScalars(state, input) == 2) {
        int nframe = THCudaTensor_size(state, input, 0);
        int ndim = THCudaTensor_size(state, input, 1);
        #if defined USE_GCD && USE_GCD >= 1
        nllcrit_cuda_async_forw_t *ctx;
        assert((ctx = (nllcrit_cuda_async_forw_t *)malloc(sizeof(*ctx))));
        *ctx = (nllcrit_cuda_async_forw_t)
                    {.input_data = input_data, .weights_data = weights_data,
                     .target_data = target_data, .output_data = output_data,
                     .total_weight_data = total_weight_data,
                     .ignore_index = ignore_index,
                     .reduction = reduction, .nframe = nframe, .ndim = ndim};
        fake_cuda_dispatch(ctx, nllcrit_cuda_async_forw_fn,
                           THCState_getCurrentStream(state));
        #else //USE_GCD
        /*
        cunn_ClassNLLCriterion_updateGradInput_kernel<scalar_t>
          <<<1, NTHREADS, 0, THCState_getCurrentStream(state)>>>(
            gradInput_data,
            gradOutput_data,
            target_data,
            weights_data,
            total_weight_data,
            reduction == at::Reduction::Mean,
            THCTensor_(size)(state, input, 0),
            THCTensor_(size)(state, input, 1),
            n_classes,
            ignore_index);
         */
        float outputAcc = 0.0, total_weightAcc = 0.0, cur_weight = 0.0;
        int i = 0, t = 0;
        #pragma omp parallel for \
        firstprivate(cur_weight,t,nframe,ndim), \
        reduction(+:outputAcc,total_weightAcc), schedule(static)
        for (i = 0; i < nframe; i++) {
            t = target_data[i];
            if (t != (int)ignore_index) {
                //assert(t >= 0 && t < n_classes);
                cur_weight = weights_data ? weights_data[t] : 1.0;
                outputAcc += -1 * (input_data[i * ndim + t]) * cur_weight;
                total_weightAcc += cur_weight;
            }
        }
        *total_weight_data = total_weightAcc;
        if ((reduction == at::Reduction::Mean) && total_weightAcc > 0)
            *output_data = outputAcc / total_weightAcc;
        else
            *output_data = outputAcc;
        #endif //USE_GCD
    } else
        assert(0); /* shouldn't happen */
    //THCudaCheck(cudaGetLastError());

    if (weights)
        THCudaTensor_free(state, weights); //seems to only free host memory ;-(
    THCIndexTensor_(free)(state, target);
    THCudaTensor_free(state, input);

    LEAVE(hijack_torch_cuda, THNN_CudaClassNLLCriterion_updateOutput);
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *gradOutput_data, *weights_data, *total_weight_data, *gradInput_data;
    THCIndex_t *target_data;
    int64_t ignore_index, reduction;
    int nframe, ndim;
} nllcrit_cuda_async_back_t;

__attribute__((used,noinline))
static void nllcrit_cuda_async_back_fn(void *ctx)
{
    ENTER(hijack_torch_cuda, nllcrit_cuda_async_back_fn);

    nllcrit_cuda_async_back_t c = *((nllcrit_cuda_async_back_t *)ctx);

    const float *gradOutput_data = c.gradOutput_data,
          *weights_data = c.weights_data,
          *total_weight_data = c.total_weight_data;
    float *gradInput_data = c.gradInput_data;
    const THCIndex_t *target_data = c.target_data;
    const int64_t ignore_index = c.ignore_index, reduction = c.reduction;
    const int nframe = c.nframe, ndim = c.ndim;

    if (*total_weight_data <= 0) {
        LEAVE(hijack_torch_cuda, nllcrit_cuda_async_back_fn);
        return;
    }

    int i = 0, t = 0;
    float cur_weight = 0.0;
    const float norm = (reduction == at::Reduction::Mean) ?
                       1. / (*total_weight_data) : 1.;

    #pragma omp parallel for \
    firstprivate(cur_weight,norm,t,nframe,ndim), schedule(static)
    for (i = 0; i < nframe; i++) {
        t = target_data[i];
        if (t != (int)ignore_index) {
            //assert(t >= 0 && t < n_classes);
            cur_weight = weights_data ? weights_data[t] : 1.0;
            gradInput_data[i * ndim + t] =
                -cur_weight * norm * gradOutput_data[0];
        }
    }

    free(ctx);

    LEAVE(hijack_torch_cuda, nllcrit_cuda_async_back_fn);
}
#endif //USE_GCD

void THNN_CudaClassNLLCriterion_updateGradInput(THCState *state,
                                                THCTensor *input,
                                                THCIndexTensor *target,
                                                THCTensor *gradOutput,
                                                THCTensor *gradInput,
                                                int64_t reduction,
                                                THCTensor *weights,
                                                THCTensor *total_weight,
                                                int64_t ignore_index)
{
    ENTER(hijack_torch_cuda, THNN_CudaClassNLLCriterion_updateGradInput);

    int n_dims = THCudaTensor_nDimensionLegacyNoScalars(state, input);
    //int n_classes = THCudaTensor_size(state, input, n_dims - 1);

    THCudaTensor_resizeAs(state, gradInput, input);
    THCudaTensor_zero(state, gradInput);

    /*
    if (weights)
        THCUNN_assertSameGPU(state, 5, weights, input, target, gradInput,
                             total_weight);
    else
        THCUNN_assertSameGPU(state, 4, input, target, gradInput, total_weight);
    */

    //int64_t batch_size =
    //    (n_dims == 1) ? 1 : THCudaTensor_size(state, input, 0);
    //int64_t num_targets =
    //    THCudaLongTensor_sizeLegacyNoScalars(state, target, 0);

    if (reduction == at::Reduction::None && n_dims == 2)
        assert(0);
        // skip this part for now -> not needed for resnet

    weights = weights ? THCudaTensor_newContiguous(state, weights) : NULL;
    target = THCIndexTensor_(newContiguous)(state, target);

    //THCUNN_check_dim_size(state, gradOutput, 1, 0, 1);
    float *gradOutput_data = THCudaTensor_data(state, gradOutput);
    float *weights_data = weights ? THCudaTensor_data(state, weights) : NULL;
    float *gradInput_data = THCudaTensor_data(state, gradInput);
    THCIndex_t  *target_data = THCIndexTensor_(data)(state, target);
    float *total_weight_data = THCudaTensor_data(state, total_weight);

    if (THCudaTensor_nDimensionLegacyNoScalars(state, input) == 1) {
        assert(0);
    } else {
        int nframe = THCudaTensor_size(state, input, 0);
        int ndim = THCudaTensor_size(state, input, 1);
        /*
        cunn_ClassNLLCriterion_updateGradInput_kernel<scalar_t>
          <<<1, NTHREADS, 0, THCState_getCurrentStream(state)>>>(
            gradInput_data,
            gradOutput_data,
            target_data,
            weights_data,
            total_weight_data,
            reduction == at::Reduction::Mean,
            THCTensor_(size)(state, input, 0),
            THCTensor_(size)(state, input, 1),
            n_classes,
            ignore_index);
         */
        #if defined USE_GCD && USE_GCD >= 1
        nllcrit_cuda_async_back_t *ctx;
        assert((ctx = (nllcrit_cuda_async_back_t *)malloc(sizeof(*ctx))));
        *ctx = (nllcrit_cuda_async_back_t)
                    {.gradOutput_data = gradOutput_data,
                     .weights_data = weights_data,
                     .total_weight_data = total_weight_data,
                     .gradInput_data = gradInput_data,
                     .target_data = target_data, .ignore_index = ignore_index,
                     .reduction = reduction, .nframe = nframe, .ndim = ndim};
        fake_cuda_dispatch(ctx, nllcrit_cuda_async_back_fn,
                           THCState_getCurrentStream(state));
        #else //USE_GCD
        if (*total_weight_data <= 0) {
            LEAVE(hijack_torch_cuda,
                  THNN_CudaClassNLLCriterion_updateGradInput);
            return;
        }

        int i = 0, t = 0;
        float cur_weight = 0.0;
        float norm =
            (reduction == at::Reduction::Mean) ? 1. / (*total_weight_data) : 1.;

        #pragma omp parallel for \
        firstprivate(cur_weight,norm,t,nframe,ndim), schedule(static)
        for (i = 0; i < nframe; i++) {
            t = target_data[i];
            if (t != (int)ignore_index) {
                //assert(t >= 0 && t < n_classes);
                cur_weight = weights_data ? weights_data[t] : 1.0;
                gradInput_data[i * ndim + t] =
                    -cur_weight * norm * gradOutput_data[0];
            }
        }
        #endif //USE_GCD
    }
    //THCudaCheck(cudaGetLastError());

    if (weights)
        THCudaTensor_free(state, weights);
    THCIndexTensor_(free)(state, target);

    LEAVE(hijack_torch_cuda, THNN_CudaClassNLLCriterion_updateGradInput);
}
#else //USE_MocCUDA
#endif //USE_MocCUDA
