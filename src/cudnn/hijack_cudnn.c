#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <cblas.h>
#include <omp.h>

#include "hijack_cudnn.h"
#include "utils.h"

#include "addTensor.h"
#include "async.h"

#ifdef REFLAPACK
    #define blasint int
    #define BLASFN(FUNC) cblas_##FUNC
#elif defined FUJITSU
    #define blasint FJ_MATHLIB_TYPE_INT
    #define BLASFN(FUNC) sl_cblas_##FUNC
#elif defined ARMPL
    #define blasint armpl_int_t
    #define BLASFN(FUNC) cblas_##FUNC
#else
    #define BLASFN(FUNC) cblas_##FUNC
#endif

#define OLDV0
//#define NEWV0
#if defined(NEWV0)
#include "batched_blas.h"
#endif

#ifdef DEBUG
static
#else
inline
#endif
int idx(const int d1, const int d2, const int d3, const int d4,
               const int s[4])
{
    return (d1 * s[0] + d2 * s[1] + d3 * s[2] + d4 * s[3]);
}

#if defined USE_GCD && USE_GCD >= 1
static cudnnTensorDescriptor_t
tensDescDeepCpy(const cudnnTensorDescriptor_t desc)
{
    if (!desc)
        return NULL;

    cudnnTensorDescriptor_t tensorDesc;
    assert((tensorDesc = (struct cudnnTensorStruct *)malloc(sizeof(*desc))));
    *tensorDesc = *desc;
    const size_t nbDimsInByte = desc->nbDims * sizeof(int);
    assert(((*tensorDesc).dim = (int *)malloc(nbDimsInByte)));
    memcpy((*tensorDesc).dim, desc->dim, nbDimsInByte);
    assert(((*tensorDesc).stride = (int *)malloc(nbDimsInByte)));
    memcpy((*tensorDesc).stride, desc->stride, nbDimsInByte);

    return tensorDesc;
}

static cudnnFilterDescriptor_t
filtDescDeepCpy(const cudnnFilterDescriptor_t desc)
{
    if (!desc)
        return NULL;

    cudnnFilterDescriptor_t filterDesc;
    assert((filterDesc = (struct cudnnFilterStruct *)malloc(sizeof(*desc))));
    *filterDesc = *desc;
    const size_t nbDimsInByte = desc->nbDims * sizeof(int);
    assert(((*filterDesc).dim = (int *)malloc(nbDimsInByte)));
    memcpy((*filterDesc).dim, desc->dim, nbDimsInByte);

    return filterDesc;
}

static cudnnConvolutionDescriptor_t
convDescDeepCpy(const cudnnConvolutionDescriptor_t desc)
{
    if (!desc)
        return NULL;

    cudnnConvolutionDescriptor_t convDesc;
    assert((convDesc = (struct cudnnConvolutionStruct *)malloc(sizeof(*desc))));
    *convDesc = *desc;
    const size_t arrayLengthInByte = desc->arrayLength * sizeof(int);
    assert(((*convDesc).pad = (int *)malloc(arrayLengthInByte)));
    memcpy((*convDesc).pad, desc->pad, arrayLengthInByte);
    assert(((*convDesc).filterStride = (int *)malloc(arrayLengthInByte)));
    memcpy((*convDesc).filterStride, desc->filterStride, arrayLengthInByte);
    assert(((*convDesc).dilation = (int *)malloc(arrayLengthInByte)));
    memcpy((*convDesc).dilation, desc->dilation, arrayLengthInByte);

    return convDesc;
}
#endif

static void freeTensDesc(cudnnTensorDescriptor_t desc)
{
    if (desc) {
        if (desc->dim)
            free(desc->dim);
        if (desc->stride)
            free(desc->stride);
        free(desc);
    }
}

static void freeFiltDesc(cudnnFilterDescriptor_t desc)
{
    if (desc) {
        if (desc->dim)
            free(desc->dim);
        free(desc);
    }
}

static void freeConvDesc(cudnnConvolutionDescriptor_t desc)
{
    if (desc) {
        if (desc->pad)
            free(desc->pad);
        if (desc->filterStride)
            free(desc->filterStride);
        if (desc->dilation)
            free(desc->dilation);
        free(desc);
    }
}

#ifdef DEBUG
static void
tensDescToStr(const char *str, const cudnnTensorDescriptor_t desc)
{
    fprintf(stderr, "%s = %p", str, (void*)desc);
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (!desc)
        return;
    fprintf(stderr, "{%d,%d,%d", desc->dataType, desc->format, desc->nbDims);
    fprintfIntArray(desc->dim, desc->nbDims);
    fprintfIntArray(desc->stride, desc->nbDims);
    fprintf(stderr, "}");
#endif
}

static void
filtDescToStr(const char *str, const cudnnFilterDescriptor_t desc)
{
    fprintf(stderr, "%s = %p", str, (void*)desc);
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (!desc)
        return;
    fprintf(stderr, "{%d,%d,%d", desc->dataType, desc->format, desc->nbDims);
    fprintfIntArray(desc->dim, desc->nbDims);
    fprintf(stderr, "}");
#endif
}

static void
convDescToStr(const char *str, const cudnnConvolutionDescriptor_t desc)
{
    fprintf(stderr, "%s = %p", str, (void*)desc);
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (!desc)
        return;
    fprintf(stderr, "{%d,", desc->arrayLength);
    fprintfIntArray(desc->pad, desc->arrayLength);
    fprintfIntArray(desc->filterStride, desc->arrayLength);
    fprintfIntArray(desc->dilation, desc->arrayLength);
    fprintf(stderr, "%d,%d,%d,%d", desc->mode, desc->computeType,
            desc->groupCount, desc->mathType);
    fprintf(stderr, "}");
#endif
}
#endif //DEBUG

////////////////////////////////////////////////////////////////////////////////

cudnnStatus_t cudnnActivationBackward()
{
    ENTER(cudnn_lib_entry, cudnnActivationBackward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnActivationBackward);
    return rcs;
}

cudnnStatus_t cudnnActivationForward()
{
    ENTER(cudnn_lib_entry, cudnnActivationForward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnActivationForward);
    return rcs;
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float *tA, *tC;
    float alp, bet;
    cudnnTensorDescriptor_t aDesc, cDesc;
} cudnnAddTensor_async_t;

__attribute__((used,noinline))
static void
cudnnAddTensor_async_fn(void *ctx)
{
    ENTER(cudnn_lib_entry, cudnnAddTensor_async_fn);

    cudnnAddTensor_async_t c = *((cudnnAddTensor_async_t *)ctx);

    addTensorNd(c.alp, c.tA, c.aDesc->dim, c.aDesc->stride, c.aDesc->nbDims,
                c.bet, c.tC, c.cDesc->dim, c.cDesc->stride, c.cDesc->nbDims);

    freeTensDesc(c.aDesc);
    freeTensDesc(c.cDesc);
    free(ctx);

    LEAVE(cudnn_lib_entry, cudnnAddTensor_async_fn);
}
#endif //USE_GCD

/* Tensor Bias addition : C = alpha * A + beta * C  */
cudnnStatus_t cudnnAddTensor(cudnnHandle_t handle,
                             const void *alpha,
                             const cudnnTensorDescriptor_t aDesc,
                             const void *A,
                             const void *beta,
                             const cudnnTensorDescriptor_t cDesc,
                             void *C)
{
    ENTER(cudnn_lib_entry, cudnnAddTensor);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  handle = %p, alpha = %f, ",
            rcs, (void*)handle, *((float*)alpha));
    tensDescToStr("aDesc", aDesc);
    fprintf(stderr, ", A = %p, beta = %f, ", A, *((float*)beta));
    tensDescToStr("cDesc", cDesc);
    fprintf(stderr, ", C = %p\n", C);
    #endif

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    int i = 0;
    if (aDesc->dataType != cDesc->dataType || aDesc->nbDims != cDesc->nbDims) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    for (i = 0; i < aDesc->nbDims; i++)
        if (aDesc->dim[i] != cDesc->dim[i] && aDesc->dim[i] != 1) {
            rcs = CUDNN_STATUS_BAD_PARAM;
            goto exit;
        }

    assert(4 == cDesc->nbDims || CUDNN_DATA_FLOAT == cDesc->dataType);
    //assert both are fully-packed
    for (i = 0; i < aDesc->nbDims-1; i++) {
        assert(aDesc->stride[i] == aDesc->dim[i+1] * aDesc->stride[i+1] &&
               cDesc->stride[i] == cDesc->dim[i+1] * cDesc->stride[i+1]);
    };  assert(1 == aDesc->stride[aDesc->nbDims-1] &&
               1 == cDesc->stride[cDesc->nbDims-1]);

    float *tA = (float *)A;
    float *tC = (float *)C;
    const float alp = *((float*)alpha), bet = *((float*)beta);
    #if defined USE_GCD && USE_GCD >= 1
    cudnnAddTensor_async_t *ctx;
    assert((ctx = (cudnnAddTensor_async_t *)malloc(sizeof(*ctx))));
    *ctx = (cudnnAddTensor_async_t){.tA = tA, .tC = tC, .alp = alp, .bet = bet,
                                    .aDesc = tensDescDeepCpy(aDesc),
                                    .cDesc = tensDescDeepCpy(cDesc)};
    fake_cuda_dispatch(ctx, cudnnAddTensor_async_fn, (*handle).stream);
    #else //USE_GCD
    addTensorNd(alp, tA, aDesc->dim, aDesc->stride, aDesc->nbDims,
                bet, tC, cDesc->dim, cDesc->stride, cDesc->nbDims);
    #endif //USE_GCD

#else
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnAddTensor, handle, alpha,
                          aDesc, A, beta, cDesc, C);
#endif

exit:
    LEAVE(cudnn_lib_entry, cudnnAddTensor);
    return rcs;
}

cudnnStatus_t cudnnBatchNormalizationBackward()
{
    ENTER(cudnn_lib_entry, cudnnBatchNormalizationBackward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnBatchNormalizationBackward);
    return rcs;
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float alphaDataDiff, betaDataDiff, alphaParamDiff, betaParamDiff;
    cudnnTensorDescriptor_t xDesc;
    void *xData;
    cudnnTensorDescriptor_t yDesc;
    void *yData;
    cudnnTensorDescriptor_t dyDesc;
    void *dyData;
    cudnnTensorDescriptor_t dzDesc;
    void *dzData;
    cudnnTensorDescriptor_t dxDesc;
    void *dxData;
    cudnnTensorDescriptor_t dBnScaleBiasDesc;
    void *bnScaleData, *bnBiasData, *dBnScaleData, *dBnBiasData;
    double epsilon;
    void *savedMean, *savedInvVariance;
} cudnnBNormBackwardEx_async_t;

__attribute__((used,noinline))
static void
cudnnBNormBackwardEx_async_fn(void *ctx)
{
    ENTER(cudnn_lib_entry, cudnnBNormBackwardEx_async_fn);

    cudnnBNormBackwardEx_async_t cc = *((cudnnBNormBackwardEx_async_t *)ctx);

    int i, n, c;
    const int N = cc.dyDesc->dim[0], C = cc.dyDesc->dim[1];
    const int H = cc.dyDesc->dim[2], W = cc.dyDesc->dim[3];

    const float alpDataDiff = cc.alphaDataDiff;
    const float betDataDiff = cc.betaDataDiff;
    const float alpParamDiff = cc.alphaParamDiff;
    const float betParamDiff = cc.betaParamDiff;
    const float /* *Beta = (float*)bnBiasData,*/ *Gamma = (float*)cc.bnScaleData;
    float *dBeta = (float*)cc.dBnBiasData, *dGamma = (float*)cc.dBnScaleData;
    float *dmean, *dvar;
    assert((dmean = (float*)malloc(C * sizeof(float)))); // dmu
    assert((dvar = (float*)malloc(C * sizeof(float))));  // dsigma
    /*const double epsilon = cc.epsilon;*/
    float *mean, *invVar;
    if (!cc.savedMean /*&& !cc.savedInvVariance*/) {
        //mean = malloc(...);
        //invVar = malloc(...);
        assert(0); // would need to recalc here...
    } else {
        mean = (float*)cc.savedMean;
        invVar = (float*)cc.savedInvVariance;
    }

    float *X = (float*)cc.xData, *dY = (float*)cc.dyData, *dX = (float*)cc.dxData;
    float *t_X, *t_dY, *t_dX;

    prescale_output(betDataDiff, dX, cc.dxDesc->dim[0] * cc.dxDesc->stride[0]);
    prescale_output(betParamDiff, dGamma,
                    cc.dBnScaleBiasDesc->dim[0] * cc.dBnScaleBiasDesc->stride[0]);
    prescale_output(betParamDiff, dBeta,
                    cc.dBnScaleBiasDesc->dim[0] * cc.dBnScaleBiasDesc->stride[0]);

    //https://medium.com/samkirkiles/spatial-batchnorm-backprop-implementation-notes-8ccde1ac62a2
    #pragma omp parallel for private(t_X,t_dY,t_dX,n,i), \
    firstprivate(N,C,H,W,alpDataDiff,alpParamDiff), schedule(static)
    for (c = 0; c < C; c++) {
//        fprintf(stderr, "dbg[%d]: c[%d/%d]\n",omp_get_thread_num(),c,C);
        float tmp_sum1 = 0.0;
        float tmp_sum2 = 0.0;
        float tmp_sum3 = 0.0, tmp;
        for (n = 0; n < N; n++) {
//            fprintf(stderr, "dbg[%d]: n[%d/%d]\n",omp_get_thread_num(),n,N);
            t_X = X + (n * cc.xDesc->stride[0] + c * cc.xDesc->stride[1]);
            t_dY = dY + (n * cc.dyDesc->stride[0] + c * cc.dyDesc->stride[1]);
            for (i = 0; i < H * W; i++) {
//                fprintf(stderr, "dbg[%d]: i[%d/%d] %p %p %p %p\n",omp_get_thread_num(),i,H*W,t_dY+i,t_X+i,mean+c,invVar+c);
                tmp_sum1 += t_dY[i];
                tmp = t_X[i] - mean[c];
                tmp_sum2 += t_dY[i] * tmp;
                tmp_sum3 += tmp;
                //tmp_sum2 += t_dY[i] * ((t_X[i] - mean[c]) * invVar[c]);
            }
        }
        tmp_sum2 *= invVar[c];
        dBeta[c] = alpParamDiff * tmp_sum1;
        dGamma[c] = alpParamDiff * tmp_sum2;
        //
//        tmp_sum1 = 0.0;
//        for (n = 0; n < N; n++) {
////            fprintf(stderr, "dbg1[%d]: n[%d/%d]\n",omp_get_thread_num(),n,N);
//            t_X = X + (n * cc.xDesc->stride[0] + c * cc.xDesc->stride[1]);
//            t_dY = dY + (n * cc.dyDesc->stride[0] + c * cc.dyDesc->stride[1]);
//            for (i = 0; i < H * W; i++)
//// {
////                fprintf(stderr, "dbg1[%d]: i[%d/%d] %p %p %p %p %p %f\n",omp_get_thread_num(),i,H*W,t_dY+i,t_X+i,mean+c,invVar+c,Gamma,0.0);
//                tmp_sum1 += t_dY[i] * (t_X[i] - mean[c]);
////                tmp_sum1 += (t_dY[i] * Gamma[c] /*dxhat*/) * (t_X[i] - mean[c]);
////                          * -0.5 * (invVar[c]*invVar[c]*invVar[c] /*(sig+ep)**-1.5*/);
////            }
//        }
        float tmp_sum4 = tmp_sum2;
        tmp_sum4 *= Gamma[c];
        tmp_sum4 *= -0.5 * (invVar[c]*invVar[c] /*(sig+ep)**-1.5*/);
        //tmp_sum3 *= -0.5 * (invVar[c]*invVar[c]*invVar[c] /*(sig+ep)**-1.5*/);
        dvar[c] = tmp_sum4;
        //
        float tmp_sum5 = tmp_sum1;
        float tmp_sum6 = -2.0 * tmp_sum3;
//        for (n = 0; n < N; n++) {
//            t_X = X + (n * cc.xDesc->stride[0] + c * cc.xDesc->stride[1]);
//            t_dY = dY + (n * cc.dyDesc->stride[0] + c * cc.dyDesc->stride[1]);
//            for (i = 0; i < H * W; i++) {
////                tmp_sum5 += (t_dY[i] * Gamma[c] /*dxhat*/);
//                tmp_sum6 += -2.0 * (t_X[i] - mean[c]);
////                tmp_sum1 +=
////                    (t_dY[i] * Gamma[c] /*dxhat*/) * -invVar[c]
////                    + dvar[c] * (-2.0 * (t_X[i] - mean[c])) / (N * H * W);
//            }
//        }
        tmp_sum5 *= Gamma[c] * -invVar[c];
        tmp_sum6 *= dvar[c] / (N * H * W);
        tmp_sum5 += tmp_sum6;
        dmean[c] = tmp_sum5;
        //
        for (n = 0; n < N; n++) {
            t_X = X + (n * cc.xDesc->stride[0] + c * cc.xDesc->stride[1]);
            t_dX = dX + (n * cc.dxDesc->stride[0] + c * cc.dxDesc->stride[1]);
            t_dY = dY + (n * cc.dyDesc->stride[0] + c * cc.dyDesc->stride[1]);
            for (i = 0; i < H * W; i++)
                t_dX[i] = alpDataDiff *
                    ( (t_dY[i] * Gamma[c] /*dxhat*/) * invVar[c]
                      + dvar[c] * (2.0 * (t_X[i] - mean[c])) / (N * H * W)
                      + dmean[c] / (N * H * W) );
        }
    }

    free(dmean);
    free(dvar);
    if (!cc.savedMean /*&& !cc.savedInvVariance*/) {
        free(mean);
        free(invVar);
    }

    freeTensDesc(cc.xDesc);
    freeTensDesc(cc.yDesc);
    freeTensDesc(cc.dyDesc);
    freeTensDesc(cc.dzDesc);
    freeTensDesc(cc.dxDesc);
    freeTensDesc(cc.dBnScaleBiasDesc);
    free(ctx);

    LEAVE(cudnn_lib_entry, cudnnBNormBackwardEx_async_fn);
}
#endif //USE_GCD

cudnnStatus_t cudnnBatchNormalizationBackwardEx(cudnnHandle_t handle,
                                                cudnnBatchNormMode_t mode,
                                                cudnnBatchNormOps_t bnOps,
                                                const void *alphaDataDiff,
                                                const void *betaDataDiff,
                                                const void *alphaParamDiff,
                                                const void *betaParamDiff,
                                                const cudnnTensorDescriptor_t xDesc,
                                                const void *xData,
                                                const cudnnTensorDescriptor_t yDesc,
                                                const void *yData,
                                                const cudnnTensorDescriptor_t dyDesc,
                                                const void *dyData,
                                                const cudnnTensorDescriptor_t dzDesc,
                                                void *dzData,
                                                const cudnnTensorDescriptor_t dxDesc,
                                                void *dxData,
                                                const cudnnTensorDescriptor_t dBnScaleBiasDesc,
                                                const void *bnScaleData,
                                                const void *bnBiasData,
                                                void *dBnScaleData, //resultBnScaleDiff
                                                void *dBnBiasData,  //resultBnBiasDiff
                                                double epsilon,
                                                const void *savedMean,
                                                const void *savedInvVariance,
                                                cudnnActivationDescriptor_t activationDesc,
                                                void *workSpace,
                                                size_t workSpaceSizeInBytes,
                                                void *reserveSpace,
                                                size_t reserveSpaceSizeInBytes)
{
    ENTER(cudnn_lib_entry, cudnnBatchNormalizationBackwardEx);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

    #ifdef DEBUG
    fprintf(stderr,
            "\tret=%d  handle = %p, mode = %d, bnOps = %d, "
            "alphaDataDiff = %f, betaDataDiff = %f, "
            "alphaParamDiff = %f, betaParamDiff = %f, ",
            rcs, handle, mode, bnOps,
            *((float*)alphaDataDiff), *((float*)betaDataDiff),
            *((float*)alphaParamDiff), *((float*)betaParamDiff));
    tensDescToStr("xDesc", xDesc);
    fprintf(stderr, ", xData = %p, ", xData);
    tensDescToStr("yDesc", yDesc);
    fprintf(stderr, ", yData = %p, ", yData);
    tensDescToStr("dyDesc", dyDesc);
    fprintf(stderr, ", dyData = %p, ", dyData);
    tensDescToStr("dzDesc", dzDesc);
    fprintf(stderr, ", dzData = %p, ", dzData);
    tensDescToStr("dxDesc", dxDesc);
    fprintf(stderr, "}, dxData = %p, ", dxData);
    tensDescToStr("dBnScaleBiasDesc", dBnScaleBiasDesc);
    fprintf(stderr,
            ", bnScaleData = %p, bnBiasData = %p, "
            "dBnScaleData = %p, dBnBiasData = %p, epsilon = %f, "
            "savedMean = %p, savedInvVariance = %p, activationDesc = %p, "
            "workSpace = %p, workSpaceSizeInBytes = %lu, "
            "reserveSpace = %p, reserveSpaceSizeInBytes = %lu\n",
            bnScaleData, bnBiasData,
            dBnScaleData, dBnBiasData, epsilon,
            savedMean, savedInvVariance, (void*)activationDesc,
            workSpace, workSpaceSizeInBytes,
            reserveSpace, reserveSpaceSizeInBytes);
    #endif

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (NULL == alphaDataDiff || NULL == betaDataDiff
        || NULL == alphaParamDiff || NULL == betaParamDiff
        || NULL == xData || NULL == dyData || NULL == dxData
        || NULL == bnScaleData || NULL == dBnScaleData || NULL == dBnBiasData) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (xDesc && (xDesc->nbDims < 4 || xDesc->nbDims > 5)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (yDesc && (yDesc->nbDims < 4 || yDesc->nbDims > 5)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (dxDesc && (dxDesc->nbDims < 4 || dxDesc->nbDims > 5)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (xDesc && yDesc &&
        (xDesc->nbDims != yDesc->nbDims
         || xDesc->dataType != yDesc->dataType)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (xDesc && dxDesc && (xDesc->nbDims != dxDesc->nbDims
                            || xDesc->dataType != dxDesc->dataType)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    int d, xDim = 1, dBnDim = 1;
    for (d = 1; d < xDesc->nbDims; d++)
        xDim *= xDesc->dim[d];
    for (d = 0; d < dBnScaleBiasDesc->nbDims; d++)
        dBnDim *= dBnScaleBiasDesc->dim[d];
    if (((CUDNN_BATCHNORM_SPATIAL == mode
          || CUDNN_BATCHNORM_SPATIAL_PERSISTENT == mode)
         && dBnDim != xDesc->dim[1])
        || (CUDNN_BATCHNORM_PER_ACTIVATION == mode && dBnDim != xDim)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if ((NULL == savedMean && NULL != savedInvVariance)
        || (NULL != savedMean&& NULL == savedInvVariance)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (epsilon < CUDNN_BN_MIN_EPSILON) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }

    assert(CUDNN_BATCHNORM_SPATIAL == mode
           && CUDNN_BATCHNORM_OPS_BN == bnOps); //only support this for now

    assert(CUDNN_DATA_FLOAT == xDesc->dataType
           && CUDNN_DATA_FLOAT == dyDesc->dataType
           && CUDNN_DATA_FLOAT == dxDesc->dataType
           && CUDNN_DATA_FLOAT == dBnScaleBiasDesc->dataType);

    assert(4 == xDesc->nbDims);     //FIXME: do 5d later

    #if defined USE_GCD && USE_GCD >= 1
    cudnnBNormBackwardEx_async_t *ctx;
    assert((ctx = (cudnnBNormBackwardEx_async_t *)malloc(sizeof(*ctx))));
    *ctx = (cudnnBNormBackwardEx_async_t){.alphaDataDiff = *((float*)alphaDataDiff),
                                          .betaDataDiff = *((float*)betaDataDiff),
                                          .alphaParamDiff = *((float*)alphaParamDiff),
                                          .betaParamDiff = *((float*)betaParamDiff),
                                          .xDesc = tensDescDeepCpy(xDesc),
                                          .xData = (void*)xData,
                                          .yDesc = tensDescDeepCpy(yDesc),
                                          .yData = (void*)yData,
                                          .dyDesc = tensDescDeepCpy(dyDesc),
                                          .dyData = (void*)dyData,
                                          .dzDesc = tensDescDeepCpy(dzDesc),
                                          .dzData = dzData,
                                          .dxDesc = tensDescDeepCpy(dxDesc),
                                          .dxData = dxData,
                                          .dBnScaleBiasDesc = tensDescDeepCpy(dBnScaleBiasDesc),
                                          .bnScaleData = (void*)bnScaleData,
                                          .bnBiasData = (void*)bnBiasData,
                                          .dBnScaleData = dBnScaleData,
                                          .dBnBiasData = dBnBiasData,
                                          .epsilon = epsilon,
                                          .savedMean = (void*)savedMean,
                                          .savedInvVariance = (void*)savedInvVariance};
    fake_cuda_dispatch(ctx, cudnnBNormBackwardEx_async_fn, (*handle).stream);
    #else //USE_GCD
    int i, n, c;
    const int N = dyDesc->dim[0], C = dyDesc->dim[1];
    const int H = dyDesc->dim[2], W = dyDesc->dim[3];

    const float alpDataDiff = *((float*)alphaDataDiff);
    const float betDataDiff = *((float*)betaDataDiff);
    const float alpParamDiff = *((float*)alphaParamDiff);
    const float betParamDiff = *((float*)betaParamDiff);
    const float /* *Beta = (float*)bnBiasData,*/ *Gamma = (float*)bnScaleData;
    float *dBeta = (float*)dBnBiasData, *dGamma = (float*)dBnScaleData;
    float *dmean, *dvar;
    assert((dmean = (float*)malloc(C * sizeof(float)))); // dmu
    assert((dvar = (float*)malloc(C * sizeof(float))));  // dsigma
    float *mean, *invVar;
    if (!savedMean /*&& !savedInvVariance*/) {
        //mean = malloc(...);
        //invVar = malloc(...);
        assert(0); // would need to recalc here...
    } else {
        mean = (float*)savedMean;
        invVar = (float*)savedInvVariance;
    }

    float *X = (float*)xData, *dY = (float*)dyData, *dX = (float*)dxData;
    float *t_X, *t_dY, *t_dX, tmp_sum1, tmp_sum2;

    prescale_output(betDataDiff, dX, dxDesc->dim[0] * dxDesc->stride[0]);
    prescale_output(betParamDiff, dGamma,
                    dBnScaleBiasDesc->dim[0] * dBnScaleBiasDesc->stride[0]);
    prescale_output(betParamDiff, dBeta,
                    dBnScaleBiasDesc->dim[0] * dBnScaleBiasDesc->stride[0]);

    //https://medium.com/samkirkiles/spatial-batchnorm-backprop-implementation-notes-8ccde1ac62a2
    #pragma omp parallel for private(t_X,t_dY,t_dX,tmp_sum1,tmp_sum2,n,i), \
    firstprivate(N,C,H,W,alpDataDiff,alpParamDiff,epsilon), schedule(static)
    for (c = 0; c < C; c++) {
        tmp_sum1 = 0.0;
        tmp_sum2 = 0.0;
        for (n = 0; n < N; n++) {
            t_X = X + (n*xDesc->stride[0] + c*xDesc->stride[1]);
            t_dY = dY + (n*dyDesc->stride[0] + c*dyDesc->stride[1]);
            for (i = 0; i < H * W; i++) {
                tmp_sum1 += t_dY[i];
                tmp_sum2 += t_dY[i] * ((t_X[i] - mean[c]) * invVar[c]);
            }
        }
        dBeta[c] = alpParamDiff * tmp_sum1;
        dGamma[c] = alpParamDiff * tmp_sum2;
        //
        tmp_sum1 = 0.0;
        for (n = 0; n < N; n++) {
            t_X = X + (n*xDesc->stride[0] + c*xDesc->stride[1]);
            t_dY = dY + (n*dyDesc->stride[0] + c*dyDesc->stride[1]);
            for (i = 0; i < H * W; i++)
                tmp_sum1 +=
                    (t_dY[i] * Gamma[c] /*dxhat*/) * (t_X[i] - mean[c])
                    * -0.5 * (invVar[c]*invVar[c]*invVar[c] /*(sig+ep)**-1.5*/);
        }
        dvar[c] = tmp_sum1;
        //
        tmp_sum1 = 0.0;
        for (n = 0; n < N; n++) {
            t_X = X + (n*xDesc->stride[0] + c*xDesc->stride[1]);
            t_dY = dY + (n*dyDesc->stride[0] + c*dyDesc->stride[1]);
            for (i = 0; i < H * W; i++)
                tmp_sum1 +=
                    (t_dY[i] * Gamma[c] /*dxhat*/) * -invVar[c]
                    + dvar[c] * (-2.0 * (t_X[i] - mean[c])) / (N * H * W);
        }
        dmean[c] = tmp_sum1;
        //
        for (n = 0; n < N; n++) {
            t_X = X + (n*xDesc->stride[0] + c*xDesc->stride[1]);
            t_dX = dX + (n*dxDesc->stride[0] + c*dxDesc->stride[1]);
            t_dY = dY + (n*dyDesc->stride[0] + c*dyDesc->stride[1]);
            for (i = 0; i < H * W; i++)
                t_dX[i] = alpDataDiff *
                    ( (t_dY[i] * Gamma[c] /*dxhat*/) * invVar[c]
                      + dvar[c] * (2.0 * (t_X[i] - mean[c])) / (N * H * W)
                      + dmean[c] / (N * H * W) );
        }
    }

    free(dmean);
    free(dvar);
    if (!savedMean /*&& !savedInvVariance*/) {
        free(mean);
        free(invVar);
    }
    #endif //USE_GCD

#else
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1,
                          cudnnBatchNormalizationBackwardEx,
                          handle, mode, bnOps,
                          alphaDataDiff, betaDataDiff,
                          alphaParamDiff, betaParamDiff,
                          xDesc, xData, yDesc, yData,
                          dyDesc, dyData, dzDesc, dzData, dxDesc, dxData,
                          dBnScaleBiasDesc, bnScaleData, bnBiasData,
                          dBnScaleData, dBnBiasData,
                          epsilon,
                          savedMean, savedInvVariance, activationDesc,
                          workSpace, workSpaceSizeInBytes,
                          reserveSpace, reserveSpaceSizeInBytes);
#endif

exit:
    LEAVE(cudnn_lib_entry, cudnnBatchNormalizationBackwardEx);
    return rcs;
}

cudnnStatus_t cudnnBatchNormalizationForwardInference()
{
    ENTER(cudnn_lib_entry, cudnnBatchNormalizationForwardInference);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnBatchNormalizationForwardInference);
    return rcs;
}

cudnnStatus_t cudnnBatchNormalizationForwardTraining()
{
    ENTER(cudnn_lib_entry, cudnnBatchNormalizationForwardTraining);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnBatchNormalizationForwardTraining);
    return rcs;
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float alpha, beta;
    cudnnTensorDescriptor_t xDesc;
    void *xData;
    cudnnTensorDescriptor_t zDesc;
    void *zData;
    cudnnTensorDescriptor_t yDesc;
    void *yData;
    cudnnTensorDescriptor_t bnScaleBiasMeanVarDesc;
    void *bnScale, *bnBias;
    double exponentialAverageFactor;
    void *resultRunningMean, *resultRunningVariance;
    double epsilon;
    void *resultSaveMean, *resultSaveInvVariance;
} cudnnBNormForwardTrainingEx_async_t;

__attribute__((used,noinline))
static void
cudnnBNormForwardTrainingEx_async_fn(void *ctx)
{
    ENTER(cudnn_lib_entry, cudnnBNormForwardTrainingEx_async_fn);

    cudnnBNormForwardTrainingEx_async_t cc =
        *((cudnnBNormForwardTrainingEx_async_t *)ctx);

    int i, n, c;
    const int N = cc.xDesc->dim[0], C = cc.xDesc->dim[1];
    const int H = cc.xDesc->dim[2], W = cc.xDesc->dim[3];

    const float alp = cc.alpha, bet = cc.beta;
    float *Beta = (float*)cc.bnBias, *Gamma = (float*)cc.bnScale;
    float *runningMean = (float*)cc.resultRunningMean;
    float *runningVariance = (float*)cc.resultRunningVariance;
    const double exponentialAverageFactor = cc.exponentialAverageFactor,
          epsilon = cc.epsilon;

    float *X = (float*)cc.xData, *tX, *Y = (float*)cc.yData, *tY;
    float tmp_mean, *mean, tmp_var, *invVar;
    if (!cc.resultSaveMean /*&& !cc.resultSaveInvVariance*/) {
        assert((mean = (float*)malloc(C * sizeof(float))));
        assert((invVar = (float*)malloc(C * sizeof(float))));
    } else {
        mean = (float*)cc.resultSaveMean;
        invVar = (float*)cc.resultSaveInvVariance;
    }

    prescale_output(bet, Y, cc.yDesc->dim[0] * cc.yDesc->stride[0]);

    //https://papers.nips.cc/paper/7996-understanding-batch-normalization.pdf
    //https://gluon.mxnet.io/chapter04_convolutional-neural-networks/cnn-batch-norm-scratch.html
    //https://medium.com/samkirkiles/spatial-batchnorm-backprop-implementation-notes-8ccde1ac62a2
    #pragma omp parallel for private(tX,tY,tmp_mean,tmp_var,n,i), \
    firstprivate(N,C,H,W,alp,epsilon,exponentialAverageFactor), schedule(static)
    for (c = 0; c < C; c++) {
        tmp_mean = 0.0;
        for (n = 0; n < N; n++) {
            tX = X + (n * cc.xDesc->stride[0] + c * cc.xDesc->stride[1]);
            for (i = 0; i < H * W; i++)
                tmp_mean += tX[i];
        }
        mean[c] = tmp_mean / (N * H * W);
    //}
    //#pragma omp parallel for private(tX,tmp_var,n,i), schedule(static)
    //for (c = 0; c < C; c++) {
        tmp_var = 0.0;
        for (n = 0; n < N; n++) {
            tX = X + (n * cc.xDesc->stride[0] + c * cc.xDesc->stride[1]);
            for (i = 0; i < H * W; i++)
                tmp_var += (tX[i] - mean[c]) * (tX[i] - mean[c]);
        }
        tmp_var /= (N * H * W);
        invVar[c] = (float)(1.0 / sqrt(tmp_var + epsilon));
        //var[c] = tmp_var / (N * H * W);
    //}
    //#pragma omp parallel for schedule(static)
    //for (c = 0; c < C; c++) {
        float aGi_c = alp * Gamma[c] * invVar[c], aB_c = alp * Beta[c];
        for (n = 0; n < N; n++) {
            tX = X + (n * cc.xDesc->stride[0] + c * cc.xDesc->stride[1]);
            tY = Y + (n * cc.yDesc->stride[0] + c * cc.yDesc->stride[1]);
            for (i = 0; i < H * W; i++) {
                tY[i] += aGi_c * (tX[i] - mean[c]) + aB_c;
                //tY[i] +=
                //    alp * (Gamma[c] * (tX[i] - mean[c]) * invVar[c] + Beta[c]);
                //tY[i] += alp *
                //    (float)(Gamma[c] *
                //            ((tX[i] - mean[c]) / sqrt(var[c] + epsilon))
                //            + Beta[c]);
            }
        }
    //
        if (runningMean /*&& runningVariance*/) {
            runningMean[c] = runningMean[c]
                             * (float)(1.0 - exponentialAverageFactor)
                             + mean[c] * exponentialAverageFactor;
            runningVariance[c] = runningVariance[c]
                                 * (float)(1.0 - exponentialAverageFactor)
                                 + tmp_var * exponentialAverageFactor;
                                 //+ var[c] * exponentialAverageFactor;
        }
    }
    if (!cc.resultSaveMean /*&& !cc.resultSaveInvVariance*/) {
        free(mean);
        free(invVar);
    }

    freeTensDesc(cc.xDesc);
    freeTensDesc(cc.zDesc);
    freeTensDesc(cc.yDesc);
    freeTensDesc(cc.bnScaleBiasMeanVarDesc);
    free(ctx);

    LEAVE(cudnn_lib_entry, cudnnBNormForwardTrainingEx_async_fn);
}
#endif //USE_GCD

cudnnStatus_t cudnnBatchNormalizationForwardTrainingEx(cudnnHandle_t handle,
                                                       cudnnBatchNormMode_t mode,
                                                       cudnnBatchNormOps_t bnOps,
                                                       const void *alpha,
                                                       const void *beta,
                                                       const cudnnTensorDescriptor_t xDesc,
                                                       const void *xData,
                                                       const cudnnTensorDescriptor_t zDesc,
                                                       const void *zData,
                                                       const cudnnTensorDescriptor_t yDesc,
                                                       void *yData,
                                                       const cudnnTensorDescriptor_t bnScaleBiasMeanVarDesc,
                                                       const void *bnScale,
                                                       const void *bnBias,
                                                       double exponentialAverageFactor,
                                                       void *resultRunningMean,
                                                       void *resultRunningVariance,
                                                       double epsilon,
                                                       void *resultSaveMean,
                                                       void *resultSaveInvVariance,
                                                       cudnnActivationDescriptor_t activationDesc,
                                                       void *workspace,
                                                       size_t workSpaceSizeInBytes,
                                                       void *reserveSpace,
                                                       size_t reserveSpaceSizeInBytes)
{
    ENTER(cudnn_lib_entry, cudnnBatchNormalizationForwardTrainingEx);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

    #ifdef DEBUG
    fprintf(stderr,
            "\tret=%d  handle = %p, mode = %d, bnOps = %d, "
            "alpha = %f, beta = %f, ",
            rcs, handle, mode, bnOps, *((float*)alpha), *((float*)beta));
    tensDescToStr("xDesc", xDesc);
    fprintf(stderr, ", xData = %p, ", xData);
    tensDescToStr("zDesc", zDesc);
    fprintf(stderr, ", zData = %p, ", zData);
    tensDescToStr("yDesc", yDesc);
    fprintf(stderr, ", yData = %p, ", yData);
    tensDescToStr("bnScaleBiasMeanVarDesc", bnScaleBiasMeanVarDesc);
    fprintf(stderr,
            ", bnScale = %p, bnBias = %p, exponentialAverageFactor = %f"
            "resultRunningMean = %p, resultRunningVariance = %p, epsilon = %f, "
            "resultSaveMean = %p, resultSaveInvVariance = %p, "
            "activationDesc = %p, "
            "workSpace = %p, workSpaceSizeInBytes = %lu, "
            "reserveSpace = %p, reserveSpaceSizeInBytes = %lu\n",
            bnScale, bnBias, exponentialAverageFactor,
            resultRunningMean, resultRunningVariance, epsilon,
            resultSaveMean, resultSaveInvVariance,
            (void*)activationDesc,
            workspace, workSpaceSizeInBytes,
            reserveSpace, reserveSpaceSizeInBytes);
    #endif

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (NULL == alpha || NULL == beta || NULL == xData || NULL == yData
        || NULL == bnScale || NULL == bnBias) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (xDesc->nbDims < 4 || xDesc->nbDims > 5
        || yDesc->nbDims < 4 || yDesc->nbDims > 5
        || xDesc->nbDims != yDesc->nbDims
        || xDesc->dataType != yDesc->dataType) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    int d, xDim = 1, bnDim = 1;
    for (d = 1; d < xDesc->nbDims; d++)
        xDim *= xDesc->dim[d];
    for (d = 0; d < bnScaleBiasMeanVarDesc->nbDims; d++)
        bnDim *= bnScaleBiasMeanVarDesc->dim[d];
    if (((CUDNN_BATCHNORM_SPATIAL == mode
          || CUDNN_BATCHNORM_SPATIAL_PERSISTENT == mode)
         && bnDim != xDesc->dim[1])
        || (CUDNN_BATCHNORM_PER_ACTIVATION == mode && bnDim != xDim)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if ((NULL == resultRunningMean && NULL != resultRunningVariance)
        || (NULL != resultRunningMean && NULL == resultRunningVariance)
        || (NULL == resultSaveMean && NULL != resultSaveInvVariance)
        || (NULL != resultSaveMean && NULL == resultSaveInvVariance)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (epsilon < CUDNN_BN_MIN_EPSILON) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (zDesc && CUDNN_BATCHNORM_OPS_BN_ADD_ACTIVATION == bnOps
        && !(zDesc->nbDims == xDesc->nbDims
             && 0 == memcmp(zDesc->dim, xDesc->dim,
                            xDesc->nbDims * sizeof(int)))) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }

    assert(CUDNN_BATCHNORM_SPATIAL == mode
           && CUDNN_BATCHNORM_OPS_BN == bnOps); //only support this for now

    assert(CUDNN_DATA_FLOAT == bnScaleBiasMeanVarDesc->dataType);
    assert(4 == xDesc->nbDims);     //FIXME: do 5d later

    #if defined USE_GCD && USE_GCD >= 1
    cudnnBNormForwardTrainingEx_async_t *ctx;
    assert((ctx = (cudnnBNormForwardTrainingEx_async_t *)malloc(sizeof(*ctx))));
    *ctx = (cudnnBNormForwardTrainingEx_async_t){.alpha = *((float*)alpha),
                                                 .beta = *((float*)beta),
                                                 .xDesc = tensDescDeepCpy(xDesc),
                                                 .xData = (void*)xData,
                                                 .zDesc = tensDescDeepCpy(zDesc),
                                                 .zData = (void*)zData,
                                                 .yDesc = tensDescDeepCpy(yDesc),
                                                 .yData = (void*)yData,
                                                 .bnScaleBiasMeanVarDesc = tensDescDeepCpy(bnScaleBiasMeanVarDesc),
                                                 .bnScale = (void*)bnScale,
                                                 .bnBias = (void*)bnBias,
                                                 .exponentialAverageFactor = exponentialAverageFactor,
                                                 .resultRunningMean = resultRunningMean,
                                                 .resultRunningVariance = resultRunningVariance,
                                                 .epsilon = epsilon,
                                                 .resultSaveMean = resultSaveMean,
                                                 .resultSaveInvVariance = resultSaveInvVariance};
    fake_cuda_dispatch(ctx, cudnnBNormForwardTrainingEx_async_fn,
                       (*handle).stream);
    #else //USE_GCD
    int i, n, c;
    const int N = xDesc->dim[0], C = xDesc->dim[1];
    const int H = xDesc->dim[2], W = xDesc->dim[3];

    const float alp = *((float*)alpha), bet = *((float*)beta);
    float *Beta = (float*)bnBias, *Gamma = (float*)bnScale;
    float *runningMean = (float*)resultRunningMean;
    float *runningVariance = (float*)resultRunningVariance;

    float *X = (float*)xData, *tX, *Y = (float*)yData, *tY;
    float tmp_mean, *mean, tmp_var, *invVar;
    if (!resultSaveMean /*&& !resultSaveInvVariance*/) {
        assert((mean = (float*)malloc(C * sizeof(float))));
        assert((invVar = (float*)malloc(C * sizeof(float))));
    } else {
        mean = (float*)resultSaveMean;
        invVar = (float*)resultSaveInvVariance;
    }

    prescale_output(bet, Y, yDesc->dim[0] * yDesc->stride[0]);

    //https://papers.nips.cc/paper/7996-understanding-batch-normalization.pdf
    //https://gluon.mxnet.io/chapter04_convolutional-neural-networks/cnn-batch-norm-scratch.html
    //https://medium.com/samkirkiles/spatial-batchnorm-backprop-implementation-notes-8ccde1ac62a2
    #pragma omp parallel for private(tX,tY,tmp_mean,tmp_var,n,i), \
    firstprivate(N,C,H,W,alp,epsilon,exponentialAverageFactor), schedule(static)
    for (c = 0; c < C; c++) {
        tmp_mean = 0.0;
        for (n = 0; n < N; n++) {
            tX = X + (n*xDesc->stride[0] + c*xDesc->stride[1]);
            for (i = 0; i < H * W; i++)
                tmp_mean += tX[i];
        }
        mean[c] = tmp_mean / (N * H * W);
    //}
    //#pragma omp parallel for private(tX,tmp_var,n,i), schedule(static)
    //for (c = 0; c < C; c++) {
        tmp_var = 0.0;
        for (n = 0; n < N; n++) {
            tX = X + (n*xDesc->stride[0] + c*xDesc->stride[1]);
            for (i = 0; i < H * W; i++)
                tmp_var += (tX[i] - mean[c]) * (tX[i] - mean[c]);
        }
        tmp_var /= (N * H * W);
        invVar[c] = (float)(1.0 / sqrt(tmp_var + epsilon));
        //var[c] = tmp_var / (N * H * W);
    //}
    //#pragma omp parallel for schedule(static)
    //for (c = 0; c < C; c++) {
        for (n = 0; n < N; n++) {
            tX = X + (n*xDesc->stride[0] + c*xDesc->stride[1]);
            tY = Y + (n*yDesc->stride[0] + c*yDesc->stride[1]);
            for (i = 0; i < H * W; i++) {
                tY[i] +=
                    alp * (Gamma[c] * (tX[i] - mean[c]) * invVar[c] + Beta[c]);
                //tY[i] += alp *
                //    (float)(Gamma[c] *
                //            ((tX[i] - mean[c]) / sqrt(var[c] + epsilon))
                //            + Beta[c]);
            }
        }
    //
        if (runningMean /*&& runningVariance*/) {
            runningMean[c] = runningMean[c]
                             * (float)(1.0 - exponentialAverageFactor)
                             + mean[c] * exponentialAverageFactor;
            runningVariance[c] = runningVariance[c]
                                 * (float)(1.0 - exponentialAverageFactor)
                                 + tmp_var * exponentialAverageFactor;
                                 //+ var[c] * exponentialAverageFactor;
        }
    }
    if (!resultSaveMean /*&& !resultSaveInvVariance*/) {
        free(mean);
        free(invVar);
    }
    #endif //USE_GCD

#else
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1,
                          cudnnBatchNormalizationForwardTrainingEx,
                          handle, mode, bnOps, alpha, beta,
                          xDesc, xData, zDesc, zData, yDesc, yData,
                          bnScaleBiasMeanVarDesc, bnScale, bnBias,
                          exponentialAverageFactor,
                          resultRunningMean, resultRunningVariance,
                          epsilon,
                          resultSaveMean, resultSaveInvVariance,
                          activationDesc,
                          workspace, workSpaceSizeInBytes,
                          reserveSpace, reserveSpaceSizeInBytes);
#endif

exit:
    LEAVE(cudnn_lib_entry, cudnnBatchNormalizationForwardTrainingEx);
    return rcs;
}

cudnnStatus_t cudnnConvolutionBackwardBias()
{
    ENTER(cudnn_lib_entry, cudnnConvolutionBackwardBias);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnConvolutionBackwardBias);
    return rcs;
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float alpha;
    cudnnFilterDescriptor_t wDesc;
    void *w;
    cudnnTensorDescriptor_t dyDesc;
    void *dy;
    cudnnConvolutionDescriptor_t convDesc;
    float beta;
    cudnnTensorDescriptor_t dxDesc;
    void *dx;
} cudnnConvolutionBackwardData_async_t;

__attribute__((used,noinline))
static void
cudnnConvolutionBackwardData_async_fn(void *ctx)
{
    ENTER(cudnn_lib_entry, cudnnConvolutionBackwardData_async_fn);

    cudnnConvolutionBackwardData_async_t cc =
        *((cudnnConvolutionBackwardData_async_t *)ctx);

    const float alp = cc.alpha, bet = cc.beta;
    const float *W = (float *)cc.w, *dY = (float *)cc.dy;
    float *dX = (float*)cc.dx, *t_dX;

    const int Wstride[4] = {cc.wDesc->dim[1]*cc.wDesc->dim[2]*cc.wDesc->dim[3],
                            cc.wDesc->dim[2]*cc.wDesc->dim[3],
                            cc.wDesc->dim[3],
                            1};
    prescale_output(bet, dX, cc.dxDesc->dim[0] * cc.dxDesc->stride[0]);

    if (2 == cc.convDesc->arrayLength &&
        cc.convDesc->filterStride[0] == cc.convDesc->filterStride[1] &&
        cc.convDesc->filterStride[0] > 0 && cc.convDesc->filterStride[0] < 3 &&
        1 == cc.convDesc->dilation[0] && 1 == cc.convDesc->dilation[1]) {
        //https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/making_faster.html

        assert(4 == cc.dxDesc->nbDims);

        /*
            filter_col = np.reshape(w,(F,-1)).T
            dout_i = dout[i,:,:,:]
            dbias_sum = np.reshape(dout_i,(F,-1))
            dbias_sum = dbias_sum.T
            dmul = dbias_sum
            dim_col = dmul.dot(filter_col.T)
              => dim_col = dout_i**T X w
            dx_padded = col2im_back(dim_col,H_prime,W_prime,stride,HH,WW,C)
            dx[i,:,:,:] = dx_padded[:,pad_num:H+pad_num,pad_num:W+pad_num]
              => dx[i,:,:,:] =
                    dx[i,:,h_start:h_start+hh,w_start:w_start+ww] += dim_col[j,:]
        */
        const blasint m = cc.dyDesc->dim[2] * cc.dyDesc->dim[3];
        const blasint k = cc.dyDesc->dim[1];
        const blasint n = cc.wDesc->dim[1] * cc.wDesc->dim[2] * cc.wDesc->dim[3];
        const blasint lda = m, ldb = n, ldc = n,
              hh = cc.wDesc->dim[2], ww = cc.wDesc->dim[3];

        #pragma omp parallel
        {
            int i;
            float *mat;
            assert((mat = (float*)malloc(m * n * sizeof(float))));

            #pragma omp for firstprivate(alp,m,k,n,lda,ldb,ldc,hh,ww), \
            schedule(static)
            for (i = 0; i < cc.dxDesc->dim[0]; i++) {
                TIME_GEMM_START(RowMaj, 1, 0, m, n, k, alp, 0.0, lda, ldb, ldc);
                BLASFN(sgemm)(CblasRowMajor, CblasTrans, CblasNoTrans, m, n, k,
                              alp, &dY[i * cc.dyDesc->stride[0]], lda, W, ldb,
                              0.0, mat, ldc);
                TIME_GEMM_STOP(core, m, n, k);

                #ifndef VV0 //this version is faster... :-(
                const int p_h = cc.convDesc->pad[0], p_w = cc.convDesc->pad[1],
                      s_h = cc.convDesc->filterStride[0],
                      s_w = cc.convDesc->filterStride[1],
                      H = cc.dxDesc->dim[2], W = cc.dxDesc->dim[3];
                const int H_prime = (H + 2 * p_h - hh) / s_h + 1,
                      W_prime = (W + 2 * p_w - ww) / s_w + 1;
                float *dx_padded = col2im_back(mat, H_prime, W_prime,
                                               cc.convDesc->filterStride[0],
                                               cc.wDesc->dim[2],
                                               cc.wDesc->dim[3],
                                               cc.wDesc->dim[1]);
                assert(dx_padded);
                int c, dx_h, dx_w;
                for (c = 0; c < cc.dxDesc->dim[1]; c++) {
                    t_dX = dX + i * cc.dxDesc->stride[0]
                           + c * cc.dxDesc->stride[1];
                    const float *t_dx_padded =
                        dx_padded
                        + c * ((H_prime - 1) * s_h + hh)
                            * ((W_prime - 1) * s_w + ww);
                    for (dx_h = 0; dx_h < cc.dxDesc->dim[2]; dx_h++)
                        for (dx_w = 0; dx_w < cc.dxDesc->dim[3]; dx_w++)
                            t_dX[dx_h * H + dx_w] =
                                t_dx_padded[(dx_h + p_h)
                                            * ((H_prime - 1) * s_h + hh)
                                            + (dx_w + p_w)];
                }
                free(dx_padded);
                #else //VV0
                int channel, j, dx_h, dx_w;
                const int p_h = cc.convDesc->pad[0], p_w = cc.convDesc->pad[1],
                      s_h = cc.convDesc->filterStride[0],
                      s_w = cc.convDesc->filterStride[1],
                      dx_height = cc.dxDesc->dim[2],
                      dx_width = cc.dxDesc->dim[3],
                      H_prime = (dx_height + 2 * p_h - hh) / s_h + 1,
                      W_prime = (dx_width + 2 * p_w - ww) / s_w + 1;
                for (channel = 0; channel < cc.dxDesc->dim[1]; channel++) {
                    t_dX = dX + i * cc.dxDesc->stride[0]
                              + channel * cc.dxDesc->stride[1];
                    for (dx_h = 0; dx_h < dx_height; dx_h++) {
                        for (dx_w = 0; dx_w < dx_width; dx_w++) {
                            //fprintf(stderr, "dX(%d,%d) [pad=%d,str=%d,hh=%d,ww=%d]\n",dx_h,dx_w,p_h,s_h,hh,ww);
                            //find right 'blocks' which contrib to dX[dx_h,dx_w]
                            for (j = 0; j < H_prime * W_prime; j++) {
                                const int h_start = (j / W_prime) * s_h,
                                    w_start = (j % W_prime) * s_w;
                                //jump down if too far up
                                if (h_start + hh - 1 < dx_h + p_h) {
                                    j += W_prime - 1;
                                    continue;
                                }
                                //step right if too far left
                                if (w_start + ww - 1 < dx_w + p_w)
                                    continue;
                                //jump all the way to right if passed window
                                if (w_start > dx_w + p_w) {
                                    j += W_prime - j % W_prime - 1;
                                    continue;
                                }
                                //if we are below, then don't further search
                                if (h_start > dx_h + p_h)
                                    break;
                                const float *t_mat = mat + j * n + channel * (hh * ww); //row
                                //fprintf(stderr, "    j=%d h_start=%d w_start=%d idx(%d,%d)\n",j,h_start,w_start,(dx_h + p_h) - h_start,dx_w + p_w - w_start);
                                *t_dX += t_mat[((dx_h + p_h) - h_start) * ww
                                               + (dx_w + p_w - w_start)];
                            }
                            t_dX++;
                        }
                    }
                }
                #endif //VV0
            }
            free(mat);
        }
    } else {
        assert(0);

        //https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/convolution_layer.html
        int i, filter, channel, dx_h, dx_w, k_h, k_w;
        const int n = cc.dxDesc->dim[0],
              /*p_h = cc.convDesc->pad[0], p_w = cc.convDesc->pad[1],*/
              s_h = cc.convDesc->filterStride[0],
              s_w = cc.convDesc->filterStride[1],
              num_filters = cc.wDesc->dim[0],
              input_channels = cc.dxDesc->dim[1],
              dx_height = cc.dxDesc->dim[2], dx_width = cc.dxDesc->dim[3],
              kernel_height = cc.wDesc->dim[2], kernel_width = cc.wDesc->dim[3];

        #pragma omp parallel for \
        private(i,filter,dx_h,dx_w,channel,k_h,k_w), \
        firstprivate(alp,n,s_h,s_w,num_filters,input_channels, \
                     dx_height,dx_width,kernel_height,kernel_width), \
        schedule(static)
        for (i = 0; i < n; i++)
            for (filter = 0; filter < num_filters; filter++)
                for (dx_h = 0; dx_h < dx_height; dx_h += s_h)
                    for (dx_w = 0; dx_w < dx_width; dx_w += s_w)
                        for (channel = 0; channel < input_channels; channel++)
                            for (k_h = -kernel_height / 2;
                                 k_h < kernel_height - kernel_height / 2;
                                 k_h++)
                                for (k_w = -kernel_width / 2;
                                     k_w < kernel_width - kernel_width / 2;
                                     k_w++) {
                                    if (dx_h+k_h < 0 ||
                                        dx_h+k_h >= cc.dxDesc->dim[2] ||
                                        dx_w+k_w < 0 ||
                                        dx_w+k_w >= cc.dxDesc->dim[3])
                                        continue;
                                    dX[idx(i,channel,dx_h+k_h,dx_w+k_w,
                                           cc.dxDesc->stride)] += alp *
                                    (dY[idx(i,filter,dx_h/s_h,dx_w/s_w,
                                            cc.dyDesc->stride)] *
                                     W[idx(filter,channel,
                                           k_h+kernel_height/2,
                                           k_w+kernel_width/2,
                                           Wstride)]);
                                }
/*                            for (k_h = 0; k_h < kernel_height; k_h++)
                                for (k_w = 0; k_w < kernel_width; k_w++) {
                                    if (dx_h+k_h-p_h < 0 ||
                                        dx_h+k_h-p_h >= cc.dxDesc->dim[2] ||
                                        dx_w+k_w-p_w < 0 ||
                                        dx_w+k_w-p_w >= cc.dxDesc->dim[3])
                                        continue;
                                    dX[idx(i,channel,
                                           (dx_h+k_h-p_h),(dx_w+k_w-p_w),
                                           cc.dxDesc->stride)] += alp *
                                    (dY[idx(i,filter,dx_h/s_h,dx_w/s_w,
                                            cc.dyDesc->stride)] *
                                     W[idx(filter,channel,k_h,k_w,Wstride)]);
                                }*/
    }
    //fprintf(stderr,"cudnnConvolutionBackwardData out(dx):\n");
    //fprintfFloatArray(dX, cc.dxDesc->dim[0]*cc.dxDesc->dim[1]*cc.dxDesc->dim[2]*cc.dxDesc->dim[3]);

    freeFiltDesc(cc.wDesc);
    freeTensDesc(cc.dyDesc);
    freeConvDesc(cc.convDesc);
    freeTensDesc(cc.dxDesc);
    free(ctx);

    LEAVE(cudnn_lib_entry, cudnnConvolutionBackwardData_async_fn);
}
#endif //USE_GCD

cudnnStatus_t cudnnConvolutionBackwardData(cudnnHandle_t handle,
                                           const void *alpha,
                                           const cudnnFilterDescriptor_t wDesc,
                                           const void *w,
                                           const cudnnTensorDescriptor_t dyDesc,
                                           const void *dy,
                                           const cudnnConvolutionDescriptor_t convDesc,
                                           cudnnConvolutionBwdDataAlgo_t algo,
                                           void *workSpace,
                                           size_t workSpaceSizeInBytes,
                                           const void *beta,
                                           const cudnnTensorDescriptor_t dxDesc,
                                           void *dx)
{
    ENTER(cudnn_lib_entry, cudnnConvolutionBackwardData);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  handle = %p, alpha = %f, ",
            rcs, (void*)handle, *((float*)alpha));
    filtDescToStr("wDesc", wDesc);
    fprintf(stderr, ", w = %p, ", w);
    tensDescToStr("dyDesc", dyDesc);
    fprintf(stderr, ", dy = %p, ", dy);
    convDescToStr("convDesc", convDesc);
    fprintf(stderr, ", algo = %d, workSpace = %p, workSpaceSizeInBytes = %lu, "
            "beta = %f, ", algo, workSpace, workSpaceSizeInBytes,
            *((float*)beta));
    tensDescToStr("dxDesc", dxDesc);
    fprintf(stderr, ", dx = %p\n", dx);
    #endif

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (NULL == handle || NULL == dyDesc || NULL == wDesc || NULL == convDesc
        || NULL == dxDesc || NULL == dy || NULL == w || NULL == dx
        || NULL == alpha || NULL == beta) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (wDesc->nbDims < 3
        || wDesc->nbDims != dxDesc->nbDims || wDesc->nbDims != dyDesc->nbDims
        || wDesc->dataType != dxDesc->dataType
        || wDesc->dataType != dyDesc->dataType) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (wDesc->nbDims < 4 || wDesc->nbDims > 5
        || dxDesc->nbDims < 4 || dxDesc->nbDims > 5
        || dyDesc->nbDims < 4 || dyDesc->nbDims > 5) {
        rcs = CUDNN_STATUS_NOT_SUPPORTED;
        goto exit;
    }
    int j;
    for (j = 0; j < wDesc->nbDims; j++)
        if (dxDesc->stride[j] < 0 || dyDesc->stride[j] < 0) {
            rcs = CUDNN_STATUS_NOT_SUPPORTED;
            goto exit;
        }

    assert(CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_PRECOMP_GEMM == (int)algo);
    assert(CUDNN_DATA_FLOAT == wDesc->dataType &&
           CUDNN_DATA_FLOAT == dyDesc->dataType &&
           CUDNN_DATA_FLOAT == dxDesc->dataType);
    assert(CUDNN_CROSS_CORRELATION == convDesc->mode &&
           CUDNN_DATA_FLOAT == convDesc->computeType &&
           1 == convDesc->groupCount &&
           CUDNN_DEFAULT_MATH == convDesc->mathType);
    assert(CUDNN_TENSOR_NCHW == dxDesc->format);
    assert(4 == dxDesc->nbDims);

    #if defined USE_GCD && USE_GCD >= 1
    cudnnConvolutionBackwardData_async_t *ctx;
    assert((ctx = (cudnnConvolutionBackwardData_async_t *)malloc(sizeof(*ctx))));
    *ctx = (cudnnConvolutionBackwardData_async_t){.alpha = *((float*)alpha),
                                                  .wDesc = filtDescDeepCpy(wDesc),
                                                  .w = (void*)w,
                                                  .dyDesc = tensDescDeepCpy(dyDesc),
                                                  .dy = (void*)dy,
                                                  .convDesc = convDescDeepCpy(convDesc),
                                                  .beta = *((float*)beta),
                                                  .dxDesc = tensDescDeepCpy(dxDesc),
                                                  .dx = dx};
    fake_cuda_dispatch(ctx, cudnnConvolutionBackwardData_async_fn,
                       (*handle).stream);
    #else //USE_GCD
    const float alp = *((float*)alpha), bet = *((float*)beta);
    const float *W = (float *)w, *dY = (float *)dy;
    float *dX = (float*)dx, *t_dX;

    int i;
    const int Wstride[4] = {wDesc->dim[1]*wDesc->dim[2]*wDesc->dim[3],
                            wDesc->dim[2]*wDesc->dim[3],
                            wDesc->dim[3],
                            1};
    prescale_output(bet, dX, dxDesc->dim[0] * dxDesc->stride[0]);

    if (2 == convDesc->arrayLength &&
        convDesc->filterStride[0] == convDesc->filterStride[1] &&
        convDesc->filterStride[0] > 0 && convDesc->filterStride[0] < 3 &&
        1 == convDesc->dilation[0] && 1 == convDesc->dilation[1]) {
        //https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/making_faster.html

        assert(4 == dxDesc->nbDims);

        /*
            filter_col = np.reshape(w,(F,-1)).T
            dout_i = dout[i,:,:,:]
            dbias_sum = np.reshape(dout_i,(F,-1))
            dbias_sum = dbias_sum.T
            dmul = dbias_sum
            dim_col = dmul.dot(filter_col.T)
              => dim_col = dout_i**T X w
            dx_padded = col2im_back(dim_col,H_prime,W_prime,stride,HH,WW,C)
            dx[i,:,:,:] = dx_padded[:,pad_num:H+pad_num,pad_num:W+pad_num]
              => dx[i,:,:,:] =
                    dx[i,:,h_start:h_start+hh,w_start:w_start+ww] += dim_col[j,:]
        */
        const blasint m = dyDesc->dim[2] * dyDesc->dim[3];
        const blasint k = dyDesc->dim[1];
        const blasint n = wDesc->dim[1] * wDesc->dim[2] * wDesc->dim[3];
        const blasint lda = m, ldb = n, ldc = n,
              hh = wDesc->dim[2], ww = wDesc->dim[3];

        #pragma omp parallel
        {
            float *mat;
            assert((mat = (float*)malloc(m * n * sizeof(float))));

            #pragma omp for firstprivate(alp,m,k,n,lda,ldb,ldc,hh,ww), \
            schedule(static)
            for (i = 0; i < dxDesc->dim[0]; i++) {
                TIME_GEMM_START(RowMaj, 1, 0, m, n, k, alp, 0.0, lda, ldb, ldc);
                BLASFN(sgemm)(CblasRowMajor, CblasTrans, CblasNoTrans, m, n, k,
                              alp, &dY[i * dyDesc->stride[0]], lda, W, ldb,
                              0.0, mat, ldc);
                TIME_GEMM_STOP(core, m, n, k);

                #ifndef VV0 //this version is faster... :-(
                const int p_h = convDesc->pad[0], p_w = convDesc->pad[1],
                      s_h = convDesc->filterStride[0],
                      s_w = convDesc->filterStride[1],
                      H = dxDesc->dim[2], W = dxDesc->dim[3];
                const int H_prime = (H + 2 * p_h - hh) / s_h + 1,
                      W_prime = (W + 2 * p_w - ww) / s_w + 1;
                float *dx_padded = col2im_back(mat, H_prime, W_prime,
                                               convDesc->filterStride[0],
                                               wDesc->dim[2], wDesc->dim[3],
                                               wDesc->dim[1]);
                assert(dx_padded);
                int c, dx_h, dx_w;
                for (c = 0; c < dxDesc->dim[1]; c++) {
                    t_dX = dX + i * dxDesc->stride[0] + c * dxDesc->stride[1];
                    const float *t_dx_padded =
                        dx_padded
                        + c * ((H_prime - 1) * s_h + hh)
                            * ((W_prime - 1) * s_w + ww);
                    for (dx_h = 0; dx_h < dxDesc->dim[2]; dx_h++)
                        for (dx_w = 0; dx_w < dxDesc->dim[3]; dx_w++)
                            t_dX[dx_h * H + dx_w] =
                                t_dx_padded[(dx_h + p_h)
                                            * ((H_prime - 1) * s_h + hh)
                                            + (dx_w + p_w)];
                }
                free(dx_padded);
                #else //VV0
                int channel, j, dx_h, dx_w;
                const int p_h = convDesc->pad[0], p_w = convDesc->pad[1],
                      s_h = convDesc->filterStride[0],
                      s_w = convDesc->filterStride[1],
                      dx_height = dxDesc->dim[2], dx_width = dxDesc->dim[3],
                      H_prime = (dx_height + 2 * p_h - hh) / s_h + 1,
                      W_prime = (dx_width + 2 * p_w - ww) / s_w + 1;
                for (channel = 0; channel < dxDesc->dim[1]; channel++) {
                    t_dX = dX + i * dxDesc->stride[0]
                              + channel * dxDesc->stride[1];
                    for (dx_h = 0; dx_h < dx_height; dx_h++) {
                        for (dx_w = 0; dx_w < dx_width; dx_w++) {
                            //fprintf(stderr, "dX(%d,%d) [pad=%d,str=%d,hh=%d,ww=%d]\n",dx_h,dx_w,p_h,s_h,hh,ww);
                            //find right 'blocks' which contrib to dX[dx_h,dx_w]
                            for (j = 0; j < H_prime * W_prime; j++) {
                                const int h_start = (j / W_prime) * s_h,
                                    w_start = (j % W_prime) * s_w;
                                //jump down if too far up
                                if (h_start + hh - 1 < dx_h + p_h) {
                                    j += W_prime - 1;
                                    continue;
                                }
                                //step right if too far left
                                if (w_start + ww - 1 < dx_w + p_w)
                                    continue;
                                //jump all the way to right if passed window
                                if (w_start > dx_w + p_w) {
                                    j += W_prime - j % W_prime - 1;
                                    continue;
                                }
                                //if we are below, then don't further search
                                if (h_start > dx_h + p_h)
                                    break;
                                const float *t_mat = mat + j * n + channel * (hh * ww); //row
                                //fprintf(stderr, "    j=%d h_start=%d w_start=%d idx(%d,%d)\n",j,h_start,w_start,(dx_h + p_h) - h_start,dx_w + p_w - w_start);
                                *t_dX += t_mat[((dx_h + p_h) - h_start) * ww
                                               + (dx_w + p_w - w_start)];
                            }
                            t_dX++;
                        }
                    }
                }
                #endif //VV0
            }
            free(mat);
        }
    } else {
        //https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/convolution_layer.html
        int filter, channel, dx_h, dx_w, k_h, k_w;
        const int n = dxDesc->dim[0],
              p_h = convDesc->pad[0], p_w = convDesc->pad[1],
              s_h = convDesc->filterStride[0], s_w = convDesc->filterStride[1],
              num_filters = wDesc->dim[0], input_channels = dxDesc->dim[1],
              dx_height = dxDesc->dim[2], dx_width = dxDesc->dim[3],
              kernel_height = wDesc->dim[2], kernel_width = wDesc->dim[3];

        #pragma omp parallel for \
        private(i,filter,dx_h,dx_w,channel,k_h,k_w), \
        firstprivate(alp,n,p_h,p_w,s_h,s_w,num_filters,input_channels, \
                     dx_height,dx_width,kernel_height,kernel_width), \
        schedule(static)
        for (i = 0; i < n; i++)
            for (filter = 0; filter < num_filters; filter++)
                for (dx_h = 0; dx_h < dx_height; dx_h += s_h)
                    for (dx_w = 0; dx_w < dx_width; dx_w += s_w)
                        for (channel = 0; channel < input_channels; channel++)
                            for (k_h = 0; k_h < kernel_height; k_h++)
                                for (k_w = 0; k_w < kernel_width; k_w++) {
                                    if (dx_h+k_h-p_h < 0 ||
                                        dx_h+k_h-p_h >= dxDesc->dim[2] ||
                                        dx_w+k_w-p_w < 0 ||
                                        dx_w+k_w-p_w >= dxDesc->dim[3])
                                        continue;
                                    dX[idx(i,channel,
                                           (dx_h+k_h-p_h),(dx_w+k_w-p_w),
                                           dxDesc->stride)] += alp *
                                    (dY[idx(i,filter,dx_h/s_h,dx_w/s_w,
                                            dyDesc->stride)] *
                                     W[idx(filter,channel,k_h,k_w, Wstride)]);
                                }
    }
    //fprintf(stderr,"cudnnConvolutionBackwardData out(dx):\n");
    //fprintfFloatArray(dX, dxDesc->dim[0]*dxDesc->dim[1]*dxDesc->dim[2]*dxDesc->dim[3]);
    #endif //USE_GCD

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnConvolutionBackwardData,
                          handle, alpha, wDesc, w, dyDesc, dy, convDesc, algo,
                          workSpace, workSpaceSizeInBytes, beta, dxDesc, dx);

#endif //USE_MocCUDA

exit:
    LEAVE(cudnn_lib_entry, cudnnConvolutionBackwardData);
    return rcs;
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float alpha;
    cudnnTensorDescriptor_t xDesc;
    void *x;
    cudnnTensorDescriptor_t dyDesc;
    void *dy;
    cudnnConvolutionDescriptor_t convDesc;
    float beta;
    cudnnFilterDescriptor_t dwDesc;
    void *dw;
} cudnnConvolutionBackwardFilter_async_t;

__attribute__((used,noinline))
static void
cudnnConvolutionBackwardFilter_async_fn(void *ctx)
{
    ENTER(cudnn_lib_entry, cudnnConvolutionBackwardFilter_async_fn);

    cudnnConvolutionBackwardFilter_async_t cc =
        *((cudnnConvolutionBackwardFilter_async_t *)ctx);

    const float alp = cc.alpha, bet = cc.beta;
    const float *X = (float *)cc.x, *dY = (float *)cc.dy;
    float *dW = (float*)cc.dw;

    const int dWstride[4] = {cc.dwDesc->dim[1]*cc.dwDesc->dim[2]*cc.dwDesc->dim[3],
                             cc.dwDesc->dim[2]*cc.dwDesc->dim[3],
                             cc.dwDesc->dim[3],
                             1};
    prescale_output(bet, dW, cc.dwDesc->dim[0] * dWstride[0]);

    int i;
    if (2 == cc.convDesc->arrayLength &&
        cc.convDesc->filterStride[0] == cc.convDesc->filterStride[1] &&
        cc.convDesc->filterStride[0] > 0 && cc.convDesc->filterStride[0] < 3 &&
        1 == cc.convDesc->dilation[0] && 1 == cc.convDesc->dilation[1]) {
        //https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/making_faster.html

        assert(4 == cc.xDesc->nbDims);

        /*
            im = x[i,:,:,:]
            im_col = im2col(im_pad,HH,WW,stride)
            dout_i = dout[i,:,:,:]
            dbias_sum = np.reshape(dout_i,(F,-1))
            dbias_sum = dbias_sum.T
            dmul = dbias_sum
            dfilter_col = (im_col.T).dot(dmul)
            dw += np.reshape(dfilter_col.T,(F,C,HH,WW))
              => dw += dfilter_col**T
                        = (im_col**T * dbias_sum**T)**T
                        = dbias_sum * im_col //transpose rules
                        = dout_i * im_col
        */
        const int dWc_stride = cc.dwDesc->dim[0] * dWstride[0],
              omp_max_thr = omp_get_max_threads();
        float *dW_col = (float*)malloc(min(cc.xDesc->dim[0], omp_max_thr)
                                       * (dWc_stride * sizeof(float)));
        assert(dW_col);
        float fac = 0.0; //to accumulate, go to 1.0 after 1st round
        #pragma omp parallel for firstprivate(dWc_stride,fac), schedule(static)
        for (i = 0; i < cc.xDesc->dim[0]; i++) {
            float *mat;
            if (1 == cc.convDesc->filterStride[0])
                mat = im2col_padded_stride1(X, cc.xDesc->dim, cc.xDesc->stride,
                                            cc.convDesc->pad, cc.dwDesc->dim, i,
                                            NULL);
            else
                mat = im2col_padded_stride2(X, cc.xDesc->dim, cc.xDesc->stride,
                                            cc.convDesc->pad, cc.dwDesc->dim, i,
                                            NULL);
            assert(mat);

            const blasint m = cc.dyDesc->dim[1];
            const blasint k = cc.dyDesc->dim[2] * cc.dyDesc->dim[3];
            const blasint n = cc.dwDesc->dim[1] * cc.dwDesc->dim[2] * cc.dwDesc->dim[3];
            const blasint lda = k, ldb = k, ldc = n;
            TIME_GEMM_START(RowMaj, 0, 1, m, n, k, 1.0, fac, lda, ldb, ldc);
            BLASFN(sgemm)(CblasRowMajor, CblasNoTrans, CblasTrans, m, n, k,
                          1.0, &dY[i * cc.dyDesc->stride[0]], lda, mat, ldb,
                          fac, &dW_col[omp_get_thread_num() * dWc_stride], ldc);
            TIME_GEMM_STOP(core, m, n, k);
            free(mat);
            if (0.0 == fac)
                fac = 1.0;
        }
        //tree-like reduce
        int gap;
        const int reduce_len = min(cc.xDesc->dim[0], omp_max_thr);
        for(gap = 1; gap < reduce_len; gap *= 2) {
            #pragma omp parallel for firstprivate(gap,reduce_len), \
            schedule(static,1)
            for (i = 0; i < reduce_len - gap; i += 2 * gap) {
                //dW_col[i * dWc_stride] += dW_col[(i+gap) * dWc_stride];
                BLASFN(saxpy)(dWc_stride,
                              1.0, &dW_col[(i + gap) * dWc_stride], 1,
                              &dW_col[i * dWc_stride], 1);
            }
        }
        //dW += alp * dW_col[0];
        cblas_saxpy(((blasint)dWc_stride), alp, dW_col, ((blasint)1), dW,
                    ((blasint)1));
        free(dW_col);
   } else {
        assert(0);

        //https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/convolution_layer.html
        int filter, channel, dy_h, dy_w, k_h, k_w;
        const int n = cc.xDesc->dim[0],
              /*p_h = cc.convDesc->pad[0], p_w = cc.convDesc->pad[1],*/
              s_h = cc.convDesc->filterStride[0],
              s_w = cc.convDesc->filterStride[1],
              num_filters = cc.dwDesc->dim[0], input_channels = cc.xDesc->dim[1],
              dy_height = cc.dyDesc->dim[2], dy_width = cc.dyDesc->dim[3],
              kernel_height = cc.dwDesc->dim[2], kernel_width = cc.dwDesc->dim[3];

        #pragma omp parallel for \
        private(i,filter,channel,dy_h,dy_w,k_h,k_w), \
        firstprivate(alp,n,s_h,s_w,num_filters,input_channels, \
                     dy_height,dy_width,kernel_height,kernel_width), \
        schedule(static)
        for (i = 0; i < n; i++)
            for (filter = 0; filter < num_filters; filter++)
                for (dy_h = 0; dy_h < dy_height; dy_h++)
                    for (dy_w = 0; dy_w < dy_width; dy_w++)
                        for (channel = 0; channel < input_channels; channel++)
                            for (k_h = -kernel_height / 2;
                                 k_h < kernel_height - kernel_height / 2;
                                 k_h++)
                                for (k_w = -kernel_width / 2;
                                     k_w < kernel_width - kernel_width / 2;
                                     k_w++) {
                                    if (dy_h*s_h+k_h < 0 ||
                                        dy_h*s_h+k_h >= cc.xDesc->dim[2] ||
                                        dy_w*s_w+k_w < 0 ||
                                        dy_w*s_w+k_w >= cc.xDesc->dim[3])
                                        continue;
                                    dW[idx(filter,channel,
                                           (k_h+kernel_height/2),
                                           (k_w+kernel_width/2),
                                           dWstride)] += alp *
                                    dY[idx(i,filter,dy_h,dy_w,
                                           cc.dyDesc->stride)]
                                    * X[idx(i,channel,
                                            dy_h*s_h+k_h,
                                            dy_w*s_w+k_w,
                                            cc.xDesc->stride)];
                                }
/*                            for (k_h = 0; k_h < kernel_height; k_h++)
                                for (k_w = 0; k_w < kernel_width; k_w++) {
                                    if (dy_h*s_h+k_h-p_h < 0 ||
                                        dy_h*s_h+k_h-p_h >= cc.xDesc->dim[2] ||
                                        dy_w*s_w+k_w-p_w < 0 ||
                                        dy_w*s_w+k_w-p_w >= cc.xDesc->dim[3])
                                        continue;
                                    dW[idx(filter,channel,k_h,k_w,
                                           dWstride)] += alp *
                                    dY[idx(i,filter,dy_h,dy_w,cc.dyDesc->stride)]
                                    * X[idx(i,channel, (dy_h*s_h+k_h-p_h),
                                            (dy_w*s_w+k_w-p_w), cc.xDesc->stride)];
                                }*/
    }
    //fprintf(stderr,"cudnnConvolutionBackwardFilter out(dw):\n");
    //fprintfFloatArray(dW, cc.dwDesc->dim[0]*cc.dwDesc->dim[1]*cc.dwDesc->dim[2]*cc.dwDesc->dim[3]);

    freeTensDesc(cc.xDesc);
    freeTensDesc(cc.dyDesc);
    freeConvDesc(cc.convDesc);
    freeFiltDesc(cc.dwDesc);
    free(ctx);

    LEAVE(cudnn_lib_entry, cudnnConvolutionBackwardFilter_async_fn);
}
#endif //USE_GCD

cudnnStatus_t cudnnConvolutionBackwardFilter(cudnnHandle_t handle,
                                             const void *alpha,
                                             const cudnnTensorDescriptor_t xDesc,
                                             const void *x,
                                             const cudnnTensorDescriptor_t dyDesc,
                                             const void *dy,
                                             const cudnnConvolutionDescriptor_t convDesc,
                                             cudnnConvolutionBwdFilterAlgo_t algo,
                                             void *workSpace,
                                             size_t workSpaceSizeInBytes,
                                             const void *beta,
                                             const cudnnFilterDescriptor_t dwDesc,
                                             void *dw)
{
    ENTER(cudnn_lib_entry, cudnnConvolutionBackwardFilter);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  handle = %p, alpha = %f, ",
            rcs, (void*)handle, *((float*)alpha));
    tensDescToStr("xDesc", xDesc);
    fprintf(stderr, ", x = %p, ", x);
    tensDescToStr("dyDesc", dyDesc);
    fprintf(stderr, ", dy = %p, ", dy);
    convDescToStr("convDesc", convDesc);
    fprintf(stderr, ", algo = %d, workSpace = %p, workSpaceSizeInBytes = %lu, "
            "beta = %f, ", algo, workSpace, workSpaceSizeInBytes,
            *((float*)beta));
    filtDescToStr("dwDesc", dwDesc);
    fprintf(stderr, ", dw = %p\n", dw);
    #endif

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (NULL == handle || NULL == xDesc || NULL == dyDesc
        || NULL == convDesc || NULL == dwDesc || NULL == x || NULL == dy
        || NULL == dw || NULL == alpha || NULL == beta) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (xDesc->nbDims < 3
        || xDesc->nbDims != dyDesc->nbDims || xDesc->nbDims != dwDesc->nbDims
        || xDesc->dataType != dyDesc->dataType
        || xDesc->dataType != dwDesc->dataType) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (xDesc->nbDims < 4 || xDesc->nbDims > 5
        || dyDesc->nbDims < 4 || dyDesc->nbDims > 5
        || dwDesc->nbDims < 4 || dwDesc->nbDims > 5) {
        rcs = CUDNN_STATUS_NOT_SUPPORTED;
        goto exit;
    }
    int j;
    for (j = 0; j < xDesc->nbDims; j++)
        if (xDesc->stride[j] < 0 || dyDesc->stride[j] < 0) {
            rcs = CUDNN_STATUS_NOT_SUPPORTED;
            goto exit;
        }

    ///////  run im2col + gemm   ////
    assert(CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_PRECOMP_GEMM == (int)algo);
    assert(CUDNN_DATA_FLOAT == xDesc->dataType &&
           CUDNN_DATA_FLOAT == dyDesc->dataType &&
           CUDNN_DATA_FLOAT == dwDesc->dataType);
    assert(CUDNN_CROSS_CORRELATION == convDesc->mode &&
           CUDNN_DATA_FLOAT == convDesc->computeType &&
           1 == convDesc->groupCount &&
           CUDNN_DEFAULT_MATH == convDesc->mathType);
    assert(CUDNN_TENSOR_NCHW == dwDesc->format);
    assert(4 == dwDesc->nbDims);

    #if defined USE_GCD && USE_GCD >= 1
    cudnnConvolutionBackwardFilter_async_t *ctx;
    assert((ctx = (cudnnConvolutionBackwardFilter_async_t *)malloc(sizeof(*ctx))));
    *ctx = (cudnnConvolutionBackwardFilter_async_t){.alpha = *((float*)alpha),
                                                    .xDesc = tensDescDeepCpy(xDesc),
                                                    .x = (void*)x,
                                                    .dyDesc = tensDescDeepCpy(dyDesc),
                                                    .dy = (void*)dy,
                                                    .convDesc = convDescDeepCpy(convDesc),
                                                    .beta = *((float*)beta),
                                                    .dwDesc = filtDescDeepCpy(dwDesc),
                                                    .dw = dw};
    fake_cuda_dispatch(ctx, cudnnConvolutionBackwardFilter_async_fn,
                       (*handle).stream);
    #else //USE_GCD
    const float alp = *((float*)alpha), bet = *((float*)beta);
    const float *X = (float *)x, *dY = (float *)dy;
    float *dW = (float*)dw;

    int i;
    const int dWstride[4] = {dwDesc->dim[1]*dwDesc->dim[2]*dwDesc->dim[3],
                             dwDesc->dim[2]*dwDesc->dim[3],
                             dwDesc->dim[3],
                             1};
    prescale_output(bet, dW, dwDesc->dim[0] * dWstride[0]);

    if (2 == convDesc->arrayLength &&
        convDesc->filterStride[0] == convDesc->filterStride[1] &&
        convDesc->filterStride[0] > 0 && convDesc->filterStride[0] < 3 &&
        1 == convDesc->dilation[0] && 1 == convDesc->dilation[1]) {
        //https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/making_faster.html

        assert(4 == xDesc->nbDims);

        /*
            im = x[i,:,:,:]
            im_col = im2col(im_pad,HH,WW,stride)
            dout_i = dout[i,:,:,:]
            dbias_sum = np.reshape(dout_i,(F,-1))
            dbias_sum = dbias_sum.T
            dmul = dbias_sum
            dfilter_col = (im_col.T).dot(dmul)
            dw += np.reshape(dfilter_col.T,(F,C,HH,WW))
              => dw += dfilter_col**T
                        = (im_col**T * dbias_sum**T)**T
                        = dbias_sum * im_col //transpose rules
                        = dout_i * im_col
        */
        const int dWc_stride = dwDesc->dim[0] * dWstride[0],
              omp_max_thr = omp_get_max_threads();
        float *dW_col = (float*)malloc(min(xDesc->dim[0], omp_max_thr)
                                       * (dWc_stride * sizeof(float)));
        assert(dW_col);
        float fac = 0.0; //to accumulate, go to 1.0 after 1st round
        #pragma omp parallel for firstprivate(dWc_stride,fac), \
        schedule(static)
        for (i = 0; i < xDesc->dim[0]; i++) {
            float *mat;
            if (1 == convDesc->filterStride[0])
                mat = im2col_padded_stride1(X, xDesc->dim, xDesc->stride,
                                            convDesc->pad, dwDesc->dim, i, NULL);
            else
                mat = im2col_padded_stride2(X, xDesc->dim, xDesc->stride,
                                            convDesc->pad, dwDesc->dim, i, NULL);
            assert(mat);

            const blasint m = dyDesc->dim[1];
            const blasint k = dyDesc->dim[2] * dyDesc->dim[3];
            const blasint n = dwDesc->dim[1] * dwDesc->dim[2] * dwDesc->dim[3];
            const blasint lda = k, ldb = k, ldc = n;
            TIME_GEMM_START(RowMaj, 0, 1, m, n, k, 1.0, fac, lda, ldb, ldc);
            BLASFN(sgemm)(CblasRowMajor, CblasNoTrans, CblasTrans, m, n, k,
                          1.0, &dY[i * dyDesc->stride[0]], lda, mat, ldb,
                          fac, &dW_col[omp_get_thread_num() * dWc_stride], ldc);
            TIME_GEMM_STOP(core, m, n, k);
            free(mat);
            if (0.0 == fac)
                fac = 1.0;
        }
        //tree-like reduce
        int gap;
        const int reduce_len = min(xDesc->dim[0], omp_max_thr);
        for(gap = 1; gap < reduce_len; gap *= 2) {
            #pragma omp parallel for firstprivate(gap,reduce_len), \
            schedule(static,1)
            for (i = 0; i < reduce_len - gap; i += 2 * gap) {
                //dW_col[i * dWc_stride] += dW_col[(i+gap) * dWc_stride];
                BLASFN(saxpy)(dWc_stride,
                              1.0, &dW_col[(i + gap) * dWc_stride], 1,
                              &dW_col[i * dWc_stride], 1);
            }
        }
        //dW += alp * dW_col[0];
        cblas_saxpy(((blasint)dWc_stride), alp, dW_col, ((blasint)1), dW,
                    ((blasint)1));
        free(dW_col);
   } else {
        //https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/convolution_layer.html
        int filter, channel, dy_h, dy_w, k_h, k_w;
        const int n = xDesc->dim[0],
              p_h = convDesc->pad[0],
              p_w = convDesc->pad[1],
              s_h = convDesc->filterStride[0],
              s_w = convDesc->filterStride[1],
              num_filters = dwDesc->dim[0], input_channels = xDesc->dim[1],
              dy_height = dyDesc->dim[2], dy_width = dyDesc->dim[3],
              kernel_height = dwDesc->dim[2], kernel_width = dwDesc->dim[3];

        #pragma omp parallel for \
        private(i,filter,channel,dy_h,dy_w,k_h,k_w), \
        firstprivate(alp,n,p_h,p_w,s_h,s_w,num_filters,input_channels, \
                     dy_height,dy_width,kernel_height,kernel_width), \
        schedule(static)
        for (i = 0; i < n; i++)
            for (filter = 0; filter < num_filters; filter++)
                for (dy_h = 0; dy_h < dy_height; dy_h++)
                    for (dy_w = 0; dy_w < dy_width; dy_w++)
                        for (channel = 0; channel < input_channels; channel++)
                            for (k_h = 0; k_h < kernel_height; k_h++)
                                for (k_w = 0; k_w < kernel_width; k_w++) {
                                    if (dy_h*s_h+k_h-p_h < 0 ||
                                        dy_h*s_h+k_h-p_h >= xDesc->dim[2] ||
                                        dy_w*s_w+k_w-p_w < 0 ||
                                        dy_w*s_w+k_w-p_w >= xDesc->dim[3])
                                        continue;
                                    dW[idx(filter,channel,k_h,k_w,
                                           dWstride)] += alp *
                                    dY[idx(i,filter,dy_h,dy_w,dyDesc->stride)]
                                    * X[idx(i,channel, (dy_h*s_h+k_h-p_h),
                                            (dy_w*s_w+k_w-p_w), xDesc->stride)];
                                }
    }
    //fprintf(stderr,"cudnnConvolutionBackwardFilter out(dw):\n");
    //fprintfFloatArray(dW, dwDesc->dim[0]*dwDesc->dim[1]*dwDesc->dim[2]*dwDesc->dim[3]);
    #endif //USE_GCD

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnConvolutionBackwardFilter,
                          handle, alpha, xDesc, x, dyDesc, dy, convDesc, algo,
                          workSpace, workSpaceSizeInBytes, beta, dwDesc, dw);
#endif //USE_MocCUDA

exit:
    LEAVE(cudnn_lib_entry, cudnnConvolutionBackwardFilter);
    return rcs;
}

cudnnStatus_t cudnnConvolutionBiasActivationForward()
{
    ENTER(cudnn_lib_entry, cudnnConvolutionBiasActivationForward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnConvolutionBiasActivationForward);
    return rcs;
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    float alpha;
    cudnnTensorDescriptor_t xDesc;
    void *x;
    cudnnFilterDescriptor_t wDesc;
    void *w;
    cudnnConvolutionDescriptor_t convDesc;
    float beta;
    cudnnTensorDescriptor_t yDesc;
    void *y;
} cudnnConvolutionForward_async_t;

__attribute__((used,noinline))
static void
cudnnConvolutionForward_async_fn(void *ctx)
{
    ENTER(cudnn_lib_entry, cudnnConvolutionForward_async_fn);

    cudnnConvolutionForward_async_t cc =
        *((cudnnConvolutionForward_async_t *)ctx);

    const float alp = cc.alpha, bet = cc.beta;
    const float *X = (float *)cc.x, *W = (float *)cc.w;
    float *Y = (float *)cc.y;

    int i;
    //IMPLICIT_PRECOMP_GEMM -> Dilation: 1 for all dimensions
    if (2 == cc.convDesc->arrayLength &&
        cc.convDesc->filterStride[0] == cc.convDesc->filterStride[1] &&
        cc.convDesc->filterStride[0] > 0 && cc.convDesc->filterStride[0] < 3 &&
        1 == cc.convDesc->dilation[0] && 1 == cc.convDesc->dilation[1]) {

        assert(4 == cc.xDesc->nbDims);

        //https://cs231n.github.io/convolutional-networks/#conv
        //https://humboldt-wi.github.io/blog/research/information_systems_1718/02convolutionalneuralnetworks/
        //https://sahnimanas.github.io/post/anatomy-of-a-high-performance-convolution/
        //https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/making_faster.html
        TIME_GEMM_START("TIMEINGFORWORD", 0,0, 0,0,0, .0,.0, 0,0,0);
        #if defined(NEWV0)
        {

        const int m = cc.wDesc->dim[0]; // num_filters
        const int k = cc.wDesc->dim[1] * cc.wDesc->dim[2] * cc.wDesc->dim[3]; // channels * filtersize
        const int n = cc.yDesc->dim[2] * cc.yDesc->dim[3];
        const int lda = k, ldb = n, ldc = n;

        const int omp_max_thr = omp_get_max_threads();
        float **mat;
        assert((mat = (float **)calloc(omp_max_thr, sizeof(float *))));

//        const int _nested_ = omp_get_nested();

        int img, img_to_process;
        for (img = 0; img < cc.xDesc->dim[0]; img += omp_max_thr) {
            img_to_process = min(cc.xDesc->dim[0] - img, omp_max_thr);
//            omp_set_nested((img_to_process < omp_max_thr));
            #pragma omp parallel for num_threads(img_to_process), \
            firstprivate(img), schedule(static,1)
            for (i = 0; i < img_to_process; i++) {
                TIME_GEMM_START(im2col, cc.convDesc->filterStride[0],0, 0,0,0, .0,.0, 0,0,0);
                if (1 == cc.convDesc->filterStride[0])
                    mat[i] = im2col_padded_stride1(X, cc.xDesc->dim,
                                                   cc.xDesc->stride,
                                                   cc.convDesc->pad,
                                                   cc.wDesc->dim,
                                                   img + i, mat[i]);
                else
                    mat[i] = im2col_padded_stride2(X, cc.xDesc->dim,
                                                   cc.xDesc->stride,
                                                   cc.convDesc->pad,
                                                   cc.wDesc->dim,
                                                   img + i, mat[i]);
                assert(mat[i]);
                TIME_GEMM_STOP(im2col, 1, 1, 1);
            }
//            omp_set_nested(_nested_);

            if (img_to_process == omp_max_thr) {
                #pragma omp parallel for num_threads(omp_max_thr), \
                firstprivate(img), schedule(static,1)
                for (i = 0; i < omp_max_thr; i++) {
                    TIME_GEMM_START(RowMaj, 0, 0, m, n, k, alp, bet, lda, ldb, ldc);
                    BLASFN(sgemm)(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                                  m, n, k, alp, W, lda, mat[i], ldb, bet,
                                  &Y[(img + i) * (m * n)], ldc);
                    TIME_GEMM_STOP(core, m, n, k);
                }
            } else {
                const int num_groups = omp_max_thr;
                int info[1] = {BblasErrorsReportNone};
                bblas_enum_t *_transa, *_transb;
                assert((_transa = malloc(sizeof(bblas_enum_t) * num_groups)));
                assert((_transb = malloc(sizeof(bblas_enum_t) * num_groups)));
                int *_group_size, *_m, *_n, *_k, *_lda, *_ldb, *_ldc;
                assert((_group_size = malloc(sizeof(int) * num_groups)));
                assert((_m = malloc(sizeof(int) * num_groups)));
                assert((_n = malloc(sizeof(int) * num_groups)));
                assert((_k = malloc(sizeof(int) * num_groups)));
                assert((_lda = malloc(sizeof(int) * num_groups)));
                assert((_ldb = malloc(sizeof(int) * num_groups)));
                assert((_ldc = malloc(sizeof(int) * num_groups)));
                float *_alp, *_bet, **_A, **_B, **_C;
                assert((_alp = malloc(sizeof(float) * num_groups)));
                assert((_bet = malloc(sizeof(float) * num_groups)));
                const int total_batch_count = num_groups * img_to_process;
                assert((_A = malloc(sizeof(float *) * total_batch_count)));
                assert((_B = malloc(sizeof(float *) * total_batch_count)));
                assert((_C = malloc(sizeof(float *) * total_batch_count)));

                div_t res = div(n, omp_max_thr);
                for (i = 0; i < num_groups; i++) {
                    _group_size[i] = img_to_process;
                    _transa[i] = BblasNoTrans;
                    _transb[i] = BblasNoTrans;
                    _m[i] = m;
                    _n[i] = ((0 == res.rem) ? res.quot :
                             ((i < res.rem) ? res.quot + 1 : res.quot));
                    _k[i] = k;
                    _lda[i] = lda;
                    _ldb[i] = ldb;
                    _ldc[i] = _n[i];
                    _alp[i] = alp;
                    _bet[i] = bet;
                }

                for (i = 0; i < total_batch_count; i++)
                    _A[i] = (float *)W;

                int j, row;
                for (i = 0; i < num_groups; i++)
                    for (j = 0; j < _group_size[i]; j++)
                        _B[i * _group_size[i] + j] = &(mat[j][i * _n[i]]);

                TIME_GEMM_START(BblasRowMajor, 0, 0, m, img_to_process * n, k,
                                alp, bet, *_lda, *_ldb, *_ldc);

                #pragma omp parallel for num_threads(num_groups), \
                firstprivate(m,n,bet), private(j,row), schedule(static,1)
                for (i = 0; i < num_groups; i++)
                    for (j = 0; j < _group_size[i]; j++) {
                        _C[i * _group_size[i] + j] = malloc(sizeof(float) * m * _n[i]);
                        if (0.0 != bet) {
                            for (row = 0; row < m; row++)
                                memcpy(&(_C[i * _group_size[i] + j][row * _n[i]]),
                                       &Y[(img + j) * (m * n) + i * _n[i] + row * ldc],
                                       sizeof(float) * _n[i]);
                        }
                    }

                {TIME_GEMM_START(BblasRowMajor, 0, 0, m, img_to_process * n, k,
                                alp, bet, *_lda, *_ldb, *_ldc);
                blas_sgemm_batch(num_groups, _group_size,
                                 BblasRowMajor, _transa, _transb, _m, _n, _k,
                                 _alp, (const float **)_A, _lda,
                                 (const float **)_B, _ldb,
                                 _bet, (float **)_C, _ldc, info);
                TIME_GEMM_STOP(batched_sgemm, m, img_to_process * n, k);}

                #pragma omp parallel for num_threads(num_groups), \
                firstprivate(m,n), private(j,row), schedule(static,1)
		for (i = 0; i < num_groups; i++)
		    for (j = 0; j < _group_size[i]; j++) {
                        for (row = 0; row < m; row++) {
                            memcpy(&Y[(img + j) * (m * n) + i * _n[i] + row * ldc],
                                   &(_C[i * _group_size[i] + j][row * _n[i]]),
                                   sizeof(float) * _n[i]);
                        }
                        free(_C[i * _group_size[i] + j]);
                    }

                TIME_GEMM_STOP(batched_sgemm, m, img_to_process * n, k);

                free(_group_size); free(_transa); free(_transb);
                free(_m); free(_n); free(_k);
                free(_lda); free(_ldb); free(_ldc);
                free(_alp); free(_bet);
                free(_A); free(_B); free(_C);
            }
        }
        for (i = 0; i < omp_max_thr; i++)
            if (mat[i])
                free(mat[i]);
        free(mat);

        }
        #elif defined(OLDV0) //FIXME
//float *YY = malloc(sizeof(float) * cc.yDesc->dim[0] * cc.yDesc->dim[1] * cc.yDesc->dim[2] * cc.yDesc->dim[3]); assert(YY);
        {

        const blasint m = cc.wDesc->dim[0];
        const blasint k = cc.wDesc->dim[1] * cc.wDesc->dim[2] * cc.wDesc->dim[3];
        const blasint n = cc.yDesc->dim[2] * cc.yDesc->dim[3];
        const blasint lda = k, ldb = n, ldc = n;
        #pragma omp parallel for firstprivate(alp,bet,m,n,k,lda,ldb,ldc), \
        schedule(static)
        for (i = 0; i < cc.xDesc->dim[0]; i++) {
            float *mat;
            {TIME_GEMM_START(memory, 0,0, 0,0,0, .0,.0, 0,0,0);
            if (1 == cc.convDesc->filterStride[0])
                mat = im2col_padded_stride1(X, cc.xDesc->dim, cc.xDesc->stride,
                                            cc.convDesc->pad, cc.wDesc->dim, i,
                                            NULL);
            else
                mat = im2col_padded_stride2(X, cc.xDesc->dim, cc.xDesc->stride,
                                            cc.convDesc->pad, cc.wDesc->dim, i,
                                            NULL);
            assert(mat);
            TIME_GEMM_STOP(memory, 1, 1, 1);}
            TIME_GEMM_START(RowMaj, 0, 0, m, n, k, alp, bet, lda, ldb, ldc);
            /*if (m < 128 && n > 10240 && k < 128) { //replace sgemm with saxpy
                blasint i_m, i_n, i_k;
                for (i_m = 0; i_m < m; i_m++) {
                    float *t_Y = &Y[i * (m * n) + i_m * n];
                    BLASFN(sscal)(n, bet, t_Y, 1);
                    for (i_k = 0; i_k < k; i_k++)
                        BLASFN(saxpy)(n, alp * W[i_m*k+i_k], &mat[i_k * n], 1, t_Y, 1);
                }
            } else {*/
            BLASFN(sgemm)(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k,
                          alp, W, lda, mat, ldb, bet, &Y[i*(m*n)], ldc);
//                          alp, W, lda, mat, ldb, bet, &YY[i*(m*n)], ldc);
            //}
            TIME_GEMM_STOP(core, m, n, k);

            free(mat);
        }
//fprintf(stderr, "\nmemcmp (Y =?= YY) %d\n\n", (0==memcmp(Y, YY, sizeof(float) * cc.yDesc->dim[0] * cc.yDesc->dim[1] * cc.yDesc->dim[2] * cc.yDesc->dim[3])));free(YY);

        }
        #elif defined (VERYOLDV0) //V0
        {

        assert(cc.convDesc->filterStride[0]==1);
        float *mat = im2col_padded_stride1_v0(X, cc.xDesc->dim, cc.xDesc->stride,
                                           cc.convDesc->pad, cc.wDesc->dim);
        assert(mat);

        const blasint m = cc.wDesc->dim[0]; // num_filters
        const blasint k = cc.wDesc->dim[1] * cc.wDesc->dim[2] * cc.wDesc->dim[3]; // channels * filtersize
        const blasint n = cc.yDesc->dim[0] * cc.xDesc->dim[2] * cc.xDesc->dim[3]; // images * h * w
        const blasint lda = k, ldb = n, ldc = n;

        cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k,
                    alp, W, lda, mat, ldb, bet, Y, ldc);

        assert(4 == cc.yDesc->nbDims);
        col2im(Y, cc.yDesc->dim, cc.yDesc->stride, mat);

        free(mat);

        }
        #endif //V0
        TIME_GEMM_STOP("TIMEINGFORWORD", 1,1,1);
    } else {
        assert(0); //lets hope we dont come here into the slow part of town

        //trivial impl:
        //for i in 0..n
        //  for filter in 0..num_filters
        //    for channel in 0..input_channels
        //      for out_h in 0..output_height
        //        for out_w in 0..output_width
        //          for k_h in 0..kernel_height
        //            for k_w in 0..kernel_width
        //              output[i, filter, out_h, out_w] +=
        //                kernel[filter, channel, k_h, k_w] *
        //                input[i, channel, out_h + k_h, out_w + k_w];
        int filter, channel, out_h, out_w, k_h, k_w;
        const int n = cc.xDesc->dim[0],
              s_h = cc.convDesc->filterStride[0],
              s_w = cc.convDesc->filterStride[1],
              num_filters = cc.wDesc->dim[0], input_channels = cc.xDesc->dim[1],
              output_height = cc.yDesc->dim[2], output_width = cc.yDesc->dim[3],
              kernel_height = cc.wDesc->dim[2], kernel_width = cc.wDesc->dim[3],
              Wstride[4] = {cc.wDesc->dim[1]*cc.wDesc->dim[2]*cc.wDesc->dim[3],
                            cc.wDesc->dim[2]*cc.wDesc->dim[3],
                            cc.wDesc->dim[3],
                            1};

        prescale_output(bet, Y, cc.yDesc->dim[0] * cc.yDesc->stride[0]);

        #pragma omp parallel for \
        private(i,filter,channel,out_h,out_w,k_h,k_w), \
        firstprivate(n,s_h,s_w,num_filters,input_channels, \
                     output_height,output_width,kernel_height,kernel_width), \
        schedule(static)
        for (i = 0; i < n; i++)
            for (filter = 0; filter < num_filters; filter++)
                for (channel = 0; channel < input_channels; channel++)
                    for (out_h = 0; out_h < output_height; out_h++)
                        for (out_w = 0; out_w < output_width; out_w++)
                            for (k_h = -kernel_height / 2;
                                 k_h < kernel_height - kernel_height / 2;
                                 k_h++)
                                for (k_w = -kernel_width / 2;
                                     k_w < kernel_width - kernel_width / 2;
                                     k_w++) {
                                    if (out_h*s_h+k_h < 0 ||
                                        out_h*s_h+k_h >= cc.xDesc->dim[2] ||
                                        out_w*s_w+k_w < 0 ||
                                        out_w*s_w+k_w >= cc.xDesc->dim[3])
                                       continue;
                                    Y[idx(i,filter,out_h,out_w,cc.yDesc->stride)]
                                        += alp *
                                    W[idx(filter,channel,(k_h+kernel_height/2),
                                          (k_w+kernel_width/2),Wstride)]
                                    * X[idx(i,channel,(out_h*s_h+k_h),
                                            (out_w*s_w+k_w),cc.xDesc->stride)];
                                }
    }

    freeTensDesc(cc.xDesc);
    freeFiltDesc(cc.wDesc);
    freeConvDesc(cc.convDesc);
    freeTensDesc(cc.yDesc);
    free(ctx);

    LEAVE(cudnn_lib_entry, cudnnConvolutionForward_async_fn);
}
#endif //USE_GCD

cudnnStatus_t cudnnConvolutionForward(cudnnHandle_t handle,
                                      const void *alpha,
                                      const cudnnTensorDescriptor_t xDesc,
                                      const void *x,
                                      const cudnnFilterDescriptor_t wDesc,
                                      const void *w,
                                      const cudnnConvolutionDescriptor_t convDesc,
                                      cudnnConvolutionFwdAlgo_t algo,
                                      void *workSpace,
                                      size_t workSpaceSizeInBytes,
                                      const void *beta,
                                      const cudnnTensorDescriptor_t yDesc,
                                      void *y)
{
    ENTER(cudnn_lib_entry, cudnnConvolutionForward);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  handle = %p, alpha = %f, ",
            rcs, (void*)handle, *((float*)alpha));
    tensDescToStr("xDesc", xDesc);
    fprintf(stderr, ", x = %p, ", x);
    filtDescToStr("wDesc", wDesc);
    fprintf(stderr, ", w = %p, ", w);
    convDescToStr("convDesc", convDesc);
    fprintf(stderr, ", algo = %d, workSpace = %p, "
            "workSpaceSizeInBytes = %lu, beta = %f, ",
            algo, workSpace, workSpaceSizeInBytes, *((float*)beta));
    tensDescToStr("yDesc", yDesc);
    fprintf(stderr, ", y = %p\n", y);
    #endif

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (NULL == handle || NULL == xDesc || NULL == wDesc || NULL == convDesc
        || NULL == yDesc || NULL == x || NULL == w || NULL == y
        || NULL == alpha || NULL == beta) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (xDesc->nbDims < 3
        || xDesc->nbDims != yDesc->nbDims || xDesc->nbDims != wDesc->nbDims
        || xDesc->nbDims != convDesc->arrayLength + 2
        || xDesc->dataType != yDesc->dataType
        || xDesc->dataType != wDesc->dataType) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (xDesc->nbDims < 4 || xDesc->nbDims > 5
        || yDesc->nbDims < 4 || yDesc->nbDims > 5
        || wDesc->nbDims < 4 || wDesc->nbDims > 5) {
        rcs = CUDNN_STATUS_NOT_SUPPORTED;
        goto exit;
    }
    int j;
    for (j = 0; j < xDesc->nbDims; j++)
        if (xDesc->stride[j] < 0 || yDesc->stride[j] < 0) {
            rcs = CUDNN_STATUS_NOT_SUPPORTED;
            goto exit;
        }

    assert(CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_PRECOMP_GEMM == algo);
    assert(CUDNN_DATA_FLOAT == xDesc->dataType &&
           CUDNN_DATA_FLOAT == wDesc->dataType &&
           CUDNN_DATA_FLOAT == yDesc->dataType);
    assert(CUDNN_CROSS_CORRELATION == convDesc->mode &&
           CUDNN_DATA_FLOAT == convDesc->computeType &&
           1 == convDesc->groupCount &&
           CUDNN_DEFAULT_MATH == convDesc->mathType);
    assert(CUDNN_TENSOR_NCHW == wDesc->format);

    assert(convDesc->pad[0] < 4 && convDesc->pad[1] < 4);
    assert(convDesc->filterStride[0] < 3 && convDesc->filterStride[1] < 3);
    assert(1 == convDesc->dilation[0] && 1 == convDesc->dilation[1]);

    #if defined USE_GCD && USE_GCD >= 1
    cudnnConvolutionForward_async_t *ctx;
    assert((ctx = (cudnnConvolutionForward_async_t *)malloc(sizeof(*ctx))));
    *ctx = (cudnnConvolutionForward_async_t){.alpha = *((float *)alpha),
                                             .xDesc = tensDescDeepCpy(xDesc),
                                             .x = (void*)x,
                                             .wDesc = filtDescDeepCpy(wDesc),
                                             .w = (void*)w,
                                             .convDesc = convDescDeepCpy(convDesc),
                                             .beta = *((float *)beta),
                                             .yDesc = tensDescDeepCpy(yDesc),
                                             .y = y};
    fake_cuda_dispatch(ctx, cudnnConvolutionForward_async_fn,
                       (*handle).stream);
    #else //USE_GCD
    const float alp = *((float *)alpha), bet = *((float *)beta);
    const float *X = (float *)x, *W = (float *)w;
    float *Y = (float *)y;
    int i;

    //IMPLICIT_PRECOMP_GEMM -> Dilation: 1 for all dimensions
    if (2 == convDesc->arrayLength &&
        convDesc->filterStride[0] == convDesc->filterStride[1] &&
        convDesc->filterStride[0] > 0 && convDesc->filterStride[0] < 3 &&
        1 == convDesc->dilation[0] && 1 == convDesc->dilation[1]) {

        assert(4 == xDesc->nbDims);

        //https://cs231n.github.io/convolutional-networks/#conv
        //https://humboldt-wi.github.io/blog/research/information_systems_1718/02convolutionalneuralnetworks/
        //https://sahnimanas.github.io/post/anatomy-of-a-high-performance-convolution/
        //https://leonardoaraujosantos.gitbooks.io/artificial-inteligence/content/making_faster.html
        #ifndef V0
        #pragma omp parallel for firstprivate(alp,bet), schedule(static)
        for (i = 0; i < xDesc->dim[0]; i++) {
            float *mat;
            if (1 == convDesc->filterStride[0])
                mat = im2col_padded_stride1(X, xDesc->dim, xDesc->stride,
                                            convDesc->pad, wDesc->dim, i, NULL);
            else
                mat = im2col_padded_stride2(X, xDesc->dim, xDesc->stride,
                                            convDesc->pad, wDesc->dim, i, NULL);
            assert(mat);
            const blasint m = wDesc->dim[0];
            const blasint k = wDesc->dim[1] * wDesc->dim[2] * wDesc->dim[3];
            const blasint n = yDesc->dim[2] * yDesc->dim[3];
            const blasint lda = k, ldb = n, ldc = n;
            TIME_GEMM_START(RowMaj, 0, 0, m, n, k, alp, bet, lda, ldb, ldc);
            BLASFN(sgemm)(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k,
                          alp, W, lda, mat, ldb, bet, &Y[i*(m*n)], ldc);
            TIME_GEMM_STOP(core, m, n, k);

            free(mat);
        }
        #else //V0
        float *mat = im2col_padded_stride1(X, xDesc->dim, xDesc->stride,
                                           convDesc->pad, wDesc->dim, NULL);
        assert(mat);

        const blasint m = wDesc->dim[0]; // num_filters
        const blasint k = wDesc->dim[1] * wDesc->dim[2] * wDesc->dim[3]; // channels * filtersize
        const blasint n = yDesc->dim[0] * xDesc->dim[2] * xDesc->dim[3]; // images * h * w
        const blasint lda = k, ldb = n, ldc = n;

        cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k,
                    alp, W, lda, mat, ldb, bet, Y, ldc);

        assert(4 == yDesc->nbDims);
        col2im(Y, yDesc->dim, yDesc->stride, mat);

        free(mat);
        #endif //V0
    } else {
        assert(0); //lets hope we dont come here into the slow part of town

        //trivial impl:
        //for i in 0..n
        //  for filter in 0..num_filters
        //    for channel in 0..input_channels
        //      for out_h in 0..output_height
        //        for out_w in 0..output_width
        //          for k_h in 0..kernel_height
        //            for k_w in 0..kernel_width
        //              output[i, filter, out_h, out_w] +=
        //                kernel[filter, channel, k_h, k_w] *
        //                input[i, channel, out_h + k_h, out_w + k_w];
        int filter, channel, out_h, out_w, k_h, k_w;
        const int n = xDesc->dim[0],
              s_h = convDesc->filterStride[0], s_w = convDesc->filterStride[1],
              num_filters = wDesc->dim[0], input_channels = xDesc->dim[1],
              output_height = yDesc->dim[2], output_width = yDesc->dim[3],
              kernel_height = wDesc->dim[2], kernel_width = wDesc->dim[3],
              Wstride[4] = {wDesc->dim[1]*wDesc->dim[2]*wDesc->dim[3],
                            wDesc->dim[2]*wDesc->dim[3],
                            wDesc->dim[3],
                            1};

        prescale_output(bet, Y, yDesc->dim[0] * yDesc->stride[0]);

        #pragma omp parallel for \
        private(i,filter,channel,out_h,out_w,k_h,k_w), \
        firstprivate(n,s_h,s_w,num_filters,input_channels, \
                     output_height,output_width,kernel_height,kernel_width), \
        schedule(static)
        for (i = 0; i < n; i++)
            for (filter = 0; filter < num_filters; filter++)
                for (channel = 0; channel < input_channels; channel++)
                    for (out_h = 0; out_h < output_height; out_h++)
                        for (out_w = 0; out_w < output_width; out_w++)
                            for (k_h = -kernel_height / 2;
                                 k_h < kernel_height - kernel_height / 2;
                                 k_h++)
                                for (k_w = -kernel_width / 2;
                                     k_w < kernel_width - kernel_width / 2;
                                     k_w++) {
                                    if (out_h*s_h+k_h < 0 ||
                                        out_h*s_h+k_h >= xDesc->dim[2] ||
                                        out_w*s_w+k_w < 0 ||
                                        out_w*s_w+k_w >= xDesc->dim[3])
                                       continue;
                                    Y[idx(i,filter,out_h,out_w,yDesc->stride)]
                                        += alp *
                                    W[idx(filter,channel,(k_h+kernel_height/2),
                                          (k_w+kernel_width/2),Wstride)]
                                    * X[idx(i,channel,(out_h*s_h+k_h),
                                            (out_w*s_w+k_w),xDesc->stride)];
                                }
    }
    #endif //USE_GCD

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnConvolutionForward,
                          handle, alpha, xDesc, x, wDesc, w, convDesc, algo,
                          workSpace, workSpaceSizeInBytes, beta, yDesc, y);
#endif //USE_MocCUDA

exit:
    LEAVE(cudnn_lib_entry, cudnnConvolutionForward);
    return rcs;
}

cudnnStatus_t cudnnCopyAlgorithmDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCopyAlgorithmDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCopyAlgorithmDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreate(cudnnHandle_t *handle)
{
    ENTER(cudnn_lib_entry, cudnnCreate);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    assert((*handle = (struct cudnnContext *)calloc(1, sizeof(**handle))));
#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnCreate, handle);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  phandle = %p, handle = %p\n",
            rcs, handle, (void*)(*handle));
    #endif

    LEAVE(cudnn_lib_entry, cudnnCreate);
    return rcs;
}

cudnnStatus_t cudnnCreateActivationDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCreateActivationDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateActivationDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreateAlgorithmDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCreateAlgorithmDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateAlgorithmDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreateAlgorithmPerformance()
{
    ENTER(cudnn_lib_entry, cudnnCreateAlgorithmPerformance);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateAlgorithmPerformance);
    return rcs;
}

cudnnStatus_t cudnnCreateAttnDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCreateAttnDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateAttnDescriptor);
    return rcs;
}

cudnnStatus_t
cudnnCreateConvolutionDescriptor(cudnnConvolutionDescriptor_t *convDesc)
{
    ENTER(cudnn_lib_entry, cudnnCreateConvolutionDescriptor);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    assert((*convDesc =
                (struct cudnnConvolutionStruct *)malloc(sizeof(**convDesc))));
#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnCreateConvolutionDescriptor,
                          convDesc);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  pconvDesc = %p, convDesc = %p\n",
            rcs, convDesc, (void*)(*convDesc));
    #endif

    LEAVE(cudnn_lib_entry, cudnnCreateConvolutionDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreateCTCLossDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCreateCTCLossDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateCTCLossDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreateDropoutDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCreateDropoutDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateDropoutDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreateFilterDescriptor(cudnnFilterDescriptor_t *filterDesc)
{
    ENTER(cudnn_lib_entry, cudnnCreateFilterDescriptor);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    assert((*filterDesc =
                (struct cudnnFilterStruct *)malloc(sizeof(**filterDesc))));
#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnCreateFilterDescriptor,
                          filterDesc);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  pfilterDesc = %p, filterDesc = %p\n",
            rcs, filterDesc, (void*)(*filterDesc));
    #endif

    LEAVE(cudnn_lib_entry, cudnnCreateFilterDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreateFusedOpsConstParamPack()
{
    ENTER(cudnn_lib_entry, cudnnCreateFusedOpsConstParamPack);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateFusedOpsConstParamPack);
    return rcs;
}

cudnnStatus_t cudnnCreateFusedOpsPlan()
{
    ENTER(cudnn_lib_entry, cudnnCreateFusedOpsPlan);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateFusedOpsPlan);
    return rcs;
}

cudnnStatus_t cudnnCreateFusedOpsVariantParamPack()
{
    ENTER(cudnn_lib_entry, cudnnCreateFusedOpsVariantParamPack);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateFusedOpsVariantParamPack);
    return rcs;
}

cudnnStatus_t cudnnCreateLRNDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCreateLRNDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateLRNDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreateOpTensorDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCreateOpTensorDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateOpTensorDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreatePersistentRNNPlan()
{
    ENTER(cudnn_lib_entry, cudnnCreatePersistentRNNPlan);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreatePersistentRNNPlan);
    return rcs;
}

cudnnStatus_t cudnnCreatePoolingDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCreatePoolingDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreatePoolingDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreateReduceTensorDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCreateReduceTensorDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateReduceTensorDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreateRNNDataDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCreateRNNDataDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateRNNDataDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreateRNNDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCreateRNNDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateRNNDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreateSeqDataDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCreateSeqDataDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateSeqDataDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreateSpatialTransformerDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCreateSpatialTransformerDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateSpatialTransformerDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreateTensorDescriptor(cudnnTensorDescriptor_t *tensorDesc)
{
    ENTER(cudnn_lib_entry, cudnnCreateTensorDescriptor);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    assert((*tensorDesc =
                (struct cudnnTensorStruct *)malloc(sizeof(**tensorDesc))));
#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnCreateTensorDescriptor,
                         tensorDesc);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  ptensorDesc = %p, tensorDesc = %p\n",
            rcs, tensorDesc, (void*)(*tensorDesc));
    #endif

    LEAVE(cudnn_lib_entry, cudnnCreateTensorDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCreateTensorTransformDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnCreateTensorTransformDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCreateTensorTransformDescriptor);
    return rcs;
}

cudnnStatus_t cudnnCTCLoss()
{
    ENTER(cudnn_lib_entry, cudnnCTCLoss);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnCTCLoss);
    return rcs;
}

cudnnStatus_t cudnnDeriveBNTensorDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDeriveBNTensorDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDeriveBNTensorDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroy(cudnnHandle_t handle)
{
    ENTER(cudnn_lib_entry, cudnnDestroy);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (handle) {
        fake_cuda_synchronize((*handle).stream); //FIXME: needed?
        free(handle);
    }
#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnDestroy, handle);
#endif //USE_MocCUDA

    #ifdef DEBUG
    if (handle)
        fprintf(stderr, "\tret=%d  handle = %p\n", rcs, (void*)handle);
    #endif

    LEAVE(cudnn_lib_entry, cudnnDestroy);
    return rcs;
}

cudnnStatus_t cudnnDestroyActivationDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDestroyActivationDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyActivationDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroyAlgorithmDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDestroyAlgorithmDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyAlgorithmDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroyAlgorithmPerformance()
{
    ENTER(cudnn_lib_entry, cudnnDestroyAlgorithmPerformance);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyAlgorithmPerformance);
    return rcs;
}

cudnnStatus_t cudnnDestroyAttnDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDestroyAttnDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyAttnDescriptor);
    return rcs;
}

cudnnStatus_t
cudnnDestroyConvolutionDescriptor(cudnnConvolutionDescriptor_t convDesc)
{
    ENTER(cudnn_lib_entry, cudnnDestroyConvolutionDescriptor);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    freeConvDesc(convDesc);
#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1,
                          cudnnDestroyConvolutionDescriptor, convDesc);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  convDesc = %p\n", rcs, (void*)convDesc);
    #endif

    LEAVE(cudnn_lib_entry, cudnnDestroyConvolutionDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroyCTCLossDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDestroyCTCLossDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyCTCLossDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroyDropoutDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDestroyDropoutDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyDropoutDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroyFilterDescriptor(cudnnFilterDescriptor_t filterDesc)
{
    ENTER(cudnn_lib_entry, cudnnDestroyFilterDescriptor);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    freeFiltDesc(filterDesc);
#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnDestroyFilterDescriptor,
                          filterDesc);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  filterDesc = %p\n", rcs, (void*)filterDesc);
    #endif

    LEAVE(cudnn_lib_entry, cudnnDestroyFilterDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroyFusedOpsConstParamPack()
{
    ENTER(cudnn_lib_entry, cudnnDestroyFusedOpsConstParamPack);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyFusedOpsConstParamPack);
    return rcs;
}

cudnnStatus_t cudnnDestroyFusedOpsPlan()
{
    ENTER(cudnn_lib_entry, cudnnDestroyFusedOpsPlan);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyFusedOpsPlan);
    return rcs;
}

cudnnStatus_t cudnnDestroyFusedOpsVariantParamPack()
{
    ENTER(cudnn_lib_entry, cudnnDestroyFusedOpsVariantParamPack);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyFusedOpsVariantParamPack);
    return rcs;
}

cudnnStatus_t cudnnDestroyLRNDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDestroyLRNDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyLRNDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroyOpTensorDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDestroyOpTensorDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyOpTensorDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroyPersistentRNNPlan()
{
    ENTER(cudnn_lib_entry, cudnnDestroyPersistentRNNPlan);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyPersistentRNNPlan);
    return rcs;
}

cudnnStatus_t cudnnDestroyPoolingDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDestroyPoolingDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyPoolingDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroyReduceTensorDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDestroyReduceTensorDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyReduceTensorDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroyRNNDataDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDestroyRNNDataDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyRNNDataDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroyRNNDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDestroyRNNDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyRNNDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroySeqDataDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDestroySeqDataDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroySeqDataDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroySpatialTransformerDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDestroySpatialTransformerDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroySpatialTransformerDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroyTensorDescriptor(cudnnTensorDescriptor_t tensorDesc)
{
    ENTER(cudnn_lib_entry, cudnnDestroyTensorDescriptor);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    freeTensDesc(tensorDesc);
#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnDestroyTensorDescriptor,
                          tensorDesc);
#endif

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  tensorDesc = %p\n", rcs, (void*)tensorDesc);
    #endif

    LEAVE(cudnn_lib_entry, cudnnDestroyTensorDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDestroyTensorTransformDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnDestroyTensorTransformDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDestroyTensorTransformDescriptor);
    return rcs;
}

cudnnStatus_t cudnnDivisiveNormalizationBackward()
{
    ENTER(cudnn_lib_entry, cudnnDivisiveNormalizationBackward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDivisiveNormalizationBackward);
    return rcs;
}

cudnnStatus_t cudnnDivisiveNormalizationForward()
{
    ENTER(cudnn_lib_entry, cudnnDivisiveNormalizationForward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDivisiveNormalizationForward);
    return rcs;
}

cudnnStatus_t cudnnDropoutBackward()
{
    ENTER(cudnn_lib_entry, cudnnDropoutBackward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDropoutBackward);
    return rcs;
}

cudnnStatus_t cudnnDropoutForward()
{
    ENTER(cudnn_lib_entry, cudnnDropoutForward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDropoutForward);
    return rcs;
}

cudnnStatus_t cudnnDropoutGetReserveSpaceSize()
{
    ENTER(cudnn_lib_entry, cudnnDropoutGetReserveSpaceSize);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDropoutGetReserveSpaceSize);
    return rcs;
}

cudnnStatus_t cudnnDropoutGetStatesSize()
{
    ENTER(cudnn_lib_entry, cudnnDropoutGetStatesSize);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnDropoutGetStatesSize);
    return rcs;
}

cudnnStatus_t cudnnFindConvolutionBackwardDataAlgorithm()
{
    ENTER(cudnn_lib_entry, cudnnFindConvolutionBackwardDataAlgorithm);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnFindConvolutionBackwardDataAlgorithm);
    return rcs;
}

//FIXME
cudnnStatus_t cudnnGetConvolutionBackwardDataAlgorithm_v7(cudnnHandle_t handle,
                                                          const cudnnFilterDescriptor_t filterDesc,
                                                          const cudnnTensorDescriptor_t diffDesc,
                                                          const cudnnConvolutionDescriptor_t convDesc,
                                                          const cudnnTensorDescriptor_t gradDesc,
                                                          const int requestedAlgoCount,
                                                          int *returnedAlgoCount,
                                                          cudnnConvolutionBwdDataAlgoPerf_t *perfResults);

cudnnStatus_t cudnnFindConvolutionBackwardDataAlgorithmEx(
        cudnnHandle_t                          handle,
        const cudnnFilterDescriptor_t          wDesc,
        const void                            *w,
        const cudnnTensorDescriptor_t          dyDesc,
        const void                            *dy,
        const cudnnConvolutionDescriptor_t     convDesc,
        const cudnnTensorDescriptor_t          dxDesc,
        void                                  *dx,
        const int                              requestedAlgoCount,
        int                                   *returnedAlgoCount,
        cudnnConvolutionBwdDataAlgoPerf_t     *perfResults,
        void                                  *workSpace,
        size_t                                 workSpaceSizeInBytes)
{
    ENTER(cudnn_lib_entry, cudnnFindConvolutionBackwardDataAlgorithmEx);
    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;
    cudnnGetConvolutionBackwardDataAlgorithm_v7(handle, wDesc, dyDesc, convDesc,
                                                dxDesc, 1, returnedAlgoCount,
                                                perfResults);
    //fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnFindConvolutionBackwardDataAlgorithmEx);
    return rcs;
}

cudnnStatus_t cudnnFindConvolutionBackwardFilterAlgorithm()
{
    ENTER(cudnn_lib_entry, cudnnFindConvolutionBackwardFilterAlgorithm);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnFindConvolutionBackwardFilterAlgorithm);
    return rcs;
}

//FIXME
cudnnStatus_t cudnnGetConvolutionBackwardFilterAlgorithm_v7(cudnnHandle_t handle,
                                                            const cudnnTensorDescriptor_t srcDesc,
                                                            const cudnnTensorDescriptor_t diffDesc,
                                                            const cudnnConvolutionDescriptor_t convDesc,
                                                            const cudnnFilterDescriptor_t gradDesc,
                                                            const int requestedAlgoCount,
                                                            int *returnedAlgoCount,
                                                            cudnnConvolutionBwdFilterAlgoPerf_t *perfResults);

cudnnStatus_t cudnnFindConvolutionBackwardFilterAlgorithmEx(
        cudnnHandle_t                          handle,
        const cudnnTensorDescriptor_t          xDesc,
        const void                            *x,
        const cudnnTensorDescriptor_t          dyDesc,
        const void                            *dy,
        const cudnnConvolutionDescriptor_t     convDesc,
        const cudnnFilterDescriptor_t          dwDesc,
        void                                  *dw,
        const int                              requestedAlgoCount,
        int                                   *returnedAlgoCount,
        cudnnConvolutionBwdFilterAlgoPerf_t   *perfResults,
        void                                  *workSpace,
        size_t                                 workSpaceSizeInBytes)
{
    ENTER(cudnn_lib_entry, cudnnFindConvolutionBackwardFilterAlgorithmEx);
    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;
    cudnnGetConvolutionBackwardFilterAlgorithm_v7(handle, xDesc, dyDesc,
                                                  convDesc, dwDesc, 1,
                                                  returnedAlgoCount,
                                                  perfResults);
    //fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnFindConvolutionBackwardFilterAlgorithmEx);
    return rcs;
}

cudnnStatus_t cudnnFindConvolutionForwardAlgorithm()
{
    ENTER(cudnn_lib_entry, cudnnFindConvolutionForwardAlgorithm);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnFindConvolutionForwardAlgorithm);
    return rcs;
}

//FIXME
cudnnStatus_t cudnnGetConvolutionForwardAlgorithm_v7(cudnnHandle_t handle,
                                                     const cudnnTensorDescriptor_t srcDesc,
                                                     const cudnnFilterDescriptor_t filterDesc,
                                                     const cudnnConvolutionDescriptor_t convDesc,
                                                     const cudnnTensorDescriptor_t destDesc,
                                                     const int requestedAlgoCount,
                                                     int *returnedAlgoCount,
                                                     cudnnConvolutionFwdAlgoPerf_t *perfResults);

cudnnStatus_t cudnnFindConvolutionForwardAlgorithmEx(
        cudnnHandle_t                      handle,
        const cudnnTensorDescriptor_t      xDesc,
        const void                        *x,
        const cudnnFilterDescriptor_t      wDesc,
        const void                        *w,
        const cudnnConvolutionDescriptor_t convDesc,
        const cudnnTensorDescriptor_t      yDesc,
        void                              *y,
        const int                          requestedAlgoCount,
        int                               *returnedAlgoCount,
        cudnnConvolutionFwdAlgoPerf_t     *perfResults,
        void                              *workSpace,
        size_t                             workSpaceSizeInBytes)
{
    ENTER(cudnn_lib_entry, cudnnFindConvolutionForwardAlgorithmEx);
    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;
    cudnnGetConvolutionForwardAlgorithm_v7(handle, xDesc, wDesc, convDesc,
                                           yDesc, 1, returnedAlgoCount,
                                           perfResults);
    //fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnFindConvolutionForwardAlgorithmEx);
    return rcs;
}

cudnnStatus_t cudnnFindRNNBackwardDataAlgorithmEx()
{
    ENTER(cudnn_lib_entry, cudnnFindRNNBackwardDataAlgorithmEx);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnFindRNNBackwardDataAlgorithmEx);
    return rcs;
}

cudnnStatus_t cudnnFindRNNBackwardWeightsAlgorithmEx()
{
    ENTER(cudnn_lib_entry, cudnnFindRNNBackwardWeightsAlgorithmEx);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnFindRNNBackwardWeightsAlgorithmEx);
    return rcs;
}

cudnnStatus_t cudnnFindRNNForwardInferenceAlgorithmEx()
{
    ENTER(cudnn_lib_entry, cudnnFindRNNForwardInferenceAlgorithmEx);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnFindRNNForwardInferenceAlgorithmEx);
    return rcs;
}

cudnnStatus_t cudnnFindRNNForwardTrainingAlgorithmEx()
{
    ENTER(cudnn_lib_entry, cudnnFindRNNForwardTrainingAlgorithmEx);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnFindRNNForwardTrainingAlgorithmEx);
    return rcs;
}

cudnnStatus_t cudnnFusedOpsExecute()
{
    ENTER(cudnn_lib_entry, cudnnFusedOpsExecute);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnFusedOpsExecute);
    return rcs;
}

cudnnStatus_t cudnnGetActivationDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetActivationDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetActivationDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetAlgorithmDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetAlgorithmDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetAlgorithmDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetAlgorithmPerformance()
{
    ENTER(cudnn_lib_entry, cudnnGetAlgorithmPerformance);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetAlgorithmPerformance);
    return rcs;
}

cudnnStatus_t cudnnGetAlgorithmSpaceSize()
{
    ENTER(cudnn_lib_entry, cudnnGetAlgorithmSpaceSize);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetAlgorithmSpaceSize);
    return rcs;
}

cudnnStatus_t cudnnGetAttnDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetAttnDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetAttnDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetBatchNormalizationBackwardExWorkspaceSize(cudnnHandle_t handle,
                                                                cudnnBatchNormMode_t mode,
                                                                cudnnBatchNormOps_t bnOps,
                                                                const cudnnTensorDescriptor_t xDesc,
                                                                const cudnnTensorDescriptor_t yDesc,
                                                                const cudnnTensorDescriptor_t dyDesc,
                                                                const cudnnTensorDescriptor_t dzDesc,
                                                                const cudnnTensorDescriptor_t dxDesc,
                                                                const cudnnTensorDescriptor_t dBnScaleBiasDesc,
                                                                const cudnnActivationDescriptor_t activationDesc,
                                                                size_t *sizeInBytes)
{
    ENTER(cudnn_lib_entry, cudnnGetBatchNormalizationBackwardExWorkspaceSize);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  handle = %p, mode = %d, bnOps = %d, ",
            rcs, handle, mode, bnOps);
    tensDescToStr("xDesc", xDesc);
    fprintf(stderr, ", ");
    tensDescToStr("yDesc", yDesc);
    fprintf(stderr, ", ");
    tensDescToStr("dyDesc", dyDesc);
    fprintf(stderr, ", ");
    tensDescToStr("dzDesc", dzDesc);
    fprintf(stderr, ", ");
    tensDescToStr("dxDesc", dxDesc);
    fprintf(stderr, ", ");
    tensDescToStr("dBnScaleBiasDesc", dBnScaleBiasDesc);
    fprintf(stderr, ", activationDesc = %p, sizeInBytes = %p",
            (void*)activationDesc, sizeInBytes);
    #endif

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if ((xDesc->nbDims < 4 || xDesc->nbDims > 5)
        || (yDesc->nbDims < 4 || yDesc->nbDims > 5)
        || (dxDesc->nbDims < 4 || dxDesc->nbDims > 5)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    int d, xDim = 1, dBnDim = 1;
    for (d = 1; d < xDesc->nbDims; d++)
        xDim *= xDesc->dim[d];
    for (d = 0; d < dBnScaleBiasDesc->nbDims; d++)
        dBnDim *= dBnScaleBiasDesc->dim[d];
    if (((CUDNN_BATCHNORM_SPATIAL == mode
          || CUDNN_BATCHNORM_SPATIAL_PERSISTENT == mode)
         && dBnDim != xDesc->dim[1])
        || (CUDNN_BATCHNORM_PER_ACTIVATION == mode && dBnDim != xDim)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (!(xDesc->nbDims == dxDesc->nbDims
          && 0 == memcmp(dxDesc->dim, xDesc->dim,
                         xDesc->nbDims * sizeof(int)))
        || !(xDesc->nbDims == dyDesc->nbDims
             && 0 == memcmp(dyDesc->dim, xDesc->dim,
                            xDesc->nbDims * sizeof(int)))) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }

    *sizeInBytes = 0;

#else
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1,
                          cudnnGetBatchNormalizationBackwardExWorkspaceSize,
                          handle, mode, bnOps, xDesc, yDesc, dyDesc, dzDesc,
                          dxDesc, dBnScaleBiasDesc, activationDesc,
                          sizeInBytes);
#endif

    #ifdef DEBUG
    fprintf(stderr, ", sizeInBytes = %lu\n", *sizeInBytes);
    #endif

exit:
    LEAVE(cudnn_lib_entry, cudnnGetBatchNormalizationBackwardExWorkspaceSize);
    return rcs;
}

cudnnStatus_t cudnnGetBatchNormalizationForwardTrainingExWorkspaceSize(cudnnHandle_t handle,
                                                                       cudnnBatchNormMode_t mode,
                                                                       cudnnBatchNormOps_t bnOps,
                                                                       const cudnnTensorDescriptor_t xDesc,
                                                                       const cudnnTensorDescriptor_t zDesc,
                                                                       const cudnnTensorDescriptor_t yDesc,
                                                                       const cudnnTensorDescriptor_t bnScaleBiasMeanVarDesc,
                                                                       const cudnnActivationDescriptor_t activationDesc,
                                                                       size_t *sizeInBytes)
{
    ENTER(cudnn_lib_entry,
          cudnnGetBatchNormalizationForwardTrainingExWorkspaceSize);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  handle = %p, mode = %d, bnOps = %d, ",
            rcs, handle, mode, bnOps);
    tensDescToStr("xDesc", xDesc);
    fprintf(stderr, ", ");
    tensDescToStr("zDesc", zDesc);
    fprintf(stderr, ", ");
    tensDescToStr("yDesc", yDesc);
    fprintf(stderr, ", ");
    tensDescToStr("bnScaleBiasMeanVarDesc", bnScaleBiasMeanVarDesc);
    fprintf(stderr, ", activationDesc = %p, sizeInBytes = %p",
            (void*)activationDesc, sizeInBytes);
    #endif

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if ((xDesc->nbDims < 4 || xDesc->nbDims > 5)
        || (yDesc->nbDims < 4 || yDesc->nbDims > 5)
        || (zDesc->nbDims < 4 || zDesc->nbDims > 5)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    int d, xDim = 1, dBnDim = 1;
    for (d = 1; d < xDesc->nbDims; d++)
        xDim *= xDesc->dim[d];
    for (d = 0; d < bnScaleBiasMeanVarDesc->nbDims; d++)
        dBnDim *= bnScaleBiasMeanVarDesc->dim[d];
    if (((CUDNN_BATCHNORM_SPATIAL == mode
          || CUDNN_BATCHNORM_SPATIAL_PERSISTENT == mode)
         && dBnDim != xDesc->dim[1])
        || (CUDNN_BATCHNORM_PER_ACTIVATION == mode && dBnDim != xDim)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (!(xDesc->nbDims == yDesc->nbDims
          && 0 == memcmp(yDesc->dim, xDesc->dim,
                         xDesc->nbDims * sizeof(int)))) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }

    assert(CUDNN_BATCHNORM_SPATIAL == mode && CUDNN_BATCHNORM_OPS_BN == bnOps);

    *sizeInBytes = 0;

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1,
                          cudnnGetBatchNormalizationForwardTrainingExWorkspaceSize,
                          handle, mode, bnOps, xDesc, zDesc, yDesc,
                          bnScaleBiasMeanVarDesc, activationDesc,
                          sizeInBytes);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, ", sizeInBytes = %lu\n", *sizeInBytes);
    #endif

exit:
    LEAVE(cudnn_lib_entry,
          cudnnGetBatchNormalizationForwardTrainingExWorkspaceSize);
    return rcs;
}

cudnnStatus_t cudnnGetBatchNormalizationTrainingExReserveSpaceSize(cudnnHandle_t handle,
                                                                   cudnnBatchNormMode_t mode,
                                                                   cudnnBatchNormOps_t bnOps,
                                                                   const cudnnActivationDescriptor_t activationDeactivationDesc,
                                                                   const cudnnTensorDescriptor_t xDesc,
                                                                   size_t *sizeInBytes)
{
    ENTER(cudnn_lib_entry,
          cudnnGetBatchNormalizationTrainingExReserveSpaceSize);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

    #ifdef DEBUG
    fprintf(stderr,
            "\tret=%d  handle = %p, mode = %d, bnOps = %d, "
            "activationDeactivationDesc = %p",
            rcs, handle, mode, bnOps, (void*)activationDeactivationDesc);
    tensDescToStr("xDesc", xDesc);
    #endif

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (xDesc->nbDims < 4 || xDesc->nbDims > 5) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }

    assert(CUDNN_BATCHNORM_SPATIAL == mode && CUDNN_BATCHNORM_OPS_BN == bnOps);

    *sizeInBytes = 0;

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1,
                          cudnnGetBatchNormalizationTrainingExReserveSpaceSize,
                          handle, mode, bnOps, activationDeactivationDesc,
                          xDesc, sizeInBytes);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, ", sizeInBytes = %lu\n", *sizeInBytes);
    #endif

exit:
    LEAVE(cudnn_lib_entry,
          cudnnGetBatchNormalizationTrainingExReserveSpaceSize);
    return rcs;
}

cudnnStatus_t cudnnGetCallback()
{
    ENTER(cudnn_lib_entry, cudnnGetCallback);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetCallback);
    return rcs;
}

cudnnStatus_t cudnnGetConvolution2dDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetConvolution2dDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolution2dDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetConvolution2dForwardOutputDim()
{
    ENTER(cudnn_lib_entry, cudnnGetConvolution2dForwardOutputDim);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolution2dForwardOutputDim);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardDataAlgorithm()
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionBackwardDataAlgorithm);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionBackwardDataAlgorithm);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardDataAlgorithmMaxCount()
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionBackwardDataAlgorithmMaxCount);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionBackwardDataAlgorithmMaxCount);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardDataAlgorithm_v7(cudnnHandle_t handle,
                                                          const cudnnFilterDescriptor_t filterDesc,
                                                          const cudnnTensorDescriptor_t diffDesc,
                                                          const cudnnConvolutionDescriptor_t convDesc,
                                                          const cudnnTensorDescriptor_t gradDesc,
                                                          const int requestedAlgoCount,
                                                          int *returnedAlgoCount,
                                                          cudnnConvolutionBwdDataAlgoPerf_t *perfResults)
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionBackwardDataAlgorithm_v7);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

    #ifdef DEBUG
    fprintf(stderr,
            "\tret=%d  handle = %p, filterDesc = %p, diffDesc = %p, "
            "convDesc = %p, gradDesc = %p, "
            "requestedAlgoCount = %d, returnedAlgoCount = %d",
            rcs, handle, (void*)filterDesc, (void*)diffDesc,
            (void*)convDesc, (void*)gradDesc,
            requestedAlgoCount, *returnedAlgoCount);
    #endif

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (NULL == handle|| NULL == filterDesc || NULL == diffDesc
        || NULL == convDesc || NULL == gradDesc
        || NULL == returnedAlgoCount || NULL == perfResults
        || requestedAlgoCount < 1) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (filterDesc->dataType != diffDesc->dataType
        || filterDesc->dataType != gradDesc->dataType) {
        //|| diffDesc->dim[1] != gradDesc->dim[1] /*hope its NCHW->seems not...*/) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    //if (some other checks) {
    //    rcs = CUDNN_STATUS_BAD_PARAM;
    //    goto exit;
    //}

    *returnedAlgoCount = requestedAlgoCount;
    perfResults->algo = CUDNN_CONVOLUTION_BWD_DATA_ALGO_1;  //changing
    perfResults->status = CUDNN_STATUS_SUCCESS;
    perfResults->time = -1.0;
    perfResults->memory = 2476;                             //changing
    perfResults->determinism = CUDNN_DETERMINISTIC;
    perfResults->mathType = CUDNN_DEFAULT_MATH;

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1,
                          cudnnGetConvolutionBackwardDataAlgorithm_v7,
                          handle, filterDesc, diffDesc, convDesc, gradDesc,
                          requestedAlgoCount, returnedAlgoCount,
                          perfResults);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, ", perfResults = %p[%d,%d,%f,%lu,%d,%d]\n",
            perfResults, perfResults->algo, perfResults->status,
            perfResults->time, perfResults->memory,
            perfResults->determinism, perfResults->mathType);
    #endif

exit:
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionBackwardDataAlgorithm_v7);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardDataWorkspaceSize(cudnnHandle_t handle,
                                                           const cudnnFilterDescriptor_t wDesc,
                                                           const cudnnTensorDescriptor_t dyDesc,
                                                           const cudnnConvolutionDescriptor_t convDesc,
                                                           const cudnnTensorDescriptor_t dxDesc,
                                                           cudnnConvolutionBwdDataAlgo_t algo,
                                                           size_t *sizeInBytes)
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionBackwardDataWorkspaceSize);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  handle = %p, wDesc = %p, dyDesc = %p, "
            "convDesc = %p, dxDesc = %p, algo = %d",
            rcs, handle, (void*)wDesc, (void*)dyDesc, (void*)convDesc,
            (void*)dxDesc, algo);
    #endif

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (dyDesc->dataType != dxDesc->dataType
        || dyDesc->dim[1] == dxDesc->dim[1] /*hope its NCHW*/) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    //if (some other checks) {
    //    rcs = CUDNN_STATUS_BAD_PARAM;
    //    goto exit;
    //}

    *sizeInBytes = 0;

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1,
                          cudnnGetConvolutionBackwardDataWorkspaceSize,
                          handle, wDesc, dyDesc, convDesc, dxDesc, algo,
                          sizeInBytes);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, ", sizeInBytes = %lu\n", *sizeInBytes);
    #endif

exit:
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionBackwardDataWorkspaceSize);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardFilterAlgorithm()
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionBackwardFilterAlgorithm);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionBackwardFilterAlgorithm);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardFilterAlgorithmMaxCount()
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionBackwardFilterAlgorithmMaxCount);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionBackwardFilterAlgorithmMaxCount);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardFilterAlgorithm_v7(cudnnHandle_t handle,
                                                            const cudnnTensorDescriptor_t srcDesc,
                                                            const cudnnTensorDescriptor_t diffDesc,
                                                            const cudnnConvolutionDescriptor_t convDesc,
                                                            const cudnnFilterDescriptor_t gradDesc,
                                                            const int requestedAlgoCount,
                                                            int *returnedAlgoCount,
                                                            cudnnConvolutionBwdFilterAlgoPerf_t *perfResults)
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionBackwardFilterAlgorithm_v7);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

    #ifdef DEBUG
    fprintf(stderr,
            "\tret=%d  handle = %p, srcDesc = %p, diffDesc = %p, "
            "convDesc = %p, gradDesc = %p, "
            "requestedAlgoCount = %d, returnedAlgoCount = %d",
            rcs, handle, (void*)srcDesc, (void*)diffDesc,
            (void*)convDesc, (void*)gradDesc,
            requestedAlgoCount, *returnedAlgoCount);
    #endif

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (NULL == handle|| NULL == srcDesc || NULL == diffDesc
        || NULL == convDesc || NULL == gradDesc
        || NULL == returnedAlgoCount || NULL == perfResults
        || requestedAlgoCount < 1) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (srcDesc->dataType != diffDesc->dataType
        || srcDesc->dataType != gradDesc->dataType) {
        //|| srcDesc->dim[1] != diffDesc->dim[1] /*hope its NCHW->seems not...*/) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    //if (some other checks) {
    //    rcs = CUDNN_STATUS_BAD_PARAM;
    //    goto exit;
    //}

    *returnedAlgoCount = requestedAlgoCount;
    perfResults->algo = CUDNN_CONVOLUTION_BWD_FILTER_ALGO_1;    //changing
    perfResults->status = CUDNN_STATUS_SUCCESS;
    perfResults->time = -1.0;
    perfResults->memory = 2516;                                 //changing
    perfResults->determinism = CUDNN_DETERMINISTIC;
    perfResults->mathType = CUDNN_DEFAULT_MATH;

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1,
                          cudnnGetConvolutionBackwardFilterAlgorithm_v7,
                          handle, srcDesc, diffDesc, convDesc, gradDesc,
                          requestedAlgoCount, returnedAlgoCount, perfResults);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, ", perfResults = %p[%d,%d,%f,%lu,%d,%d]\n",
            perfResults, perfResults->algo, perfResults->status,
            perfResults->time, perfResults->memory,
            perfResults->determinism, perfResults->mathType);
    #endif


exit:
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionBackwardFilterAlgorithm_v7);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionBackwardFilterWorkspaceSize(
        cudnnHandle_t                       handle,
        const cudnnTensorDescriptor_t       xDesc,
        const cudnnTensorDescriptor_t       dyDesc,
        const cudnnConvolutionDescriptor_t  convDesc,
        const cudnnFilterDescriptor_t       dwDesc,
        cudnnConvolutionBwdFilterAlgo_t     algo,
        size_t                             *sizeInBytes)
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionBackwardFilterWorkspaceSize);
    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;
    *sizeInBytes = 0;
    //fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionBackwardFilterWorkspaceSize);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionForwardAlgorithm()
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionForwardAlgorithm);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionForwardAlgorithm);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionForwardAlgorithmMaxCount()
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionForwardAlgorithmMaxCount);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionForwardAlgorithmMaxCount);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionForwardAlgorithm_v7(cudnnHandle_t handle,
                                                     const cudnnTensorDescriptor_t srcDesc,
                                                     const cudnnFilterDescriptor_t filterDesc,
                                                     const cudnnConvolutionDescriptor_t convDesc,
                                                     const cudnnTensorDescriptor_t destDesc,
                                                     const int requestedAlgoCount,
                                                     int *returnedAlgoCount,
                                                     cudnnConvolutionFwdAlgoPerf_t *perfResults)
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionForwardAlgorithm_v7);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

    #ifdef DEBUG
    fprintf(stderr,
            "\tret=%d  handle = %p, "
            "srcDesc = %p, filterDesc = %p, convDesc = %p, destDesc = %p, "
            "requestedAlgoCount = %d, returnedAlgoCount = %d",
            rcs, (void*)handle,
            (void*)srcDesc, (void*)filterDesc,
            (void*)convDesc, (void*)destDesc,
            requestedAlgoCount, *returnedAlgoCount);
    #endif

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (NULL == handle|| NULL == srcDesc || NULL == filterDesc
        || NULL == convDesc || NULL == destDesc
        || NULL == returnedAlgoCount || NULL == perfResults
        || requestedAlgoCount < 1) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    if (srcDesc->nbDims < 3
        || srcDesc->nbDims != filterDesc->nbDims
        || srcDesc->nbDims != destDesc->nbDims
        || srcDesc->dataType != filterDesc->dataType
        || srcDesc->dataType != destDesc->dataType) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }

    *returnedAlgoCount = requestedAlgoCount;
    perfResults->algo = CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_PRECOMP_GEMM;
    perfResults->status = CUDNN_STATUS_SUCCESS;
    perfResults->time = -1.0;
    perfResults->memory = 301064; //FIXME...not sure where this num comes from
    perfResults->determinism = CUDNN_DETERMINISTIC;
    perfResults->mathType = CUDNN_DEFAULT_MATH;

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1,
                          cudnnGetConvolutionForwardAlgorithm_v7,
                          handle, srcDesc, filterDesc, convDesc, destDesc,
                          requestedAlgoCount, returnedAlgoCount, perfResults);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, ", perfResults = %p[%d,%d,%f,%lu,%d,%d]\n",
            perfResults, perfResults->algo, perfResults->status,
            perfResults->time, perfResults->memory,
            perfResults->determinism, perfResults->mathType);
    #endif

exit:
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionForwardAlgorithm_v7);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionForwardWorkspaceSize(
        cudnnHandle_t   handle,
        const   cudnnTensorDescriptor_t         xDesc,
        const   cudnnFilterDescriptor_t         wDesc,
        const   cudnnConvolutionDescriptor_t    convDesc,
        const   cudnnTensorDescriptor_t         yDesc,
        cudnnConvolutionFwdAlgo_t               algo,
        size_t                                 *sizeInBytes)
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionForwardWorkspaceSize);
    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;
    *sizeInBytes = 0;
    //fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionForwardWorkspaceSize);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionGroupCount()
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionGroupCount);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionGroupCount);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionMathType()
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionMathType);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionMathType);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionNdDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionNdDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionNdDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionNdForwardOutputDim()
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionNdForwardOutputDim);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionNdForwardOutputDim);
    return rcs;
}

cudnnStatus_t cudnnGetConvolutionReorderType()
{
    ENTER(cudnn_lib_entry, cudnnGetConvolutionReorderType);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetConvolutionReorderType);
    return rcs;
}

cudnnStatus_t cudnnGetCTCLossDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetCTCLossDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetCTCLossDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetCTCLossDescriptorEx()
{
    ENTER(cudnn_lib_entry, cudnnGetCTCLossDescriptorEx);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetCTCLossDescriptorEx);
    return rcs;
}

cudnnStatus_t cudnnGetCTCLossWorkspaceSize()
{
    ENTER(cudnn_lib_entry, cudnnGetCTCLossWorkspaceSize);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetCTCLossWorkspaceSize);
    return rcs;
}

cudnnStatus_t cudnnGetCudartVersion()
{
    ENTER(cudnn_lib_entry, cudnnGetCudartVersion);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetCudartVersion);
    return rcs;
}

cudnnStatus_t cudnnGetDropoutDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetDropoutDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetDropoutDescriptor);
    return rcs;
}

const char *cudnnGetErrorString(cudnnStatus_t status)
{
    ENTER(cudnn_lib_entry, cudnnGetErrorString);

    char *rs;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    assert((rs = (char *)malloc(256 * sizeof(char))));
    snprintf(rs, 44, "WRN: fake_a64fx_cuda has some trouble now!\n");
#else //USE_MocCUDA
    rs = REAL_CUDNN_CALL(cudnn_lib_entry, v2, cudnnGetErrorString, status);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%s  status = %d\n", rs, status);
    #endif

    LEAVE(cudnn_lib_entry, cudnnGetErrorString);
    return rs;
}

cudnnStatus_t cudnnGetFilter4dDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetFilter4dDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetFilter4dDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetFilterNdDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetFilterNdDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetFilterNdDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetFilterSizeInBytes()
{
    ENTER(cudnn_lib_entry, cudnnGetFilterSizeInBytes);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetFilterSizeInBytes);
    return rcs;
}

cudnnStatus_t cudnnGetFoldedConvBackwardDataDescriptors()
{
    ENTER(cudnn_lib_entry, cudnnGetFoldedConvBackwardDataDescriptors);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetFoldedConvBackwardDataDescriptors);
    return rcs;
}

cudnnStatus_t cudnnGetFusedOpsConstParamPackAttribute()
{
    ENTER(cudnn_lib_entry, cudnnGetFusedOpsConstParamPackAttribute);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetFusedOpsConstParamPackAttribute);
    return rcs;
}

cudnnStatus_t cudnnGetFusedOpsVariantParamPackAttribute()
{
    ENTER(cudnn_lib_entry, cudnnGetFusedOpsVariantParamPackAttribute);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetFusedOpsVariantParamPackAttribute);
    return rcs;
}

cudnnStatus_t cudnnGetLRNDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetLRNDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetLRNDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetMultiHeadAttnBuffers()
{
    ENTER(cudnn_lib_entry, cudnnGetMultiHeadAttnBuffers);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetMultiHeadAttnBuffers);
    return rcs;
}

cudnnStatus_t cudnnGetMultiHeadAttnWeights()
{
    ENTER(cudnn_lib_entry, cudnnGetMultiHeadAttnWeights);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetMultiHeadAttnWeights);
    return rcs;
}

cudnnStatus_t cudnnGetOpTensorDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetOpTensorDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetOpTensorDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetPooling2dDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetPooling2dDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetPooling2dDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetPooling2dForwardOutputDim()
{
    ENTER(cudnn_lib_entry, cudnnGetPooling2dForwardOutputDim);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetPooling2dForwardOutputDim);
    return rcs;
}

cudnnStatus_t cudnnGetPoolingNdDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetPoolingNdDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetPoolingNdDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetPoolingNdForwardOutputDim()
{
    ENTER(cudnn_lib_entry, cudnnGetPoolingNdForwardOutputDim);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetPoolingNdForwardOutputDim);
    return rcs;
}

cudnnStatus_t cudnnGetProperty()
{
    ENTER(cudnn_lib_entry, cudnnGetProperty);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetProperty);
    return rcs;
}

cudnnStatus_t cudnnGetReduceTensorDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetReduceTensorDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetReduceTensorDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetReductionIndicesSize()
{
    ENTER(cudnn_lib_entry, cudnnGetReductionIndicesSize);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetReductionIndicesSize);
    return rcs;
}

cudnnStatus_t cudnnGetReductionWorkspaceSize()
{
    ENTER(cudnn_lib_entry, cudnnGetReductionWorkspaceSize);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetReductionWorkspaceSize);
    return rcs;
}

cudnnStatus_t cudnnGetRNNBackwardDataAlgorithmMaxCount()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNBackwardDataAlgorithmMaxCount);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNBackwardDataAlgorithmMaxCount);
    return rcs;
}

cudnnStatus_t cudnnGetRNNBackwardWeightsAlgorithmMaxCount()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNBackwardWeightsAlgorithmMaxCount);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNBackwardWeightsAlgorithmMaxCount);
    return rcs;
}

cudnnStatus_t cudnnGetRNNBiasMode()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNBiasMode);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNBiasMode);
    return rcs;
}

cudnnStatus_t cudnnGetRNNDataDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNDataDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNDataDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetRNNDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetRNNForwardInferenceAlgorithmMaxCount()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNForwardInferenceAlgorithmMaxCount);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNForwardInferenceAlgorithmMaxCount);
    return rcs;
}

cudnnStatus_t cudnnGetRNNForwardTrainingAlgorithmMaxCount()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNForwardTrainingAlgorithmMaxCount);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNForwardTrainingAlgorithmMaxCount);
    return rcs;
}

cudnnStatus_t cudnnGetRNNLinLayerBiasParams()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNLinLayerBiasParams);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNLinLayerBiasParams);
    return rcs;
}

cudnnStatus_t cudnnGetRNNLinLayerMatrixParams()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNLinLayerMatrixParams);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNLinLayerMatrixParams);
    return rcs;
}

cudnnStatus_t cudnnGetRNNMatrixMathType()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNMatrixMathType);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNMatrixMathType);
    return rcs;
}

cudnnStatus_t cudnnGetRNNPaddingMode()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNPaddingMode);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNPaddingMode);
    return rcs;
}

cudnnStatus_t cudnnGetRNNParamsSize()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNParamsSize);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNParamsSize);
    return rcs;
}

cudnnStatus_t cudnnGetRNNProjectionLayers()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNProjectionLayers);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNProjectionLayers);
    return rcs;
}

cudnnStatus_t cudnnGetRNNTrainingReserveSize()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNTrainingReserveSize);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNTrainingReserveSize);
    return rcs;
}

cudnnStatus_t cudnnGetRNNWorkspaceSize()
{
    ENTER(cudnn_lib_entry, cudnnGetRNNWorkspaceSize);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetRNNWorkspaceSize);
    return rcs;
}

cudnnStatus_t cudnnGetSeqDataDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetSeqDataDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetSeqDataDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetStream()
{
    ENTER(cudnn_lib_entry, cudnnGetStream);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetStream);
    return rcs;
}

cudnnStatus_t cudnnGetTensor4dDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetTensor4dDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetTensor4dDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetTensorNdDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetTensorNdDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetTensorNdDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetTensorSizeInBytes()
{
    ENTER(cudnn_lib_entry, cudnnGetTensorSizeInBytes);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetTensorSizeInBytes);
    return rcs;
}

cudnnStatus_t cudnnGetTensorTransformDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnGetTensorTransformDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetTensorTransformDescriptor);
    return rcs;
}

cudnnStatus_t cudnnGetVersion()
{
    ENTER(cudnn_lib_entry, cudnnGetVersion);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnGetVersion);
    return rcs;
}

cudnnStatus_t cudnnIm2Col()
{
    ENTER(cudnn_lib_entry, cudnnIm2Col);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnIm2Col);
    return rcs;
}

cudnnStatus_t cudnnInitTransformDest()
{
    ENTER(cudnn_lib_entry, cudnnInitTransformDest);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnInitTransformDest);
    return rcs;
}

cudnnStatus_t cudnnLRNCrossChannelBackward()
{
    ENTER(cudnn_lib_entry, cudnnLRNCrossChannelBackward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnLRNCrossChannelBackward);
    return rcs;
}

cudnnStatus_t cudnnLRNCrossChannelForward()
{
    ENTER(cudnn_lib_entry, cudnnLRNCrossChannelForward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnLRNCrossChannelForward);
    return rcs;
}

cudnnStatus_t cudnnMakeFusedOpsPlan()
{
    ENTER(cudnn_lib_entry, cudnnMakeFusedOpsPlan);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnMakeFusedOpsPlan);
    return rcs;
}

cudnnStatus_t cudnnMultiHeadAttnBackwardData()
{
    ENTER(cudnn_lib_entry, cudnnMultiHeadAttnBackwardData);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnMultiHeadAttnBackwardData);
    return rcs;
}

cudnnStatus_t cudnnMultiHeadAttnBackwardWeights()
{
    ENTER(cudnn_lib_entry, cudnnMultiHeadAttnBackwardWeights);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnMultiHeadAttnBackwardWeights);
    return rcs;
}

cudnnStatus_t cudnnMultiHeadAttnForward()
{
    ENTER(cudnn_lib_entry, cudnnMultiHeadAttnForward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnMultiHeadAttnForward);
    return rcs;
}

cudnnStatus_t cudnnOpTensor()
{
    ENTER(cudnn_lib_entry, cudnnOpTensor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnOpTensor);
    return rcs;
}

cudnnStatus_t cudnnPoolingBackward()
{
    ENTER(cudnn_lib_entry, cudnnPoolingBackward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnPoolingBackward);
    return rcs;
}

cudnnStatus_t cudnnPoolingForward()
{
    ENTER(cudnn_lib_entry, cudnnPoolingForward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnPoolingForward);
    return rcs;
}

cudnnStatus_t cudnnQueryRuntimeError()
{
    ENTER(cudnn_lib_entry, cudnnQueryRuntimeError);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnQueryRuntimeError);
    return rcs;
}

cudnnStatus_t cudnnReduceTensor()
{
    ENTER(cudnn_lib_entry, cudnnReduceTensor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnReduceTensor);
    return rcs;
}

cudnnStatus_t cudnnReorderFilterAndBias()
{
    ENTER(cudnn_lib_entry, cudnnReorderFilterAndBias);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnReorderFilterAndBias);
    return rcs;
}

cudnnStatus_t cudnnRestoreAlgorithm()
{
    ENTER(cudnn_lib_entry, cudnnRestoreAlgorithm);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnRestoreAlgorithm);
    return rcs;
}

cudnnStatus_t cudnnRestoreDropoutDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnRestoreDropoutDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnRestoreDropoutDescriptor);
    return rcs;
}

cudnnStatus_t cudnnRNNBackwardData()
{
    ENTER(cudnn_lib_entry, cudnnRNNBackwardData);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnRNNBackwardData);
    return rcs;
}

cudnnStatus_t cudnnRNNBackwardDataEx()
{
    ENTER(cudnn_lib_entry, cudnnRNNBackwardDataEx);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnRNNBackwardDataEx);
    return rcs;
}

cudnnStatus_t cudnnRNNBackwardWeights()
{
    ENTER(cudnn_lib_entry, cudnnRNNBackwardWeights);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnRNNBackwardWeights);
    return rcs;
}

cudnnStatus_t cudnnRNNBackwardWeightsEx()
{
    ENTER(cudnn_lib_entry, cudnnRNNBackwardWeightsEx);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnRNNBackwardWeightsEx);
    return rcs;
}

cudnnStatus_t cudnnRNNForwardInference()
{
    ENTER(cudnn_lib_entry, cudnnRNNForwardInference);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnRNNForwardInference);
    return rcs;
}

cudnnStatus_t cudnnRNNForwardInferenceEx()
{
    ENTER(cudnn_lib_entry, cudnnRNNForwardInferenceEx);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnRNNForwardInferenceEx);
    return rcs;
}

cudnnStatus_t cudnnRNNForwardTraining()
{
    ENTER(cudnn_lib_entry, cudnnRNNForwardTraining);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnRNNForwardTraining);
    return rcs;
}

cudnnStatus_t cudnnRNNForwardTrainingEx()
{
    ENTER(cudnn_lib_entry, cudnnRNNForwardTrainingEx);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnRNNForwardTrainingEx);
    return rcs;
}

cudnnStatus_t cudnnRNNGetClip()
{
    ENTER(cudnn_lib_entry, cudnnRNNGetClip);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnRNNGetClip);
    return rcs;
}

cudnnStatus_t cudnnRNNSetClip()
{
    ENTER(cudnn_lib_entry, cudnnRNNSetClip);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnRNNSetClip);
    return rcs;
}

cudnnStatus_t cudnnSaveAlgorithm()
{
    ENTER(cudnn_lib_entry, cudnnSaveAlgorithm);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSaveAlgorithm);
    return rcs;
}

cudnnStatus_t cudnnScaleTensor()
{
    ENTER(cudnn_lib_entry, cudnnScaleTensor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnScaleTensor);
    return rcs;
}

cudnnStatus_t cudnnSetActivationDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetActivationDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetActivationDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetAlgorithmDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetAlgorithmDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetAlgorithmDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetAlgorithmPerformance()
{
    ENTER(cudnn_lib_entry, cudnnSetAlgorithmPerformance);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetAlgorithmPerformance);
    return rcs;
}

cudnnStatus_t cudnnSetAttnDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetAttnDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetAttnDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetCallback()
{
    ENTER(cudnn_lib_entry, cudnnSetCallback);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetCallback);
    return rcs;
}

cudnnStatus_t cudnnSetConvolution2dDescriptor(cudnnConvolutionDescriptor_t convDesc,
                                              int pad_h, int pad_w,
                                              int u, int v,
                                              int dilation_h, int dilation_w,
                                              cudnnConvolutionMode_t mode,
                                              cudnnDataType_t computeType)
{
    ENTER(cudnn_lib_entry, cudnnSetConvolution2dDescriptor);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (NULL == convDesc || pad_h < 0 || pad_w < 0 || u < 1 || v < 1
        || dilation_h < 1 || dilation_w < 1
        || (CUDNN_CONVOLUTION != mode && CUDNN_CROSS_CORRELATION != mode)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }

    (*convDesc).arrayLength = 2;
    int arrayLengthInByte = (*convDesc).arrayLength * sizeof(int);
    assert(((*convDesc).pad = (int *)malloc(arrayLengthInByte)));
    (*convDesc).pad[0] = pad_h;
    (*convDesc).pad[1] = pad_w;
    assert(((*convDesc).filterStride = (int *)malloc(arrayLengthInByte)));
    (*convDesc).filterStride[0] = u;
    (*convDesc).filterStride[1] = v;
    assert(((*convDesc).dilation = (int *)malloc(arrayLengthInByte)));
    (*convDesc).dilation[0] = dilation_h;
    (*convDesc).dilation[1] = dilation_w;
    (*convDesc).mode = mode;
    (*convDesc).computeType = computeType;

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnSetConvolution2dDescriptor,
                          convDesc, pad_h, pad_w, u, v, dilation_h, dilation_w,
                          mode, computeType);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr,
            "\tret=%d  convDesc = %p, pad_h = %d, pad_w = %d, "
            "u = %d, v = %d, dilation_h = %d, dilation_w = %d, "
            "mode = %d, computeType = %d\n",
            rcs, (void*)convDesc, pad_h, pad_w,
            u, v, dilation_h, dilation_w,
            mode, computeType);
    #endif

exit:
    LEAVE(cudnn_lib_entry, cudnnSetConvolution2dDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetConvolutionGroupCount(cudnnConvolutionDescriptor_t convDesc,
                                            int groupCount)
{
    ENTER(cudnn_lib_entry, cudnnSetConvolutionGroupCount);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (NULL == convDesc) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }

    (*convDesc).groupCount = groupCount;

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1,
                          cudnnSetConvolutionGroupCount,
                          convDesc, groupCount);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  convDesc = %p, groupCount = %d\n",
            rcs, convDesc, groupCount);
    #endif

exit:
    LEAVE(cudnn_lib_entry, cudnnSetConvolutionGroupCount);
    return rcs;
}

cudnnStatus_t cudnnSetConvolutionMathType(cudnnConvolutionDescriptor_t convDesc,
                                          cudnnMathType_t mathType)
{
    ENTER(cudnn_lib_entry, cudnnSetConvolutionMathType);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (NULL == convDesc
        || (CUDNN_DEFAULT_MATH != mathType
            && CUDNN_TENSOR_OP_MATH != mathType
            && CUDNN_TENSOR_OP_MATH_ALLOW_CONVERSION != mathType)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }

    (*convDesc).mathType = mathType;

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnSetConvolutionMathType,
                          convDesc, mathType);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  convDesc = %p, mathType = %d\n",
            rcs, convDesc, mathType);
    #endif

exit:
    LEAVE(cudnn_lib_entry, cudnnSetConvolutionMathType);
    return rcs;
}

cudnnStatus_t cudnnSetConvolutionNdDescriptor(cudnnConvolutionDescriptor_t convDesc,
                                              int arrayLength,
                                              const int padA[],
                                              const int filterStrideA[],
                                              const int dilationA[],
                                              cudnnConvolutionMode_t mode,
                                              cudnnDataType_t computeType)
{
    ENTER(cudnn_lib_entry, cudnnSetConvolutionNdDescriptor);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (NULL == convDesc || arrayLength < 0
        || (CUDNN_CONVOLUTION != mode && CUDNN_CROSS_CORRELATION != mode)
        || (computeType < 0 || computeType > 8)) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    int i;
    for (i = 0; i < arrayLength; i++)
        if (padA[i] < 0 || filterStrideA[i] < 1 || dilationA[i] < 1) {
            rcs = CUDNN_STATUS_BAD_PARAM;
            goto exit;
        }
    if (arrayLength > CUDNN_DIM_MAX) {
        rcs = CUDNN_STATUS_NOT_SUPPORTED;
        goto exit;
    }

    (*convDesc).arrayLength = arrayLength;
    const size_t arrayLengthInByte = arrayLength * sizeof(int);
    assert(((*convDesc).pad = (int *)malloc(arrayLengthInByte)));
    memcpy((*convDesc).pad, padA, arrayLengthInByte);
    assert(((*convDesc).filterStride = (int *)malloc(arrayLengthInByte)));
    memcpy((*convDesc).filterStride, filterStrideA, arrayLengthInByte);
    assert(((*convDesc).dilation = (int *)malloc(arrayLengthInByte)));
    memcpy((*convDesc).dilation, dilationA, arrayLengthInByte);
    (*convDesc).mode = mode;
    (*convDesc).computeType = computeType;

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1,
                          cudnnSetConvolutionNdDescriptor,
                          convDesc, arrayLength, padA, filterStrideA, dilationA,
                          mode, computeType);
#endif

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  ", rcs);
    convDescToStr("convDesc", convDesc);
    fprintf(stderr, ", mode = %d, computeType = %d\n", mode, computeType);
    #endif

exit:
    LEAVE(cudnn_lib_entry, cudnnSetConvolutionNdDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetConvolutionReorderType()
{
    ENTER(cudnn_lib_entry, cudnnSetConvolutionReorderType);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetConvolutionReorderType);
    return rcs;
}

cudnnStatus_t cudnnSetCTCLossDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetCTCLossDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetCTCLossDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetCTCLossDescriptorEx()
{
    ENTER(cudnn_lib_entry, cudnnSetCTCLossDescriptorEx);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetCTCLossDescriptorEx);
    return rcs;
}

cudnnStatus_t cudnnSetDropoutDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetDropoutDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetDropoutDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetFilter4dDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetFilter4dDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetFilter4dDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetFilterNdDescriptor(cudnnFilterDescriptor_t filterDesc,
                                         cudnnDataType_t dataType,
                                         cudnnTensorFormat_t format,
                                         int nbDims,
                                         const int filterDimA[])
{
    ENTER(cudnn_lib_entry, cudnnSetFilterNdDescriptor);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if ((CUDNN_TENSOR_NCHW != format && CUDNN_TENSOR_NHWC != format
         && CUDNN_TENSOR_NCHW_VECT_C != format)
        || dataType < CUDNN_DATA_FLOAT || dataType > CUDNN_DATA_INT8x32) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    int i;
    for (i = 0; i < nbDims; i++)
        if (filterDimA[i] < 1) {
            rcs = CUDNN_STATUS_BAD_PARAM;
            goto exit;
        }
    if (nbDims > CUDNN_DIM_MAX) {
        rcs = CUDNN_STATUS_NOT_SUPPORTED;
        goto exit;
    }

    (*filterDesc).dataType = dataType;
    (*filterDesc).format = format;
    (*filterDesc).nbDims = nbDims;
    const size_t nbDimsInByte = nbDims * sizeof(int);
    assert(((*filterDesc).dim = (int *)malloc(nbDimsInByte)));
    memcpy((*filterDesc).dim, filterDimA, nbDimsInByte);

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnSetFilterNdDescriptor,
                          filterDesc, dataType, format, nbDims,
                          filterDimA);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  ", rcs);
    filtDescToStr("filterDesc", filterDesc);
    fprintf(stderr, "\n");
    #endif

exit:
    LEAVE(cudnn_lib_entry, cudnnSetFilterNdDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetFusedOpsConstParamPackAttribute()
{
    ENTER(cudnn_lib_entry, cudnnSetFusedOpsConstParamPackAttribute);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetFusedOpsConstParamPackAttribute);
    return rcs;
}

cudnnStatus_t cudnnSetFusedOpsVariantParamPackAttribute()
{
    ENTER(cudnn_lib_entry, cudnnSetFusedOpsVariantParamPackAttribute);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetFusedOpsVariantParamPackAttribute);
    return rcs;
}

cudnnStatus_t cudnnSetLRNDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetLRNDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetLRNDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetOpTensorDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetOpTensorDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetOpTensorDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetPersistentRNNPlan()
{
    ENTER(cudnn_lib_entry, cudnnSetPersistentRNNPlan);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetPersistentRNNPlan);
    return rcs;
}

cudnnStatus_t cudnnSetPooling2dDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetPooling2dDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetPooling2dDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetPoolingNdDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetPoolingNdDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetPoolingNdDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetReduceTensorDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetReduceTensorDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetReduceTensorDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetRNNAlgorithmDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetRNNAlgorithmDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetRNNAlgorithmDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetRNNBiasMode()
{
    ENTER(cudnn_lib_entry, cudnnSetRNNBiasMode);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetRNNBiasMode);
    return rcs;
}

cudnnStatus_t cudnnSetRNNDataDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetRNNDataDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetRNNDataDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetRNNDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetRNNDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetRNNDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetRNNDescriptor_v5()
{
    ENTER(cudnn_lib_entry, cudnnSetRNNDescriptor_v5);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetRNNDescriptor_v5);
    return rcs;
}

cudnnStatus_t cudnnSetRNNDescriptor_v6()
{
    ENTER(cudnn_lib_entry, cudnnSetRNNDescriptor_v6);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetRNNDescriptor_v6);
    return rcs;
}

cudnnStatus_t cudnnSetRNNMatrixMathType()
{
    ENTER(cudnn_lib_entry, cudnnSetRNNMatrixMathType);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetRNNMatrixMathType);
    return rcs;
}

cudnnStatus_t cudnnSetRNNPaddingMode()
{
    ENTER(cudnn_lib_entry, cudnnSetRNNPaddingMode);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetRNNPaddingMode);
    return rcs;
}

cudnnStatus_t cudnnSetRNNProjectionLayers()
{
    ENTER(cudnn_lib_entry, cudnnSetRNNProjectionLayers);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetRNNProjectionLayers);
    return rcs;
}

cudnnStatus_t cudnnSetSeqDataDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetSeqDataDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetSeqDataDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetSpatialTransformerNdDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetSpatialTransformerNdDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetSpatialTransformerNdDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetStream(cudnnHandle_t handle, cudaStream_t streamId)
{
    ENTER(cudnn_lib_entry, cudnnSetStream);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (NULL == handle) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }

    (*handle).stream = streamId;

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnSetStream, handle,
                          streamId);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  handle = %p, streamId = %p\n",
            rcs, (void*)handle, (void*)streamId);
    #endif

exit:
    LEAVE(cudnn_lib_entry, cudnnSetStream);
    return rcs;
}

cudnnStatus_t cudnnSetTensor()
{
    ENTER(cudnn_lib_entry, cudnnSetTensor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetTensor);
    return rcs;
}

cudnnStatus_t cudnnSetTensor4dDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetTensor4dDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetTensor4dDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetTensor4dDescriptorEx()
{
    ENTER(cudnn_lib_entry, cudnnSetTensor4dDescriptorEx);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetTensor4dDescriptorEx);
    return rcs;
}

cudnnStatus_t cudnnSetTensorNdDescriptor(cudnnTensorDescriptor_t tensorDesc,
                                         cudnnDataType_t dataType,
                                         int nbDims,
                                         const int dimA[],
                                         const int strideA[])
{
    ENTER(cudnn_lib_entry, cudnnSetTensorNdDescriptor);

    cudnnStatus_t rcs = CUDNN_STATUS_SUCCESS;

#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (nbDims < 4 || nbDims > CUDNN_DIM_MAX) {
        rcs = CUDNN_STATUS_NOT_SUPPORTED;
        goto exit;
    }
    if (dataType < CUDNN_DATA_FLOAT || dataType > CUDNN_DATA_INT8x32) {
        rcs = CUDNN_STATUS_BAD_PARAM;
        goto exit;
    }
    int i = 0;
    for (i = 0; i < nbDims; i++)
        if (dimA[i] < 1) {
            rcs = CUDNN_STATUS_BAD_PARAM;
            goto exit;
        }

    (*tensorDesc).dataType = dataType;
    if (4 == nbDims)
        (*tensorDesc).format = CUDNN_TENSOR_NCHW;
    else
        (*tensorDesc).format = 0xff; //no clue
    (*tensorDesc).nbDims = nbDims;
    const size_t nbDimsInByte = nbDims * sizeof(int);
    assert(((*tensorDesc).dim = (int *)malloc(nbDimsInByte)));
    memcpy((*tensorDesc).dim, dimA, nbDimsInByte);
    assert(((*tensorDesc).stride = (int *)malloc(nbDimsInByte)));
    memcpy((*tensorDesc).stride, strideA, nbDimsInByte);

#else //USE_MocCUDA
    rcs = REAL_CUDNN_CALL(cudnn_lib_entry, v1, cudnnSetTensorNdDescriptor,
                          tensorDesc, dataType, nbDims, dimA, strideA);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  ", rcs);
    tensDescToStr("tensorDesc", tensorDesc);
    fprintf(stderr, "\n");
    #endif

exit:
    LEAVE(cudnn_lib_entry, cudnnSetTensorNdDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSetTensorNdDescriptorEx()
{
    ENTER(cudnn_lib_entry, cudnnSetTensorNdDescriptorEx);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetTensorNdDescriptorEx);
    return rcs;
}

cudnnStatus_t cudnnSetTensorTransformDescriptor()
{
    ENTER(cudnn_lib_entry, cudnnSetTensorTransformDescriptor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSetTensorTransformDescriptor);
    return rcs;
}

cudnnStatus_t cudnnSoftmaxBackward()
{
    ENTER(cudnn_lib_entry, cudnnSoftmaxBackward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSoftmaxBackward);
    return rcs;
}

cudnnStatus_t cudnnSoftmaxForward()
{
    ENTER(cudnn_lib_entry, cudnnSoftmaxForward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSoftmaxForward);
    return rcs;
}

cudnnStatus_t cudnnSpatialTfGridGeneratorBackward()
{
    ENTER(cudnn_lib_entry, cudnnSpatialTfGridGeneratorBackward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSpatialTfGridGeneratorBackward);
    return rcs;
}

cudnnStatus_t cudnnSpatialTfGridGeneratorForward()
{
    ENTER(cudnn_lib_entry, cudnnSpatialTfGridGeneratorForward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSpatialTfGridGeneratorForward);
    return rcs;
}

cudnnStatus_t cudnnSpatialTfSamplerBackward()
{
    ENTER(cudnn_lib_entry, cudnnSpatialTfSamplerBackward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSpatialTfSamplerBackward);
    return rcs;
}

cudnnStatus_t cudnnSpatialTfSamplerForward()
{
    ENTER(cudnn_lib_entry, cudnnSpatialTfSamplerForward);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnSpatialTfSamplerForward);
    return rcs;
}

cudnnStatus_t cudnnTransformFilter()
{
    ENTER(cudnn_lib_entry, cudnnTransformFilter);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnTransformFilter);
    return rcs;
}

cudnnStatus_t cudnnTransformTensor()
{
    ENTER(cudnn_lib_entry, cudnnTransformTensor);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnTransformTensor);
    return rcs;
}

cudnnStatus_t cudnnTransformTensorEx()
{
    ENTER(cudnn_lib_entry, cudnnTransformTensorEx);
    cudnnStatus_t rcs = CUDNN_STATUS_NOT_SUPPORTED;
    fprintf(stderr, "nope"); show_backtrace();
    LEAVE(cudnn_lib_entry, cudnnTransformTensorEx);
    return rcs;
}
