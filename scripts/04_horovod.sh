#!/bin/bash
SDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )" && pwd )"
source "${SDIR}/host.env"
source "${SDIR}/../dep/python.env"

cd ${SDIR}/../dep/horovod
HOROVODDIR=$(pwd)

buildDEBUG=0
buildRELEASE=1

python3 -m pip install --upgrade cffi==1.15.0	#XXX: update first, otherwise horovod tries and fails
python3 -m pip install --upgrade pytest==5.4.3
python3 -m pip install --upgrade mock==3.0.5

git submodule update --init --recursive
git checkout -- \
	./setup.py

if [[ "$(hostname -s)" = "kiev"* ]]; then

	if [ ${buildDEBUG} -ge 1 ]; then
		rm -rf build
		CC="${MocCC}" CFLAGS="-UNDEBUG -g -O2" CXX="${MocCXX}" CXXFLAGS="-UNDEBUG -g -O2" \
			HOROVOD_WITH_PYTORCH=1 HOROVOD_WITH_MPI=1 HOROVOD_WITHOUT_GLOO=1 \
			HOROVOD_WITHOUT_TENSORFLOW=1 HOROVOD_WITHOUT_MXNET=1 \
			HOROVOD_CUDA_HOME="${CUDA_TOOLKIT_ROOT_DIR}" HOROVOD_GPU=CUDA \
		python3 setup.py bdist_wheel 2>&1 | tee comp_debug
		mkdir -p debug_dist ; mv dist/horovod-*.whl debug_dist/
	fi

	if [ ${buildRELEASE} -ge 1 ]; then
		rm -rf build
		CC="${MocCC}" CFLAGS="${MocCFLAGS}" CXX="${MocCXX}" CXXFLAGS="${MocCXXFLAGS}" \
			HOROVOD_WITH_PYTORCH=1 HOROVOD_WITH_MPI=1 HOROVOD_WITHOUT_GLOO=1 \
			HOROVOD_WITHOUT_TENSORFLOW=1 HOROVOD_WITHOUT_MXNET=1 \
			HOROVOD_CUDA_HOME="${CUDA_TOOLKIT_ROOT_DIR}" HOROVOD_GPU=CUDA \
		python3 setup.py bdist_wheel 2>&1 | tee comp_release
			#XXX: "if you have a proprietary MPI implementation with GPU support..." NO!!!
			#HOROVOD_GPU_ALLREDUCE=MPI HOROVOD_GPU_ALLGATHER=MPI \
			#HOROVOD_GPU_BROADCAST=MPI HOROVOD_CPU_OPERATIONS=MPI \
	fi

elif [[ "$(hostname -s)" = "epyc"* ]]; then

	sleep 0

elif lscpu | grep 'sve' >/dev/null 2>&1; then

	if [ ${buildDEBUG} -ge 1 ]; then
		rm -rf build
		#spack load gcc@11.2.0 /nphnrhl
		#LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${SDIR}/../dep/pytorch/ssl2/lib" \
		#OMPI_CC="gcc" OMPI_CXX="g++" \
		#CC="gcc" CFLAGS="-ffast-math -march=native -msve-vector-bits=scalable" \
		#CXX="g++" CXXFLAGS="-ffast-math -march=native -msve-vector-bits=scalable" \
		INCLUDE="" C_INCLUDE_PATH="" CPLUS_INCLUDE_PATH="" \
			LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${SDIR}/../dep/pytorch/ssl2/lib" \
			PATH="${PATH}:${CUDA_TOOLKIT_ROOT_DIR}/bin" \
			CC="${MocCC}" CFLAGS="-UNDEBUG -g -O2" CXX="${MocCXX}" CXXFLAGS="-UNDEBUG -g -O2" \
			LD="${MocCC}" LDSHARED="${MocCC} -shared" \
			HOROVOD_MPICXX_SHOW="echo clang++ -I$(readlink -f $(dirname $(which mpifcc))/../include/mpi/fujitsu) -pthread -L$(readlink -f $(dirname $(which mpifcc))/../lib64) -lmpi_cxx -lmpi_cxx -lmpi -lfjstring_internal" \
			HOROVOD_WITH_PYTORCH=1 HOROVOD_WITH_MPI=1 HOROVOD_WITHOUT_GLOO=1 \
			HOROVOD_WITHOUT_TENSORFLOW=1 HOROVOD_WITHOUT_MXNET=1 \
			HOROVOD_CUDA_HOME="${CUDA_TOOLKIT_ROOT_DIR}" HOROVOD_GPU=CUDA \
		python3 setup.py bdist_wheel 2>&1 | tee comp_debug
		mkdir -p debug_dist ; mv dist/horovod-*.whl debug_dist/
	fi

	if [ ${buildRELEASE} -ge 1 ]; then
		rm -rf build
		INCLUDE="" C_INCLUDE_PATH="" CPLUS_INCLUDE_PATH="" \
			LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${SDIR}/../dep/pytorch/ssl2/lib" \
			PATH="${PATH}:${CUDA_TOOLKIT_ROOT_DIR}/bin" \
			CC="${MocCC}" CFLAGS="${MocCFLAGS}" CXX="${MocCXX}" CXXFLAGS="${MocCXXFLAGS}" \
			LD="${MocCC}" LDSHARED="${MocCC} -shared" \
			HOROVOD_MPICXX_SHOW="echo clang++ -I$(readlink -f $(dirname $(which mpifcc))/../include/mpi/fujitsu) -pthread -L$(readlink -f $(dirname $(which mpifcc))/../lib64) -lmpi_cxx -lmpi_cxx -lmpi -lfjstring_internal" \
			HOROVOD_WITH_PYTORCH=1 HOROVOD_WITH_MPI=1 HOROVOD_WITHOUT_GLOO=1 \
			HOROVOD_WITHOUT_TENSORFLOW=1 HOROVOD_WITHOUT_MXNET=1 \
			HOROVOD_CUDA_HOME="${CUDA_TOOLKIT_ROOT_DIR}" HOROVOD_GPU=CUDA \
		python3 setup.py bdist_wheel 2>&1 | tee comp_release
	fi

	#try:
	# source ./init.env
	# echo -e '#include <cblas.h>\n#include <omp.h>\nint dummy (void);\nint dummy (void) { return 0; }' > dummy.c
	# clang -c dummy.c -fPIC
	# clang dummy.o -fuse-ld=lld -I$(readlink -f $(dirname $(which mpifcc))/../include) -L$(readlink -f $(dirname $(which mpifcc))/../lib64) -Wl,-rpath=$(readlink -f $(dirname $(which clang))/../lib) $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjhpctag.o $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjlang08.o $(readlink -f $(dirname $(which mpifcc))/../lib64)/fjomp.o -lfjomphk -lfjomp -lfj90rt2 -lssl2mtexsve -lssl2mtsve -lfj90i -lfj90fmt_sve -lfj90f -lfjsrcinfo -lfj90rt -lfjompcrt -lfjprofcore -lfjprofomp -lm -lrt -latomic -lpthread -lelf -lz -ldl -shared -o libopenblas.so
	# CURRDIR=$(pwd) ; cd dep/horovod/test/ ; LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${CURRDIR}" LD_PRELOAD="${CURRDIR}/libopenblas.so" mpiexec -n 1 pytest -v --capture=no ./test_torch.py ; cd -
	# CURRDIR=$(pwd) ; cd dep/horovod/test/ ; echo LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${CURRDIR}/dep/pytorch/ssl2/lib" LD_PRELOAD="${CURRDIR}/lib/libMocCUDA.so" pytest -v --capture=no ./test_torch.py >./a.sh ; chmod +x ./a.sh ; mpiexec -n 1 bash -c ./a.sh ; cd -
	#DEBUG='-DDEBUG -O0 -g' GrandCentralDispatch='-DUSE_GCD=0' make -f Makefile.${MocHOST} -B
	#CURRDIR=$(pwd) ; cd dep/horovod/test/ ; echo LD_LIBRARY_PATH=\"${LD_LIBRARY_PATH}:${CURRDIR}/dep/pytorch/ssl2/lib\" LD_PRELOAD=\"${CURRDIR}/lib/libMocCUDA.so\" python ./test_torch.py >./a.sh ; chmod +x ./a.sh ; echo -e 'run\nbacktrace\n' >cmd.f ; rm -rf ./gdbx ; mpiexec -gdbx "$(pwd)/cmd.f" -fjdbg-out-dir "$(pwd)" -n 1 bash -c ./a.sh ; cd -

else
	echo 'Err: unknown system; config please' ; exit 1
fi

cd "${SDIR}"/   # step outside build dir to prevent strange pip issues
if ! python3 -m pip list | grep horovod >/dev/null 2>&1 ; then
	python3 -m pip install --upgrade "${HOROVODDIR}"/dist/horovod-*.whl
else
	python3 -m pip install --upgrade --force-reinstall --no-deps "${HOROVODDIR}"/dist/horovod-*.whl
fi

#from official repo:
# python3 -m pip install --upgrade pytest==5.4.3
# python3 -m pip install --upgrade mock==3.0.5
# HOROVOD_WITHOUT_GLOO=1 python3 -m pip install \
#	--upgrade --force-reinstall --no-deps --no-cache-dir 'horovod[pytorch]==0.19.5'
# mpirun -x PATH -x LD_LIBRARY_PATH -np 2 -mca btl ^openib \
#	pytest -v --capture=no ./dep/horovod/test/test_torch.py
# mpirun -x PATH -x LD_LIBRARY_PATH -H kiev2,kiev3 -np 2 \
#	pytest -v --capture=no ./dep/horovod/test/test_torch.py
