#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

#include "passthrough.h"

int _cudart_inited_ = 0;

entry_t cudart_lib_entry[] = {
    {.fn_ptr = NULL, .name = "cudaCreateChannelDesc"},
    {.fn_ptr = NULL, .name = "cudaDeviceGetAttribute"},
    {.fn_ptr = NULL, .name = "cudaDeviceGetStreamPriorityRange"},
    {.fn_ptr = NULL, .name = "cudaDeviceSynchronize"},
    {.fn_ptr = NULL, .name = "cudaEventCreateWithFlags"},
    {.fn_ptr = NULL, .name = "cudaEventDestroy"},
    {.fn_ptr = NULL, .name = "cudaEventQuery"},
    {.fn_ptr = NULL, .name = "cudaEventRecord"},
    {.fn_ptr = NULL, .name = "cudaFree"},
    {.fn_ptr = NULL, .name = "cudaFreeHost"},
    {.fn_ptr = NULL, .name = "cudaFuncGetAttributes"},
    {.fn_ptr = NULL, .name = "cudaGetDevice"},
    {.fn_ptr = NULL, .name = "cudaGetDeviceCount"},
    {.fn_ptr = NULL, .name = "cudaGetDeviceProperties"},
    {.fn_ptr = NULL, .name = "cudaGetLastError"},
    {.fn_ptr = NULL, .name = "cudaHostAlloc"},
    {.fn_ptr = NULL, .name = "cudaHostGetDevicePointer"},
    {.fn_ptr = NULL, .name = "cudaLaunchKernel"},
    {.fn_ptr = NULL, .name = "cudaMalloc"},
    {.fn_ptr = NULL, .name = "cudaMemcpy"},
    {.fn_ptr = NULL, .name = "cudaMemcpyAsync"},
    {.fn_ptr = NULL, .name = "cudaMemGetInfo"},
    {.fn_ptr = NULL, .name = "cudaMemsetAsync"},
    {.fn_ptr = NULL, .name = "cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags"},
    {.fn_ptr = NULL, .name = "cudaMemsetAsync"},
    {.fn_ptr = NULL, .name = "cudaSetDevice"},
    {.fn_ptr = NULL, .name = "cudaStreamCreateWithFlags"},
    {.fn_ptr = NULL, .name = "cudaStreamCreateWithPriority"},
    {.fn_ptr = NULL, .name = "cudaStreamDestroy"},
    {.fn_ptr = NULL, .name = "cudaStreamSynchronize"}
};

int load_cudart_lib() {
    void *table = NULL;
    int i = 0, ret = 0;

    const char* cudartpath = getenv("SWITCHCUDART");
    if (cudartpath)
        table = dlopen(cudartpath, RTLD_NOW | RTLD_NODELETE);
    else
        table = dlopen(PATHTOCUDA_ROOT "/lib64/libcudart.so",
                       RTLD_NOW | RTLD_NODELETE);
    //table = dlopen(".local/lib/python3.6/site-packages/torch/lib/libcudart-1b201d85.so.10.1",
    //               RTLD_NOW | RTLD_NODELETE);
    if (!table) {
        fprintf(stderr, "cant find library libcudart.so\n");
        ret = 1;
        goto error;
    }

    for (i = 0; i < CUDART_ENTRY_END; i++) {
        cudart_lib_entry[i].fn_ptr = dlsym(table, cudart_lib_entry[i].name);
        if (!cudart_lib_entry[i].fn_ptr) {
            fprintf(stderr, "cant find function %s\n",
                    cudart_lib_entry[i].name);
            ret = 1;
            goto clean;
        } else
            fprintf(stderr, "found function %s\n", cudart_lib_entry[i].name);
    }

clean:
    dlclose(table);
error:
    return ret;
}

inline int cudart_lib_inited() {
    return (0 != _cudart_inited_);
}

///////////////////////////////////////////////////////////////////////////////

int _cudnn_inited_ = 0;

entry_t cudnn_lib_entry[] = {
    {.fn_ptr = NULL, .name = "cudnnActivationBackward"},
    {.fn_ptr = NULL, .name = "cudnnActivationForward"},
    {.fn_ptr = NULL, .name = "cudnnAddTensor"},
    {.fn_ptr = NULL, .name = "cudnnBatchNormalizationBackward"},
    {.fn_ptr = NULL, .name = "cudnnBatchNormalizationBackwardEx"},
    {.fn_ptr = NULL, .name = "cudnnBatchNormalizationForwardInference"},
    {.fn_ptr = NULL, .name = "cudnnBatchNormalizationForwardTraining"},
    {.fn_ptr = NULL, .name = "cudnnBatchNormalizationForwardTrainingEx"},
    {.fn_ptr = NULL, .name = "cudnnConvolutionBackwardBias"},
    {.fn_ptr = NULL, .name = "cudnnConvolutionBackwardData"},
    {.fn_ptr = NULL, .name = "cudnnConvolutionBackwardFilter"},
    {.fn_ptr = NULL, .name = "cudnnConvolutionBiasActivationForward"},
    {.fn_ptr = NULL, .name = "cudnnConvolutionForward"},
    {.fn_ptr = NULL, .name = "cudnnCopyAlgorithmDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreate"},
    {.fn_ptr = NULL, .name = "cudnnCreateActivationDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateAlgorithmDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateAlgorithmPerformance"},
    {.fn_ptr = NULL, .name = "cudnnCreateAttnDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateConvolutionDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateCTCLossDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateDropoutDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateFilterDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateFusedOpsConstParamPack"},
    {.fn_ptr = NULL, .name = "cudnnCreateFusedOpsPlan"},
    {.fn_ptr = NULL, .name = "cudnnCreateFusedOpsVariantParamPack"},
    {.fn_ptr = NULL, .name = "cudnnCreateLRNDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateOpTensorDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreatePersistentRNNPlan"},
    {.fn_ptr = NULL, .name = "cudnnCreatePoolingDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateReduceTensorDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateRNNDataDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateRNNDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateSeqDataDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateSpatialTransformerDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateTensorDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCreateTensorTransformDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnCTCLoss"},
    {.fn_ptr = NULL, .name = "cudnnDeriveBNTensorDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroy"},
    {.fn_ptr = NULL, .name = "cudnnDestroyActivationDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroyAlgorithmDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroyAlgorithmPerformance"},
    {.fn_ptr = NULL, .name = "cudnnDestroyAttnDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroyConvolutionDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroyCTCLossDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroyDropoutDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroyFilterDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroyFusedOpsConstParamPack"},
    {.fn_ptr = NULL, .name = "cudnnDestroyFusedOpsPlan"},
    {.fn_ptr = NULL, .name = "cudnnDestroyFusedOpsVariantParamPack"},
    {.fn_ptr = NULL, .name = "cudnnDestroyLRNDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroyOpTensorDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroyPersistentRNNPlan"},
    {.fn_ptr = NULL, .name = "cudnnDestroyPoolingDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroyReduceTensorDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroyRNNDataDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroyRNNDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroySeqDataDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroySpatialTransformerDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroyTensorDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDestroyTensorTransformDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnDivisiveNormalizationBackward"},
    {.fn_ptr = NULL, .name = "cudnnDivisiveNormalizationForward"},
    {.fn_ptr = NULL, .name = "cudnnDropoutBackward"},
    {.fn_ptr = NULL, .name = "cudnnDropoutForward"},
    {.fn_ptr = NULL, .name = "cudnnDropoutGetReserveSpaceSize"},
    {.fn_ptr = NULL, .name = "cudnnDropoutGetStatesSize"},
    {.fn_ptr = NULL, .name = "cudnnFindConvolutionBackwardDataAlgorithm"},
    {.fn_ptr = NULL, .name = "cudnnFindConvolutionBackwardDataAlgorithmEx"},
    {.fn_ptr = NULL, .name = "cudnnFindConvolutionBackwardFilterAlgorithm"},
    {.fn_ptr = NULL, .name = "cudnnFindConvolutionBackwardFilterAlgorithmEx"},
    {.fn_ptr = NULL, .name = "cudnnFindConvolutionForwardAlgorithm"},
    {.fn_ptr = NULL, .name = "cudnnFindConvolutionForwardAlgorithmEx"},
    {.fn_ptr = NULL, .name = "cudnnFindRNNBackwardDataAlgorithmEx"},
    {.fn_ptr = NULL, .name = "cudnnFindRNNBackwardWeightsAlgorithmEx"},
    {.fn_ptr = NULL, .name = "cudnnFindRNNForwardInferenceAlgorithmEx"},
    {.fn_ptr = NULL, .name = "cudnnFindRNNForwardTrainingAlgorithmEx"},
    {.fn_ptr = NULL, .name = "cudnnFusedOpsExecute"},
    {.fn_ptr = NULL, .name = "cudnnGetActivationDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetAlgorithmDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetAlgorithmPerformance"},
    {.fn_ptr = NULL, .name = "cudnnGetAlgorithmSpaceSize"},
    {.fn_ptr = NULL, .name = "cudnnGetAttnDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetBatchNormalizationBackwardExWorkspaceSize"},
    {.fn_ptr = NULL, .name = "cudnnGetBatchNormalizationForwardTrainingExWorkspaceSize"},
    {.fn_ptr = NULL, .name = "cudnnGetBatchNormalizationTrainingExReserveSpaceSize"},
    {.fn_ptr = NULL, .name = "cudnnGetCallback"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolution2dDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolution2dForwardOutputDim"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionBackwardDataAlgorithm"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionBackwardDataAlgorithmMaxCount"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionBackwardDataAlgorithm_v7"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionBackwardDataWorkspaceSize"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionBackwardFilterAlgorithm"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionBackwardFilterAlgorithmMaxCount"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionBackwardFilterAlgorithm_v7"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionBackwardFilterWorkspaceSize"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionForwardAlgorithm"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionForwardAlgorithmMaxCount"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionForwardAlgorithm_v7"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionForwardWorkspaceSize"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionGroupCount"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionMathType"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionNdDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionNdForwardOutputDim"},
    {.fn_ptr = NULL, .name = "cudnnGetConvolutionReorderType"},
    {.fn_ptr = NULL, .name = "cudnnGetCTCLossDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetCTCLossDescriptorEx"},
    {.fn_ptr = NULL, .name = "cudnnGetCTCLossWorkspaceSize"},
    {.fn_ptr = NULL, .name = "cudnnGetCudartVersion"},
    {.fn_ptr = NULL, .name = "cudnnGetDropoutDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetErrorString"},
    {.fn_ptr = NULL, .name = "cudnnGetFilter4dDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetFilterNdDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetFilterSizeInBytes"},
    {.fn_ptr = NULL, .name = "cudnnGetFoldedConvBackwardDataDescriptors"},
    {.fn_ptr = NULL, .name = "cudnnGetFusedOpsConstParamPackAttribute"},
    {.fn_ptr = NULL, .name = "cudnnGetFusedOpsVariantParamPackAttribute"},
    {.fn_ptr = NULL, .name = "cudnnGetLRNDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetMultiHeadAttnBuffers"},
    {.fn_ptr = NULL, .name = "cudnnGetMultiHeadAttnWeights"},
    {.fn_ptr = NULL, .name = "cudnnGetOpTensorDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetPooling2dDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetPooling2dForwardOutputDim"},
    {.fn_ptr = NULL, .name = "cudnnGetPoolingNdDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetPoolingNdForwardOutputDim"},
    {.fn_ptr = NULL, .name = "cudnnGetProperty"},
    {.fn_ptr = NULL, .name = "cudnnGetReduceTensorDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetReductionIndicesSize"},
    {.fn_ptr = NULL, .name = "cudnnGetReductionWorkspaceSize"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNBackwardDataAlgorithmMaxCount"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNBackwardWeightsAlgorithmMaxCount"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNBiasMode"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNDataDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNForwardInferenceAlgorithmMaxCount"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNForwardTrainingAlgorithmMaxCount"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNLinLayerBiasParams"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNLinLayerMatrixParams"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNMatrixMathType"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNPaddingMode"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNParamsSize"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNProjectionLayers"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNTrainingReserveSize"},
    {.fn_ptr = NULL, .name = "cudnnGetRNNWorkspaceSize"},
    {.fn_ptr = NULL, .name = "cudnnGetSeqDataDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetStream"},
    {.fn_ptr = NULL, .name = "cudnnGetTensor4dDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetTensorNdDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetTensorSizeInBytes"},
    {.fn_ptr = NULL, .name = "cudnnGetTensorTransformDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnGetVersion"},
    {.fn_ptr = NULL, .name = "cudnnIm2Col"},
    {.fn_ptr = NULL, .name = "cudnnInitTransformDest"},
    {.fn_ptr = NULL, .name = "cudnnLRNCrossChannelBackward"},
    {.fn_ptr = NULL, .name = "cudnnLRNCrossChannelForward"},
    {.fn_ptr = NULL, .name = "cudnnMakeFusedOpsPlan"},
    {.fn_ptr = NULL, .name = "cudnnMultiHeadAttnBackwardData"},
    {.fn_ptr = NULL, .name = "cudnnMultiHeadAttnBackwardWeights"},
    {.fn_ptr = NULL, .name = "cudnnMultiHeadAttnForward"},
    {.fn_ptr = NULL, .name = "cudnnOpTensor"},
    {.fn_ptr = NULL, .name = "cudnnPoolingBackward"},
    {.fn_ptr = NULL, .name = "cudnnPoolingForward"},
    {.fn_ptr = NULL, .name = "cudnnQueryRuntimeError"},
    {.fn_ptr = NULL, .name = "cudnnReduceTensor"},
    {.fn_ptr = NULL, .name = "cudnnReorderFilterAndBias"},
    {.fn_ptr = NULL, .name = "cudnnRestoreAlgorithm"},
    {.fn_ptr = NULL, .name = "cudnnRestoreDropoutDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnRNNBackwardData"},
    {.fn_ptr = NULL, .name = "cudnnRNNBackwardDataEx"},
    {.fn_ptr = NULL, .name = "cudnnRNNBackwardWeights"},
    {.fn_ptr = NULL, .name = "cudnnRNNBackwardWeightsEx"},
    {.fn_ptr = NULL, .name = "cudnnRNNForwardInference"},
    {.fn_ptr = NULL, .name = "cudnnRNNForwardInferenceEx"},
    {.fn_ptr = NULL, .name = "cudnnRNNForwardTraining"},
    {.fn_ptr = NULL, .name = "cudnnRNNForwardTrainingEx"},
    {.fn_ptr = NULL, .name = "cudnnRNNGetClip"},
    {.fn_ptr = NULL, .name = "cudnnRNNSetClip"},
    {.fn_ptr = NULL, .name = "cudnnSaveAlgorithm"},
    {.fn_ptr = NULL, .name = "cudnnScaleTensor"},
    {.fn_ptr = NULL, .name = "cudnnSetActivationDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetAlgorithmDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetAlgorithmPerformance"},
    {.fn_ptr = NULL, .name = "cudnnSetAttnDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetCallback"},
    {.fn_ptr = NULL, .name = "cudnnSetConvolution2dDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetConvolutionGroupCount"},
    {.fn_ptr = NULL, .name = "cudnnSetConvolutionMathType"},
    {.fn_ptr = NULL, .name = "cudnnSetConvolutionNdDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetConvolutionReorderType"},
    {.fn_ptr = NULL, .name = "cudnnSetCTCLossDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetCTCLossDescriptorEx"},
    {.fn_ptr = NULL, .name = "cudnnSetDropoutDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetFilter4dDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetFilterNdDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetFusedOpsConstParamPackAttribute"},
    {.fn_ptr = NULL, .name = "cudnnSetFusedOpsVariantParamPackAttribute"},
    {.fn_ptr = NULL, .name = "cudnnSetLRNDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetOpTensorDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetPersistentRNNPlan"},
    {.fn_ptr = NULL, .name = "cudnnSetPooling2dDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetPoolingNdDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetReduceTensorDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetRNNAlgorithmDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetRNNBiasMode"},
    {.fn_ptr = NULL, .name = "cudnnSetRNNDataDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetRNNDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetRNNDescriptor_v5"},
    {.fn_ptr = NULL, .name = "cudnnSetRNNDescriptor_v6"},
    {.fn_ptr = NULL, .name = "cudnnSetRNNMatrixMathType"},
    {.fn_ptr = NULL, .name = "cudnnSetRNNPaddingMode"},
    {.fn_ptr = NULL, .name = "cudnnSetRNNProjectionLayers"},
    {.fn_ptr = NULL, .name = "cudnnSetSeqDataDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetSpatialTransformerNdDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetStream"},
    {.fn_ptr = NULL, .name = "cudnnSetTensor"},
    {.fn_ptr = NULL, .name = "cudnnSetTensor4dDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetTensor4dDescriptorEx"},
    {.fn_ptr = NULL, .name = "cudnnSetTensorNdDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSetTensorNdDescriptorEx"},
    {.fn_ptr = NULL, .name = "cudnnSetTensorTransformDescriptor"},
    {.fn_ptr = NULL, .name = "cudnnSoftmaxBackward"},
    {.fn_ptr = NULL, .name = "cudnnSoftmaxForward"},
    {.fn_ptr = NULL, .name = "cudnnSpatialTfGridGeneratorBackward"},
    {.fn_ptr = NULL, .name = "cudnnSpatialTfGridGeneratorForward"},
    {.fn_ptr = NULL, .name = "cudnnSpatialTfSamplerBackward"},
    {.fn_ptr = NULL, .name = "cudnnSpatialTfSamplerForward"},
    {.fn_ptr = NULL, .name = "cudnnTransformFilter"},
    {.fn_ptr = NULL, .name = "cudnnTransformTensor"},
    {.fn_ptr = NULL, .name = "cudnnTransformTensorEx"}
};

int load_cudnn_lib() {
    void *table = NULL;
    int i = 0, ret = 0;

    const char* cudnnpath = getenv("SWITCHCUDNN");
    if (cudnnpath)
        table = dlopen(cudnnpath, RTLD_NOW | RTLD_NODELETE);
    else
        table = dlopen(PATHTOCUDNN_ROOT "/lib64/libcudnn.so",
                       RTLD_NOW | RTLD_NODELETE);

    if (!table) {
        fprintf(stderr, "cant find library libcudnn.so\n");
        ret = 1;
        goto error;
    }

    for (i = 0; i < CUDNN_ENTRY_END; i++) {
        cudnn_lib_entry[i].fn_ptr = dlsym(table, cudnn_lib_entry[i].name);
        if (!cudnn_lib_entry[i].fn_ptr) {
            fprintf(stderr, "cant find function %s\n",
                    cudnn_lib_entry[i].name);
            ret = 1;
            goto clean;
        } else
            fprintf(stderr, "found function %s\n", cudnn_lib_entry[i].name);
    }

clean:
    dlclose(table);
error:
    return ret;
}


inline int cudnn_lib_inited() {
    return (0 != _cudnn_inited_);
}

///////////////////////////////////////////////////////////////////////////////

int _cublas_inited_ = 0;

entry_t cublas_lib_entry[] = {
    {.fn_ptr = NULL, .name = "cublasCreate_v2"},
    {.fn_ptr = NULL, .name = "cublasDestroy_v2"},
    {.fn_ptr = NULL, .name = "cublasSetStream_v2"},
    {.fn_ptr = NULL, .name = "cublasSgemm_v2"},
};

int load_cublas_lib() {
    void *table = NULL;
    int i = 0, ret = 0;

    const char* cublaspath = getenv("SWITCHCUBLAS");
    if (cublaspath)
        table = dlopen(cublaspath, RTLD_NOW | RTLD_NODELETE);
    else
        table = dlopen(PATHTOCUDA_ROOT "/lib64/libcublas.so",
                       RTLD_NOW | RTLD_NODELETE);
    //table = dlopen(".local/lib/python3.6/site-packages/torch/lib/libcublas-1b201d85.so.10.1",
    //               RTLD_NOW | RTLD_NODELETE);
    if (!table) {
        fprintf(stderr, "cant find library libcublas.so\n");
        ret = 1;
        goto error;
    }

    for (i = 0; i < CUBLAS_ENTRY_END; i++) {
        cublas_lib_entry[i].fn_ptr = dlsym(table, cublas_lib_entry[i].name);
        if (!cublas_lib_entry[i].fn_ptr) {
            fprintf(stderr, "cant find function %s\n",
                    cublas_lib_entry[i].name);
            ret = 1;
            goto clean;
        } else
            fprintf(stderr, "found function %s\n", cublas_lib_entry[i].name);
    }

clean:
    dlclose(table);
error:
    return ret;
}

inline int cublas_lib_inited() {
    return (0 != _cublas_inited_);
}

///////////////////////////////////////////////////////////////////////////////
