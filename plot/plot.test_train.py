import numpy as np
import re

cmg = [1, 2, 3, 4]
omp = [2**x for x in range(0,6+1)] + [x for x in range(6, 48+1, 6)]
bs  = [2**x for x in range(0,8+1)] + [x for x in range(12, 288+1, 12)] + [6]

omp.sort()
bs.sort()

l_omp, l_bs = len(omp), len(bs)

omp_map = np.reshape(l_bs * omp, (l_bs, l_omp)).T
bs_map  = np.reshape(l_omp * bs, (l_omp, l_bs))

native   = np.zeros((l_omp, l_bs)) + np.nan
fakecuda = np.zeros((l_omp, l_bs)) + np.nan
dnnl     = np.zeros((l_omp, l_bs)) + np.nan

header_reg = re.compile(r'^batch size:\s+(\d+)\s+numa:\s+[-\w]+\s+omp:\s+(\d+)')
mean_t_red = re.compile(r'^mean\s+([-+]?\d*\.\d+|\d+)')

for c in cmg:
    for r in ['native', 'fakecuda', 'dnnl']:
        for o in omp:
            try:
                with open('res/%s_cmg%s_omp%s.log' % (r, c, o), 'r') as f:
                    r_b, r_o = None, None
                    for line in f:
                        if header_reg.match(line):
                            r_h = header_reg.match(line)
                            r_b, r_o = int(r_h.group(1)), int(r_h.group(2))
                            assert(o == r_o and r_b in bs)
                        if mean_t_red.match(line):
                            r_m = float(mean_t_red.match(line).group(1))
                            if r is 'native':
                                native[omp.index(r_o), bs.index(r_b)] = r_m
                            elif r is 'fakecuda':
                                fakecuda[omp.index(r_o), bs.index(r_b)] = r_m
                            elif r is 'dnnl':
                                dnnl[omp.index(r_o), bs.index(r_b)] = r_m
                            r_b, r_o = None, None
            except FileNotFoundError:
                print('WRN: missing file res/%s_cmg%s_omp%s.log' % (r, c, o))
                pass

    with open('plot_cmg%s.m' % c, 'w') as f:
        f.write('graphics_toolkit ("qt");\n')
        f.write('d = [\n')
        for o in omp:
            for b in bs:
                io, ib = omp.index(o), bs.index(b)
                f.write('%s %s %s %s %s\n'
                        % (o, b,
                           native[io, ib], fakecuda[io, ib], dnnl[io, ib]))
        f.write('];\n')
        f.write('f1 = surf(d(1:%s:%s,1), d(1:%s,2), reshape(d(:,3),%s,%s), \
                "FaceColor","r");\n' % (l_bs, l_omp*l_bs, l_bs, l_bs, l_omp))
        f.write('hold on\n')
        f.write('f2 = surf(d(1:%s:%s,1), d(1:%s,2), reshape(d(:,4),%s,%s), \
                "FaceColor","g");\n' % (l_bs, l_omp*l_bs, l_bs, l_bs, l_omp))
        f.write('hold on\n')
        f.write('f3 = surf(d(1:%s:%s,1), d(1:%s,2), reshape(d(:,5),%s,%s), \
                "FaceColor","b");\n' % (l_bs, l_omp*l_bs, l_bs, l_bs, l_omp))
        f.write('title("A64FX with %s CMGs");\n' % c)
        f.write('xlabel("#OMP", "FontSize", 20);\n')
        f.write('ylabel("batch size", "FontSize", 20);\n')
        f.write('zlabel("t/epoch", "FontSize", 20);\n')
        f.write('h = legend([f1, f2, f3], {"native", "FakeCUDA", "DNNL"});\n')
        f.write('set (h, "fontsize", 16);\n')
        f.write('pause\n')
    with open('plot_cmg%s_v2.m' % c, 'w') as f:
        f.write('graphics_toolkit ("qt");\n')
        f.write('d = [\n')
        for o in omp:
            for b in bs:
                io, ib = omp.index(o), bs.index(b)
                f.write('%s %s %s\n' % (o, b, dnnl[io, ib] / fakecuda[io, ib]))
        f.write('];\n')
        f.write('f1 = surf(d(1:%s:%s,1), d(1:%s,2), reshape(d(:,3),%s,%s));\n'
                % (l_bs, l_omp*l_bs, l_bs, l_bs, l_omp))
        f.write('zlim([0 2]);\n')
        f.write('caxis([0 2]);\n')
        f.write('colormap(winter);\n')
        f.write('colorbar;\n')
        f.write('title("A64FX with %s CMGs");\n' % c)
        f.write('xlabel("#OMP", "FontSize", 20);\n')
        f.write('ylabel("batch size", "FontSize", 20);\n')
        f.write('zlabel("Speedup", "FontSize", 20);\n')
        f.write('h = legend(f1, "Speedup(/down) of FakeCUDA");\n')
        f.write('set (h, "fontsize", 16);\n')
        f.write('pause\n')
