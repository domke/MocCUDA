#include <cblas.h>
#include <omp.h>

#ifdef REFLAPACK
    #define blasint int
    #define BLASFN(FUNC) cblas_##FUNC
#elif defined FUJITSU
    #define blasint FJ_MATHLIB_TYPE_INT
    #define BLASFN(FUNC) sl_cblas_##FUNC
#elif defined ARMPL
    #define blasint armpl_int_t
    #define BLASFN(FUNC) cblas_##FUNC
#else
    #define BLASFN(FUNC) cblas_##FUNC
#endif

// just fake 2 functions for now to fool pytorch's cmake checks for blas/lapack
int sgemm_ (void) __attribute__((noinline));
int sgemm_ (void) {
    #pragma omp parallel
    {}
    const int m=1, k=1, n=1;
    float a=.0, b=.0, *A=NULL, *B=NULL, *C=NULL;
    BLASFN(sgemm)(CblasRowMajor, CblasTrans, CblasNoTrans, m, n, k,
                  a, A, m, B, n, b, C, n);
    return 0;
}

int cheev_ (void) __attribute__((noinline));
int cheev_ (void) {
    return sgemm_();
}
