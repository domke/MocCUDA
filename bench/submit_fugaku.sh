#!/bin/bash
SDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )" && pwd )"
cd ${SDIR}

###  SINGLE NODE  ###

NODES=1
PPN=1
SUBMITCMD="pjsub -L node=${NODES} -L rscunit=rscunit_ft01 -L rscgrp=small -L elapse=23:59:59 -L freq=2200 -L throttling_state=0 -L issue_state=0 -L ex_pipe_state=0 -L eco_state=0 -L retention_state=0 --llio localtmp-size=80Gi --mpi max-proc-per-node=${PPN}"

BACKEND=native
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}150" ./01_benchmarker_infer.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}150" ./02_benchmarker_train.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}150" ./03_test_train.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}170" ./01_benchmarker_infer.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}170" ./02_benchmarker_train.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}170" ./03_test_train.sh

BACKEND=dnnl
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}150" ./01_benchmarker_infer.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}150" ./02_benchmarker_train.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}150" ./03_test_train.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}170" ./01_benchmarker_infer.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}170" ./02_benchmarker_train.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}170" ./03_test_train.sh

BACKEND=moccuda
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}" ./01_benchmarker_infer.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}" ./02_benchmarker_train.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}" ./03_test_train.sh

NODES=1
PPN=4
SUBMITCMD="pjsub -L node=${NODES} -L rscunit=rscunit_ft01 -L rscgrp=small -L elapse=23:59:59 -L freq=2200 -L throttling_state=0 -L issue_state=0 -L ex_pipe_state=0 -L eco_state=0 -L retention_state=0 --llio localtmp-size=80Gi --mpi max-proc-per-node=${PPN}"

BACKEND=native
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}150" ./04_pytorch_synthetic_benchmark.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}170" ./04_pytorch_synthetic_benchmark.sh
BACKEND=dnnl
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}150" ./04_pytorch_synthetic_benchmark.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}170" ./04_pytorch_synthetic_benchmark.sh
BACKEND=moccuda
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}" ./04_pytorch_synthetic_benchmark.sh

###  MULTI NODE  ###

NODES=12
PPN=4
SUBMITCMD="pjsub -L node=${NODES} -L rscunit=rscunit_ft01 -L rscgrp=small-torus -L elapse=23:59:59 -L freq=2200 -L throttling_state=0 -L issue_state=0 -L ex_pipe_state=0 -L eco_state=0 -L retention_state=0 --llio localtmp-size=80Gi --mpi max-proc-per-node=${PPN}"

BACKEND=native
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}150,BENCHMINRANK=${PPN},BENCHSTEPRANK=${PPN},BENCHMAXRANK=$((${PPN}*${NODES})),BENCHPPN=${PPN}" ./04_pytorch_synthetic_benchmark.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}170,BENCHMINRANK=${PPN},BENCHSTEPRANK=${PPN},BENCHMAXRANK=$((${PPN}*${NODES})),BENCHPPN=${PPN}" ./04_pytorch_synthetic_benchmark.sh
BACKEND=dnnl
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}150,BENCHMINRANK=${PPN},BENCHSTEPRANK=${PPN},BENCHMAXRANK=$((${PPN}*${NODES})),BENCHPPN=${PPN}" ./04_pytorch_synthetic_benchmark.sh
eval ${SUBMITCMD} -x "BACKEND=${BACKEND}170,BENCHMINRANK=${PPN},BENCHSTEPRANK=${PPN},BENCHMAXRANK=$((${PPN}*${NODES})),BENCHPPN=${PPN}" ./04_pytorch_synthetic_benchmark.sh
BACKEND=moccuda
eval ${SUBMITCMD} -x "BACKEND=${BACKEND},BENCHMINRANK=${PPN},BENCHSTEPRANK=${PPN},BENCHMAXRANK=$((${PPN}*${NODES})),BENCHPPN=${PPN}" ./04_pytorch_synthetic_benchmark.sh
