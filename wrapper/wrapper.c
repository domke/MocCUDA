#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <dlfcn.h>
#include <pthread.h>

#define UNW_LOCAL_ONLY
#include <libunwind.h>
#include <demangle.h>
#ifdef TIMING
#include <time.h>
struct timespec __start__, __finish__;
#endif


#define min(a,b)                \
    ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
       _a < _b ? _a : _b;       \
     })

static void write_backtrace(FILE *file)
{
    unw_cursor_t cursor;
    unw_context_t context;

    // Initialize cursor to current frame for local unwinding.
    unw_getcontext(&context);
    unw_init_local(&cursor, &context);

    // Unwind frames one by one, going up the frame stack.
    while (unw_step(&cursor) > 0) {
        unw_word_t offset, pc;
        unw_get_reg(&cursor, UNW_REG_IP, &pc);
        if (pc == 0)
            break;
        fprintf(file, "    0x%lx:", pc);

        char sym[256];

        if (0 == unw_get_proc_name(&cursor, sym, sizeof(sym), &offset)) {
            char* nameptr = sym;
            char* demangled = cplus_demangle(sym, DMGL_PARAMS);
            if (demangled)
                nameptr = demangled;
            fprintf(file, " (%s+0x%lx)\n", nameptr, offset);
        } else {
            fprintf(file,
                    " -- error: unable to obtain symbol name for this frame\n");
        }
    }
}

static void* known_func_array[512];
static int known_func = 0;

static int func_is_known(const void *func)
{
    int i;
    void *tFunc = (void*)func;
    for (i = 0; i < known_func; i++)
        if (tFunc == known_func_array[i])
            return 1;
    known_func_array[known_func++] = tFunc;
    return 0;
}

#define SCOREP_MOVABLE_NULL 0
#define SCOREP_INVALID_REGION SCOREP_MOVABLE_NULL

typedef enum SCOREP_LibwrapMode {
    SCOREP_LIBWRAP_MODE_SHARED,
    SCOREP_LIBWRAP_MODE_STATIC,
    SCOREP_LIBWRAP_MODE_WEAK,
} SCOREP_LibwrapMode;

typedef struct SCOREP_LibwrapAttributes SCOREP_LibwrapAttributes;
typedef struct SCOREP_LibwrapHandle SCOREP_LibwrapHandle;

struct SCOREP_LibwrapAttributes {
    int                version;
    const char*        name;
    const char*        display_name;
    SCOREP_LibwrapMode mode;
    void               ( * init )( SCOREP_LibwrapHandle* libwrapHandle );
    int                number_of_shared_libs;
    const char**       shared_libs;
};

struct SCOREP_LibwrapHandle {
    const SCOREP_LibwrapAttributes* attributes;
    SCOREP_LibwrapHandle*    next;
    pthread_mutex_t                 region_definition_lock;
    uint32_t                        number_of_shared_lib_handles;
    void*                           shared_lib_handles[];
};

typedef uint32_t SCOREP_Allocator_MovableMemory;
typedef SCOREP_Allocator_MovableMemory SCOREP_AnyHandle;
typedef SCOREP_AnyHandle SCOREP_RegionHandle;

uint32_t _lock_inited_ = 0;
pthread_mutex_t lock;

FILE *file = NULL;

#define FN_NAME_MAXLEN 128

struct fn {
    char func[FN_NAME_MAXLEN];
    char lib[FN_NAME_MAXLEN];
    int printed;
    struct fn *next;
};
struct fn *fn_list = NULL, *fn_list_last = NULL;
uint32_t fn_list_size = 0;

void
SCOREP_Libwrap_EarlySharedPtrInit( const char* func,
                                   void**      funcPtr )
{
    if (!_lock_inited_)
        assert( (_lock_inited_ = (0 == pthread_mutex_init(&lock, NULL))) );

    pthread_mutex_lock(&lock);

    *funcPtr = dlsym( RTLD_NEXT, func );

    pthread_mutex_unlock(&lock);
}

void
SCOREP_Libwrap_Create( SCOREP_LibwrapHandle**          handle,
                       const SCOREP_LibwrapAttributes* attributes )
{
    pthread_mutex_lock(&lock);

    assert( (*handle = malloc(sizeof(SCOREP_LibwrapHandle )
                              + attributes->number_of_shared_libs
                              * sizeof( void* ))) );

    assert( (0 ==
             pthread_mutex_init(&(*handle )->region_definition_lock, NULL)) );

    ( *handle )->attributes = attributes;

/*    ( *handle )->number_of_shared_lib_handles = 0;

    assert( ( *handle )->attributes->mode == SCOREP_LIBWRAP_MODE_SHARED );

    int i = 0;
    for (; i < ( *handle )->attributes->number_of_shared_libs; i++) {
        const char* lib_name = attributes->shared_libs[ i ];
        assert( (( *handle )->shared_lib_handles[
                    ( *handle )->number_of_shared_lib_handles ] =
                 dlopen( lib_name, RTLD_LAZY | RTLD_LOCAL )) );
        (*handle)->number_of_shared_lib_handles++;
    }
*/
    if ( attributes->init )
        attributes->init( *handle );

    pthread_mutex_unlock(&lock);
}

void
SCOREP_Libwrap_DefineRegion( SCOREP_LibwrapHandle* handle,
                             SCOREP_RegionHandle*  region,
                             int*                  regionFiltered,
                             const char*           func,
                             const char*           symbol,
                             const char*           file,
                             int                   line )
{
    pthread_mutex_lock( &(handle->region_definition_lock) );

    if ( *region != SCOREP_INVALID_REGION ) {
        pthread_mutex_unlock(&(handle->region_definition_lock));
        return;
    }

    struct fn *new_fn;
    assert( (new_fn = (struct fn*)malloc(sizeof(*new_fn))) );

    int len = min(strchrnul(func, '(') - func, FN_NAME_MAXLEN);
    strncpy(new_fn->func, func, len);
    if (len < FN_NAME_MAXLEN)
        new_fn->func[len] = '\0';

    const char* lib_name = handle->attributes->shared_libs[ 0 ];
    strncpy(new_fn->lib, lib_name, FN_NAME_MAXLEN);

    new_fn->printed = 0;

    if (!fn_list)
        fn_list = new_fn;
    else
        fn_list_last->next = new_fn;

    fn_list_last = new_fn;
    *region = ++fn_list_size;

    pthread_mutex_unlock( &(handle->region_definition_lock) );
}

int
SCOREP_Libwrap_EnterMeasurement( void ) { return 1; }

void *launchFunc = NULL;
void set_launch_func( const void *func )
{
    launchFunc = (void *)func;
}

void
SCOREP_Libwrap_EnterRegion( SCOREP_RegionHandle region )
{
    assert(region <= fn_list_size);
    int i = 0;
    struct fn *curr = fn_list;
    for (; i < region - 1; i++)
        curr = curr->next;
#ifndef TIMING
    if (curr->printed)
        return;
    else
        curr->printed = 1;
#endif
    if (!file) {
        const char* file_name = getenv("WRAPPER_OUTPUT");
        if (file_name)
            assert( (file = fopen(file_name, "a")) );
        else
            assert( (file = fopen("cuda.lib.call.log", "a")) );
    }
#ifndef TIMING
    //if (0 == strncmp(curr->func, "cudaLaunchKernel", 16)) {
    //check [cuda|cu]LaunchXYZ
    if (NULL != strcasestr(curr->func, "LaunchKernel") ||
        NULL != strcasestr(curr->func, "LaunchCooperativeKernel") ||
        NULL != strcasestr(curr->func, "LaunchCooperativeKernelMultiDevice")) {
        if (!func_is_known(launchFunc)) {
            fprintf(file, "Calling %s:%s\n", curr->lib, curr->func);
            fprintf(file, "  backtrace:\n");
            write_backtrace(file);
        }
        curr->printed = 0; // reset to print again
        return;
    }
    fprintf(file, "Calling %s:%s\n", curr->lib, curr->func);
#else
    fprintf(file, "Calling %s:%s (rt:", curr->lib, curr->func);
    clock_gettime(CLOCK_REALTIME, &__start__);
#endif
}

int
SCOREP_Libwrap_EnterWrappedRegion( void ) { return 0; }

void
SCOREP_Libwrap_EnterWrapper( SCOREP_RegionHandle region ) {}

void
SCOREP_Libwrap_ExitMeasurement( void ) {}

void
SCOREP_Libwrap_ExitRegion( SCOREP_RegionHandle region )
{
/*
    assert(region <= fn_list_size);
    int i = 0;
    struct fn *curr = fn_list;
    for (; i < region - 1; i++)
        curr = curr->next;
    if (curr->printed)
        return;
    else
        curr->printed = 1;
    if (!file)
        assert( (file = fopen("cuda.lib.call.log", "a")) );
    fprintf(file, "LEAVE %s:%s\n", curr->lib, curr->func);
*/
#ifdef TIMING
    clock_gettime(CLOCK_REALTIME, &__finish__);
    long __seconds__ = __finish__.tv_sec - __start__.tv_sec;
    long __ns__ = __finish__.tv_nsec - __start__.tv_nsec;
    if (__start__.tv_nsec > __finish__.tv_nsec) {
        --__seconds__;
        __ns__ += 1e9;
    }
    fprintf(file, "%e)\n", (double)__seconds__ + (double)__ns__/(double)1e9);
#endif
}

void
SCOREP_Libwrap_ExitWrappedRegion( int previous ) {}

void
SCOREP_Libwrap_ExitWrapper( SCOREP_RegionHandle region ) {}

void
SCOREP_Libwrap_SharedPtrInit( SCOREP_LibwrapHandle* handle,
                              const char*           func,
                              void**                funcPtr ) {}
