. $HOME/spack/share/spack/setup-env.sh
spack load libjpeg arch=linux-rhel8-a64fx
spack load cmake arch=linux-rhel8-a64fx
ulimit -s 8192
ulimit -i 32467
ulimit -l 6384
ulimit -q 819200

export TCSDS_PATH=/opt/FJSVxtclanga/tcsds-1.2.26
export PREFIX=$HOME/pytorch.cuda
export VENV_NAME=fccbuild
export PATH=${TCSDS_PATH}/bin:$PATH
export LD_LIBRARY_PATH=${TCSDS_PATH}/lib64:$LD_LIBRARY_PATH
export CC="fcc -Nclang -Kfast -Knolargepage"
export CXX="FCC -Nclang -Kfast -Knolargepage"
export OPT=-O3
export ac_cv_opt_olimit_ok=no
export ac_cv_olimit_ok=no
export ac_cv_cflags_warn_all=''

#Download Host Installer for Linux RHEL 8 x86_64
wget http://developer.download.nvidia.com/compute/cuda/11.0.1/local_installers/cuda-repo-rhel8-11-0-local-11.0.1_450.36.06-1.x86_64.rpm
mkdir ~/cuda; cd ~/cuda
rpm2cpio ~/cuda-repo-rhel8-11-0-local-11.0.1_450.36.06-1.aarch64.rpm | cpio -idmv
for x in `ls ./var/cuda-repo-rhel8-11-0-local/*rpm`; do rpm2cpio $x | cpio -idmv; done
export CUDA_TOOLKIT_ROOT_DIR=$HOME/cuda/usr/local/cuda-11.0
export PATH=$CUDA_TOOLKIT_ROOT_DIR/bin:$PATH
export LD_LIBRARY_PATH=$CUDA_TOOLKIT_ROOT_DIR/lib64:$LD_LIBRARY_PATH

#get cudnn.h from user/pw secured server and place under ~/cuda/usr/local/cuda-11.0/include/
https://developer.nvidia.com/compute/machine-learning/cudnn/secure/8.0.1.13/11.0_20200626/cudnn-11.0-linux-x64-v8.0.1.13.tgz
tar -xf ~/cudnn-11.0-linux-x64-v8.0.1.13.tgz cuda/include/cudnn.h -O > $HOME/cuda/usr/local/cuda-11.0/include/cudnn.h
export CUDNN_INCLUDE_DIR=$CUDA_TOOLKIT_ROOT_DIR/include
export CUDNN_LIB_DIR=$CUDA_TOOLKIT_ROOT_DIR/lib64
export LD_LIBRARY_PATH=$CUDNN_LIB_DIR:$LD_LIBRARY_PATH

#get nccl from user/pw secured server
https://developer.nvidia.com/compute/machine-learning/nccl/secure/v2.7/prod/nccl-repo-rhel8-2.7.6-ga-cuda11.0-1-1.aarch64.rpm
cd ~/cuda
rpm2cpio ~/nccl-repo-rhel8-2.7.6-ga-cuda11.0-1-1.aarch64.rpm | cpio -idmv
for x in `ls ./var/nccl-repo-2.7.6-ga-cuda11.0/*rpm`; do rpm2cpio $x | cpio -idmv; done
export NCCL_ROOT_DIR=$HOME/cuda/usr
export NCCL_INCLUDE_DIR=$NCCL_ROOT_DIR/include
export NCCL_LIB_DIR=$NCCL_ROOT_DIR/lib64
export LD_LIBRARY_PATH=$NCCL_LIB_DIR:$LD_LIBRARY_PATH

#Python 3.8.2
cd ~
curl -O https://www.python.org/ftp/python/3.8.2/Python-3.8.2.tgz
tar zxf Python-3.8.2.tgz
cd Python-3.8.2/
export CC="fcc -Nclang -Kfast"
export CXX="FCC -Nclang -Kfast"
./configure --enable-shared --disable-ipv6 --target=aarch64 --build=aarch64 --prefix=$PREFIX/python-3.8.2.largepage
make clean; make -j16
mv python python_org
FCC -Nclang -Kfast --linkfortran -SSL2 -Kopenmp -Nlibomp -o python Programs/python.o -L. -lpython3.8 -ldl -lutil -lm
make install
export CC="fcc -Nclang -Kfast -Knolargepage"
export CXX="FCC -Nclang -Kfast -Knolargepage"
./configure --enable-shared --disable-ipv6 --target=aarch64 --build=aarch64 --prefix=$PREFIX/python-3.8.2
make clean; make -j16
mv python python_org
FCC -Nclang -Kfast -Knolargepage --linkfortran -SSL2 -Kopenmp -Nlibomp -o python Programs/python.o -L. -lpython3.8 -ldl -lutil -lm
make install

# torch 1.5
cd ~
git clone https://github.com/pytorch/pytorch.git ~/pytorch.cuda
cd ${PREFIX}/
git checkout -b v1.5.0 refs/tags/v1.5.0
git submodule sync
git submodule update --init --recursive
patch -p1 < ~/fake_a64fx_cuda/torch.v1.4.0.patch
cp ~/fake_a64fx_cuda/libcudnn.so $CUDA_TOOLKIT_ROOT_DIR/lib64

export PATH=${PREFIX}/python-3.8.2/bin:$PATH
export LD_LIBRARY_PATH=${TCSDS_PATH}/lib64:${PREFIX}/python-3.8.2/lib:$LD_LIBRARY_PATH
unset CMAKE_LIBRARY_PATH
export CC="fcc -Nclang -Kfast"
export CXX="FCC -Nclang -Kfast"
patch -p1 < ~/20200702_fj/up/fj_pytorch.patch

cd ${PREFIX}/
${PREFIX}/python-3.8.2/bin/python3.8 -m venv ${VENV_NAME}
source ${PREFIX}/${VENV_NAME}/bin/activate

python3 -m pip install --upgrade ~/pip.cache/PyYAML-5.2.tar.gz
python3 -m pip install --upgrade ~/pip.cache/wheel-0.33.6.tar.gz
python3 -m pip install --upgrade ~/pip.cache/Cython-0.29.20.tar.gz
##python3 -m pip install --upgrade ~/pip.cache/setuptools-45.1.0-py3-none-any.whl
python3 -m pip install --upgrade --no-build-isolation ~/pip.cache/numpy-1.18.5.zip
python3 -m pip install --upgrade ~/pip.cache/six-1.5.0-py2.py3-none-any.whl
python3 -m pip install --upgrade ~/pip.cache/python_dateutil-2.6.1-py2.py3-none-any.whl
python3 -m pip install --upgrade ~/pip.cache/pytz-2017.2-py2.py3-none-any.whl
python3 -m pip install --upgrade ~/pip.cache/pandas-0.25.3.tar.gz
python3 -m pip install --upgrade ~/pip.cache/pyparsing-2.4.7-py2.py3-none-any.whl
python3 -m pip install --upgrade ~/pip.cache/packaging-20.1-py2.py3-none-any.whl
python3 -m pip install --upgrade ~/pip.cache/smmap2-2.0.5-py2.py3-none-any.whl
python3 -m pip install --upgrade ~/pip.cache/gitdb2-2.0.6-py2.py3-none-any.whl
python3 -m pip install --upgrade ~/pip.cache/GitPython-3.0.5-py3-none-any.whl
python3 -m pip install --upgrade ~/pip.cache/semver-2.9.0-py2.py3-none-any.whl
python3 -m pip install --upgrade ~/pip.cache/version_query-1.1.0-py3-none-any.whl
python3 -m pip install --upgrade ~/pip.cache/psutil-5.7.0.tar.gz
python3 -m pip install --upgrade ~/pip.cache/Pint-0.9-py2.py3-none-any.whl
python3 -m pip install --upgrade ~/pip.cache/py-cpuinfo-5.0.0.tar.gz
python3 -m pip install --upgrade ~/pip.cache/pyudev-0.21.0.tar.gz
python3 -m pip install --upgrade ~/pip.cache/system_query-0.2.7-py3-none-any.whl'[cpu,hdd,ram,swap]'
python3 -m pip install --upgrade --no-build-isolation ~/pip.cache/Pillow-6.1.0.tar.gz
python3 -m pip install --upgrade ~/pip.cache/pybind11-2.5.0.tar.gz
python3 -m pip install --upgrade ~/pip.cache/pycparser-2.20-py2.py3-none-any.whl
python3 -m pip install --upgrade ~/pip.cache/cffi-1.14.0.tar.gz
python3 -m pip install --upgrade ~/pip.cache/cloudpickle-1.3.0-py2.py3-none-any.whl
python3 -m pip install --upgrade ~/pip.cache/tqdm-4.46.0-py2.py3-none-any.whl
python3 -m pip install --upgrade ~/pip.cache/future-0.18.2.tar.gz

cd third_party/ideep/
rm -rf mkl-dnn/
git clone https://github.com/fujitsu/dnnl_aarch64.git mkl-dnn
cd mkl-dnn/
git checkout f06144363d791c696a36ef42984cdcc5de5f093b
git submodule sync
git submodule update --init --recursive
mkdir third_party/build_xed_aarch64
cd third_party/build_xed_aarch64/
../xbyak_translator_aarch64/translator/third_party/xed/mfile.py --shared examples install --cc="${TCSDS_PATH}/bin/fcc -Nclang -Kfast" --cxx="${TCSDS_PATH}/bin/FCC -Nclang -Kfast"
cd kits/
ln -sf xed-install-base-* xed
cd ../../../
mkdir build; cd build
cmake ..
make -j48
cd ../../../../
export LD_LIBRARY_PATH=${PREFIX}/third_party/ideep/mkl-dnn/third_party/build_xed_aarch64/kits/xed/lib:$LD_LIBRARY_PATH

cat <<EOF > /tmp/g++
#!/bin/bash
#nvcc has odd way of checking g++
if [ "x\`echo \$@ | awk '\$1 ~ /^\/tmp/tmpxft'\`" != "x" ]; then
	/usr/bin/g++ -E \$@
	exit
fi
/usr/bin/g++ \`echo \$@ | sed -e 's/-Kfast/-O3/g' -e 's/-Kopenmp/-fopenmp/g' -e 's/-Nlibomp//g' -e 's/-Qunused-arguments/-Wunused-parameter/g' -e 's/-fcolor-diagnostics//g' -e 's/-Wno-constexpr-not-const//g' -e 's/-Wno-c++14-extensions//g' -e 's/-Wno-aligned-allocation-unavailable//g' -e 's/-Wno-inconsistent-missing-override//g' -e 's/-Wno-unused-private-field//g' -e 's/-Wno-unknown-warning-option//g' -e 's/-Wno-typedef-redefinition//g' -e 's/-Wno-invalid-partial-specialization//g'\`
EOF
chmod +x /tmp/g++

######BLAS=SSL2 BUILD_NAMEDTENSOR=OFF BUILD_TYPE=Release DISABLE_NUMA=1 USE_CUDA=ON USE_EXCEPTION_PTR=1 USE_GFLAGS=OFF USE_GLOG=OFF USE_MKLDNN=ON USE_MPI=OFF USE_NCCL=OFF USE_NNPACK=OFF USE_LAPACK=1 USE_NATIVE_ARCH=1 USE_FBGEMM=OFF USE_STATIC_DISPATCH=OFF USE_STATIC_NCCL=0 CUDA_USE_STATIC_CUDA_RUNTIME=0 CUDNN_STATIC=0 CAFFE2_STATIC_LINK_CUDA=0 USE_STATIC_CUDNN=0 VERBOSE=1 BUILD_TEST=0 NO_DISTRIBUTED=1 MAX_JOBS=24 TORCH_NVCC_FLAGS="-ccbin /tmp/g++" python3 setup.py bdist_wheel 2>&1 | tee comp
cp /usr/share/cmake/Modules/FindMPI.cmake cmake/Modules/
sed -i -e 's/ mpcc / mpifcc /' -e 's/ mpCC / mpiFCC /' -e 's/mpifc)/mpifrt)/' -e 's#${CMAKE_CURRENT_LIST_DIR}#/usr/share/cmake/Modules#' ./cmake/Modules/FindMPI.cmake
# https://pytorch.org/docs/stable/distributed.html#which-backend-to-use
BLAS=SSL2 BUILD_NAMEDTENSOR=OFF BUILD_TYPE=Release DISABLE_NUMA=1 USE_CUDA=ON USE_EXCEPTION_PTR=1 USE_GFLAGS=OFF USE_GLOG=OFF USE_MKLDNN=ON USE_MPI=ON USE_GLOO=OFF USE_NCCL=OFF USE_NNPACK=OFF USE_LAPACK=1 USE_NATIVE_ARCH=1 USE_FBGEMM=OFF USE_XNNPACK=OFF USE_STATIC_DISPATCH=OFF USE_STATIC_NCCL=0 CUDA_USE_STATIC_CUDA_RUNTIME=0 CUDNN_STATIC=0 CAFFE2_STATIC_LINK_CUDA=0 USE_STATIC_CUDNN=0 VERBOSE=1 BUILD_TEST=0 USE_DISTRIBUTED=1 MAX_JOBS=24 TORCH_NVCC_FLAGS="-ccbin /tmp/g++" python3 setup.py bdist_wheel 2>&1 | tee comp
#TH_BINARY_BUILD=1

# install
cd ~
python3 -m pip install --upgrade ${PREFIX}/dist/torch-1.4.0a0+7f73f1d-cp38-cp38-linux_aarch64.whl

#cd ~/fake_a64fx_cuda
#make clean ; make
#cd ${PREFIX}/${VENV_NAME}/lib
#ln -s ~/fake_a64fx_cuda/lib/libopenblas.so .
#export CC="fcc -Nclang -Kfast -SSL2"
#export CXX="FCC -Nclang -Kfast -SSL2"
#python3 -m pip install --upgrade --no-build-isolation ~/pip.cache/scipy-1.4.1.tar.gz
#export CC="fcc -Nclang -Kfast"
#export CXX="FCC -Nclang -Kfast"

# get torchvision

cd ${PREFIX}/
git clone https://github.com/pytorch/vision.git
cd vision/
git checkout -b v0.5.0 v0.5.0
cd ${PREFIX}/${VENV_NAME}/bin
mv python3.8 python3.8.0
ln -s ${PREFIX}/python-3.8.2.largepage/bin/python3.8 python3.8
cd -
python3 setup.py bdist_wheel 2>&1 | tee comp
cd ${PREFIX}/${VENV_NAME}/bin
mv python3.8.0 python3.8
cd ~
python3 -m pip install --upgrade ${PREFIX}/vision/dist/torchvision-0.5.0a0+85b8fbf-cp38-cp38-linux_aarch64.whl

# install horovod
export MPI_HOME=${TCSDS_PATH}
export HOROVOD_MPICXX_SHOW="${MPI_HOME}/bin/mpiFCC -show"
cd $HOME
git clone --recursive https://github.com/horovod/horovod.git
cd horovod/
git checkout -b v0.19.0 refs/tags/v0.19.0
patch -p1 < $HOME/20200702_fj/up/fj_hvd.patch
cd ${PREFIX}/${VENV_NAME}/bin
mv python3.8 python3.8.0
ln -s ${PREFIX}/python-3.8.2.largepage/bin/python3.8 python3.8
cd -
HOROVOD_WITHOUT_GLOO=1 HOROVOD_WITH_PYTORCH=1 HOROVOD_WITHOUT_TENSORFLOW=1 HOROVOD_WITHOUT_MXNET=1 HOROVOD_WITH_MPI=1 HOROVOD_GPU=CUDA python3 setup.py clean
rm -rf build/
HOROVOD_WITHOUT_GLOO=1 HOROVOD_WITH_PYTORCH=1 HOROVOD_WITHOUT_TENSORFLOW=1 HOROVOD_WITHOUT_MXNET=1 HOROVOD_WITH_MPI=1 HOROVOD_GPU=CUDA python3 setup.py bdist_wheel 2>&1 | tee comp
cd ${PREFIX}/${VENV_NAME}/bin
mv python3.8.0 python3.8
cd ~
python3 -m pip install --upgrade ~/horovod/dist/horovod-0.19.0-cp38-cp38-linux_aarch64.whl
python3 -c 'import horovod.torch as hvd'


# run horovod test case (FJ fuck around with LD_PRELOAD, so setting via -x doesnt work)
cat <<EOF > $HOME/htc.sh
#!/bin/bash
LD_PRELOAD=$HOME/fake_a64fx_cuda/libfake.so python3 -u $HOME/pytorch_synthetic_benchmark.py --batch 12 --num-iters 2
EOF
chmod +x $HOME/htc.sh
mpiexec -x LD_LIBRARY_PATH -x OMP_NUM_THREADS=12 -np 1 $HOME/htc.sh


cd $HOME/benchmarker
OMP_NUM_THREADS=12 LD_PRELOAD=~/fake_a64fx_cuda/libfake.so numactl -N 4 -m 4 python3 -m benchmarker --mode=training --framework=pytorch --problem=resnet50 --problem_size=$((1*48)) --batch_size=$((1*48)) --gpu 0
