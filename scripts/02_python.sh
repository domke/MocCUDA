#!/bin/bash
SDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )" && pwd )"
source "${SDIR}/host.env"

cd ${SDIR}/../dep ; mkdir -p install
WDIR="$(pwd)"

if [[ "$(hostname -s)" = "kiev"* ]]; then

	sleep 0

elif [[ "$(hostname -s)" = "epyc"* ]]; then

	sleep 0

elif lscpu | grep 'sve' >/dev/null 2>&1; then

	#export OPT=-O3
	#export ac_cv_opt_olimit_ok=no
	#export ac_cv_olimit_ok=no
	#export ac_cv_cflags_warn_all=''
	#export ax_cv_c_float_words_bigendian=no

	#PYVER="3.8.6"
	#if [ ! -f ${WDIR}/Python-${PYVER}.tgz ] ; then wget https://www.python.org/ftp/python/${PYVER}/Python-${PYVER}.tgz -O ${WDIR}/Python-${PYVER}.tgz ; fi
	#if [ ! -d ${SCRATCHDIR}/Python-${PYVER} ] ; then cd "${SCRATCHDIR}"/ ; tar xzf ${WDIR}/Python-${PYVER}.tgz ; fi
	#cd "${SCRATCHDIR}"/Python-${PYVER}
	#rm -rf ./build ; mkdir -p ./build ; cd ./build
	#CC="${MocCC}" CFLAGS="${MocCFLAGSnoLTO}" AR="${MocAR}" \
	#	../configure --prefix="${WDIR}/install/python" \
	#	--enable-shared --disable-ipv6 --target=aarch64 --build=aarch64
	#make -j32 install 2>&1 | tee comp
	#export PATH="$PATH:${WDIR}/install/python/bin"

	PRELOADLIBS="/home/apps/oss/PyTorch-1.7.0/lib/libtcmalloc.so"

else
	echo 'Err: unknown system; config please' ; exit 1
fi

cd ${WDIR}/install
rm -rf ./py3_venv
python3 -m venv py3_venv
source ./py3_venv/bin/activate

CC="${MocCC}" CFLAGS="${MocCFLAGS}" CXX="${MocCXX}" CXXFLAGS="${MocCXXFLAGS}" \
python3 -m pip install --upgrade wheel
CC="${MocCC}" CFLAGS="${MocCFLAGS}" CXX="${MocCXX}" CXXFLAGS="${MocCXXFLAGS}" \
python3 -m pip install --upgrade cython

cat <<EOF > ${WDIR}/python.env
export PRELOADLIBS="${PRELOADLIBS}"
export VENV_ROOT=${WDIR}/install/py3_venv
source \${VENV_ROOT}/bin/activate
export VENV_SITEPACKAGES=\$(python -c 'import sys ; print([p for p in sys.path if "site-packages" in p][0])')
EOF
