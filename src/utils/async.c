#include <dispatch/dispatch.h>
#include <stdlib.h>
#include <string.h>

#include "hijack_cuda_runtime_api.h"

#define MAX_QUEUES 255  // update label/uint8_t in create_dpq if more than 2^8

static int __inited__ = 0;

typedef struct dpq {
    dispatch_queue_t queue;
    cudaStream_t stream;
} dpq_t;

static dpq_t active_queues[MAX_QUEUES];
static uint8_t num_active_queues;

static inline dispatch_queue_t create_dpq()
{
    char label[10];
    snprintf(label, 10, "queue:%03u", (uint8_t)num_active_queues);
    return dispatch_queue_create(label, DISPATCH_QUEUE_SERIAL);
}

static void __initialize_dispatch__(cudaStream_t stream) {
    memset(active_queues, 0, MAX_QUEUES * sizeof(dpq_t));
    active_queues[num_active_queues].queue = create_dpq();
    active_queues[num_active_queues].stream = stream;
    num_active_queues++;
}

static inline int __dispatch_is_initialized__(cudaStream_t stream)
{
    if (__inited__)
        return __inited__;
    else {
        __initialize_dispatch__(stream);
        return (__inited__ = 1);
    }
}

typedef struct {
    int in;
    bool o;
} flip_bit_t;

__attribute__((used,noinline)) // make sure its not opti out
static void __drain_queue__(void *ctx)
{
#ifdef NODEBUG
    fprintf(stderr, "syncing...\n");
#endif
    flip_bit_t *c = (flip_bit_t *)ctx;
    if (c->in > 2)
        c->o = true;
    else
        c->o = false;
}

//TODO:
// does not account for cudaStreamCreateWith[Flags | Priority and
// cudaStreamDestroy -> should provide fn to cover those, BUT those aren't
// used in our test case yet

void fake_cuda_sync_dispatch(void *ctx, void (*fn)(void*), cudaStream_t stream)
{
    assert(__dispatch_is_initialized__(stream));

    uint8_t i;
    for (i = 0; i < num_active_queues; i++)
        if (stream == active_queues[i].stream) {
            dispatch_sync_f(active_queues[i].queue, ctx, fn);
            return;
        }
    assert(num_active_queues < MAX_QUEUES);
    active_queues[num_active_queues++].queue = create_dpq();
}

void fake_cuda_dispatch(void *ctx, void (*fn)(void*), cudaStream_t stream)
{
    assert(__dispatch_is_initialized__(stream));

    uint8_t i;
    for (i = 0; i < num_active_queues; i++)
        if (stream == active_queues[i].stream) {
            dispatch_async_f(active_queues[i].queue, ctx, fn);
            //dispatch_sync_f(active_queues[i].queue, ctx, fn);
            return;
        }
    assert(num_active_queues < MAX_QUEUES);
    active_queues[num_active_queues++].queue = create_dpq();
}

void fake_cuda_synchronize(cudaStream_t stream)
{
    assert(__dispatch_is_initialized__(stream));

    uint8_t i;
    flip_bit_t ctx;
    for (i = 0; i < num_active_queues; i++)
        if (stream == active_queues[i].stream) {
            ctx = (flip_bit_t){.in = i, .o = false};
            dispatch_sync_f(active_queues[i].queue, &ctx, __drain_queue__);
            return;
        }
    assert(0);
}

void fake_cuda_synchronize_all()
{
    uint8_t i;
    flip_bit_t ctx;
    for (i = 0; i < num_active_queues; i++) {
        ctx = (flip_bit_t){.in = i, .o = false};
        dispatch_sync_f(active_queues[i].queue, &ctx, __drain_queue__);
    }
}
