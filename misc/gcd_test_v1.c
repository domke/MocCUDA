#include <stdio.h>
#include <dispatch/dispatch.h>

static void timer_did_fire(void *context)
{
    printf("Strawberry fields...\n");
}

int main(int argc, const char *argv[])
{
    dispatch_source_t timer = dispatch_source_create(
        DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());

    dispatch_source_set_event_handler_f(timer, timer_did_fire);
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC,
                              0.5 * NSEC_PER_SEC);
    dispatch_resume(timer);
    dispatch_main();

    return 0;
}
