#pragma once

#include <stdio.h>
#include <assert.h>
#include <time.h>

typedef struct {
    void *fn_ptr;
    char *name;
} entry_t;

#ifdef NDEBUG
#error "Do not set NDEBUG because some calls are guarded embedded in asserts!"
#endif

#define __GIGA__ ((double)1e9)
#define __StoNS__ 1e9

#if defined (DEBUG) || defined (FUNC_TIMINGS)

#define ENTER(table, sym)                                                       \
    struct timespec __start__, __finish__;                                      \
    fprintf(stderr, "ENTER %s:%s\n", #table, #sym);                             \
    clock_gettime(CLOCK_REALTIME, &__start__);

#define LEAVE(table, sym)                                                       \
    clock_gettime(CLOCK_REALTIME, &__finish__);                                 \
    long __seconds__ = __finish__.tv_sec - __start__.tv_sec;                    \
    long __ns__ = __finish__.tv_nsec - __start__.tv_nsec;                       \
    if (__start__.tv_nsec > __finish__.tv_nsec) {                               \
        --__seconds__;                                                          \
        __ns__ += __StoNS__;                                                    \
    }                                                                           \
    fprintf(stderr, "LEAVE %s:%s (rt:%e)\n", #table, #sym,                      \
            (double)__seconds__ + (__ns__ / (double)__StoNS__));
#else

#define ENTER(table, sym)
#define LEAVE(table, sym)

#endif

#ifdef GEMM_TIMINGS

#define TIME_GEMM_START(maj, tA, tB, m, n, k, alp, bet, lA, lB, lC)             \
    fprintf(stderr,                                                             \
            "sgemm: %s T,T=(%d,%d) M,N,K=(%d,%d,%d) alp,bet=(%e,%e),"           \
            " ld{A,B,C}=(%d,%d,%d)\n",                                          \
	    #maj, tA, tB, m, n, k, alp, bet, lA, lB, lC);                       \
    struct timespec __gemm_start__, __gemm_finish__;                            \
    clock_gettime(CLOCK_REALTIME, &__gemm_start__);

#define TIME_GEMM_STOP(para, m, n, k)                                           \
    clock_gettime(CLOCK_REALTIME, &__gemm_finish__);                            \
    long __gemm_sec__ = __gemm_finish__.tv_sec - __gemm_start__.tv_sec;         \
    long __gemm_ns__ = __gemm_finish__.tv_nsec - __gemm_start__.tv_nsec;        \
    if (__gemm_start__.tv_nsec > __gemm_finish__.tv_nsec) {                     \
        --__gemm_sec__;                                                         \
        __gemm_ns__ += __StoNS__;                                               \
    }                                                                           \
    const double                                                                \
            __gemm_time__ = __gemm_sec__ + (__gemm_ns__ / (double)__StoNS__),   \
            _m_ = m, _n_ = n, _k_ = k, _2mnk_ = 2.0 * _m_ * _n_ * _k_;          \
    fprintf(stderr, "sgemm (%s) in gflop/s: %f (flop/byte: %f; rt:%e)\n",       \
            #para, (_2mnk_ / __GIGA__) / __gemm_time__,                         \
            _2mnk_ / (4.0 * (_m_ * _n_ + _m_ * _k_ + _n_ * _k_)),               \
            __gemm_time__);

#else

#define TIME_GEMM_START(maj, tA, tB, m, n, k, alp, bet, lA, lB, lC)
#define TIME_GEMM_STOP(para, m, n, k)

#endif

///////////////////////////////////////////////////////////////////////////////

//static int _cudart_inited_ = 0;
extern int _cudart_inited_;

extern entry_t cudart_lib_entry[];

#define CUDART_ENTRY_ENUM(x) ENTRY_##x

#define CUDART_FIND_ENTRY(table, sym)                               \
    ({                                                              \
        (table)[CUDART_ENTRY_ENUM(sym)].fn_ptr;                     \
     })

#define REAL_CUDA_RT_API_CALL(table, vers, sym, ...)                \
    ({                                                              \
        if (!_cudart_inited_) {                                     \
            assert(0 == load_cudart_lib());                         \
            _cudart_inited_ = 1;                                    \
        }                                                           \
        /*fprintf(stderr, "%s:%s:%s\n", #table, #sym, #__VA_ARGS__);*/  \
        cuda_rt_sym_t_##vers _entry = CUDART_FIND_ENTRY(table, sym);\
        _entry(__VA_ARGS__);                                        \
     })

typedef enum {
    CUDART_ENTRY_ENUM(cudaCreateChannelDesc),
    CUDART_ENTRY_ENUM(cudaDeviceGetAttribute),
    CUDART_ENTRY_ENUM(cudaDeviceGetStreamPriorityRange),
    CUDART_ENTRY_ENUM(cudaDeviceSynchronize),
    CUDART_ENTRY_ENUM(cudaEventCreateWithFlags),
    CUDART_ENTRY_ENUM(cudaEventDestroy),
    CUDART_ENTRY_ENUM(cudaEventQuery),
    CUDART_ENTRY_ENUM(cudaEventRecord),
    CUDART_ENTRY_ENUM(cudaFree),
    CUDART_ENTRY_ENUM(cudaFreeHost),
    CUDART_ENTRY_ENUM(cudaFuncGetAttributes),
    CUDART_ENTRY_ENUM(cudaGetDevice),
    CUDART_ENTRY_ENUM(cudaGetDeviceCount),
    CUDART_ENTRY_ENUM(cudaGetDeviceProperties),
    CUDART_ENTRY_ENUM(cudaGetLastError),
    CUDART_ENTRY_ENUM(cudaHostAlloc),
    CUDART_ENTRY_ENUM(cudaHostGetDevicePointer),
    CUDART_ENTRY_ENUM(cudaLaunchKernel),
    CUDART_ENTRY_ENUM(cudaMalloc),
    CUDART_ENTRY_ENUM(cudaMemcpy),
    CUDART_ENTRY_ENUM(cudaMemcpyAsync),
    CUDART_ENTRY_ENUM(cudaMemGetInfo),
    CUDART_ENTRY_ENUM(cudaMemsetAsync),
    CUDART_ENTRY_ENUM(cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags),
    CUDART_ENTRY_ENUM(cudaPeekAtLastError),
    CUDART_ENTRY_ENUM(cudaSetDevice),
    CUDART_ENTRY_ENUM(cudaStreamCreateWithFlags),
    CUDART_ENTRY_ENUM(cudaStreamCreateWithPriority),
    CUDART_ENTRY_ENUM(cudaStreamDestroy),
    CUDART_ENTRY_ENUM(cudaStreamSynchronize),
    //
    CUDART_ENTRY_END
} cudart_entry_enum_t;

int load_cudart_lib();

///////////////////////////////////////////////////////////////////////////////

extern int _cudnn_inited_;
extern entry_t cudnn_lib_entry[];

#define CUDNN_ENTRY_ENUM(x) ENTRY_##x

#define CUDNN_FIND_ENTRY(table, sym)                                \
    ({                                                              \
        (table)[CUDNN_ENTRY_ENUM(sym)].fn_ptr;                      \
     })

#define REAL_CUDNN_CALL(table, vers, sym, ...)                            \
    ({                                                              \
        if (!_cudnn_inited_) {                                      \
            assert(0 == load_cudnn_lib());                          \
            _cudnn_inited_ = 1;                                     \
        }                                                           \
        /*fprintf(stderr, "%s:%s:%s\n", #table, #sym, #__VA_ARGS__);*/  \
        cudnn_sym_t_##vers _entry = CUDNN_FIND_ENTRY(table, sym);   \
        _entry(__VA_ARGS__);                                        \
     })

typedef enum {
    CUDNN_ENTRY_ENUM(cudnnActivationBackward),
    CUDNN_ENTRY_ENUM(cudnnActivationForward),
    CUDNN_ENTRY_ENUM(cudnnAddTensor),
    CUDNN_ENTRY_ENUM(cudnnBatchNormalizationBackward),
    CUDNN_ENTRY_ENUM(cudnnBatchNormalizationBackwardEx),
    CUDNN_ENTRY_ENUM(cudnnBatchNormalizationForwardInference),
    CUDNN_ENTRY_ENUM(cudnnBatchNormalizationForwardTraining),
    CUDNN_ENTRY_ENUM(cudnnBatchNormalizationForwardTrainingEx),
    CUDNN_ENTRY_ENUM(cudnnConvolutionBackwardBias),
    CUDNN_ENTRY_ENUM(cudnnConvolutionBackwardData),
    CUDNN_ENTRY_ENUM(cudnnConvolutionBackwardFilter),
    CUDNN_ENTRY_ENUM(cudnnConvolutionBiasActivationForward),
    CUDNN_ENTRY_ENUM(cudnnConvolutionForward),
    CUDNN_ENTRY_ENUM(cudnnCopyAlgorithmDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreate),
    CUDNN_ENTRY_ENUM(cudnnCreateActivationDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateAlgorithmDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateAlgorithmPerformance),
    CUDNN_ENTRY_ENUM(cudnnCreateAttnDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateConvolutionDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateCTCLossDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateDropoutDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateFilterDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateFusedOpsConstParamPack),
    CUDNN_ENTRY_ENUM(cudnnCreateFusedOpsPlan),
    CUDNN_ENTRY_ENUM(cudnnCreateFusedOpsVariantParamPack),
    CUDNN_ENTRY_ENUM(cudnnCreateLRNDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateOpTensorDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreatePersistentRNNPlan),
    CUDNN_ENTRY_ENUM(cudnnCreatePoolingDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateReduceTensorDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateRNNDataDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateRNNDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateSeqDataDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateSpatialTransformerDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateTensorDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCreateTensorTransformDescriptor),
    CUDNN_ENTRY_ENUM(cudnnCTCLoss),
    CUDNN_ENTRY_ENUM(cudnnDeriveBNTensorDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroy),
    CUDNN_ENTRY_ENUM(cudnnDestroyActivationDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroyAlgorithmDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroyAlgorithmPerformance),
    CUDNN_ENTRY_ENUM(cudnnDestroyAttnDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroyConvolutionDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroyCTCLossDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroyDropoutDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroyFilterDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroyFusedOpsConstParamPack),
    CUDNN_ENTRY_ENUM(cudnnDestroyFusedOpsPlan),
    CUDNN_ENTRY_ENUM(cudnnDestroyFusedOpsVariantParamPack),
    CUDNN_ENTRY_ENUM(cudnnDestroyLRNDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroyOpTensorDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroyPersistentRNNPlan),
    CUDNN_ENTRY_ENUM(cudnnDestroyPoolingDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroyReduceTensorDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroyRNNDataDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroyRNNDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroySeqDataDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroySpatialTransformerDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroyTensorDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDestroyTensorTransformDescriptor),
    CUDNN_ENTRY_ENUM(cudnnDivisiveNormalizationBackward),
    CUDNN_ENTRY_ENUM(cudnnDivisiveNormalizationForward),
    CUDNN_ENTRY_ENUM(cudnnDropoutBackward),
    CUDNN_ENTRY_ENUM(cudnnDropoutForward),
    CUDNN_ENTRY_ENUM(cudnnDropoutGetReserveSpaceSize),
    CUDNN_ENTRY_ENUM(cudnnDropoutGetStatesSize),
    CUDNN_ENTRY_ENUM(cudnnFindConvolutionBackwardDataAlgorithm),
    CUDNN_ENTRY_ENUM(cudnnFindConvolutionBackwardDataAlgorithmEx),
    CUDNN_ENTRY_ENUM(cudnnFindConvolutionBackwardFilterAlgorithm),
    CUDNN_ENTRY_ENUM(cudnnFindConvolutionBackwardFilterAlgorithmEx),
    CUDNN_ENTRY_ENUM(cudnnFindConvolutionForwardAlgorithm),
    CUDNN_ENTRY_ENUM(cudnnFindConvolutionForwardAlgorithmEx),
    CUDNN_ENTRY_ENUM(cudnnFindRNNBackwardDataAlgorithmEx),
    CUDNN_ENTRY_ENUM(cudnnFindRNNBackwardWeightsAlgorithmEx),
    CUDNN_ENTRY_ENUM(cudnnFindRNNForwardInferenceAlgorithmEx),
    CUDNN_ENTRY_ENUM(cudnnFindRNNForwardTrainingAlgorithmEx),
    CUDNN_ENTRY_ENUM(cudnnFusedOpsExecute),
    CUDNN_ENTRY_ENUM(cudnnGetActivationDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetAlgorithmDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetAlgorithmPerformance),
    CUDNN_ENTRY_ENUM(cudnnGetAlgorithmSpaceSize),
    CUDNN_ENTRY_ENUM(cudnnGetAttnDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetBatchNormalizationBackwardExWorkspaceSize),
    CUDNN_ENTRY_ENUM(cudnnGetBatchNormalizationForwardTrainingExWorkspaceSize),
    CUDNN_ENTRY_ENUM(cudnnGetBatchNormalizationTrainingExReserveSpaceSize),
    CUDNN_ENTRY_ENUM(cudnnGetCallback),
    CUDNN_ENTRY_ENUM(cudnnGetConvolution2dDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetConvolution2dForwardOutputDim),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionBackwardDataAlgorithm),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionBackwardDataAlgorithmMaxCount),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionBackwardDataAlgorithm_v7),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionBackwardDataWorkspaceSize),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionBackwardFilterAlgorithm),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionBackwardFilterAlgorithmMaxCount),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionBackwardFilterAlgorithm_v7),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionBackwardFilterWorkspaceSize),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionForwardAlgorithm),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionForwardAlgorithmMaxCount),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionForwardAlgorithm_v7),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionForwardWorkspaceSize),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionGroupCount),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionMathType),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionNdDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionNdForwardOutputDim),
    CUDNN_ENTRY_ENUM(cudnnGetConvolutionReorderType),
    CUDNN_ENTRY_ENUM(cudnnGetCTCLossDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetCTCLossDescriptorEx),
    CUDNN_ENTRY_ENUM(cudnnGetCTCLossWorkspaceSize),
    CUDNN_ENTRY_ENUM(cudnnGetCudartVersion),
    CUDNN_ENTRY_ENUM(cudnnGetDropoutDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetErrorString),
    CUDNN_ENTRY_ENUM(cudnnGetFilter4dDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetFilterNdDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetFilterSizeInBytes),
    CUDNN_ENTRY_ENUM(cudnnGetFoldedConvBackwardDataDescriptors),
    CUDNN_ENTRY_ENUM(cudnnGetFusedOpsConstParamPackAttribute),
    CUDNN_ENTRY_ENUM(cudnnGetFusedOpsVariantParamPackAttribute),
    CUDNN_ENTRY_ENUM(cudnnGetLRNDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetMultiHeadAttnBuffers),
    CUDNN_ENTRY_ENUM(cudnnGetMultiHeadAttnWeights),
    CUDNN_ENTRY_ENUM(cudnnGetOpTensorDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetPooling2dDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetPooling2dForwardOutputDim),
    CUDNN_ENTRY_ENUM(cudnnGetPoolingNdDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetPoolingNdForwardOutputDim),
    CUDNN_ENTRY_ENUM(cudnnGetProperty),
    CUDNN_ENTRY_ENUM(cudnnGetReduceTensorDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetReductionIndicesSize),
    CUDNN_ENTRY_ENUM(cudnnGetReductionWorkspaceSize),
    CUDNN_ENTRY_ENUM(cudnnGetRNNBackwardDataAlgorithmMaxCount),
    CUDNN_ENTRY_ENUM(cudnnGetRNNBackwardWeightsAlgorithmMaxCount),
    CUDNN_ENTRY_ENUM(cudnnGetRNNBiasMode),
    CUDNN_ENTRY_ENUM(cudnnGetRNNDataDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetRNNDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetRNNForwardInferenceAlgorithmMaxCount),
    CUDNN_ENTRY_ENUM(cudnnGetRNNForwardTrainingAlgorithmMaxCount),
    CUDNN_ENTRY_ENUM(cudnnGetRNNLinLayerBiasParams),
    CUDNN_ENTRY_ENUM(cudnnGetRNNLinLayerMatrixParams),
    CUDNN_ENTRY_ENUM(cudnnGetRNNMatrixMathType),
    CUDNN_ENTRY_ENUM(cudnnGetRNNPaddingMode),
    CUDNN_ENTRY_ENUM(cudnnGetRNNParamsSize),
    CUDNN_ENTRY_ENUM(cudnnGetRNNProjectionLayers),
    CUDNN_ENTRY_ENUM(cudnnGetRNNTrainingReserveSize),
    CUDNN_ENTRY_ENUM(cudnnGetRNNWorkspaceSize),
    CUDNN_ENTRY_ENUM(cudnnGetSeqDataDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetStream),
    CUDNN_ENTRY_ENUM(cudnnGetTensor4dDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetTensorNdDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetTensorSizeInBytes),
    CUDNN_ENTRY_ENUM(cudnnGetTensorTransformDescriptor),
    CUDNN_ENTRY_ENUM(cudnnGetVersion),
    CUDNN_ENTRY_ENUM(cudnnIm2Col),
    CUDNN_ENTRY_ENUM(cudnnInitTransformDest),
    CUDNN_ENTRY_ENUM(cudnnLRNCrossChannelBackward),
    CUDNN_ENTRY_ENUM(cudnnLRNCrossChannelForward),
    CUDNN_ENTRY_ENUM(cudnnMakeFusedOpsPlan),
    CUDNN_ENTRY_ENUM(cudnnMultiHeadAttnBackwardData),
    CUDNN_ENTRY_ENUM(cudnnMultiHeadAttnBackwardWeights),
    CUDNN_ENTRY_ENUM(cudnnMultiHeadAttnForward),
    CUDNN_ENTRY_ENUM(cudnnOpTensor),
    CUDNN_ENTRY_ENUM(cudnnPoolingBackward),
    CUDNN_ENTRY_ENUM(cudnnPoolingForward),
    CUDNN_ENTRY_ENUM(cudnnQueryRuntimeError),
    CUDNN_ENTRY_ENUM(cudnnReduceTensor),
    CUDNN_ENTRY_ENUM(cudnnReorderFilterAndBias),
    CUDNN_ENTRY_ENUM(cudnnRestoreAlgorithm),
    CUDNN_ENTRY_ENUM(cudnnRestoreDropoutDescriptor),
    CUDNN_ENTRY_ENUM(cudnnRNNBackwardData),
    CUDNN_ENTRY_ENUM(cudnnRNNBackwardDataEx),
    CUDNN_ENTRY_ENUM(cudnnRNNBackwardWeights),
    CUDNN_ENTRY_ENUM(cudnnRNNBackwardWeightsEx),
    CUDNN_ENTRY_ENUM(cudnnRNNForwardInference),
    CUDNN_ENTRY_ENUM(cudnnRNNForwardInferenceEx),
    CUDNN_ENTRY_ENUM(cudnnRNNForwardTraining),
    CUDNN_ENTRY_ENUM(cudnnRNNForwardTrainingEx),
    CUDNN_ENTRY_ENUM(cudnnRNNGetClip),
    CUDNN_ENTRY_ENUM(cudnnRNNSetClip),
    CUDNN_ENTRY_ENUM(cudnnSaveAlgorithm),
    CUDNN_ENTRY_ENUM(cudnnScaleTensor),
    CUDNN_ENTRY_ENUM(cudnnSetActivationDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetAlgorithmDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetAlgorithmPerformance),
    CUDNN_ENTRY_ENUM(cudnnSetAttnDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetCallback),
    CUDNN_ENTRY_ENUM(cudnnSetConvolution2dDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetConvolutionGroupCount),
    CUDNN_ENTRY_ENUM(cudnnSetConvolutionMathType),
    CUDNN_ENTRY_ENUM(cudnnSetConvolutionNdDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetConvolutionReorderType),
    CUDNN_ENTRY_ENUM(cudnnSetCTCLossDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetCTCLossDescriptorEx),
    CUDNN_ENTRY_ENUM(cudnnSetDropoutDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetFilter4dDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetFilterNdDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetFusedOpsConstParamPackAttribute),
    CUDNN_ENTRY_ENUM(cudnnSetFusedOpsVariantParamPackAttribute),
    CUDNN_ENTRY_ENUM(cudnnSetLRNDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetOpTensorDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetPersistentRNNPlan),
    CUDNN_ENTRY_ENUM(cudnnSetPooling2dDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetPoolingNdDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetReduceTensorDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetRNNAlgorithmDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetRNNBiasMode),
    CUDNN_ENTRY_ENUM(cudnnSetRNNDataDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetRNNDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetRNNDescriptor_v5),
    CUDNN_ENTRY_ENUM(cudnnSetRNNDescriptor_v6),
    CUDNN_ENTRY_ENUM(cudnnSetRNNMatrixMathType),
    CUDNN_ENTRY_ENUM(cudnnSetRNNPaddingMode),
    CUDNN_ENTRY_ENUM(cudnnSetRNNProjectionLayers),
    CUDNN_ENTRY_ENUM(cudnnSetSeqDataDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetSpatialTransformerNdDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetStream),
    CUDNN_ENTRY_ENUM(cudnnSetTensor),
    CUDNN_ENTRY_ENUM(cudnnSetTensor4dDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetTensor4dDescriptorEx),
    CUDNN_ENTRY_ENUM(cudnnSetTensorNdDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSetTensorNdDescriptorEx),
    CUDNN_ENTRY_ENUM(cudnnSetTensorTransformDescriptor),
    CUDNN_ENTRY_ENUM(cudnnSoftmaxBackward),
    CUDNN_ENTRY_ENUM(cudnnSoftmaxForward),
    CUDNN_ENTRY_ENUM(cudnnSpatialTfGridGeneratorBackward),
    CUDNN_ENTRY_ENUM(cudnnSpatialTfGridGeneratorForward),
    CUDNN_ENTRY_ENUM(cudnnSpatialTfSamplerBackward),
    CUDNN_ENTRY_ENUM(cudnnSpatialTfSamplerForward),
    CUDNN_ENTRY_ENUM(cudnnTransformFilter),
    CUDNN_ENTRY_ENUM(cudnnTransformTensor),
    CUDNN_ENTRY_ENUM(cudnnTransformTensorEx),
    //
    CUDNN_ENTRY_END
} cudnn_entry_enum_t;

int load_cudnn_lib();

///////////////////////////////////////////////////////////////////////////////

extern int _cublas_inited_;

extern entry_t cublas_lib_entry[];

#define CUBLAS_ENTRY_ENUM(x) ENTRY_##x

#define CUBLAS_FIND_ENTRY(table, sym)                               \
    ({                                                              \
        (table)[CUBLAS_ENTRY_ENUM(sym)].fn_ptr;                     \
     })

#define REAL_CUBLAS_CALL(table, sym, ...)                           \
    ({                                                              \
        if (!_cublas_inited_) {                                     \
            assert(0 == load_cublas_lib());                         \
            _cublas_inited_ = 1;                                    \
        }                                                           \
        /*fprintf(stderr, "%s:%s:%s\n", #table, #sym, #__VA_ARGS__);*/  \
        cublas_sym_t _entry = CUBLAS_FIND_ENTRY(table, sym);        \
        _entry(__VA_ARGS__);                                        \
     })

typedef enum {
    CUBLAS_ENTRY_ENUM(cublasCreate_v2),
    CUBLAS_ENTRY_ENUM(cublasDestroy_v2),
    CUBLAS_ENTRY_ENUM(cublasSetStream_v2),
    CUBLAS_ENTRY_ENUM(cublasSgemm_v2),
    //
    CUBLAS_ENTRY_END
} cublas_entry_enum_t;

int load_cublas_lib();

///////////////////////////////////////////////////////////////////////////////
