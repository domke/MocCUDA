set terminal svg size 1600,1200 dynamic enhanced fname 'Times' fsize 32 butt dashlength 1.0
set output "roofl-a64fx-dl.svg"

# gflops
knl_fpeak = 140.8
knm_fpeak = 140.8
bdw_fpeak = 140.8
# gb/s (per core)
knl_mpeak = 21.3
#1024 GB/s /48
knm_mpeak = 172.0
#115 + 57 GB/s /1
bdw_mpeak = 345.0
#230 + 115 GB/s /1

xmin = 0.1
xmax = 100
ymin = 1
ymax = 300
set xtics nomirror
set xrange [xmin:xmax]
set logscale x 10
set yrange [ymin:ymax]
set logscale y 10

#	Functions
mem(x,y)     = exp( log( y ) - log( x ))
min(a,b) = (a < b) ? a : b
max(a,b) = (a > b) ? a : b
knl_froof(x) = knl_fpeak
knl_mroof(x) = mem(knl_fpeak / knl_mpeak, knl_fpeak) * x
knl_rflne(x) = min(knl_froof(x), knl_mroof(x))
knm_froof(x) = knm_fpeak
knm_mroof(x) = mem(knm_fpeak / knm_mpeak, knm_fpeak) * x
knm_rflne(x) = min(knm_froof(x), knm_mroof(x))
bdw_froof(x) = bdw_fpeak
bdw_mroof(x) = mem(bdw_fpeak / bdw_mpeak, bdw_fpeak) * x
bdw_rflne(x) = min(bdw_froof(x), bdw_mroof(x))

set grid
set key left top vertical Right box width +2
set xlabel "Arithmetic Intensity (flop/byte)"
set ylabel "Gflop/s"

bdw = "#A61A00"
knl = "#00B358"
knm = "#1924B1"

set label 1 "Theor. Peak Perf per Core (FP32)" at xmax-10, 1.25*knl_froof(xmax) right
set label 2 "Per Core BW (GB/s) [L1,L2,HBM]" at 1.25*xmin, 1.6*knl_mroof(xmin) left rotate by 42

plot knl_rflne(x) lt 1 lc rgb bdw lw 4 notitle, \
     knm_rflne(x) lt 1 lc rgb bdw lw 4 notitle, \
     bdw_rflne(x) lt 1 lc rgb bdw lw 4 notitle, \
     "bytes-n-flops-inference.data" u 2:1 pt 20 ps 0.6 lc rgb knl title 'Inference', \
     "bytes-n-flops-training.data" u 2:1 pt 20 ps 0.6 lc rgb  knm title 'Training'

