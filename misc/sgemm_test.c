#include <stdio.h>
#include <stdlib.h>
//#include <bits/stdc++.h>
#include <time.h>
//#include <fj_lapack.h>
#include <cblas.h>
////// invert these two for openblas; currently set for fujitsu blas
////#define blasint FJ_MATHLIB_TYPE_INT
//#define sl_cblas_sgemm cblas_sgemm

#ifdef REFLAPACK
    #define blasint int
    #define BLASFN(FUNC) cblas_##FUNC
#elif defined FUJITSU
    #define blasint FJ_MATHLIB_TYPE_INT
    /* enforce single-threaded */
    //#define BLASFN(FUNC) sl_cblas_##FUNC
    /* let ssl2 choose */
    #define BLASFN(FUNC) cblas_##FUNC
#elif defined ARMPL
    #define blasint armpl_int_t
    #define CBLAS_TRANSPOSE int
    #define BLASFN(FUNC) cblas_##FUNC
#else
    #define BLASFN(FUNC) cblas_##FUNC
#endif

#define TIME_GEMM_START(maj, tA, tB, m, n, k, alp, bet, lA, lB, lC)             \
    fprintf(stderr,                                                             \
            "sgemm: %s T,T=(%d,%d) M,N,K=(%d,%d,%d) alp,bet=(%e,%e),"           \
            " ld{A,B,C}=(%d,%d,%d)\n",                                          \
            #maj, tA, tB, m, n, k, alp, bet, lA, lB, lC);                       \
    struct timespec __gemm_start__, __gemm_finish__;                            \
    clock_gettime(CLOCK_REALTIME, &__gemm_start__);

#define __GIGA__ ((double)1e9)
#define __StoNS__ 1e9

#define TIME_GEMM_STOP(para, r, m, n, k)                                        \
    clock_gettime(CLOCK_REALTIME, &__gemm_finish__);                            \
    long __gemm_sec__ = __gemm_finish__.tv_sec - __gemm_start__.tv_sec;         \
    long __gemm_ns__ = __gemm_finish__.tv_nsec - __gemm_start__.tv_nsec;        \
    if (__gemm_start__.tv_nsec > __gemm_finish__.tv_nsec) {                     \
        --__gemm_sec__;                                                         \
        __gemm_ns__ += __StoNS__;                                               \
    }                                                                           \
    const double                                                                \
            __gemm_time__ = __gemm_sec__ + (__gemm_ns__ / (double)__StoNS__),   \
            _r_ = r, _m_ = m, _n_ = n, _k_ = k, _2mnk_ = 2.0 * _m_ * _n_ * _k_; \
    fprintf(stderr, "sgemm (%s) in gflop/s: %f (flop/byte: %f; rt:%e)\n",       \
            #para, (_2mnk_ / __GIGA__) / (__gemm_time__ / _r_),                 \
            _2mnk_ / (4.0 * (_m_ * _n_ + _m_ * _k_ + _n_ * _k_)),               \
            (__gemm_time__ / _r_));


typedef struct gemm {
    char tA, tB;
    blasint m,n,k;
    blasint lda, ldb, ldc;
    float alpha, beta;
} sgemm_in_t;

int main() {
    int rounds = 100, heat = 1, in=62;

    sgemm_in_t sgemm_input[in]; in=0;

    //sgemm_input[in++] = (sgemm_in_t){'n', 'n', 4*1024, 4*1024, 4*1024, 4*1024, 4*1024, 4*1024, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 12544, 64, 147, 12544, 147, 12544, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 196, 1024, 256, 196, 256, 196, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 196, 1024, 512, 196, 512, 196, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 196, 256, 1024, 196, 1024, 196, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 196, 256, 2304, 196, 2304, 196, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 196, 512, 1024, 196, 1024, 196, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 2048, 32, 1000, 2048, 1000, 2048, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 3136, 128, 256, 3136, 256, 3136, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 3136, 256, 64, 3136, 64, 3136, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 3136, 64, 256, 3136, 256, 3136, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 3136, 64, 576, 3136, 576, 3136, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 3136, 64, 64, 3136, 64, 3136, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 49, 2048, 1024, 49, 1024, 49, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 49, 2048, 512, 49, 512, 49, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 49, 512, 2048, 49, 2048, 49, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 49, 512, 4608, 49, 4608, 49, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 784, 128, 1152, 784, 1152, 784, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 784, 128, 512, 784, 512, 784, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 784, 256, 512, 784, 512, 784, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 784, 512, 128, 784, 128, 784, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 'n', 784, 512, 256, 784, 256, 784, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 196, 1024, 256, 196, 1024, 196, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 196, 1024, 512, 196, 1024, 196, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 196, 2304, 256, 196, 2304, 196, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 196, 256, 1024, 196, 256, 196, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 196, 512, 1024, 196, 512, 196, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 2048, 1000, 32, 2048, 1000, 2048, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 3136, 256, 128, 3136, 256, 3136, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 3136, 256, 64, 3136, 256, 3136, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 3136, 576, 64, 3136, 576, 3136, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 3136, 64, 256, 3136, 64, 3136, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 3136, 64, 64, 3136, 64, 3136, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 49, 1024, 2048, 49, 1024, 49, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 49, 2048, 512, 49, 2048, 49, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 49, 4608, 512, 49, 4608, 49, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 49, 512, 2048, 49, 512, 49, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 784, 1152, 128, 784, 1152, 784, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 784, 128, 512, 784, 128, 784, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 784, 256, 512, 784, 256, 784, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 784, 512, 128, 784, 512, 784, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'n', 't', 784, 512, 256, 784, 512, 784, 1.000000e+00, 0.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 1000, 32, 2048, 2048, 2048, 1000, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 1024, 2048, 49, 49, 49, 1024, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 1024, 256, 196, 196, 196, 1024, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 1024, 512, 196, 196, 196, 1024, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 1152, 128, 784, 784, 784, 1152, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 128, 512, 784, 784, 784, 128, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 147, 64, 12544, 12544, 12544, 147, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 2048, 512, 49, 49, 49, 2048, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 2304, 256, 196, 196, 196, 2304, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 256, 1024, 196, 196, 196, 256, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 256, 128, 3136, 3136, 3136, 256, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 256, 512, 784, 784, 784, 256, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 256, 64, 3136, 3136, 3136, 256, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 4608, 512, 49, 49, 49, 4608, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 512, 1024, 196, 196, 196, 512, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 512, 128, 784, 784, 784, 512, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 512, 2048, 49, 49, 49, 512, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 512, 256, 784, 784, 784, 512, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 576, 64, 3136, 3136, 3136, 576, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 64, 256, 3136, 3136, 3136, 64, 1.000000e+00, 1.000000e+00};
    sgemm_input[in++] = (sgemm_in_t){'t', 'n', 64, 64, 3136, 3136, 3136, 64, 1.000000e+00, 1.000000e+00};

    for (int i=0; i<in; i++) {
        CBLAS_TRANSPOSE tA = (sgemm_input[i].tA == 'n') ? CblasNoTrans : CblasTrans, tB = (sgemm_input[i].tB == 'n') ? CblasNoTrans : CblasTrans;
        //char tA = sgemm_input[i].tA, tB = sgemm_input[i].tB;
        blasint m = sgemm_input[i].m, n = sgemm_input[i].n, k = sgemm_input[i].k;
        blasint lda = sgemm_input[i].lda, ldb = sgemm_input[i].ldb, ldc = sgemm_input[i].ldc;
        float alpha = sgemm_input[i].alpha, beta = sgemm_input[i].beta;

        // INIT
        float *A, *B, *C;
        A = (float *)malloc(sizeof(float)*m*k);
        B = (float *)malloc(sizeof(float)*k*n);
        C = (float *)malloc(sizeof(float)*m*n);
        {
        TIME_GEMM_START(CblasColMajor, tA, tB, m, n, k, alpha, beta, lda, ldb, ldc);
        #pragma omp parallel for
        for (int j=0; j<m*k; j++) A[j] = 0.1;
        #pragma omp parallel for
        for (int j=0; j<k*n; j++) B[j] = -0.1;
        #pragma omp parallel for
        for (int j=0; j<m*n; j++) C[j] = j;
        TIME_GEMM_STOP(init, 1, 0, 0, 0);
        }

        {
        // HEAT
        TIME_GEMM_START(CblasColMajor, tA, tB, m, n, k, alpha, beta, lda, ldb, ldc);
        for (int j=0; j<heat; j++)
            BLASFN(sgemm)(CblasColMajor, tA, tB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
            //sgemm_(&tA, &tB, &m, &n, &k, &alpha, A, &lda, B, &ldb, &beta, C, &ldc, 0, 0);
        TIME_GEMM_STOP(pre, heat, m, n, k);
        }

        // MEASURE
        TIME_GEMM_START(CblasColMajor, tA, tB, m, n, k, alpha, beta, lda, ldb, ldc);
        for (int j=0; j < rounds; j++)
            BLASFN(sgemm)(CblasColMajor, tA, tB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
            //sgemm_(&tA, &tB, &m, &n, &k, &alpha, A, &lda, B, &ldb, &beta, C, &ldc, 0, 0);
        TIME_GEMM_STOP(core, rounds, m, n, k);

        free(A);
        free(B);
        free(C);
    }
}
