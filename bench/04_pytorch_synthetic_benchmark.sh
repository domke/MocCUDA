#!/bin/bash
SDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )" && pwd )"
BMSN="$( basename "${BASH_SOURCE[0]:-$0}" )"

if [ -z "${BACKEND}" ]; then
	if [ -z "$1" ]; then	BACKEND="moccuda"
	else			BACKEND="`echo $1 | tr '[:upper:]' '[:lower:]'`"
	fi
fi
if lscpu | grep 'sve' >/dev/null 2>&1 && [[ "${BACKEND}" != *"moccuda"* ]]; then
	if [[ "${BACKEND}" = *"170"* ]]; then
		source ${SDIR}/../init.env "FUJITORCH170"
	else
		source ${SDIR}/../init.env "FUJITORCH150"
	fi
else
	source ${SDIR}/../init.env
fi

if [ -n "${PJM_PROC_BY_NODE}" ] && [ ${PJM_PROC_BY_NODE} -lt 2 ]; then
	echo "ERR: need at least 2 ranks per node for this test"
	exit
fi

if [[ "${BACKEND}" = *"moccuda"* ]]; then
	PRELOADLIBS="${PRELOADLIBS:+${PRELOADLIBS} }${SDIR}/../lib/libMocCUDA.so"
	PRELOG="${BACKEND}"
	RUNVERS=""
elif [[ "${BACKEND}" = *"cuda"* ]]; then
	PRELOG="${BACKEND}"
	RUNVERS=""
elif [[ "${BACKEND}" = *"dnnl"* ]]; then
	PRELOG="${BACKEND}"
	RUNVERS="--no-cuda --use-dnnl"
elif [[ "${BACKEND}" = *"native"* ]]; then
	PRELOG="${BACKEND}"
	RUNVERS="--no-cuda"
else
	echo "ERR: requested backend not supported or does not exist" ; exit 1
fi
if [ -n "${PRELOADLIBS}" ]; then	PRELOAD="LD_PRELOAD=\"${PRELOADLIBS}\""
else					PRELOAD=""
fi

BENCHPPN="${BENCHPPN:-4}"
BENCHMINRANK="${BENCHMINRANK:-2}"
BENCHSTEPRANK="${BENCHSTEPRANK:-1}"
BENCHMAXRANK="${BENCHMAXRANK:-4}"
MPIR=()
for R in $(seq ${BENCHMINRANK} ${BENCHSTEPRANK} ${BENCHMAXRANK}); do MPIR+=( ${R} ) ; done
POW4="$(for x in $(seq 0 4); do echo $((2**x)); done)"
POW6="$(for x in $(seq 0 6); do echo $((2**x)); done)"
FAC6_12="$(seq 6 6 12)"
FAC12_72="6 $(seq 12 12 72)"

cat <<EOF > "./${PJM_JOBID}pytorch_synthetic_benchmark.sh"
#!/bin/bash
$PRELOAD python3 -u ${SDIR}/src/pytorch_synthetic_benchmark.py \\
        --batch \$1 --num-iters 3 ${RUNVERS}
EOF
chmod +x "./${PJM_JOBID}pytorch_synthetic_benchmark.sh"

TMPLOG="${SCRATCHDIR}/$(hostname -s)-${BASHPID}"
RUNINFO="^Iter \|^Img\|^Total "
LOGDIR="${SDIR}/../log" ; mkdir -p "${LOGDIR}"

for N in ${!MPIR[@]}; do
	if [[ "$(hostname -s)" = "kiev"* ]] || [[ "$(hostname -s)" = "epyc"* ]]; then
		NumCORES="$(( $(lscpu | /bin/grep ^Socket | cut -d ':' -f2) * $(lscpu | /bin/grep ^Core | cut -d ':' -f2) ))"
	else
		NumCORES="$(nproc)"
	fi
	MPIMapping="--map-by slot:pe=$(((${NumCORES} / ${MPIR[$N]}) + (${NumCORES} < ${MPIR[$N]})))"
	for OMP in `echo ${POW4} ${FAC6_12} | sed -e 's/ /\n/g' | sort -n`; do
		for BS in `echo ${POW6} ${FAC12_72} | sed -e 's/ /\n/g' | sort -n`; do
			LOG="${LOGDIR}/${BMSN}_${PRELOG}_rankrange${BENCHMINRANK}to${BENCHMAXRANK}_ppn${BENCHPPN}_omp${OMP}.log"
			export OMP_NUM_THREADS=${OMP}
			echo "batch size: ${BS}  ranks: ${MPIR[$N]}  omp: ${OMP}  backend: ${BACKEND}" | tee -a ${LOG}
			if [[ "$(hostname -s)" = "kiev"* ]] || [[ "$(hostname -s)" = "epyc"* ]]; then
				timeout --kill-after=30s 10m \
					mpirun -np ${MPIR[$N]} ${MPIMapping} \
					-x LD_LIBRARY_PATH \
					-x OMP_NUM_THREADS=${OMP} \
					,/pytorch_synthetic_benchmark.sh ${BS} > ${TMPLOG} 2>&1
			elif lscpu | grep 'sve' >/dev/null 2>&1; then
				timeout --kill-after=30s 10m \
					mpiexec -of ${TMPLOG} \
					-x LD_LIBRARY_PATH \
					-x OMP_NUM_THREADS=${OMP} \
					-np ${MPIR[$N]} \
					./${PJM_JOBID}pytorch_synthetic_benchmark.sh ${BS}
			fi
			if [ "x$?" = "x137" ] || [ "x$?" = "x124" ]; then echo "OOM/time killer, stop here" >> ${LOG}; break; fi
			/usr/bin/grep "${RUNINFO}" ${TMPLOG} | tee -a ${LOG}
			sleep 1
			rm -f ${TMPLOG}
		done
	done
done

rm -f "./${PJM_JOBID}pytorch_synthetic_benchmark.sh"
