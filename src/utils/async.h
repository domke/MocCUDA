#pragma once

void fake_cuda_dispatch(void *ctx, void (*fn)(void*), cudaStream_t stream);
void fake_cuda_sync_dispatch(void *ctx, void (*fn)(void*), cudaStream_t stream);
void fake_cuda_synchronize(cudaStream_t stream);
void fake_cuda_synchronize_all();
