#!/bin/bash
SDIR="$( cd "$( dirname "${BASH_SOURCE[0]:-$0}" )" && pwd )"
source "${SDIR}/host.env"

cd ${SDIR}/../dep ; mkdir -p install
WDIR="$(pwd)"

if [[ "$(hostname -s)" = "kiev"* ]]; then

	cd ${WDIR}/install
	ln -s ${CUDA_TOOLKIT_ROOT_DIR} cuda
	ln -s ${CUDNN_ROOT} cudnn
	ln -s ${NCCL_ROOT} nccl

elif [[ "$(hostname -s)" = "epyc"* ]]; then

	sleep 0

elif lscpu | grep 'sve' >/dev/null 2>&1; then

	CUDAVER="cuda-repo-rhel8-11-6-local"
	if [ ! -d ${WDIR}/install/cuda ]; then
		if [ ! -f ${WDIR}/${CUDAVER}.rpm ]; then
			wget https://developer.download.nvidia.com/compute/cuda/11.6.0/local_installers/${CUDAVER}-11.6.0_510.39.01-1.aarch64.rpm -O ${WDIR}/${CUDAVER}.rpm
		fi
		mkdir -p ${WDIR}/install/cuda ; cd ${WDIR}/install/cuda
		rpm2cpio ${WDIR}/${CUDAVER}.rpm | cpio -idmv
		for FILE in $(ls ./var/${CUDAVER}/*.rpm) ; do rpm2cpio $FILE | cpio -idmv ; done
	fi

##JJ	CUDNNVER="v7.6.5.32"	#XXX: v8 not cmopat with torch 1.4, but no aarch64 of v7
##JJ	if [ ! -f ${WDIR}/cudnn-10.2-linux-x64-${CUDNNVER}.tgz ]; then
##JJ		echo "ERR: please download cuDNN v7.6.5 (November 18th, 2019), for CUDA 10.2 for Linux(x86_64architecture) from https://developer.nvidia.com/rdp/cudnn-archive and place tgz in folder ${SDIR}/../dep/" ; exit 1
##JJ	else
##JJ		#tar -xzf ${WDIR}/cudnn-11.3-linux-aarch64sbsa-${CUDNNVER}.tgz -C ${WDIR}/install/cudnn --strip-components 1
##JJ		source ${WDIR}/libdispatch.env
##JJ		cd "${SDIR}/../" ; LIBS="" make -f Makefile.${MocHOST} cudnn -B ; cd -
##JJ		tar -xf ${WDIR}/cudnn-10.2-linux-x64-${CUDNNVER}.tgz cuda/include/cudnn.h -O > ${CUDNN_ROOT}/include/cudnn.h
##JJ	fi
##JJ	CUDNNVER="v8.2.1.32"
##JJ	if [ ! -f ${WDIR}/cudnn-11.3-linux-aarch64sbsa-${CUDNNVER}.tgz ]; then
##JJ		echo "ERR: please download cuDNN v8.2.1 (June 7th, 2021), for CUDA 11.x for Linux (aarch64sbsa) from https://developer.nvidia.com/rdp/cudnn-archive and place tgz in folder ${SDIR}/../dep/" ; exit 1
##JJ	elif [ ! -d ${WDIR}/install/cudnn ]; then
##JJ		mkdir -p ${WDIR}/install/cudnn
##JJ		tar -xzf ${WDIR}/cudnn-11.3-linux-aarch64sbsa-${CUDNNVER}.tgz -C ${WDIR}/install/cudnn --strip-components 1
##JJ	fi
	#XXX: v8 not compat with torch 1.4, but no aarch64 of v7, so just get header from v7  ¯\_(ツ)_/¯
	CUDNNVER="v7.6.5.32"
	if [ ! -f ${WDIR}/cudnn-10.2-linux-x64-${CUDNNVER}.tgz ]; then
		echo "ERR: please download cuDNN v7.6.5 (November 18th, 2019), for CUDA 10.2 for Linux(x86_64architecture) from https://developer.nvidia.com/rdp/cudnn-archive and place tgz in folder ${SDIR}/../dep/"; exit 1
	else
		cd "${SDIR}/../" ; LIBS="" CC="${MocCC}" OPTI="${MocCFLAGSnoLTO}" make -f Makefile.${MocHOST} cudnn -B ; cd -
		tar -xf ${WDIR}/cudnn-10.2-linux-x64-${CUDNNVER}.tgz cuda/include/cudnn.h -O > ${WDIR}/install/cudnn/include/cudnn.h
	fi

	NCCLVER="2.7.8"
	if [ ! -f ${WDIR}/nccl-repo-rhel8-${NCCLVER}-ga-cuda11.0-1-1.aarch64.rpm ]; then
		echo "ERR: please download NCCL 2.7.8, for CUDA 11.0, July 24,2020 Local installer [ARM] for RedHat/CentOS 8 from https://developer.nvidia.com/nccl/nccl-legacy-downloads and place rpm in folder ${SDIR}/../dep/" ; exit 1
	elif [ ! -d ${WDIR}/install/nccl ]; then
		mkdir -p ${WDIR}/install/nccl ; cd ${WDIR}/install/nccl
		rpm2cpio ${WDIR}/nccl-repo-rhel8-${NCCLVER}-*.rpm | cpio -idmv
		for FILE in $(ls ./var/nccl-repo-${NCCLVER}-*/*.rpm) ; do rpm2cpio $FILE | cpio -idmv ; done
	fi

else
	echo 'Err: unknown system; config please' ; exit 1
fi
