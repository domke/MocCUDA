#include <stdlib.h>
//#include <string.h>
#include <execinfo.h>
#include <unistd.h>

#include <hwloc.h>
static hwloc_topology_t hwloc_topo;
static int _hwloc_topo_inited_ = 0;

#include "hijack_cuda_runtime_api.h"
#include "utils.h"
#include "async.h"

#ifndef PATHTOMocDeviceProp
#define PATHTOMocDeviceProp "."
#endif

#ifdef USE_MocREGISTERFNs
void** __cudaRegisterFatBinary(void *fatCubin)
{
    return NULL;
}

void __cudaRegisterFatBinaryEnd(void **fatCubinHandle)
{
    return;
}

void __cudaUnregisterFatBinary(void **fatCubinHandle)
{
    return;
}

void __cudaRegisterVar(void **fatCubinHandle, char *hostVar,
                       char *deviceAddress, const char *deviceName, int ext,
                       size_t size, int constant, int global)
{
    return;
}

void __cudaRegisterManagedVar(void **fatCubinHandle, void **hostVarPtrAddress,
                              char *deviceAddress, const char *deviceName,
                              int ext, size_t size, int constant, int global)
{
    return;
}

char __cudaInitModule(void **fatCubinHandle)
{
    return '0';
}

struct surfaceReference{
    int NOFUCKINGIDEA;
}; //FIXME: no clue
void __cudaRegisterSurface(void **fatCubinHandle,
                           const struct surfaceReference *hostVar,
                           const void **deviceAddress, const char *deviceName,
                           int dim, int ext)
{
    return;
}

typedef struct {
    unsigned int a[3];
} uint3;

void __cudaRegisterFunction(void **fatCubinHandle, const char *hostFun,
                            char *deviceFun, const char *deviceName,
                            int thread_limit, uint3 *tid, uint3 *bid,
                            dim3 *bDim, dim3 *gDim, int *wSize)
{
    return;
}
#endif //USE_MocREGISTERFNs

struct cudaChannelFormatDesc cudaCreateChannelDesc(int x, int y, int z, int w,
                                                   enum cudaChannelFormatKind f)
{
    ENTER(cudart_lib_entry, cudaCreateChannelDesc);

    struct cudaChannelFormatDesc desc;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    desc = (struct cudaChannelFormatDesc){.x = x, .y = y, .z = z, .w = w,
                                          .f = f};
#else //USE_MocCUDA
    desc = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v2, cudaCreateChannelDesc,
                                 x, y, z, w, f);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=<NA>  desc = {%d,%d,%d,%d,%d}\n",
            desc.x, desc.y, desc.z, desc.w, desc.f);
    #endif

    LEAVE(cudart_lib_entry, cudaCreateChannelDesc);
    return desc;
}

cudaError_t cudaDeviceGetAttribute(int *value, enum cudaDeviceAttr attr,
                                   int device)
{
    ENTER(cudart_lib_entry, cudaDeviceGetAttribute);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    //assert(0 == device);
    switch (attr) {
        /*
         * ret=0  value, attr, device = 2147483647, 5, 0
         * ret=0  value, attr, device = 65535, 6, 0
         * ret=0  value, attr, device = 65535, 7, 0
         * ret=0  value, attr, device = 1024, 2, 0
         * ret=0  value, attr, device = 1024, 3, 0
         * ret=0  value, attr, device = 64, 4, 0
         * ret=0  value, attr, device = 65536, 12, 0
         * ret=0  value, attr, device = 28, 16, 0
         * ret=0  value, attr, device = 32, 10, 0
         * ret=0  value, attr, device = 49152, 8, 0
         * ret=0  value, attr, device = 98304, 81, 0
         * ret=0  value, attr, device = 0, 34, 0
         * ret=0  value, attr, device = 2048, 39, 0
         * ret=0  value, attr, device = 512, 14, 0
         * ret=0  value, attr, device = 0, 35, 0
         * ret=0  value, attr, device = 1, 95, 0
         * ret=0  value, attr, device = 6, 75, 0
         * ret=0  value, attr, device = 1, 76, 0
         */
        case cudaDevAttrMaxBlockDimX: //2:
            *value = 1024; break;
        case cudaDevAttrMaxBlockDimY: //3:
            *value = 1024; break;
        case cudaDevAttrMaxBlockDimZ: //4:
            *value = 64; break;
        case cudaDevAttrMaxGridDimX: //5:
            *value = 2147483647; break;
        case cudaDevAttrMaxGridDimY: //6:
            *value = 65535; break;
        case cudaDevAttrMaxGridDimZ: //7:
            *value = 65535; break;
        case cudaDevAttrMaxSharedMemoryPerBlock: //8:
            *value = 49152; break;
        case cudaDevAttrWarpSize: //10:
            *value = 32; break;
        case cudaDevAttrMaxRegistersPerBlock: //12:
            *value = 65536; break;
        case cudaDevAttrTextureAlignment: //14:
            *value = 512; break;
        case cudaDevAttrMultiProcessorCount: //16:
            *value = 28; break;
        case cudaDevAttrPciDeviceId: //34:
            *value = 0; break;
        case cudaDevAttrTccDriver: //35:
            *value = 0; break;
        case cudaDevAttrMaxThreadsPerMultiProcessor: //39:
            *value = 2048; break;
        case cudaDevAttrComputeCapabilityMajor: //75:
            *value = 6; break;
        case cudaDevAttrComputeCapabilityMinor: //76:
            *value = 1; break;
        case cudaDevAttrMaxSharedMemoryPerMultiprocessor: //81:
            *value = 98304; break;
        case cudaDevAttrCooperativeLaunch: //95:
            *value = 1; break;
        default:
            fprintf(stderr, "unknown attr %d", attr);
            rce = cudaErrorApiFailureBase;
    }
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaDeviceGetAttribute,
                                value, attr, device);
#endif //USE_MocCUDA

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  value = %d, attr = %d, device = %d\n",
            rce, *value, attr, device);
    #endif

    LEAVE(cudart_lib_entry, cudaDeviceGetAttribute);
    return rce;
}

cudaError_t cudaDeviceGetStreamPriorityRange(int *leastPriority,
                                             int *greatestPriority)
{
    ENTER(cudart_lib_entry, cudaDeviceGetStreamPriorityRange);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (leastPriority)
        *leastPriority = 0;
    if (greatestPriority)
        *greatestPriority = -1;
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1,
                                cudaDeviceGetStreamPriorityRange,
                                leastPriority,
                                greatestPriority);
#endif

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  leastPriority = %d, greatestPriority = %d\n",
            rce,
            (leastPriority) ? *leastPriority : 0,
            (greatestPriority) ? *greatestPriority : 0);
    #endif

    LEAVE(cudart_lib_entry, cudaDeviceGetStreamPriorityRange);
    return rce;
}

cudaError_t cudaDeviceSynchronize(void)
{
    ENTER(cudart_lib_entry, cudaDeviceSynchronize);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    #if defined USE_GCD && USE_GCD >= 1
    fake_cuda_synchronize_all();
    #endif
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaDeviceSynchronize);
#endif

    #ifdef DEBUG
    // assert(cudaSuccess == rce);  <- not working with pip's torch
    fprintf(stderr, "\tret=%d\n", rce);
    #endif

    LEAVE(cudart_lib_entry, cudaDeviceSynchronize);
    return rce;
}

cudaError_t cudaEventCreateWithFlags(cudaEvent_t *event, unsigned int flags)
{
    ENTER(cudart_lib_entry, cudaEventCreateWithFlags);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    assert((*event = (struct CUevent_st *)malloc(sizeof(**event))));
    (*event)->flags = flags;
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaEventCreateWithFlags,
                                event, flags);
#endif //USE_MocCUDA

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  event = %p, flags = %u\n",
            rce, (void*)(*event), flags);
    #endif

    LEAVE(cudart_lib_entry, cudaEventCreateWithFlags);
    return rce;
}

cudaError_t cudaEventDestroy(cudaEvent_t event)
{
    ENTER(cudart_lib_entry, cudaEventDestroy);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    if (event)
        free(event);
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaEventDestroy, event);
#endif //USE_MocCUDA

    #ifdef DEBUG
    // assert(cudaSuccess == rce);  <- not working with pip's torch
    fprintf(stderr, "\tret=%d  event = %p\n", rce, (void*)event);
    #endif

    LEAVE(cudart_lib_entry, cudaEventDestroy);
    return rce;
}

cudaError_t cudaEventQuery(cudaEvent_t event)
{
    ENTER(cudart_lib_entry, cudaEventQuery);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    /* RT API: Returns cudaSuccess if all captured work has been completed, or
     * cudaErrorNotReady if any captured work is incomplete. */
    //FIXME: for now to avoid trouble: sync -> return success
    fake_cuda_synchronize((*event).stream);
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaEventQuery, event);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d\n", rce);
    #endif

    LEAVE(cudart_lib_entry, cudaEventQuery);
    return rce;
}

cudaError_t cudaEventRecord(cudaEvent_t event, cudaStream_t stream __dv(0))
{
    ENTER(cudart_lib_entry, cudaEventRecord);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    (*event).stream = stream;
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaEventRecord, event,
                                stream);
#endif //USE_MocCUDA

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  event = %p, stream = %p\n",
            rce, (void*)event, (void*)stream);
    #endif

    LEAVE(cudart_lib_entry, cudaEventRecord);
    return rce;
}

cudaError_t cudaFree(void *devPtr)
{
    ENTER(cudart_lib_entry, cudaFree);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    #if defined USE_GCD && USE_GCD >= 1
    // seem cudaFree is still synchronous
    fake_cuda_synchronize_all();
    #endif
    if (devPtr)
        free(devPtr);
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaFree, devPtr);
#endif //USE_MocCUDA

    #ifdef DEBUG
    // assert(cudaSuccess == rce);  <- not working with pip's torch
    fprintf(stderr, "\tret=%d  devPtr = %p\n", rce, devPtr);
    #endif

    LEAVE(cudart_lib_entry, cudaFree);
    return rce;
}

cudaError_t cudaFreeHost(void *ptr)
{
    ENTER(cudart_lib_entry, cudaFreeHost);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    #if defined USE_GCD && USE_GCD >= 1
    // cannot find proof, but lets assume the same behavior as cudaFree
    fake_cuda_synchronize_all();
    #endif
    if (ptr)
        free(ptr);
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaFreeHost, ptr);
#endif //USE_MocCUDA

    #ifdef DEBUG
    // assert(cudaSuccess == rce);  <- not working with pip's torch
    fprintf(stderr, "\tret=%d  devPtr = %p\n", rce, ptr);
    #endif

    LEAVE(cudart_lib_entry, cudaFreeHost);
    return rce;
}

cudaError_t cudaFuncGetAttributes(struct cudaFuncAttributes *attr,
                                  const void *entry)
{
    ENTER(cudart_lib_entry, cudaFuncGetAttributes);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    /* no clue, but 1080Ti return these values */
    if (attr) {
        attr->sharedSizeBytes = 0;
        attr->constSizeBytes = 0;
        attr->localSizeBytes = 0;
        attr->maxThreadsPerBlock = 1024;
        attr->numRegs = 2;
        attr->ptxVersion = 61;
        attr->binaryVersion = 61;
        attr->cacheModeCA = 0;
        attr->maxDynamicSharedSizeBytes = 49152;
        attr->preferredShmemCarveout = -1;
    }
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaFuncGetAttributes,
                                attr, entry);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr,
            "\tret=%d  attr = {%lu,%lu,%lu,%d,%d,%d,%d,%d,%d,%d}, entry = %p\n",
            rce, attr->sharedSizeBytes, attr->constSizeBytes,
            attr->localSizeBytes, attr->maxThreadsPerBlock, attr->numRegs,
            attr->ptxVersion, attr->binaryVersion, attr->cacheModeCA,
            attr->maxDynamicSharedSizeBytes, attr->preferredShmemCarveout,
            entry);
    #endif

    LEAVE(cudart_lib_entry, cudaFuncGetAttributes);
    return rce;
}

int _device = 0; //FIXME

cudaError_t cudaGetDevice(int *device)
{
    ENTER(cudart_lib_entry, cudaGetDevice);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    //*device = 0; // only one supported per node for now
    *device = _device;
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaGetDevice, device);
#endif //USE_MocCUDA

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  device = %d\n", rce, *device);
    #endif

    LEAVE(cudart_lib_entry, cudaGetDevice);
    return rce;
}

cudaError_t cudaGetDeviceCount(int *count)
{
    ENTER(cudart_lib_entry, cudaGetDeviceCount);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    //*count = 1; // only one supported per node for now
    if (!_hwloc_topo_inited_) {
        _hwloc_topo_inited_ = (0 == hwloc_topology_init(&hwloc_topo));
	assert(_hwloc_topo_inited_);
	hwloc_topology_ignore_type(hwloc_topo, HWLOC_OBJ_CACHE);
	hwloc_topology_ignore_type(hwloc_topo, HWLOC_OBJ_CORE);
	hwloc_topology_ignore_type(hwloc_topo, HWLOC_OBJ_GROUP);
	hwloc_topology_ignore_type(hwloc_topo, HWLOC_OBJ_MISC);
	hwloc_topology_ignore_type(hwloc_topo, HWLOC_OBJ_BRIDGE);
	hwloc_topology_ignore_type(hwloc_topo, HWLOC_OBJ_PCI_DEVICE);
	hwloc_topology_ignore_type(hwloc_topo, HWLOC_OBJ_OS_DEVICE);
	hwloc_topology_ignore_type(hwloc_topo, HWLOC_OBJ_TYPE_MAX);
        hwloc_topology_load(hwloc_topo);
    }
    int depth = hwloc_get_type_depth(hwloc_topo, HWLOC_OBJ_PACKAGE);
    if (HWLOC_TYPE_DEPTH_UNKNOWN == depth)
        *count = 1;
    else
        *count = hwloc_get_nbobjs_by_depth(hwloc_topo, depth);
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaGetDeviceCount,
                                count);
#endif //USE_MocCUDA

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  count = %d\n", rce, *count);
    #endif

    LEAVE(cudart_lib_entry, cudaGetDeviceCount);
    return rce;
}

cudaError_t cudaGetDeviceProperties(struct cudaDeviceProp *prop, int device)
{
    ENTER(cudart_lib_entry, cudaGetDeviceProperties);

    cudaError_t rce = cudaSuccess;
    FILE *file = NULL;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    //assert(0 == device);
    file = fopen(PATHTOMocDeviceProp "/cudaDeviceProp.b", "rb");
    if (file != NULL) {
        fread((void*)prop, 712 /*sizeof(struct cudaDeviceProp)*/, 1, file);
        fclose(file);
    } else {
        fprintf(stderr, "\n\n\nERR: cannot find the device property file "
                PATHTOMocDeviceProp"/cudaDeviceProp.b ; please provide!\n\n");
        return cudaErrorNoDevice;
    }
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaGetDeviceProperties,
                                prop, device);
    file = fopen(PATHTOMocDeviceProp "/cudaDeviceProp.b", "wb");
    if (file != NULL) {
        fwrite((void*)prop, 712 /*sizeof(struct cudaDeviceProp)*/, 1, file);
        fclose(file);
    }
#endif //USE_MocCUDA

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  device = %d\n", rce, device);
    #endif

    LEAVE(cudart_lib_entry, cudaGetDeviceProperties);
    return rce;
}

cudaError_t cudaGetLastError(void)
{
    ENTER(cudart_lib_entry, cudaGetLastError);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    // nothing to do here
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaGetLastError);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d\n", rce);
    #endif

    LEAVE(cudart_lib_entry, cudaGetLastError);
    return rce;
}

cudaError_t cudaHostAlloc(void **pHost, size_t size, unsigned int flags)
{
    ENTER(cudart_lib_entry, cudaHostAlloc);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    assert((*pHost = (void *)malloc(size)));
    #ifdef DEBUG
    fprintf(stderr, "\tcudaHostAlloc %lu %p-%p\n",
            size, *pHost, (((char*)*pHost)+size-1));
    #endif
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaHostAlloc, pHost,
                                size, flags);
#endif //USE_MocCUDA

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr,
            "\tret=%d  pHost = %p, *pHost = %p, size = %lu, flags = %u\n",
            rce, pHost, *pHost, size, flags);
    #endif

    LEAVE(cudart_lib_entry, cudaHostAlloc);
    return rce;
}

cudaError_t cudaHostGetDevicePointer(void **pDevice, void *pHost,
                                     unsigned int flags)
{
    ENTER(cudart_lib_entry, cudaHostGetDevicePointer);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    assert(0 == flags);
    *pDevice = pHost;
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaHostGetDevicePointer,
                                pDevice, pHost, flags);
#endif //USE_MocCUDA

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr,
            "\tret=%d  pDevice = %p, *pDevice = %p, pHost = %p, flags = %u\n",
            rce, pDevice, *pDevice, pHost, flags);
    #endif

    LEAVE(cudart_lib_entry, cudaHostGetDevicePointer);
    return rce;
}

cudaError_t cudaLaunchKernel(const void *func, dim3 gridDim, dim3 blockDim,
                             void **args, size_t sharedMem, cudaStream_t stream)
{
    ENTER(cudart_lib_entry, cudaLaunchKernel);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    show_backtrace();
    assert(0); // should never get here
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaLaunchKernel, func,
                                gridDim, blockDim, args, sharedMem, stream);
#endif //USE_MocCUDA

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  func = %p ('%s'), gridDim = [%u,%u,%u], "
            "blockDim = [%u,%u,%u], args = %p, sharedMem = %lu, stream = %p\n",
            rce, func, backtrace_symbols(func, 1)[0],
            gridDim.x, gridDim.y, gridDim.z, blockDim.x, blockDim.y, blockDim.z,
            args, sharedMem, (void*)stream);
    if (!func_is_known(func)) {
        fprintf(stderr, "\t\tbacktrace:\n");
        show_backtrace();
    }
    #endif

    LEAVE(cudart_lib_entry, cudaLaunchKernel);
    return rce;
}

cudaError_t cudaMalloc(void **devPtr, size_t size)
{
    ENTER(cudart_lib_entry, cudaMalloc);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    assert((*devPtr = (void *)malloc(size)));
    #ifdef DEBUG
    fprintf(stderr, "\tcudaMalloc %lu %p-%p\n",
            size, *devPtr, (((char*)*devPtr)+size-1));
    #endif
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaMalloc, devPtr, size);
#endif //USE_MocCUDA

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  devPtr = %p, *devPtr = %p, size = %lu\n",
            rce, devPtr, *devPtr, size);
    #endif

    LEAVE(cudart_lib_entry, cudaMalloc);
    return rce;
}

cudaError_t cudaMemcpy(void *dst, const void *src, size_t count,
                       enum cudaMemcpyKind kind)
{
    ENTER(cudart_lib_entry, cudaMemcpy);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    #if defined USE_GCD && USE_GCD >= 1
    // cudaMemcpy is a synchronous function similar to cudaFree
    fake_cuda_synchronize_all();
    #endif
    para_memcpy(dst, src, count);
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaMemcpy, dst, src,
                                count, kind);
#endif //USE_MocCUDA

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  dst = %p, src = %p, count = %lu, kind = %d\n",
            rce, dst, src, count, kind);
    #endif

    LEAVE(cudart_lib_entry, cudaMemcpy);
    return rce;
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    void *dst;
    void *src;
    size_t count;
} cudaMemcpyAsync_async_t;

__attribute__((used,noinline))
static void cudaMemcpyAsync_async_fn(void *ctx)
{
    ENTER(cudart_lib_entry, cudaMemcpyAsync_async_fn);

    cudaMemcpyAsync_async_t c = *((cudaMemcpyAsync_async_t *)ctx);
    para_memcpy(c.dst, c.src, c.count);
    free(ctx);

    LEAVE(cudart_lib_entry, cudaMemcpyAsync_async_fn);
}
#endif //USE_GCD

cudaError_t cudaMemcpyAsync(void *dst, const void *src, size_t count,
                            enum cudaMemcpyKind kind,
                            cudaStream_t stream __dv(0))
{
    ENTER(cudart_lib_entry, cudaMemcpyAsync);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    #if defined USE_GCD && USE_GCD >= 1
    cudaMemcpyAsync_async_t *ctx;
    assert((ctx = (cudaMemcpyAsync_async_t *)malloc(sizeof(*ctx))));
    *ctx = (cudaMemcpyAsync_async_t){.dst = dst,
                                     .src = (void*)src,
                                     .count = count};
    fake_cuda_dispatch(ctx, cudaMemcpyAsync_async_fn, stream);
    #else
    para_memcpy(dst, src, count);
    #endif
#else //FAkE
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaMemcpyAsync, dst, src,
                                count, kind, stream);
#endif

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  dst = %p, src = %p, count = %lu, kind = %d, "
            "stream = %p\n", rce, dst, src, count, kind, (void*)stream);
    #endif

    LEAVE(cudart_lib_entry, cudaMemcpyAsync);
    return rce;
}

cudaError_t cudaMemGetInfo(size_t *free, size_t *total)
{
    ENTER(cudart_lib_entry, cudaMemGetInfo);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    int nG = 1; /* number of (fake) GPUs per node */
    (void)cudaGetDeviceCount(&nG);
    *total = (size_t)(sysconf(_SC_PAGE_SIZE) * sysconf(_SC_PHYS_PAGES)) / nG;
    *free = (size_t)(sysconf(_SC_PAGE_SIZE) * sysconf(_SC_AVPHYS_PAGES)) / nG;
    // FIXME: maybe do proper calculation later, per hwloc numa/package,
    //        but for now it will do the trick
#else //FAkE
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaMemGetInfo, free,
                                total);
#endif

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d  free = %lu GiB, total = %lu GiB\n",
            rce, *free/(1L << 30), *total/(1L << 30));
    #endif

    LEAVE(cudart_lib_entry, cudaMemGetInfo);
    return rce;
}

#if defined USE_GCD && USE_GCD >= 1
typedef struct {
    void *devPtr;
    int value;
    size_t count;
} cudaMemsetAsync_async_t;

__attribute__((used,noinline))
static void cudaMemsetAsync_async_fn(void *ctx)
{
    ENTER(cudart_lib_entry, cudaMemsetAsync_async_fn);

    cudaMemsetAsync_async_t c = *((cudaMemsetAsync_async_t *)ctx);
    para_memset(c.devPtr, c.value, c.count);
    free(ctx);

    LEAVE(cudart_lib_entry, cudaMemsetAsync_async_fn);
}
#endif //USE_GCD

cudaError_t cudaMemsetAsync(void *devPtr, int value, size_t count,
                            cudaStream_t stream __dv(0))
{
    ENTER(cudart_lib_entry, cudaMemsetAsync);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    #if defined USE_GCD && USE_GCD >= 1
    cudaMemsetAsync_async_t *ctx;
    assert((ctx = (cudaMemsetAsync_async_t *)malloc(sizeof(*ctx))));
    *ctx = (cudaMemsetAsync_async_t){.devPtr = devPtr,
                                     .value = value,
                                     .count = count};
    fake_cuda_dispatch(ctx, cudaMemsetAsync_async_fn, stream);
    #else
    para_memset(devPtr, value, count);
    #endif
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaMemsetAsync, devPtr,
                                value, count, stream);
#endif

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr,
            "\tret=%d  devPtr = %p, value = %d, count = %lu, stream = %p\n",
            rce, devPtr, value, count, (void*)stream);
    #endif

    LEAVE(cudart_lib_entry, cudaMemsetAsync);
    return rce;
}

cudaError_t cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags(
         int *numBlocks,
         const void *func,
         int blockSize,
         size_t dynamicSMemSize,
         unsigned int flags)
{
    ENTER(cudart_lib_entry, cudaFuncGetAttributes);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    //FIXME: 1080Ti returns (always) 2, so go with that for now
    *numBlocks = 2;
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1,
                                cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags,
                                numBlocks, func, blockSize, dynamicSMemSize,
                                flags);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr,
            "\tret=%d  numBlocks = %d, func = %p, blockSize = %d, "
            "dynamicSMemSize = %lu, flags = %u\n",
            rce, *numBlocks, func, blockSize, dynamicSMemSize, flags);
    #endif

    LEAVE(cudart_lib_entry, cudaFuncGetAttributes);
    return rce;
}

cudaError_t cudaPeekAtLastError(void)
{
    ENTER(cudart_lib_entry, cudaPeekAtLastError);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    // nothing to do here
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaPeekAtLastError);
#endif //USE_MocCUDA

    #ifdef DEBUG
    fprintf(stderr, "\tret=%d\n", rce);
    #endif

    LEAVE(cudart_lib_entry, cudaPeekAtLastError);
    return rce;
}

cudaError_t cudaSetDevice(int device)
{
    ENTER(cudart_lib_entry, cudaSetDevice);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    //fprintf(stderr, "cudaSetDevice %d\n",device);fflush(stderr);
    //assert(0 == device);
    _device = device;
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaSetDevice, device);
#endif

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  device = %d\n", rce, device);
    #endif

    LEAVE(cudart_lib_entry, cudaSetDevice);
    return rce;
}

cudaError_t cudaStreamCreateWithFlags(cudaStream_t *pStream, unsigned int flags)
{
    ENTER(cudart_lib_entry, cudaStreamCreateWithFlags);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    assert((*pStream = (struct CUstream_st *)malloc(sizeof(**pStream))));
    (*pStream)->flags = flags;
    (*pStream)->priority = 0;
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaStreamCreateWithFlags,
                                pStream, flags);
#endif //USE_MocCUDA

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  pStream = %p, flags = %u\n",
            rce, (void*)(*pStream), flags);
    #endif

    LEAVE(cudart_lib_entry, cudaStreamCreateWithFlags);
    return rce;
}

cudaError_t cudaStreamCreateWithPriority(cudaStream_t *pStream,
                                         unsigned int flags, int priority)
{
    ENTER(cudart_lib_entry, cudaStreamCreateWithPriority);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    assert((*pStream = (struct CUstream_st *)malloc(sizeof(**pStream))));
    (*pStream)->flags = flags;
    (*pStream)->priority = priority;
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1,
                                cudaStreamCreateWithPriority,
                                pStream, flags, priority);
#endif //USE_MocCUDA

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  pStream = %p, flags = %u, priority = %d\n",
            rce, (void*)(*pStream), flags, priority);
    #endif

    LEAVE(cudart_lib_entry, cudaStreamCreateWithPriority);
    return rce;
}

cudaError_t cudaStreamDestroy(cudaStream_t stream)
{
    ENTER(cudart_lib_entry, cudaStreamDestroy);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    #if defined USE_GCD && USE_GCD >= 1
    fake_cuda_synchronize(stream);
    //TODO: clean up the stuff in async.c if this fn is really needed
    #endif
    if (stream)
        free(stream);
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaStreamDestroy,
                                stream);
#endif //USE_MocCUDA

    #ifdef DEBUG
    // assert(cudaSuccess == rce);  <- not working with pip's torch
    fprintf(stderr, "\tret=%d  stream = %p\n", rce, (void*)stream);
    #endif

    LEAVE(cudart_lib_entry, cudaStreamDestroy);
    return rce;
}

cudaError_t cudaStreamSynchronize(cudaStream_t stream)
{
    ENTER(cudart_lib_entry, cudaStreamSynchronize);

    cudaError_t rce = cudaSuccess;
#if defined USE_MocCUDA && USE_MocCUDA >= 1
    // default Stream (aka Stream '0')
    //   if (NULL == (void*)stream) {} else {}
    #if defined USE_GCD && USE_GCD >= 1
    fake_cuda_synchronize(stream);
    #endif
#else //USE_MocCUDA
    rce = REAL_CUDA_RT_API_CALL(cudart_lib_entry, v1, cudaStreamSynchronize,
                                stream);
#endif //USE_MocCUDA

    #ifdef DEBUG
    assert(cudaSuccess == rce);
    fprintf(stderr, "\tret=%d  stream = %p\n", rce, (void*)stream);
    #endif

    LEAVE(cudart_lib_entry, cudaStreamSynchronize);
    return rce;
}
